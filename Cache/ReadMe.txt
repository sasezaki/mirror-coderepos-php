PHPでキャッシュを扱うクラスです。

PearのCacheはCache_LiteとCacheでインタフェースが違うので
相互運用性が低い。キャッシュの保存先を変えるとコードの
修正箇所が多い。

なので、キャッシュの保存先を簡単に変えられるように、イン
タフェースを統一してみようかと。ついでに、キャッシュ保存
先データベースを分散出来たり、ミラーリングしたり出来
るようにしてある。


キャッシュの複数分散は、サムネイルAPIで使ってます。


キャッシュをいろいろなものに保存する実験とか
そういうのをやってます

Cache_Liteを改良してつくってたら、Zend_Cacheに同じようなものがあったのでorz


//そもそもキャッシュをなんに使うか。
//MVCの三層構造+RESTfulにすると４層でわかりにくい
RESTfulなURLを構造として持っておき
URLに紐付くデータをキャッシュとして保存しておく
{cache_key =>URL, cache_data=> object}
のを作り、ユーザーはobjectへHTTPメソッド操作をするようにする。
そういうイメージで２層構造＋バックエンドの３層構造をイメージ。


Wikiなどを見ている限り、MVC三層構造は不便だと感じる。
RESTfulでURLがデータIDを示す構造が便利だと感じている。

キャッシュのキーをURNにすることで、このような構造を簡単に作れるはず。

