<?sphp

//Examples of  Cache_Hander Class
//2008/02/04 12:45　更新
//written by hatena_id:takuya_1st


//
//一番シンプルな使い方
//
// very simple Usage
// sample 0
require_once "Cache/Utils.php";
$data = "<html>sample</html>";
cache_put_contents( "test_1",  $data );//$_SERVER["tmp"]にキャッシュする
$data = cache_get_contents("test_1");//キャッシュしたデータを読み出す

//
//キャッシュのデータをオブジェクト指向っぽく使う
//sample 1.A
//
require_once "Cache/Handler.php";
require_once "Cache/Handler/Resourece/File.php";
$handler = new Cache_Handler();
$handler->setResource( new Cache_Handler_Resource_File("./tmp") );//キャッシュの保存先を設定
$item = $handler->CacheItem("test_1");//キャッシュのIDに紐付くCache_Item を生成
if( $item->isExists() ){
	$data = $item->get();
}else{
	$data = "<html>sample</html>";
	$item->put( $data );
}
echo $data;
//
//Cache_Item オブジェクトを使わず同じことをする
//今後APIを検討してPEAR_Cache/Cache_Liteに近づけてみたい
//sample 1.B
//
require_once "Cache/Handler.php";
require_once "Cache/Handler/Resourece/File.php";
$handler = new Cache_Handler();
$handler->setResource( new Cache_Handler_Resource_File("./tmp") );//キャッシュの保存先を設定
$id = "test_1";
if( $item->isExists($id) ){//キャッシュが存在したら
	$data = $handler->getCache($id);//キャッシュを取得
}else{
	$data = "<html>sample</html>";
	$handler->setCache($id, $data);//存在しない場合は新規作成
}
echo $data;
///

//sample 2.A
//キャッシュに保存する際に圧縮など処理をする場合
require_once "Cache/Handler.php";
require_once "Cache/Handler/Resourece/File.php";
$handler = new Cache_Handler();
$handler->setResource( new Cache_Handler_Resource_File("./tmp") );//キャッシュの保存先を設定
$handler->addPlugin( new Cache_Handler_PlugIn_Gzip () );//キャッシュをGzip圧縮して保存する。
if( $item->isExists() ){
	$data = $item->get();
}else{
	$data = "<html>sample</html>";
	$item->put( $data );//Gzip圧縮で保存される
}
echo $data;

//sample 2.B
//キャッシュにオブジェクトを保存する。同時にに圧縮など処理をする場合
//
$handler = new Cache_Handler();
$handler->setResource( new Cache_Handler_Resource_File("./tmp") );//キャッシュの保存先を設定
$handler->addPlugin( new Cache_Handler_PlugIn_Serializer() );//ObjectをString化する
$handler->addPlugin( new Cache_Handler_PlugIn_Gzip () );//StringをGzip圧縮して保存する。
if( $item->isExists() ){
	$data = $item->get();//Gzipが解凍されObjectに戻される
}else{
	$data = new Sample_Class();
	$item->put( $data );//クラスの内容がString化されGzip圧縮で保存される
}
echo $data;

//
//sample 3.A
//キャッシュを分散して保存する
//
$handler = new Cache_Handler();
$res = new Cache_Handler_Resource_Multiple();//キャッシュの保存先を分割する
$res->addResource( new Cache_Handler_Resource_PDO(array("dsn"=>$dsn[0]))  );
$res->addResource( new Cache_Handler_Resource_PDO(array("dsn"=>$dsn[1]))  );
$res->addResource( new Cache_Handler_Resource_PDO(array("dsn"=>$dsn[3]))  );
$handler->setResource( $res );//キャッシュの保存先を３つのPDOに分散する
if( $item->isExists() ){
	$data = $item->get();
}else{
	$data = new Sample_Class();
	$item->put( $data );
}
echo $data;
//
//sample 3.A
//キャッシュを分散して保存しつつ、保存先別で重み付けを行う
//
$handler = new Cache_Handler();
$res = new Cache_Handler_Resource_RoundRobin ();//キャッシュの保存先を分割する
$res->addResource( new Cache_Handler_Resource_File("./tmp") , 1 );
$res->addResource( new Cache_Handler_Resource_PDO(array("dsn"=>$dsn[0])) , 2 );
$res->addResource( new Cache_Handler_Resource_PDO(array("dsn"=>$dsn[1])) , 3 );
$handler->setResource( $res );//キャッシュの保存先を優先度に従って分散する
if( $item->isExists() ){
	$data = $item->get();
}else{
	$data = new Sample_Class();
	$item->put( $data );
}
echo $data;

//sample 4.A
//メソッドの実行結果をきゃっしゅしておく
//file_get_contentsの結果をキャッシュする

$cached = new Cache_Handler_CachedMethod();//デフォルトは$_SERVER["tmp"]に保存
//$cached->setHandler( $handler );#キャッシュの保存先を指定することもできる
$ret[] = $cached->file_get_contents("http://www.yahoo.co.jp");//file_get_contentsの結果をキャッシュに保存
$ret[] = $cached->file_get_contents("http://www.yahoo.co.jp");//２回目以降はキャッシュが取り出される
$ret[0] == $ret[1] ; # true;


//
//
//
////Very Simple Usage:
////set resource 
//Cache_Handler::setResource( new Cache_Handler_Resource_File(array("path"=>"./tmp")) );
////object
//$handler = new Ester_Cache_Handler( $CacheID, $group );
//
////read cache
//if( $handler->isCached() ){	
//	return $handler->getCache();
//}
////caching data
//if($cache_content){
//	$handler->setCache( $cache_content );
//	return $result = $handler->isCached();//success:True,failed:false
//}
////remove cached data
//if(!$groups){
//	//remove all
//	$handler->clearAll();
//	$result = true;
//}else {
//	//remove cache by group
//	foreach( $group as $name ){
//		$handler->clear( $name );
//	}
//}
//
//
////////////////
////for more usage
////////////////
////Distributing Caching data to some pdo database resource
//$res_multi = new Cache_Handler_Resource_Multiple();
//$res->addResource(new Cache_Handler_Resource_PDO( array( "dsn" => $dsn[0] ) ));
//$res->addResource(new Cache_Handler_Resource_PDO( array( "dsn" => $dsn[1] ) ));
//$res->addResource(new Cache_Handler_Resource_PDO( array( "dsn" => $dsn[2] ) ));
//
////Distributing Caching data to some MemCached
//$res_memcache_multi = new Cache_Handler_Resource_Multiple();
//$res_memcache_multi->addResource(new Cache_Handler_Resource_MemCache( array( 'host'=>'localhost', 'port'=> 11211 ) ) ));
//$res_memcache_multi->addResource(new Cache_Handler_Resource_MemCache( array( 'host'=>'example.com', 'port'=> 11211 ) ) ));
//
////Mirroring Cache 
//$res_mirror = new Cache_Handler_Resource_Mirror();
//$res_mirror->addResource( $res_pdo_multi );
//$res_mirror->addResource( $res_memcache_multi );
//$handler->setResource( $res );
//
//
/////////////////
//////use Plugin for Caching
/////////////////
////Gzip Compression  Cache data
//$plugin = new Ester_Cache_GzipPlugin();
//$handler->addPlugin( $plugin );
////Save Cache_Data by BASE64 Encoding. 
//$plugin = new Ester_Cache_Base64Plugin();
//$handler->addPlugin( $plugin );
//
