<?php
/** 
 * Cache Hanlder class.
 * PHP version 5
 * cache handler make more easy to change cache strage and grouping.
 * This Handler Class supports plugin to modify cache data,
 * such as base64, zlib, serialize.
 * By Using or Custumizing plugin,
 * All type of data can be cached ( Text, PHP Object, JPEG Image..etc )
 * @author      takuya morioka <hatena id:takuya_1st>
 * @license     PHP license
 * @package     Cache_Handler
 * @category    Cache
 */
 
//コードをわかりやすくするためのClass
//このClassの利用でコードがSimpleになる
//$item = $Handler->CacheItem($id);
//if($item->isExists()){
//	return $item->get();
//}else{
//	return $item->set($data);
//}
//Cache_HandlerのAPIを一新した
require_once 'Cache/Handler/Resource/Interface.php';
require_once 'Cache/Handler/Item.php';
class Cache_Handler{
	//ストレージから読み込んだデータを一時保存しておく
	//TODO::多用するなら配列のサイズ制限をかけるべき
	protected $temp=array();
	public function __construct(){
		$this->plugins  = array();
	}
	//よく使うインスタンス化のパターンを記憶しておく
	public static function factory($ini) {
		$obj = new Cache_Handler();
		//
		//TODO:do something;
		return $obj;
	}
	public function CacheItem($id){
		return new Cache_Handler_Item($id,$this);
	}
	public function setResource(&$res){//参照渡し
		return $this->resource = $res;
	}
	public function addPlugIn(&$plugin){//参照渡し
		return $this->setPorp("plugins", $plugin);
	}
	//__set()の代用 
	//配列のproperty に値をセットする.
	//引数が配列の場合と値の場合で処理を分ける。
	//配列の場合はpropertyと配列をマージする。
	//値の場合は、propertyに末尾に値を足す。
	public function setPorp($prop_name, &$value){//参照渡し 
		try{
			if( is_array($value) ){
				$this->$prop_name =  @array_merge($this->$prop_name, $value);
			}else{
				$this->{$prop_name}[] = $value;
			}
		}catch(Exception $e){
			return false;
		}
		return true;
	}
	//キャッシュの扱い
	public function getCache($id){
		//APIとして外に出している名称
		//内部的にはreadCacheを使っている
		return $this->readCache($id);
	}
	//todo ここは、Pear::Cacheと合わせる
	public function setCache($id,$data){
		//APIとして外に出している名称
		//内部的にはupdateCacheを使っている
		return $this->updateCache($id,$data);
	}
	public function isExpired($id){
		//Tempから読み込む
		$this->readCache($id);
		$data = $this->temp;
		//有効期限を調べる
		//TODO::有効期限切れを調べるモジュールを作る
		//http://www.gnu.org/software/tar/manual/html_node/tar_109.html
		//を利用したパッケージにする
		//$ttlは文字列で指定することが出来る
		$time_to_expired = false;
		if(intval($data["ttl"])==0 ){
			//$ttl が文字列で指定されている場合
			//for example : $ttl ="+ 7day","next Thursday" など
			$time_to_expired = strtotime( $data["ttl"], strtotime($data["modifed"]) );
		}else if( is_numeric(intval($data["ttl"])) ){
			$time_to_expired  = strtotime( $data["modifed"] )+$data["ttl"];
		}
		
		if( $time_to_expired === false ){//文字列が不正なとき
			$time_to_expired = time()-3600;//有効期限切れとする
		}
		//有効期限切れかどうかチェックする
		//期限内なら　FALSE　それ以外は　TRUE
		return time() > $time_to_expired;
	}
	public function isExists($id){
		//Tempから読み込む
		$content = $this->readCache($id);
		//有効期限内かつ、コンテンツが存在
		return $content && $this->isExpired($id);
	}
	//コードの可読性のために
	public function available( $id ){
		return $this->isExists($id);
	}
	//キャッシュのメンテナンス用
	//未実装
	//public function setLastModified($id,$date){
		//$this->readCache($id);//Tempに読み込む
		//$this->temp["modifed"] = $date;
		//$this->updateCache($id);//このメソッドを使うとキャッシュ保存、TTL保存,Modfied保存,Group保存の4処理が動くので注意
	//}
	////for maintenance
	////maintenance用なので、利用法が違うことを意識してコーディングする
	//public function setCacheLifeTime($id,$ttl){
		////$this->readCache($id);//Tempに読み込む
		////$this->temp["ttl"] = $ttl;
	//}
	//Groupの扱いはちょっと思案
	//グループ一覧とキャッシュファイルと更新日付の計三回ファイルアクセスが発生しうる
	public function setCacheGroupName($id,$name_or_names_of_group ){
		return $this->resource->addGroup( $id,$name_or_names_of_group );
	}
	public function GroupNames(){
		//キャッシュに設定しているグループ名一覧を返す
		//2008/02/01 追加API
		return $this->resource->getGroupNameList();
	}
	//キャッシュを削除する系
	public function clear($id){
		return $this->resource->remove($id);
	}
	public function clearAll(){
		return $this->resource->clean();
	}
	//protected function setCacheProp($id,$prop_name){}
	protected function updateCache($id,$data) {
		//このクラスがキャッシュしているTEMPを更新する
		//プラグインを引っかける
		$content =  $this->beforeWrite( $data );
		//Resouceへ保存
		$ret = $this->resource->save( $content, $id );
		return $ret;
	}
	//キャッシュをドライバから読み出す、Templateメソッド
	protected function readCache($id) {
		if( isset($this->temp["id"]) && $this->temp["id"] === $id ) {
			$data = $this->temp;
		}else {
			$data["id"]		 = $id;
			$data["content"] = $this->resource->get( $id );
			$data["ttl"]	 = $this->resource->getTimeToLive($id);
			$data["modifed"] = $this->resource->getLastModified($id);
			$data["content"] = $this->afterRead( $data["content"] );
			//このクラスのtemporaryへ
			$this->temp=$data;
		}
		return $data["content"];
	}
	//最終更新時を返す
	public function CacheModifiedSince($id){
		$this->readCache($id);
		return $this->temp["modifed"];
	}
    /** 
    * execute plugins
    * @access protected
	*/
    protected function beforeWrite( $data )
    {
        if( $data == null ){
            return;
        }
        foreach( $this->plugins as $modifer ){
            $data = $modifer->beforeWrite( $data );
        }
        return $data;
    }
    /** 
     * execute plugins
     * @access protected
     */
    protected function afterRead( $data )
    {
        if( $data == null ){ 
            return;
        }
        foreach( array_reverse( $this->plugins ) as $modifer ){
            $data = $modifer->afterRead( $data );
        }
        return $data;
    }
}

////TEST
//if (  __FILE__ == $_SERVER["PHP_SELF"] ){

//require_once "Handler.php";
//require_once "Handler/Resource/File.php";
//require_once "Handler/PlugIn/Gzip.php";
//require_once "Handler/PlugIn/Base64.php";

////キャッシュの設定
//$handler= new Cache_Handler;
//$handler->addResource( new Cache_Handler_Resource_File(array("path"=>"./tmp")) );
//$handler->addPlugin( new Cache_Handler_PlugIn_Gzip () );//Gzip Compression  Cache data
//$handler->addPlugin( new Cache_Handler_PlugIn_Base64() );//Save Cache_Data by BASE64 Encoding. 


//$cache_items = Cache_Handler::factory();
////このような記述が出来ると嬉しいよね
//if($cache->isExists($id)){
////cache_is_Expiredでキャッシュの存在時点とライフサイクルをチェックする。かつキャッシュを読み込んでおく
	//return $cahce->getCache($id);
//}else{
	////do something 
	//$data;
	//$cache->setCache($id,$data);
//}
////これでもまだ、至る箇所にIF文がふえて勿体ない

//$data = $cache_method->call_func("file_get_contents");


//}


////キャッシュの設定は一意だと仮定する？
//class Cache_Handler_ {
   // 
    ///** 
    //* Cache resource 
    //* @access public
    //* @type   Cache_Handler_Resource_Interface
    //*/
    //public static $resource;
    ///** 
    //* Cached data filtering plugins 
    //* @access public
    //* @type   Cache_Handler_Resource_Interface
    //*/
    //public static $plugin;
    ///** 
    //* constructor 
    //* @access public 
    //* @param array $ini array( "property name" => "value " );
    //* @return void
    //*/
    //public function __construct($id, $group)
    //{
        //$this->resource =& self::$resource;
        //$this->setCacheId( $id );
        //$this->setCacheGroupName( $group );
        //$this->plugin =& self::$plugin;
    //}
    //public function CacheItem($id, $group){
    //	
    //}
    ///** 
    //* set Cache resource to this cache handler as static
    //* @access public 
    //* @type   Cache_Handler_Resource_Interface
    //* @return void
    //*/
    //public static function setResource( Cache_Handler_Resource_Interface $res )
    //{
        //self::$resource = $res;
    //}
    ///** 
    //* get Cache resource to this cache handler as static
    //* @access public 
    //* @return   Cache_Handler_Resource_Interface
    //*/
    //public static function getResource()
    //{
        //return self::$resource;
    //}
    ///** 
    //* Clear Cache group from this resource 
    //* @access public 
    //* @param  String group name 
    //* @return boolean  true (succeeded) / false (faild)
    //*/
    //public static function clearCacheGroup( $name )
    //{
        //return self::$resource->clean( $name );
    //}
    ///** 
    //* clear all cache from this resource
    //* @return boolean  true (succeeded) / false (faild)
    //*/
    //public static function clearAll()
    //{
        //self::$resource->clean();
    //}
    ///** 
    //* Add plugin to this class
    //* plugin is executed by added order
    //* @access public 
    //*/
    //public static function addPlugin( Cache_Handler_PlugIn_Interface $plugin )
    //{
        //self::$plugin[] =$plugin;
    //}
    ///** 
    //* clear all registed plugin.
    //* @access public 
    //*/
    //public static function clearPlugin()
    //{
        //unset( self::$plugin );
    //}
    ///** 
    //* Unregist plugin
    //* @access public 
    //* @param int $index  order of plugin
    //*/
    //public static function delPlugin( $index )
    //{
        //unset( self::$plugin[$index] );
    //}
    ///** 
    //* Retrieve registered plugin 
    //* @access public 
    //* @param int $index  order of plugin
    //* @return Cache_Handler_PlugIn_Interface plugin
    //*/
    //public function & getPlugin($index)
    //{
        //return $this->plugin[$index];
    //}
    ///** 
    //* execute plugin
    //* @access protected
    //*/
    //protected function beforeWrite( $data )
    //{
        //if( $data == null ){
            //return;
        //}
        //foreach( $this->plugin as $modifer ){
            //$data = $modifer->beforeWrite( $data );
        //}
        //return $data;
    //}
    ///** 
    //* execute plugin
    //* @access protected
    //*/
    //protected function afterRead( $data )
    //{
        //if( $data == null ){ 
            //return;
        //}
        //foreach( array_reverse( $this->plugin ) as $modifer ){
            //$data = $modifer->afterRead( $data );
        //}
        //return $data;
    //}
   // 
    ///**
    //* clear cache of this cache_id
    //* @access public 
    //* @param void
    //* @return void
    //*/
    //public function clear()
    //{
        //$this->resource->remove( $this->cache_id );
    //}
    ///** 
    //* check is this cache_id cached in resource.
    //* @access private 
    //* @return boolean 
    //*/
    //public function isCached( $date = null )
    //{
        //if( $this->_data == null ){
            //$this->_data = $this->getCache();
        //}
       // 
        //if( $this->_data === FALSE || $this->_data === null || $this->_data == ""){
            //return false;
        //}else{
            //return true;
        //}
    //}
    ///** 
    //* Check cache is not expired.
    //* If cache life time is expired, clear cache and return false.
    //* @access public
    //* @param String $time, cache life time
    //* @return boolean
    //*/
    //public function check()
    //{
        //if( $this->isCached( $date ) ){
            //return true;
        //}else{
            //if( $this->isExpired() ){
                //$this->clear();
            //}
            //return false;
        //}
    //}
    ///** 
    //* return true if cache is expired
    //*/
    //public function isExpired()
    //{
        //$modifed  = $this->getLastModified();
        //$lifetime = $this->getCacheLifeTime();
        //if( $lifetime == null ){//eternal cache
            //return false;
        //}else{
            //return time() >= strtotime( $modified )+$lifetime;
        //}
    //}
    ///** 
    //* return true if cache is NOT expired
    //*/
    //public function isNotExpired()
    //{
        //return !$this->isExpired();
    //}
    ///** 
    //* get cache of this cache id, and plugin applied
    //* @access public 
    //* @param void
    //* @return String cache data
    //*/
    //public function getCache()
    //{
        //$data = $this->resource->get( $this->cache_id );
        //$data = $this->afterRead( $data );
        //return $data;
    //}
    ///** 
    //* Return cached modified date of this cache id.
    //* When resource has no cache, this function returns null.
    //* @return String date 
    //*/
    //public function getLastModified()
    //{
        //if( $this->isCached() == false ){
            //return null;
        //}
        //return $this->resource->getLastModified( $this->cache_id );
    //}
    ///** 
    //* return cached life time of this cache id.
    //* when resource has no cache, this function returns null.
    //* @return int 
    //*/
    //public function getCacheLifeTime()
    //{
        //if( $this->isCached() == false ){
            //return null;
        //}
        //return $this->resource->getTimeToLive( $this->cache_id );
    //}
    ///** 
    //* set this cache id
    //* @access public 
    //* @param String $id cache id
    //* @return void
    //*/
    //public function setCacheId( $id )
    //{
        //$this->cache_id = $id;
    //}
    ///** 
    //* returns cache id
    //* @access public 
    //* @param void 
    //* @return String cache id
    //*/
    //public function getCacheId()
    //{
        //return $this->cache_id;
    //}
    ///** 
    //* add cache group name to this cache id
    //* @access public 
    //* @param String $name
    //* @return void
    //*/
    //public function addCacheGroupName( $name )
    //{
        //if( $name != ""  ){
            ////$ret = $this->resource->addIntoGroup( $this->cache_id, $name );
            //$this->group[] = $name;
            //return true;
        //}
        //return false;
    //}
   // 
    ///** 
    //* resets cache group name already set, and set cache group name.
    //* @access
    //* @param
    //* @return
    //*/
    //public function setCacheGroupName( $names )
    //{
        //if( is_array( $names ) ){
            //$this->group = $names;
        //}else{
            //$this->addCacheGroupName( $names );
        //}
        //return true;
    //}
    ///** 
    //* store cache data to this resource
    //* @access public 
    //* @param String content wanted to be cached
    //* @return void
    //*/
    //public function setCache( $data )
    //{
        //$data = $this->beforeWrite( $data );
        //return $this->resource->save( $data, $this->cache_id, $this->group );
    //}
    ///** 
    //* set or modify cache last modified date
    //* @access public 
    //* @param  String $date , if this param is skipped date("r") will be set to 
    //* @return boolean
    //*/
    //public function setLastModified( $date = null )
    //{
        //if( $date == null ){
            //$date = date("r");
        //}
        //return $this->resource->setLastModified( $this->cache_id, $date );
    //}
    ///** 
    //* set or modify cache last modified date
    //* @access public 
    //* @param  String $date , default 3600, 
    //* 'setCacheLifeTime( NULL )'  or  'setCacheLifeTime("")' is for eternal cache.
    //* @return boolean
    //*/
    //public function setCacheLifeTime( $time = 3600 )
    //{
        //if( $time == null || $time == "" ){
            //$time == "";
        //}
        //return $this->resource->setTimeToLive( $this->cache_id, $time );
    //}
//}


