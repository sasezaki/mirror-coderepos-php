<?php
//キャッシュのIDをどのように作るか
//キャッシュIDの衝突を避ける為。
//キャッシュIDの銘々規則をクラス化することで衝突を防ぐ

interface IDGenerator{
	public function generate($obj);
}
class GenIDByFunc implements IDGenerator{
	public function __construct($func_name){
		$this->func_name = $func_name;
	}
	public function generate($obj){
		$func = $this->func_name;
		return $func($obj);
	}
	public static function factory($func_name){
		return new GenIDByFunc($func_name);
	}
}
//TEST
//if (  __FILE__ == $_SERVER["PHP_SELF"] ){
//	$gen = new GenIDByFunc("md5");
//	if( "098f6bcd4621d373cade4e832627b4f6" == $gen->generate("test")){
//		echo "GenIDByFunc::generate()  is ok.\n";
//	}
//	$gen = GenIDByFunc::factory("md5");
//	if( "098f6bcd4621d373cade4e832627b4f6" == $gen->generate("test")){
//		echo "GenIDByFunc::generate()  is ok.\n";
//	}
//
//}
?>