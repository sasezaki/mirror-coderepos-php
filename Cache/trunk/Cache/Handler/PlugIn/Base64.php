<?php
/**
 * Plugin for Cache_Handler 
 * this would be useful for binary and multi byte string data,
 * in case that raw data caused sql error.
 * @package     Cache_Handler
 * @category    Cache
 */

require_once 'Cache/Handler/PlugIn/Interface.php';

class Cache_Handler_PlugIn_Base64 implements Cache_Handler_PlugIn_Interface {

    
    /** 
    * @access public
    */
    public function __construct()
    {
        //do nothing
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  Stirng cache data
    * @return Stirng Base64 encoded cache_data
    */
    public function beforeWrite( $data )
    {
        return base64_encode( $data );
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  String Base64 encoded cache data
    * @return Stirng cache_data
    */
    public function afterRead( $data )
    {
        return base64_decode( $data );
    }

}