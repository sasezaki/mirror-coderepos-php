<?php
/**
 * Plugin for Cache_Handler 
 * this would be useful for object to cache
 * @package     Cache_Handler
 * @category    Cache
 * @description crypt cache data to make difficult for reading data
 * @see http://labs.unoh.net/2007/11/php_de_crypt.html 暗号化はこれを参考
 */

//require_once 'Cache/Handler/PlugIn/Interface.php';

class Cache_Handler_PlugIn_Crypt/* implements Cache_Handler_PlugIn_Interface */{
    /** 
    * @access public
    */
    public function __construct($key="i hate buzz words")
    {
        $this->crypt_keyword = $key;
        $this->initMcrypt( 
        	(strpos( strtoupper( $_SERVER["OS"] ), "WIN" )===false) ? 
        		MCRYPT_DEV_URANDOM : MCRYPT_RAND );
    }
    private function initMcrypt($source){
		/**
		 * 初期化ベクトルを用意する
		 * Windowsの場合、MCRYPT_DEV_URANDOMの代わりにMCRYPT_RANDを使用する
		 */
		$this->iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC), $source);
		// 事前処理
		$resource = mcrypt_module_open(MCRYPT_BLOWFISH, '',  MCRYPT_MODE_CBC, '');;
    }
    private function encode($data){
    	return $encrypted_data = mcrypt_cbc(MCRYPT_BLOWFISH, $this->crypt_keyword, $data, MCRYPT_ENCRYPT, $this->iv);
    }
    private function decode($data){
    	return $encrypted_data = mcrypt_cbc(MCRYPT_BLOWFISH, $this->crypt_keyword, $data, MCRYPT_DECRYPT, $this->iv);
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  Object cache data object to store
    * @return Stirng serialized  cache data
    */
    public function beforeWrite( $data )
    {
        return $this->encode( $data );
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  String Stirng serialized  cache data
    * @return Object deserialized cache data object
    */
    public function afterRead( $data )
    {
        return $this->decode( $data );
    }

}

/////////test
//$crypter = new Cache_Handler_PlugIn_Crypt("test");
//$data = "Googleのエンジニアってすごいよね";
//echo "------------";
//echo "\n";
//echo $data = $crypter->beforeWrite($data);
//echo "\n";
//echo "------------";
//echo "\n";
//echo $data = $crypter->afterRead($data);
//echo "\n";
