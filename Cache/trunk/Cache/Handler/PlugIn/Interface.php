<?php
/**
 * Plugin Interface definition for Cache_Handler
 * @package     Cache_Handler
 * @category    Cache
 */
interface Cache_Handler_PlugIn_Interface {


    /** 
    * processiong data before writing cache
    * @access public 
    * @param  Object this is to be saved.
    * @return Object after plugin
    */
    public function beforeWrite($data);
    /** 
    * processiong data ater reading cache
    * @access public 
    * @param  Object cached data
    * @return Object after plugin
    */
    public function afterRead($data);
}