<?php
/**
 * Plugin for Cache_Handler 
 * this would be useful for object to cache
 * @package     Cache_Handler
 * @category    Cache
 */

require_once 'Cache/Handler/PlugIn/Interface.php';

//Cache_Handler_PlugIn_SerializerでSerializeする代わりにJSONで保存する
class Cache_Handler_PlugIn_JSON implements Cache_Handler_PlugIn_Interface {
    /** 
    * @access public
    */
    public function __construct($assoc=false)
    {
        $this->assoc = $assoc;
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  Object cache data object to store
    * @return Stirng serialized  cache data
    */
    public function beforeWrite( $data )
    {
		return json_encode($data);
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  String Stirng serialized  cache data
    * @return Object deserialized cache data object
    */
    public function afterRead( $data )
    {
		return json_decode($data);
    }

}