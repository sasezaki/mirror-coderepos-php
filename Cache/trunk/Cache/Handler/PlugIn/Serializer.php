<?php
/**
 * Plugin for Cache_Handler 
 * this would be useful for object to cache
 * @package     Cache_Handler
 * @category    Cache
 */

require_once 'Cache/Handler/PlugIn/Interface.php';

class Cache_Handler_PlugIn_Serializer implements Cache_Handler_PlugIn_Interface {
    /** 
    * @access public
    */
    public function __construct()
    {
        //do nothing
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  Object cache data object to store
    * @return Stirng serialized  cache data
    */
    public function beforeWrite( $data )
    {
        return serialize( $data );
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  String Stirng serialized  cache data
    * @return Object deserialized cache data object
    */
    public function afterRead( $data )
    {
        return unserialize( $data );
    }

}