<?php

/** 
 * Save cache data to Files
 * @package     Cache_Handler
 * @category    Cache
 */

require_once 'Cache/Handler/Resource/Interface.php';
//CSVテスト中
//require_once 'Cache/Handler/Resource/File_csv_utils.php';
//CSVを簡単に扱う
//TODO:Groupファイルなど共有ファイルはロックが必要
//     flock 関数の記述を参考にすると
//     [いくつかのオーペレーティングシステムでflock() はプロセスレベルで実装されています。]
//     とのこと。
//     http://jp.php.net/manual/ja/function.flock.php
//

///////////////////////////////////////////////////////////////////////////
//////////CSVを読み書きする関数
///////////////////////////////////////////////////////////////////////////
function file_put_csv( $filename,$lines, $file_mode="w", $delim=",",  $enclosure="\"" )
{
	$fp = @fopen( $filename, $file_mode );
	$state =null;
	foreach( $lines as $idx=>$line){
		$state = fputcsv( $fp, $line, $delim, $enclosure );
	}
	return $state !== false ;
}
//CSVを２次元配列に一括変換する関数
function file_get_csv($filename, $length=100,$delim=",",$enclosure="\"")
{
	$data = array();
	$fp = @fopen($filename,"r") /*or die("キャッシュファイルがない")*/;
	@flock( $fp, LOCK_EX );//ロック
	while( $fp && ($buff=fgetcsv($fp,$length,$delim,$enclosure)) !==FALSE ){
		$data[]=$buff;
	}
	@fclose($fp);//ロック解放
	return $data;
}
///////////////////////////////////////////////////////////////////////////
//キャッシュを扱うBackEndクラス。Fileシステムにキャッシュを格納する
//キャッシュは１エントリ＝１ファイルが作成される
//グループ管理のため　グループ一覧ファイルが１ファイル作成される
//更新時刻管理のため　最終更新時刻ファイルが１ファイル作成される
//SQLiteのように、いったんJournalへ更新情報を書き出して
//一意性を保証するように実行制御したいけれど、
//そこまでやるならSQLite使えばいいし。。。。
///////////////////////////////////////////////////////////////////////////
class Cache_Handler_Resource_File implements Cache_Handler_Resource_Interface {
    /** 
    * file extension for managment
    * @access public
    */
    public $ext = ".cache";//file extenstion
    /** 
    * file extension for managment
    * @access public
    */
    public $_ext = ".handler.system";
    
    /** 
    * time to live
    * @access public
    */
    public $lifetime;
    /** 
    * constructor
    * @access public
    */
    public function __construct( $ini=array() )
    {
        foreach ( $ini as $key => $val ) {
            $this->$key = $val;
        }
        $this->setFilePath( $this->path );
    }
    
    /** 
    * return cache_dir path
    * @access protected
    * @param void
    * @return string file path 
    */
    public function getFilePath()
    {
        if ( $this->path == null ) {
            //default file path
            $this->setFilePath();
        }
        return $this->path;
    }
    /** 
    * set cache dirretory, default save path is $_SERVER["TEMP"].
    * @access protected 
    * @param stirng $path, directory to save cache into.
    * @return boolean true if directory will be written
    */
    public function setFilePath( $path = null )
    {
        if ( $path == null ) {
            $path = $_SERVER["TEMP"];
        }
        //for windows compatible
        $path = str_replace( "\\", "/", $path );
        $this->path = $path;
        return $this->_checkDirectory( $this->path );
    }
    //make directory
    private function _checkDirectory( $path )
    {
        if ( !file_exists( $path ) ) {
            mkdir( $path, 770, true );
        }
        return is_writable( $path );
    }
    /** 
    * return formatted cache finename, where cache id's stored.
    * ( just filename, not included file path )
    * @access protected 
    * @param  String $id cache id
    * @return String filename
    */
    public function getFileName( $id )
    {
        return md5( $id ).$this->ext;
    }
    
    /** 
    * returns formatted cache filename ( including path ) where contains $id' cache.
    * @access protected
    * @param  String $id cache id
    * @return String filename
    */
    public function getFullPath( $id )
    {
        $path = $this->getFilePath();
        $path = trim( $path, "/\\" );
        $path = $this->getFilePath() . "/" .  $this->getFileName( $id );
        return $path;
    }
    /** 
    * implements of abstract function
    * @param $data  object cache data
    * @param $id    stirng cache id
    * @param $group array  Array of String. group name of this cache 
    * @return boolean  true (succeeded) / false (faild)
    */
    public function save(  $data, $id, $group = array("__default") )
    {
        $ret[] = file_put_contents( $this->getFullPath( $id ), $data );
        $ret[] = $this->addGroup( $id, $group ) > 0;
        $ret[] = $this->_setModified( $id, date("r") );
        $ret[] = $this->setTimeToLive( $id, $this->lifetime ) > 0;
        return (( $ret[0] > 0 ) && $ret[1] && $ret[2] && $ret[3] );
    }
    /** 
    * implements of abstract function
    * @access public
    * @param $id    stirng cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    public function remove( $id )
    {
        $ret[] = $this->removeID($id);
        $ret[] = $this->removeFromGroup( $id );
        $ret[] = $this->removeModifiedIndex( $id );
        $ret[] = $this->removeLifeTimeIndex( $id );
        return ( $ret[0] && $ret[1] && $ret[2] && $ret[3] );
    }
    private function removeID($id){
    	return unlink( $this->getFullPath($id) );
    }
    /** 
    * implements of abstract function
    * get Cache created time from this resource.
    * @access public
    * @param $id    stirng cache id
    * @return string cache created time
    */
    public function getLastModified( $id, $format = "Y/m/d H:i:s" )
    {
        return date( $format, strtotime( $this->_getModified( $id ) ) );
    }
    /** 
    * implements of abstract function
    * Set or update  Cache created(modified) time.
    * @access public
    * @param $id    stirng cache id
    * @param $date  stirng cache created time. default value date("r")
    * @return String date of cache create or modified time
    */
    public function setLastModified( $id, $date )
    {
        return $this->_setModified( $id, date( "c", strtotime( $date ) ) );
    }
    /** 
    * implements of abstract function
    * set or update cache TTL for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @param $int   int    cache lifetime , default -1
    * @return boolean  true (succeeded) / false (faild)
    */
    public function setTimeToLive( $id, $int = -1 )
    {
        return $this->setLifeTime( $id, $int );
    }
    /** 
    * implements of abstract function
    * get cache Life time for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @return int   cache lifetime. if value does not exits , this method return -1.
    */
    public function getTimeToLive( $id )
    {
        return $this->getLifeTime( $id );
    }
    /** 
    * implements of abstract function
    * without group name, clean all chache
    * @access public
    * @param  String $group    stirng cache group name
    * @param  Array  $ids array of id removed.
                     id is cache ids.
    * @return boolean  true (succeeded) / false (faild)
    */
    public function clean( $group = null, &$ids = null )
    {
        if ( $group == null ) {
            return $this->cleanAll();
        } else {
            return $this->removeGroup( $group, $ids );
        }
    }
    /** 
    * clear all chache
    * @access public
    * @return boolean  true (succeeded) / false (faild)
    */
    public function cleanAll()
    {
        $ret   = array();
        $files = scandir( $this->getFilePath() );//php5 only
        foreach ( $files as $file ) {
            if ( strpos( $file, $this->ext ) !== false ) {
                $ret[]= unlink( $this->getFilePath()."/".$file );
            }
        }
        $ret[]= unlink( $this->getGroupListFileName() );
        $ret[]= unlink( $this->_getModifiedListFileName() );
        $bool = true;
        foreach ( $ret as $b ) {
            $bool = $bool && $b;
        }
        return $bool;
    }
    
    /** 
    * implements of abstract function
    * get Cache data from this resource
    * @access public
    * @param $id    stirng cache id
    * @return string cached data, if cache does not exist, returns null
    */
    public function get( $id )
    {
        $content = @file_get_contents( $this->getFullPath( $id ) );
        if ( !$content === false ) {
            return $content;
        } else {
            return null;
        }
    }
    //---for cache system management --
    //alisas for file_put_csv file_put_csvの記述場所をまだ決めていないから
    public function saveList( $file_name, &$_array ){
    	return file_put_csv($file_name, $_array);
    }
    //alisas for file_get_csv. file_get_csvの記述場所をまだ決めていないから
    public function readList( $file_name )
    {
    	return file_get_csv( $file_name );
    }
    //---------group mangers---------
    //グループ一覧とキャッシュファイルと、更新日付の計三回ファイル読み込みがもったいない・・・
    /** 
    * Returns regitered all group name
    * @access public
    * @param  void
    * @return array array of string, group names
    */
    public function getGroupList()
    {
        return  $this->readList( $this->getGroupListFileName() );
    }
    /**
     * 未テストのメソッド。
     * returns exists groupnames
     * @access public
     * @param void
     * @return array_of_string that has groupnames
     * まだテストしていない
     */
    public function getGroupNameList() {
    	$list = $this->getGroupList();
    	function fetch($keys=array(),$line){
    		if( $line[1] ){//グループ名はCSVの2列目に格納している
	    		$keys[$line[1]] = "true";
	    	}
    		return $keys;
    	};
    	return array_keys( array_reduce($lists, "fetch" ) ) ;
    }
    /**
    * Call by handler.
    * Add $id to cache group $group
    * @access public
    * @param $id    stirng cache id
    * @param $group stirng cache group name
    * @return boolean  true (succeeded) / false (faild)
    */
    public function addIntoGroup( $id, $group ){
        return $this->addGroup( $id, $group );
    }

    /** 
    * add $id to $group
    * @access public
    * @param  String $id cache id
    * @param  String $group cache group name.
    * @return boolean  true (succeeded) / false (faild)
    */
    public function addGroup( $id, $groupnames )
    {
		//特に設定されてなければ無理して書きにに行かない。
		//disabled:ID一覧が取得できなくなる。
    	//if($groupnames == null || sizeof( $groupnames) == 0 ) return true;
		
		//文字列で来た場合
        if ( !is_array( $groupnames ) ) {
            $groupnames = array( $groupnames );
        }
        $_array = $this->getGroupList();
        $num   = sizeof( $_array );
        foreach( $groupnames as $groupname ){
        	$_array = $this->_addGroup($_array, $id, $groupname  );
        }
        if ( $num != sizeof( $_array ) ) {
        //if ( 1 ) {
            return $this->saveGroupList( $_array );
        } else {
            return 2;//not modfied
        }
    }
    protected function _addGroup( &$_array, $id,$groupname ){
        $already_exists = false;
        foreach($_array as $entry ){
        	if( $entry[0] == $id && $entry[1] == $groupname  ){
        		$already_exists = true;
        		break;
        	}
        }
        if( $already_exists === false){
        	$_array[] = array( $id,$groupname );
        }
        return $_array;
    }
    /** 
    * returns group index file name(include file path).
    * @access public
    * @param  void
    * @return String filename with path.
    */
    public function getGroupListFileName()
    {
        $filename = "group";
        return $this->getFilePath()."/".$filename.$this->_ext;
    }
    /** 
    * remove group.
    * cache ids in group will be removed, and then their content cleared.
    * @access public 
    * @param  String $groupname Cache group name
    * @param  Array  $ids reference to removed cache ids, if group has id.
    * @return boolean  true (succeeded) / false (faild)
    */
    public function removeGroup( $groupname, &$ids )
    {
        $list = $this->getGroupList();
        //remove $group from group index file
        $buff = array();
        foreach( $list as $idx => $entry ){
        	if( $entry[1] == $groupname ( $ids == null || in_array( $entry[0], $ids ) ) ){
        		$buff[] = $entry[0];//削除したIDを保存する
        		unset( $list[$idx] );
        	}
        }
        //グループリストを更新する
        $this->saveGroupList( $list );
        //ID一覧を元にTTLとModifiedを消す
        $this->removeLifeTimeIndex($buff);
        $this->removeModifiedIndex($buff);
        //IDを消す
        $this->removeID($id);
    }
    
    /** 
    * remove cache id ( = argment $id ) from group index
    * @access public
    * @param  String $id cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    public function removeFromGroup( $id, $group = null )
    {
        $list = $this->getGroupList();
        //remove from index
        foreach( $list as $idx => $entry ){
        	if( $entry[0] == $id && ( $group == null || $entry[1] == $group ) ){
        		unset( $list[$idx] );
        	}
        }
        return $this->saveGroupList( $list );
    }
    /** 
    * save group index list to csv file
    * @access public
    * @param array array of array( "id", "group name" )
    * @return boolean  true (succeeded) / false (faild)
    */
    public function saveGroupList( &$list )
    {
        $ret = $this->saveList( $this->getGroupListFileName(), $list );
        return ( $ret > 0 );
    }
    
    
    /** 
    * manage last modified 
    *
    *
    *
    *
    */

    /** 
    * return file name(including path)
    * @access private
    * @return String filename
    */
    private function _getModifiedListFileName()
    {
        $filename = "modified";
        return $this->getFilePath()."/".$filename.$this->_ext;
    }
        /** 
    * get index
    * @access public
    * @return Array 
    */
    public function getModifiedList()
    {
    	return $this->_getModified();
    }
    //Idがセットされていたらそれだけを返す
    //IDがNullの場合は全データを返す。
    /** 
    * @access private
    * @param  Stirng $id cache id
    * @return String date, last modified 
    */
    private function _getModified( $id = null ){
    	$arr = array();
    	//file_get_csvに書き換え
    	$lists = file_get_csv( $this->_getModifiedListFileName() );
    	foreach( $lists as $list ){
    		//IDのデータが見つかったらそれだけを返す。
    		if( $id !== null && $id == $list[0] ){
    			return $list[1];
    		}
    		$arr[$list[0]] = $list[1];
    	}
    	return $arr;
    }
    //private function _getModified( $id )
    //{
        //$list = $this->getModifiedList();
        //return $list[$id];
    //}
    /** 
    * @access private
    * @param  Stirng $id cache  id
    * @return String $modified  date, Last modified.
    * @return boolean  true (succeeded) / false (faild)
    */
    private function _setModified( $id, $modified )
    {
        $list = $this->getModifiedList();
        $list[$id] = $modified;
        return $this->saveModifiedList( $list );
    
    }

    /** 
    * remove cache id 's modified record.
    * @access public 
    * @param  String $id cache id.
    * @return boolean  true (succeeded) / false (faild)
    */
    public function removeModifiedIndex( $id )
    {
        $list = $this->getModifiedList();
        unset($list[$id]);
        return $this->saveModifiedList( $list );
    }
    /** 
    * save list
    * TODO::CSVの扱いを一元化する
    * @access private
    * @param array array of array( "id"=>"date" )
    * @return boolean  true (succeeded) / false (faild)
    */
    public function saveModifiedList( $list )
    {
    	$lists = array();
    	foreach( $list as $idx=>$value ){//この処理する関数ないのかなぁ
    		$lists[] = array($idx, $value);
    	}
    	$ret = $this->saveList( $this->_getModifiedListFileName(), $lists );
    	return $ret > 0;
    }
    
    
    
    //有効期限関連の処理
    //time to live
    /**
    * get index
    * @return Array 
    */
    public function getLifeTimeList()
    {
    	return $this->_getLifeTimeList();
    }
    /** 
    * @access private
    * @param  Stirng $id cache id
    * @return int time, time to live 
    */
    public function getLifeTime( $id )
    {
        return $this->_getLifeTimeList( $id );
    }
    public function _getLifeTimeList($id=null)
    {
        $lines = file_get_csv( $this->getLifeTimeFileName() );
        $array = array();
        foreach( $lines as $idx => $line ){
        	if( $id!==null && $id == $line[0] ){
        		return $line;
        	}
            $array[$line[0]] = $line[1];
        }
        return sizeof($array)!=0 ? $array: null ;
    }
    /** 
    * @access private
    * @param  Stirng $id cache  id
    * @return int $time 
    * @return boolean  true (succeeded) / false (faild)
    */
    public function setLifeTime( $id, $time=0 )
    {
    	//$time = null の時は無理して処理しない。
    	if($time==null){
    		return 2;//not modified
    	}
        $list = $this->getLifeTimeList();
        $list[$id] = $time;
        return $this->saveLifeTimeList( $list );
    
    }
    
    /** 
    * return file name(including path)
    * @return String filename
    */
    public function getLifeTimeFileName()
    {
        $filename = "life_time";
        return $this->getFilePath()."/".$filename.$this->_ext;
    }
    
    /** 
    * remove cache id 's modified record.
    * @access public 
    * @param  String $id cache id.
    * @return boolean  true (succeeded) / false (faild)
    */
    public function removeLifeTimeIndex( $id )
    {
        $list = $this->getLifeTimeList();
        if( !isset($list[$id] ) ){
        	return true;
        }
		unset($list[$id]);
        return $this->saveLifeTimeList( $list );
    }
    /** 
    * save list
    * TODO::CSVの扱いを一元化する
    * @access private
    * @param array array of array( "id"=>"date" )
    * @return boolean  true (succeeded) / false (faild)
    */
    public function saveLifeTimeList( $list )
    {
    	if(  $list == null ){ $list = array(); }
    	$lists = array();
    	foreach( $list as $idx=>$value ){//この処理する関数ないのかなぁ・・
    		$lists[] = array($idx, $value);
    	}
    	return $this->saveList( $this->getLifeTimeFileName(),$lists );
    }
}
/////////////
//単体テスト
//単体でもキャッシュの読み書きが出来るので
////test code///
//$handler = new Cache_Handler_Resource_File();
//$handler->clean();
//$data = <<<EOF
//dsfasDSf\\\a\"
//EOF;
////var_dump( $data );
//var_dump( $handler->save( $data, "aa" ) );
//var_dump( $handler->save( $data, "1" ) ) ;
//$ret = $handler->getLastModified( "1" );
////////var_dump( $handler->dsn );
//var_dump( $ret );
//var_dump( $handler->get("1") );
////var_dump( $handler->get("aa") );
////var_dump( $handler );
////$handler->remove( "1" );
////$handler->clean("default");

?>