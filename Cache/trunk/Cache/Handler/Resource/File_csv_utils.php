<?php
//CSVを簡単に扱う
function file_put_csv( $filename,$lines, $file_mode="w", $delim=",",  $enclosure="\"" )
{
	$fp = fopen( $filename, $file_mode );
	$state =null;
	foreach( $lines as $idx => $line ){
		$state = fputcsv( $fp, $line, $delim, $enclosure );
	}
	return $state;
}
//CSVを２次元配列に一括変換する関数
function file_get_csv($filename, $length=null,$delim=",",$enclosure="\"")
{
	$data = array();
	$fp = fopen($filename,"r");
	while( ($buff=fgetcsv($fp,$delim,$enclosure)) !==FALSE ){
		$data[]=$buff;
	}
	return $data;
}
?>