<?php

/** 
 *  Save cache data by memcached
 * @package     Cache_Handler
 * @category    Cache
 */

class  Cache_Handler_Resource_MemCache implements Cache_Handler_Resource_Interface {
    public $memcache;
    /** 
    * constructor for php4 compatible
    * @access public
    * @param  $ini array of string for config.
    *         ini array is like this ,array( 'host'=>'localhost', 'port'=> 11211 )
    */
    public function __construct( $ini )
    {
        $this->ttl = 3600;//default time out.
        foreach( $ini as $key => $val ){
            $this->$key = $val;
        }
        $this->memcache = new MemCache();
        $this->memcache->connect( $this->host, $this->port );
    }
    
    /** 
    * wrapper
    * @access private
    */
    public function _insert( $id, $data, $ttl )
    {
        $this->memcache->delete( $id );
        $ret = $this->memcache->set( $id, $data, 0, $ttl );
        return $ret;
    }
    /** 
    * wrapper
    * @access private
    */
    public function _delete( $id )
    {
        return $this->memcache->delete( $id );
    }
    /** 
    * Call by hander
    * @access public
    * @param $data  object cache data
    * @param $id    stirng cache id
    * @param $group array  Array of String. group name of this cache 
    * @return boolean  true (succeeded) / false (faild)
    */
    public function save(  $data, $id, $group = null )
    {
        $ret[]= $this->_insert( $id, $data, $this->ttl );
        $ret[]= $this->addGroup( $id, $group );
        $ret[]= $this->setTimeToLive( $id, $this->ttl ); 
        $ret[]= $this->setLastModified( $id, date("r") );
        return ($ret[0] && $ret[1] && $ret[2] && $ret[3] );
    }
    /** 
    * Call by hander
    * definition of remove method
    * @access public
    * @param $id    stirng cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    public function remove( $id )
    {
        $ret[] = $this->_delete( $id );
        $ret[] = $this->_leaveGroup( $id );
        $ret[] = $this->_deleteLastModified( $id );
        $ret[] = $this->_deleteTTL( $id );
        return ( $ret[0] && $ret[1] && $ret[2] && $ret[3] );
    }
    /** 
    * Call by hander
    * definition of clean method
    * without group name, clean all chache
    * @access public
    * @param  String $group    stirng cache group name
    * @param  Array  $ids array of id removed.
    * @return boolean  true (succeeded) / false (faild)
    */
    public function clean( $group =null, &$ids = null )
    {
        if( $group == null ){
            return $this->_cleanAll();
        }else{
            return $this->_removeGroup( $group, &$ids );
        }
    }
    public function _cleanAll(){
        return $this->memcache->flush();
    }
    /** 
    * Call by hander
    * definition of get method
    * get Cache data from this resource
    * @access public
    * @param $id    stirng cache id
    * @return string cached data, if cache does not exist, returns null
    */
    public function get( $id )
    {
        $content = $this->memcache->get( $id );
        if( !$content === false ){
            return $content;
        }else{
            return null;
        }
    }
    public function _getModifiedKeyName()
    {
        return ".modified";
    }
    public function _deleteLastModified( $id )
    {
        return $this->_delete( $id.$this->_getModifiedKeyName() );
    }
    /** 
    * Call by hander.
    * get Cache created time from this resource.
    * @access public
    * @param $id    stirng cache id
    * @return string cache created time
    */
    public function getLastModified( $id )
    {
        return $this->get( $id.$this->_getModifiedKeyName() );
    }
    /** 
    * Call by hander.
    * Definition of getLastModified method.
    * Set or update  Cache created(modified) time.
    * @access public
    * @param $id    stirng cache id
    * @param $date  stirng cache created time. default value date("r")
    * @return String date of cache create or modified time
    */
    public function setLastModified( $id, $date )
    {
        if( $date == null){
            $date = date("r");
        }
        return $this->_insert( $id.$this->_getModifiedKeyName(),
                               $date, $this->ttl
                             );
    }
    /** 
    * Call by hander.
    * definition of setTimeToLive method.
    * set or update cache TTL for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @param $int   int    cache lifetime , default -1
    * @return boolean  true (succeeded) / false (faild)
    */
    public function setTimeToLive( $id, $int = null )
    {
        if( $int == null ){
            $int = $this->ttl;
        }else if( $int != $this->ttl ){
            $this->ttl = $int;
            $this->memcache->delete( $id, $this->ttl );
        }
        return $this->_insert( $id.$this->_getTTLKeyName(), 
                               $this->ttl, 
                               $this->ttl 
                             );
    }
    /** 
    * Call by hander.
    * definition of getTimeToLive method.
    * get cache Life time for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @return int   cache lifetime. if value does not exits , this method return -1.
    */
    public function getTimeToLive( $id )
    {
        return $this->get( $id.$this->_getTTLKeyName() );
    }
    public function _getTTLKeyName()
    {
        return ".ttl";
    }
    public function _deleteTTL( $id )
    {
        return $this->_delete( $id.$this->_getTTLKeyName() );
    }
    public function getGroupList()
    {
        $ret = $this->get( $this->getGroupKeyName() );
        if( $ret == null ){
            return array();
        }else{
            return unserialize( $ret );
        }
    }
    public function getGroupKeyName()
    {
        return md5(".cache.groups");
    }
    //array( array( "id" => "group name" ) )
    public function saveGroupList( $array )
    {
        $str = serialize( $array );
        return $this->memcache->set( $groupname.$this->getGroupKeyName(), $str );
    }
    /**
    * Call by hander.
    * Add $id to cache group $group
    * @access public
    * @param $id    stirng cache id
    * @param $group mixed  stirng/array cache group name
    * @return boolean  true (succeeded) / false (faild)
    */
    public function addIntoGroup( $id, $group ){
        return $this->addGroup( $id, $group );
    }

    public function addGroup( $id, $groupnames )
    {
        if( $groupnames == null || $groupnames == "" ){
            return true;
        }
        if( !is_array( $groupnames ) ){
            $groupnames = array( $groupnames );
        }
        $list = $this->getGroupList();
        foreach( $groupnames as $groupname ){
            if( sizeof( $list ) == 0 ){
                $list[] = array( $id => $groupname );
                continue;
            }
            foreach( $list as $pair ){
                list( $_key, $_val ) = each( $pair );
                if( $_key == $id && $_val == $groupname ){
                    continue;
                }else{
                    $list[] = array( $id => $groupname );
                }
            }
        }
        return $this->saveGroupList( $list );
    }
    public function _removeGroup( $groupname, &$ids = null )
    {
        $list = $this->getGroupList();
        foreach( $list as $idx => $pair ){
            list( $_key, $_val ) = each( $pair );
            if( $_val == $groupname ){
                $ids[] = $_key;
                unset( $list[$idx] );
            }
        }
        $list  = array_values( $list );
        $ret[] = $this->saveGroupList( $list );
        foreach( $ids as $id ){
            $ret[] = $this->remove( $id );
        }
        return !in_array( $ret, false, true );//if $ret has false, return false;
    }
    /** 
    * remove cache id ( arg $id ) from group index
    * @access public
    * @param  String $id cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    public function removeFromGroup( $id, $group = null )
    {
        $ret[] = $this->remove( $id );
        $ret[] = $this->_leaveGroup( $id, $group );
        return !in_array( $ret, false, true );//if $ret has false, return false;
    }
    /** 
    * remove cache id ( arg $id ) from group index
    * @access public
    * @param  String $id cache id
    * @param  String $group Cache group name
    * @return boolean  true (succeeded) / false (faild)
    */
    public function _leaveGroup( $id, $group = null )
    {
        $list = $this->getGroupList();
        foreach( $list as $idx => $pair ){
            list( $_key, $_val ) = each( $pair );
            if( $_key == $id ){
                if( $group == null || $group == $_val ){
                    $ids[] = $_key;
                    unset( $list[$idx] );
                }
            }
        }
        $list  = array_values( $list );
        $ret[] = $this->saveGroupList( $list );
        return $ret[0] && $ret[1];
    
    }
}
