<?php

/** 
 * manage multiple cache resource 
 * This Class select resource to store/retrive cache_data by cache_id
 *<code>
 * //example for multiple cache
 * $multi = new Pear_Cache_Handler_Resource_Mirror();
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[0]) );
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[1]) );
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[2]) );
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[3]) );
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[4]) );
 * //add multile to handler
 * $handler = new Pear_Cache_Handler($id);
 * $handler->setResource( $multi );
 * $handler->setCache( $data );
 *<code>
 * @package     Cache_Handler
 * @category    Cache
 */

require_once 'Cache/Handler/Resource/Interface.php';

class Cache_Handler_Resource_Mirror implements Cache_Handler_Resource_Interface {
    
    /** 
    * constructor
    * @access public
    * @param $ini array of string for config.
    */
    public function __construct( $ini )
    {
        foreach( $ini as $key => $val ){
            $this->$key = $val;
        }
    }
    /** 
    * add resource instance
    * @access public 
    * @param Cache_Handler_Resource resource
    * @return
    */
    public function addResource( $resource )
    {
        $this->resources[] = $resource;
    }
    public function setResource( $resource = null )
    {
        if( is_array($resource) ){
            $this->resources = $resource;
        }else{
            $this->resources   = array();
            $this->addResource( $resource );
        }
    }
    public function getResource( $idx )
    {
        return $this->resources[$idx];
    }
    private function _target( $id = null )
    {
    	return intval( mt_srand() % sizeof($this->resources) );
    }
    /** 
    * Call by hander
    * @access public
    * @param $data  object cache data
    * @param $id    stirng cache id
    * @param $group array  Array of String. group name of this cache 
    * @return boolean  true (succeeded) / false (faild)
    */
    public function save( $data, $id, $group = null )
    {
    	foreach( $this->resources as $idx => $res ){
        	$ret[] = $this->resources[$idx]->save( $data, $id, $group );
    	}
    	return !in_array( $ret, false, true );
    }
    /** 
    * Call by hander
    * @access public
    * @param $id    stirng cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    public function remove( $id )
    {
    	foreach( $this->resources as $idx => $res ){
        	$ret[] = $this->resources[$idx]->remove( $id );
    	}
    	return !in_array( $ret, false, true );
    }
    /** 
    * Call by hander
    * without group name, clean all chache
    * @access public
    * @param  String $group    stirng cache group name
    * @param  Array  $ids array of id removed.
    * @return boolean  true (succeeded) / false (faild)
    */
    public function clean( $group =null, &$ids = null )
    {
    	foreach( $this->resources as $idx => $res ){
            $ret[] = $this->resources[$idx]->clean( $group, &$ids );
    	}
    	return !in_array( $ret, false, true );
    }
    /** 
    * Call by hander
    * definition of get method
    * @access public
    * @param $id    stirng cache id
    * @return string cached data, if cache does not exist, returns null
    */
    public function get( $id )
    {
        return $this->resources[$this->_target()]->get( $id );
    }
    /** 
    * Call by hander.
    * get Cache created time from this resource.
    * @access public
    * @param $id    stirng cache id
    * @return string cache created time
    */
    public function getLastModified( $id )
    {
        return $this->resources[$this->_target()]->getLastModified( $id );
    }
    /** 
    * Call by hander.
    * Set or update  Cache created(modified) time.
    * @access public
    * @param $id    stirng cache id
    * @param $date  stirng cache created time. default value date("r")
    * @return String date of cache create or modified time
    */
    public function setLastModified( $id, $date )
    {
    	foreach( $this->resources as $idx => $res ){
            $ret[] = $this->resources[$idx]->setLastModified( $id, $date );
    	}
    	return !in_array( $ret, false, true );
    }
    /** 
    * Call by hander.
    * set or update cache TTL for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @param $int   int    cache lifetime , default -1
    * @return boolean  true (succeeded) / false (faild)
    */
    public function setTimeToLive( $id, $int = null )
    {
    	foreach( $this->resources as $idx => $res ){
            $ret[] = $this->resources[$idx]->setTimeToLive( $id, $int );
    	}
    	return !in_array( $ret, false, true );
    }
    /** 
    * Call by hander.
    * get cache Life time for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @return int   cache lifetime. if value does not exits , this method return -1.
    */
    public function getTimeToLive( $id )
    {
        return $this->resources[$this->_target()]->getTimeToLive( $id );
    }
    /** 
    * remove cache id ( arg $id ) from group index
    * @access public
    * @param  String $id cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    public function removeFromGroup( $id, $group = null )
    {
    	foreach( $this->resources as $idx => $res ){
            $ret[] = $this->resources[$idx]->removeFromGroup( $id, $group);
    	}
    	return !in_array( $ret, false, true );
    }
    /**
    * Call by hander.
    * definition of joinGroup method.
    * Add $id to cache group $group
    * @access public
    * @param $id    stirng cache id
    * @param $group stirng cache group name
    * @return boolean  true (succeeded) / false (faild)
    */
    public function addIntoGroup( $id, $group )
    {
    	foreach( $this->resources as $idx => $res ){
            $ret[] = $this->resources[$idx]->addIntoGroup( $id, $group );
    	}
    	return !in_array( $ret, false, true );
    }
}
