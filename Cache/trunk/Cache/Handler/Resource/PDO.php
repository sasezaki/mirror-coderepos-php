<?php
/** 
 *  Save cache data by PDO
 * @package     Cache_Handler
 * @category    Cache
 */

require_once 'Cache/Handler/Resource/Interface.php';
//キャッシュをDBに格納するClass
//PDOで抽象化。
//
class Cache_Handler_Resource_PDO implements Cache_Handler_Resource_Interface {

    /** 
    * constructor 
    * @access public 
    * @param array $ini array( "property name" => "value " );
    * @return void
    */
    public function __construct( $ini= array() )
    {
       $this->setDsn();
       foreach( $ini as $key => $val ){
            $this->$key = $val;
       }
       //テーブルを初期化する
       //デーブルが定義済みか調べるSQLに方言が多いみたいなので
       //毎回初期化をためしてお茶濁す
       //とてもみっともない実装
       $this->initTable();
    }
    
    /** 
    * 
    * @access public 
    * @param  String dsn, used for new PDO() and so on
              default is sqlite
    * @return void
    */
    public function setDsn( $dsn = null )
    {
        if( $this->dsn == null &&  $dsn == null ){
            $temp = rtrim( str_replace( "\\", "/", $_SERVER["TEMP"] ), "/" );
            $dsn  = "sqlite:".$temp."/cache_handler.sqlite3.pdo";
        }
        $this->dsn = $dsn;
    }
    
    /** 
    * initialize PDO object
    * @access protected 
    * @param  void
    * @return void
    */
    protected function initDB()
    {
        if( isset( $this->pdo ) && isset($this->dsn) && $this->dsn != "" ){
            $this->pdo = new PDO( $this->dsn );
        }
    }
    /** 
    * initialize DataBase Table
    * @access protected 
    * @param  void
    * @return void
    */
    protected function initTable()
    {
    	////////////
        //TODO replace from SQL to SQL Builder( such as ORM )
        ///////////
        ///-----+------+----------+----------+
        ///| id | data | modified | lifetime |
        ///-----+------+----------+----------+
        ///
        $sql[] = "
            CREATE table cache_data (
                id varchar(100) primary key,
                data text,
                modified datetime,
                lifetime int(4)
            );";
        /////////
        $sql[] = "CREATE INDEX cache_id ON cache_data (id, modified);";
        ///////
        ///-----+------------+
        ///| id | group_name |
        ///-----+------------+
        $sql[] = "
            CREATE TABLE cache_group (
                id varchar(100),
                group_name varchar(100)
            );";
        
        /////////
        $sql[] = "CREATE INDEX cache_group_name_index 
                    ON cache_group ( group_name );";
        $sql[] = "CREATE INDEX cache_data_index 
                    ON cache_data ( id );";
        foreach( $sql as $str ){
            $ret[] = $this->execSQL( $str );
        }

    }
    
    
    /** 
    * send sql to server and get response
    * @access protected
    * @param  String SQL
    * @return boolean  true (succeeded) / false (faild)
    */
    protected function execSQL( $sql )
    {
        $this->initDB();
        //TODO::トランザクションを開始する。
        if( $this->pdo != null ){
            $sql = str_replace( "\r", " ", $sql );
            $sql = str_replace( "\n", " ", $sql );
            $sql = str_replace( "\t", " ", $sql );
            $ret =  $this->pdo->exec( $sql );
        }
        return $ret > 0;
    }
    /** 
    * quote sql special chars
    * @access protected
    */
    protected function escape( $data )
    {
        $data = $this->pdo->quote( $data );
        return $data;
    }
    /**
    * insert cache data
    * @access protected
    * @param String $id cache id
    * @param object $data cache data
    * @param String $datetime lastmodified
    * @param int    $time time to live
    * @return
    */
    protected function _insert( $id, $data, $datetime, $time = null )
    {
        $ret      = $this->_delete( $id );
        $id       = $this->escape( $id );
        $data     = $this->escape( $data );
        $datetime = $this->escape( $datetime );
        $time     = $this->escape( $time );
        $sql      = "INSERT INTO cache_data 
                        VALUES ( $id, $data, $datetime, $time );";
        return $this->execSQL( $sql );
    
    }
    /** 
    * Implements of abstract function
    * @param $data  object cache data
    * @param $id    stirng cache id
    * @param $group array  Array of String. group name of this cache 
    * @return boolean  true (succeeded) / false (faild)
    */
    public function save( $data, $id, $group = null )
    {
        $_id      = $this->getEncID( $id );
        $datetime = date('r');
        //notice !! argment order is not same as  _insert
        $ret[] = $this->_insert( $_id, $data, $datetime );
        //group
        if( $group == null ){
            $group = array();
        }else if( !is_array( $group ) ){
            $group = array( $group );
        }
        foreach( $group as $groupname ){
            $ret[] = $this->setGroup( $_id, $groupname );
        }
        //
        $bool = true;
        foreach( $ret as $b ){
            $bool = $bool && $b;
        }
        return $bool;
    }
    /** 
    * delete cache data
    * @access protected
    * @param String $id cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    protected function _delete( $id )
    {
        $_id   = $this->escape( $id );
        $sql   = "DELETE FROM cache_data WHERE id =  $_id ;";
        $ret[] = $this->execSQL( $sql );
        $ret[] = $this->_removeFromGroup( $id );
        return $ret[0] && $ret[1] && true;
    }
    /** 
    * Implements of abstract function
    * @access public
    * @param $id    stirng cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    public function remove( $id )
    {
        $id = $this->getEncID( $id );
        return $this->_delete( $id );
    }
    /** 
    * Implements of abstract function
    * without group name, clean all chache
    * @access public
    * @param  String $group    stirng cache group name
    * @param  Array  $ids array of id removed by function.
    * @return boolean  true (succeeded) / false (faild)
    */
    public function clean( $group = null, &$ids = null)
    {
        if( $group == null ){
            $ret[] = $this->cleanAll();
        }else{
            $ids = $this->getIdsInGroup( $group );
            foreach( $ids as $id ){
                $ret[] = $this->_delete( $id );
            }
            //$ret[] = $this->removeGroup( $group );
        }
        $bool = true;
        foreach( $ret as $b ){
            $bool = $bool && $b;
        }
        return $bool;
    }
    /** 
    * clear all chache
    * @access protected
    * @return boolean  true (succeeded) / false (faild)
    */
    public function cleanAll()
    {
        $sql   = "DELETE FROM cache_data;";
        $ret[] = $this->execSQL( $sql );
        $sql   = "DELETE FROM cache_group;";
        $ret[] = $this->execSQL( $sql );
        return $ret[0] && $ret[1];
    }
    /** 
    * get Cache data from PDO
    * @access protected
    * @param $id    stirng cache id
    * @return string cached data, if cache does not exist, returns null
    */
    protected function _select( $id )
    {
    	//ExpiredやGETで複数回同じQueryを出している問題点
        $_id = $this->escape( $id );
        $sql = "SELECT * FROM cache_data WHERE id = $_id";
        $res = $this->pdo->query( $sql );
        if( $res == false ){
            return null;
        }else{
            return $res->fetch();
        }
    }
    /** 
    * Implements of abstract function
    * get Cache data from this resource
    * @access public
    * @param $id    stirng cache id
    * @return string cached data, if cache does not exist, returns null
    */
    public function get( $id )
    {
        $id  = $this->getEncID( $id );
        $row = $this->_select( $id );
        return $row["data"];
    }
    /** 
    * Implements of abstract function
    * get Cache created time from this resource.
    * @access public
    * @param $id    stirng cache id
    * @return string cache created time
    */
    public function getLastModified( $id )
    {
        $id  = $this->getEncID( $id );
        $row = $this->_select( $id );
        return date( "r", strtotime( $row["modified"] ) );
    }
    /** 
    * Implements of abstract function
    * Set or update  Cache created(modified) time.
    * @access public
    * @param $id    stirng cache id
    * @param $date  stirng cache created time. default value date("r")
    * @return String date of cache create or modified time
    */
    public function setLastModified( $id, $datetime )
    {
        $id  = $this->getEncID( $id );
        $row = $this->_select( $id );
        return $this->_insert( $id, $row["data"], $datetime, $row["lifetime"] );
    }
     
    /** 
    * Call by hander.
    * set or update cache TTL for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @param $int   int    cache lifetime , default -1
    * @return boolean  true (succeeded) / false (faild)
    */
    public function setTimeToLive( $id, $int = null ){
        if( $int == null ){
            if( $this->lifetime != null ){
                $int = $this->lifetime;
            }
        }
        $_id = $this->getEncID( $id );
        $row = $this->_select( $_id );
        return $this->_insert( $id, $row["data"], $row["modified"], $int );
    }
   /** 
    * Implements of abstract function
    * get cache Life time for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @return int   cache lifetime. if value does not exits , this method return -1.
    */
    public function getTimeToLive( $id )
    {
        $id  = $this->getEncID( $id );
        $row = $this->_select( $id );
        return $row["lifetime"];
    }
    /**
    * Call by hander.
    * definition of joinGroup method.
    * Add $id to cache group $group
    * @access public
    * @param $id    stirng cache id
    * @param $group stirng cache group name
    * @return boolean  true (succeeded) / false (faild)
    */
    public function addIntoGroup( $id, $group ){
        $_id = $this->getEncID( $id );
        return $this->setGroup( $_id, $group );
    }
    /** 
    * add and update $id to $group
    * @access protected
    * @param  String $id cache id
    * @param  String $group cache group name.
    * @return boolean  true (succeeded) / false (faild)
    */
    protected function setGroup( $id, $groupname )
    {
        $this->removeFromGroup( $id, $groupname );
        $_id        = $this->escape( $id );
        $_groupname = $this->escape( $groupname );
        $sql        = "INSERT INTO cache_group VALUES ( $_id, $_groupname );";
        $ret        = $this->execSQL( $sql );
        return ( $ret );
    }
    /** 
    * remove $id from $group
    * @access public
    * @param  String $id cache id
    * @param  String $group cache group name.
    * @return boolean  true (succeeded) / false (faild)
    */
    public function removeFromGroup( $id, $groupname = null )
    {
        $_id = $this->getEncID( $id );
        return $this->_removeFromGroup( $_id, $groupname );
    }
    protected function _removeFromGroup( $id, $groupname = null )
    {
        $_id = $this->escape( $id );
        $sql = "DELETE FROM cache_group WHERE id = $_id";
        if( $groupname != null ){
            $_groupname  = $this->escape( $groupname );
            $sql        .= " AND group_name = $_groupname ";
        }
        $sql .= " ;";
        return $this->execSQL( $sql );
    }
    /** 
    * remove group. cache id and cache file in  arg $group will clear
    * @access protected 
    * @param  String $groupname Cache group name
    * @param  Array  $ids reference to removed cache ids, if group has id.
    * @return boolean  true (succeeded) / false (faild)
    */
    protected function removeGroup( $groupname )
    {
        $_groupname = $this->escape( $groupname );
        //sql
        $sql = "DELETE FROM cache_group 
                    WHERE group_name = $_groupname ;";
        $ret = $this->execSQL( $sql );
        return ( $ret > 0 );
    }
    /** 
    * return array of id , whtch included group
    * @access protected
    * @param  String $groupname Cache group name
    * @return array array of string.
    */
    protected function getIdsInGroup( $groupname )
    {
        $groupname  = $this->escape( $groupname );
        $sql       .= "SELECT * FROM cache_group 
                        WHERE group_name = $groupname ;";
        $array = array();
        $res   = $this->pdo->query( $sql );
        if( $res !== false ){
            while( ( $row =$res->fetch() ) != null ){
                $array[] = $row["id"];
            }
        }
        return $array;
    }
    /** 
    * return unique id, calculate by hash.
    * @access protected 
    * @param  String id
    * @return String hashed id
    */
    protected function getEncID( $id )
    {
        return md5( $id );
    }
}
?>