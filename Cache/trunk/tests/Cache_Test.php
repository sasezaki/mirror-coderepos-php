<?php
//phpinfo();	
//include パスの解決
set_include_path(get_include_path() . PATH_SEPARATOR . "..");

//////////////////////////////////////////
//テストコード
//////////////////////////////////////////

require_once ("Cache/Handler.php");
require_once ("Cache/Handler/Item.php");
require_once ("Cache/Utils.php");
function test_1(){
	$ret = true;
	
	//echo "File.phpを使ってキャッシュを保存:";
	//$id = "test_id_01";
	//$ret = $ret && cache_put_contents( $id, "test1", "e:/cache" );
	//$ret = $ret && cache_get_contents($id, "e:/cache");
	//var_dump ( $ret );
	return $ret;
}

function test_2(){
	$ret = true;
	//echo "File.phpを使ってItem.phpのテスト:";
	//require_once "Cache/Handler/Resource/File.php";
	//$id = "test_id_01";
	//$data = "test1";
	//$h = new Cache_Handler();
	//$h->setResource(new Cache_Handler_Resource_File( array("path"=>"e:/cache")));
	//$item = $h->CacheItem($id);
	//$ret = $ret &&  $item->set($data);
	//$ret = $ret &&  $item->get() == $data;
	//$ret = $ret &&  $item->available();
	//$ret = $ret &&  $item->head();
	//$ret = $ret &&  $item->join("test_g");
	//$ret = $ret &&  $item->delete();
	return $ret == true;

}

function test_3(){
	$ret = true;
	//echo "File.phpを使ってプラグイン関係のテスト:";
	//require_once "Cache/Handler/Resource/File.php";
	//require_once "Cache/Handler/Plugin/Serializer.php";
	//require_once "Cache/Handler/Plugin/Gzip.php";
	//require_once "Cache/Handler/Plugin/Base64.php";
	//$test = array();
	//$test[] = "aa";
	//$test[] = "aa";
	//$test[] = "aa";
	//$test[] = "aa";
	//$id = "test_id_01";
	//$data = $test;
	//$h = new Cache_Handler();
	//$h->setResource(new Cache_Handler_Resource_File( array("path"=>"e:/cache")));
	//$h->addPlugin( new Cache_Handler_Plugin_Serializer() );//array をシリアライズ
	//$h->addPlugin( new Cache_Handler_Plugin_Gzip() );//Gzip圧縮テスト
	//$h->addPlugin( new Cache_Handler_Plugin_Base64() );//Base64で文字列に
	//$item = $h->CacheItem($id);
	//$ret = $ret &&  $item->set($data);
	//$ret = $ret &&  $item->get() == $data;
	//$ret = $ret &&  $item->available();
	//$ret = $ret &&  $item->head();
	//$ret = $ret &&  $item->join("test_g");
	//$ret = $ret &&  $item->delete();
	return $ret;
}

function test_4(){
	$ret = true;
	//echo "File.phpを使ってプラグイン関係のテスト:";
	//require_once "Cache/Handler/Resource/File.php";
	//require_once "Cache/Handler/Plugin/JSON.php";
	//require_once "Cache/Handler/Plugin/Deflate.php";
	//require_once "Cache/Handler/Plugin/Crypt.php";
	//require_once "Cache/Handler/Plugin/Base64.php";
	//$test = array();
	//$test[] = "aa";
	//$test[] = "aa";
	//$test[] = "aa";
	//$test[] = "aa";
	//$id = "test_id_01";
	//$data = $test;
	//$h = new Cache_Handler();
	//$h->setResource(new Cache_Handler_Resource_File( array("path"=>"e:/cache")));
	//$h->addPlugin( new Cache_Handler_Plugin_JSON() );//array をシリアライズ
	//$h->addPlugin( new Cache_Handler_Plugin_Deflate() );//Gzip圧縮テスト
	//$h->addPlugin( new Cache_Handler_Plugin_Crypt("キーワード") );//暗号化に
	//$h->addPlugin( new Cache_Handler_Plugin_Base64() );//Base64で文字列に
	//$item = $h->CacheItem($id);
	//$ret = $ret && $item->set($data);
	//$ret = $ret && ($a = $item->get() ) == $data;
	//$ret = $ret && $item->available();
	//$ret = $ret && $item->head();
	//$ret = $ret &&  $item->join("test_g");
	//$ret = $ret &&  $item->delete();
	return $ret;
}
function test_5(){
	$ret = true;
	//echo "File.phpを使ってプラグイン関係のテスト:";
	//require_once "Cache/Handler/Resource/File.php";
	//require_once "Cache/Handler/Plugin/SrcCode.php";
	//$test = array();
	//$test[] = "aa";
	//$test[] = "aa";
	//$test[] = "aa";
	//$test[] = "aa";
	//$id = "test_id_01";
	//$data = $test;
	//$h = new Cache_Handler();
	//$h->setResource(new Cache_Handler_Resource_File( array("path"=>"e:/cache")));
	//$h->addPlugin( new Cache_Handler_Plugin_SrcCode() );//array をシリアライズ
	//$item = $h->CacheItem($id);
	//$ret = $ret && $item->set($data);
	//$ret = $ret && ($a = $item->get() ) == $data;
	//$ret = $ret && $item->available();
	//$ret = $ret && $item->head();
	//$ret = $ret &&  $item->join("test_g");
	//$ret = $ret &&  $item->delete();
	return $ret;
}
function test_6(){
	$ret = true;
	//echo "File.phpを使ってプラグイン関係のテスト:";
	//require_once "Cache/Handler/Resource/File.php";
	//require_once "Cache/Handler/Plugin/Querizer.php";
	//$test = array();
	//$test[] = "aa";
	//$test[] = "aa";
	//$test[] = "aa";
	//$test[]["test_"] = "aa";
	//$id = "test_id_01";
	//$data = $test;
	//$h = new Cache_Handler();
	//$h->setResource(new Cache_Handler_Resource_File( array("path"=>"e:/cache")));
	//$h->addPlugin( new Cache_Handler_Plugin_Querizer() );//array をシリアライズ
	//$item = $h->CacheItem($id);
	//$ret = $ret && $item->set($data);
	//$ret = $ret && ($a = $item->get() ) == $data;
	//$ret = $ret && $item->available();
	//$ret = $ret && $item->head();
	//$ret = $ret &&  $item->join("test_g");
	//$ret = $ret &&  $item->delete();
	return $ret;
}

function test_6(){
	$ret = true;
	echo "File.phpをHandlerから呼び出してテスト:";
	require_once "Cache/Handler/Resource/File.php";
	require_once "Cache/Handler/Plugin/Querizer.php";
	$test = array();
	$test[] = "aa";
	$test[] = "aa";
	$test[] = "aa";
	$test[]["test_"] = "aa";
	$id = "test_id_01";
	$data = $test;
	$h = new Cache_Handler();
	$h->setResource(new Cache_Handler_Resource_File( array("path"=>"e:/cache")));
	$h->addPlugin( new Cache_Handler_Plugin_Querizer() );//array をシリアライズ
	$h->setCache( $id, $data );
	$h->getCache($id);
	$h->isExists($id);
	$h->setCacheGroupName($id, "test_gg");
	$h->CacheModifiedSince($id);
	$h->GroupNames();
	//$item = $h->CacheItem($id);
	//$ret = $ret && $item->set($data);
	//$ret = $ret && ($a = $item->get() ) == $data;
	//$ret = $ret && $item->available();
	//$ret = $ret && $item->head();
	//$ret = $ret &&  $item->join("test_g");
	//$ret = $ret &&  $item->delete();
	return $ret;
}

//テストを呼び出す
function do_test(){
	foreach ( range(1, 100, 1)  as $i ){
		$func_name = "test_$i";
		if( !function_exists( $func_name )){
			echo "\nテスト終了\n";
			echo "EOF\n";
			break;
		}
		$result = $func_name();
		if( $result==false ){
			echo "$func_name でエラーがありました";
		}
	}
}
//SrcCode.php
//Yaml.php
//Querize.php

//TEST実行
do_test();

