<?php

    /**
    * Class/LoadClass.class.php
    * @author fbis<ittetsu.miyazaki@gmail.com>
    * @package Class
    */
    error_reporting( E_ALL );

    /**
    * Class_LoadClass
    * クラスの動的ロードを支援するクラス
    * 
    * <pre>
    * $load_class =& new Class_LoadClass('My_Class');
    * $load_class->load_class('Hoge');  // My/Class/Hoge.class.phpをロード
    * $load_class->load_class('+Hoge'); // Hoge.class.phpをロード
    * 
    * // 拡張子変更例
    * $load_class =& new Class_LoadClass('My_Class','inc');
    * $load_class->load_class('Hoge');  // My/Class/Hoge.incをロード
    * 
    * // 戻り値にはクラス名が返る
    * $class =  $load_class->load_class('Hoge');
    * $obj   =& new $class; // new My_Class_Hoge;
    * </pre>
    * 
    * HISTORY
    * 2006/09/09 ver 1.01
    * 2007/07/18 ver 1.02 loadメソッドの追加
    * 
    * @version 1.02
    * @package Class
    * @access public
    */
    class Class_LoadClass {
        
        var $_base_ext            = 'class.php';
        var $_load_base_classname = '';
        
        /**
        * コンストラクタ
        * @param string クラスの接頭辞(省略時は空文字)
        * @param string 拡張子名(省略時"class.php")
        * @access public
        */
        function Class_LoadClass ($base_class=null,$base_ext=null) {
            if ( !is_null($base_class) ) $this->load_base_classname($base_class);
            if ( !is_null($base_ext)   ) $this->base_ext($base_ext);
        }
        
        /**
        * クラスの動的ロードを行う。
        * 
        * @param string クラス名
        * @return string ロードしたクラス名
        *         boolean クラス名が不正な時はfalseを返す
        * @access public
        */
        function load_class ($class) {
            if ( strpos($class,'+') === 0 ) {
                $class = substr($class,1);
            }
            else {
                if ( $this->load_base_classname() ) {
                    $class = $this->load_base_classname().'_'.$class;
                }
            }
            
            if ( !class_exists($class) ) {
                if ( !preg_match('/^[\w]+$/',$class) ) {
                    return false;
                }
                $this->load( sprintf('%s.%s',str_replace('_','/',$class),$this->base_ext()) );
            }
            
            return $class;
        }
        
        function load ($class) {
            require_once( $class );
        }
        
        // Accessor
        function base_ext () {
            $args = func_get_args();
            if ( count($args) ) $this->_base_ext = $args[0];
            return $this->_base_ext;
        }
        function load_base_classname () {
            $args = func_get_args();
            if ( count($args) ) $this->_load_base_classname = $args[0];
            return $this->_load_base_classname;
        }
    }
