<?php

/**
 * Short description for file
 * 
 * Long description (if any) ...
 * 
 * PHP versions 4 and 5
 * 
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy 
 * the PHP License and are unable to obtain it through the web, 
 * send a note to license@php.net so we can mail you a copy immediately.
 * 
 * @category  CategoryName
 * @package   PackageName
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.1.0
 * @link      http://pear.doyouphp.jp/package/PackageName
 * @see       References to other sections (if any)...
 */
ini_set("include_path", dirname(__FILE__)."/src/" . PATH_SEPARATOR . ini_get("include_path"));

/**
 * Description for require_once
 */
require_once "Command/PharCLI.php";

// Test
$obj = new Command_PharCLI();



?>
