<?php

/**
 * Short description for file
 *
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.1.0
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */

/**
 * Description for require_once
 */
require_once 'Command/PharCLI/Parser/Option.php';

/**
 * Description for require_once
 */
require_once 'Command/PharCLI/Command/Factory.php';

/**
 * Command_PharCLI class
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */
class Command_PharCLI {

    /**
     * Description for const
     */
    const VERSION = '0.0.3-20080313';

    /**
     * Short description for function
     *
     * @return void
     * @access public
     */
    public function run() {
        $option_parser = new Command_PharCLI_Parser_Option($this->getOption());
        $context = null;
        try {
            $context = $option_parser->parse();
            $command = Command_PharCLI_Command_Factory::getInstance($context);
            $command->execute($context);
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            exit(1);
        }
    }

    /**
     * Short description for function
     *
     * @return array   Return description (if any) ...
     * @access private
     */
    private function getOption() {
        $options = array(
                array('long' => 'extract',
                      'short' => 'x',
                      'type' => 'noarg',
                      'desc' => array('extract archive')),
                array('long' => 'list',
                      'short' => 't',
                      'type' => 'noarg',
                      'desc' => array('list file in archive')),
                array('long' => 'create',
                      'short' => 'c',
                      'type' => 'noarg',
                      'desc' => array('create archive')),
                array('long' => 'file',
                      'short' => 'f',
                      'type' => 'mandatory',
                      'desc' => array('filename', 'archive file')),
                array('long' => 'directory',
                      'short' => 'C',
                      'type' => 'mandatory',
                      'desc' => array('directory', 'change directory')),
                array('long' => 'stub',
                      'type' => 'mandatory',
                      'desc' => array('filename', 'phar stub')),
                array('long' => 'verbose',
                      'short' => 'v',
                      'type' => 'noarg',
                      'desc' => array('verbose mode')),
                array('long' => 'version',
                      'type' => 'noarg',
                      'desc' => array('show version')),
            );
        if (Phar::canCompress(Phar::GZ)) {
            $options[] = array('long' => 'gzip',
                               'short' => 'z',
                               'type' => 'noarg',
                               'desc' => array('use gzip filter'));
        }
        if (Phar::canCompress(Phar::BZ2)) {
            $options[] = array('long' => 'bzip',
                               'short' => 'j',
                               'type' => 'noarg',
                               'desc' => array('use bzip2 filter'));
        }
        $config = array(
            'options' => $options,
            'parameters' => array('[file] ...', 'Some files' . PHP_EOL),
            );

        return $config;
    }
}
