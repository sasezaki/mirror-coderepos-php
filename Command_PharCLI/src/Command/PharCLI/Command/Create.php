<?php

/**
 * Short description for file
 *
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.1.0
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */

/**
 * Description for require_once
 */
require_once 'Command/PharCLI/Command.php';

/**
 * Description for require_once
 */
require_once 'Command/PharCLI/Parser/Directory.php';

/**
 * Short description for class
 *
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */
class Command_PharCLI_Command_Create implements Command {

    /**
     * Short description for function
     *
     * @param  object $context Parameter description (if any) ...
     * @return void
     * @access public
     */
    public function execute(Command_PharCLI_PharContext $context) {
        $remove_leading_slash = false;

        $phar = $context->getPhar();
        $phar->startBuffering();
        foreach ($this->_getFiles($context->getArgs()) as $file) {
            $filename = $file;
            if ($file[0] === DIRECTORY_SEPARATOR) {
                $filename = substr($filename, 1);
                if (!$remove_leading_slash) {
                    echo 'phar: Removing leading `/\' from member names' . PHP_EOL;
                    $remove_leading_slash = true;
                }
            }
            if ($context->isVerbose()) {
                echo $filename . PHP_EOL;
            }
            $phar[$filename] = file_get_contents($file);
        }

        if ($context->hasStub()) {
            $phar->setStub(file_get_contents($context->getStub()) . '__HALT_COMPILER();?>');
        }

        if ($context->hasCompressionGZ()) {
            $phar->compressAllFilesGZ();
        } else if ($context->hasCompressionBZIP2()) {
            $phar->compressAllFilesBZIP2();
        }
        $phar->stopBuffering();
    }

    /**
     * Short description for function
     *
     * @param  array   $args Parameter description (if any) ...
     * @return array   Return description (if any) ...
     * @access private
     */
    private function _getFiles(array $args) {
        $files = array();
        foreach ($args as $arg) {
            $files = array_merge($files, Command_PharCLI_Parser_Directory::getFileList($arg));
        }
        return $files;
    }
}