<?php

/**
 * Short description for file
 *
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.1.0
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */

/**
 * Description for require_once
 */
require_once 'Command/PharCLI/Command.php';

/**
 * Short description for class
 *
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */
class Command_PharCLI_Command_List implements Command {

    /**
     * Short description for function
     *
     * @param  mixed  $context Parameter description (if any) ...
     * @return void
     * @access public
     */
    public function execute(Command_PharCLI_PharContext $context) {
        $phar = $context->getPhar();
        foreach (new RecursiveIteratorIterator($phar) as $file) {
            echo str_replace('phar://' . $context->getFile() . DIRECTORY_SEPARATOR, '', $file->getPathname()) . PHP_EOL;
        }
    }
}