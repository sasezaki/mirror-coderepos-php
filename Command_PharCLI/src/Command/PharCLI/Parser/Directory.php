<?php

/**
 * Short description for file
 *
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.1.0
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */

/**
 * Short description for class
 *
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */
class Command_PharCLI_Parser_Directory
{

    /**
     * Short description for function
     *
     * @param  unknown $path Parameter description (if any) ...
     * @return unknown Return description (if any) ...
     * @access private
     * @static
     */
    private static function normalizeLastSeparator($path) {
        if (substr($path, strlen($path) - 1) === DIRECTORY_SEPARATOR) {
            $path = substr($path, 0, strlen($path) - 1);
        }
        return $path;
    }

    /**
     * Short description for function
     *
     * @param  unknown   $path Parameter description (if any) ...
     * @return unknown   Return description (if any) ...
     * @access private
     * @throws Exception Exception description (if any) ...
     * @static
     */
    private static function getBaseDirectory($path) {
        if (!is_dir($path)) {
            if ($path === dirname($path)) {
                throw new Exception('invalid dir');
            }
            return Command_PharCLI_Parser_Directory::getBaseDirectory(dirname($path));
        }
        return $path;
    }

    /**
     * Short description for function
     *
     * @param  unknown $path Parameter description (if any) ...
     * @return mixed   Return description (if any) ...
     * @access public
     * @static
     */
    public static function getFileList($path) {
        if (!is_dir($path) &&
            substr($path, strlen($path) - 1) !== DIRECTORY_SEPARATOR) {
            return glob($path, GLOB_BRACE);
        }

        $file_pattern = Command_PharCLI_Parser_Directory::normalizeLastSeparator($path)
                      . DIRECTORY_SEPARATOR . '*';
        $dir_base = Command_PharCLI_Parser_Directory::normalizeLastSeparator(
                        Command_PharCLI_Parser_Directory::getBaseDirectory($path));
        $iter =  new RecursiveIteratorIterator(
                     new RecursiveDirectoryIterator($dir_base),
                     RecursiveIteratorIterator::SELF_FIRST);

        $files = array();
        try {
            foreach ($iter as $item) {
                if ($item->isFile() &&
                    fnmatch($file_pattern, $item->getPathname())) {
//                    $files[] = substr($item->getPathname(), strlen($dir_base) + 1);
                    $files[] = $item->getPathname();
                }
            }
        } catch (UnexpectedValueException $e) {
            echo $e->getMessage() . PHP_EOL;
        }
        return $files;
    }
}
//var_dump(Command_PharCLI_Parser_Directory::getFileList('/usr/local/lib/php5/ini/'));
//var_dump(Command_PharCLI_Parser_Directory::getFileList('/usr/{bin,local/bin}/cs*'));
