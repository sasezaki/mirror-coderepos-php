<?php

/**
 * Short description for file
 *
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.1.0
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */

/**
 * Description for require_once
 */
require_once 'Console/GetoptPlus.php';

/**
 * Description for require_once
 */
require_once 'Command/PharCLI/PharContext.php';

/**
 * Short description for class
 *
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */
class Command_PharCLI_Parser_Option {

    /**
     * Description for private
     * @var    unknown
     * @access private
     */
    private $config;

    /**
     * Short description for function
     *
     * @param  unknown $config Parameter description (if any) ...
     * @return void
     * @access public
     */
    public function __construct(array $config) {
        $this->config = $config;
    }

    /**
     * Short description for function
     *
     * @return object Return description (if any) ...
     * @access public
     */
    public function parse() {
        $options = Console_Getoptplus::getoptplus($this->config, 'short2long', true);
        return $this->_validateContext(new Command_PharCLI_PharContext($options));
    }

    /**
     * Short description for function
     *
     * @param  object                   $context Parameter description (if any) ...
     * @return object                   Return description (if any) ...
     * @access private
     * @throws InvalidArgumentException Exception description (if any) ...
     * @throws RuntimeException         Exception description (if any) ...
     * @throws RuntimeException         Exception description (if any) ...
     */
    private function _validateContext(Command_PharCLI_PharContext $context) {
        if (!$context->hasCommand()) {
            throw new InvalidArgumentException('command option required. see phar --help');
        }
        if ($context->hasCompressionGZ() && !Phar::canCompress(Phar::GZ)) {
            throw new RuntimeException('phar not supported gzip compression');
        }
        if ($context->hasCompressionBZIP2() && !Phar::canCompress(Phar::BZ2)) {
            throw new RuntimeException('phar not supported bzip2 compression');
        }
        return $context;
    }
}