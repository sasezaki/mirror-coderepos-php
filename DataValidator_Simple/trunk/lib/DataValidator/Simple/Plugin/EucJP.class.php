<?PHP

require_once 'DataValidator/Simple/Plugin.class.php';

class DataValidator_Simple_Plugin_EucJP extends DataValidator_Simple_Plugin {
    
    function HIRAGANA (&$valid,&$name,&$data,&$args) {
        return preg_match('/^(?:\xA4[\x00-\xFF]|\xA1\xBC)+$/',$data) ? true : false;
    }
    
    function KATAKANA (&$valid,&$name,&$data,&$args) {
        return preg_match('/^(?:\x8E[\xA1-\xDF]|\xA5[\x00-\xFF]|\xA1\xBC)+$/',$data) ? true : false;
    }
    
    function HKATAKANA (&$valid,&$name,&$data,&$args) {
        return preg_match('/^(?:\x8E[\xA1-\xDF])+$/',$data) ? true : false;
    }
    
    function ZKATAKANA (&$valid,&$name,&$data,&$args) {
        return preg_match('/^(?:\xA5[\x00-\xFF]|\xA1\xBC)+$/',$data) ? true : false;
    }
    
    function factory_JLENGTH (&$valid,&$name,&$data,&$args) {
        if( isset($args[1]) ){
            list($n,$m) = $args;
            $len = mb_strlen($data,'EUC-JP');
            return ($n <= $len && $len <= $m) ? true : false;
        }else{
            list($n) = $args;
            return mb_strlen($data,'EUC-JP') == $n ? true : false;
        }
    }
    
}
