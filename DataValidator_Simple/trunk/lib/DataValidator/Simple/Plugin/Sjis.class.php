<?PHP

require_once 'DataValidator/Simple/Plugin.class.php';

class DataValidator_Simple_Plugin_Sjis extends DataValidator_Simple_Plugin {
    
    function HIRAGANA (&$valid,&$name,&$data,&$args) {
        return preg_match('/^(?:\x82[\x9F-\xF1]|\x81\x5B)+$/',$data) ? true : false;
    }
    
    function KATAKANA (&$valid,&$name,&$data,&$args) {
        return preg_match('/^(?:[\xA6-\xDF]|\x83[\x40-\x96]|\x81\x5B)+$/',$data) ? true : false;
    }
    
    function HKATAKANA (&$valid,&$name,&$data,&$args) {
        return preg_match('/^(?:[\xA6-\xDF])+$/',$data) ? true : false;
    }
    
    function ZKATAKANA (&$valid,&$name,&$data,&$args) {
        return preg_match('/^(?:\x83[\x40-\x96]|\x81\x5B)+$/',$data) ? true : false;
    }
    
    function factory_JLENGTH (&$valid,&$name,&$data,&$args) {
        if( isset($args[1]) ){
            list($n,$m) = $args;
            $len = mb_strlen($data,'SJIS');
            return ($n <= $len && $len <= $m) ? true : false;
        }else{
            list($n) = $args;
            return mb_strlen($data,'SJIS') == $n ? true : false;
        }
    }
    
}
