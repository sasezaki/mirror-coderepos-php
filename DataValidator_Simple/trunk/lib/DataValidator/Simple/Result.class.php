<?PHP
/**
* DataValidator/Simple/Result.class.php
* @author ittetsu miyazaki<ittetsu.miyazaki _at_ gmail.com>
* @package DataValidator_Simple
*/

/** 
* DataValidator_Simple_Result
* 結果クラス
* 
* 履歴
* 2007/07/27 ver 0.01 完成
* 2008/03/27 ver 0.02 文字コードをUTF8に変更
* 
* @version 0.02
* @package DataValidator_Simple
* @access public
*/
class DataValidator_Simple_Result {
    
    var $_valid;
    var $_result;
    
    var $has_error;
    
    /**
    * コンストラクタ
    * DataValidator_Simpleのvalidate()から生成される
    */
    function DataValidator_Simple_Result (&$valid,&$result) {
        $this->_valid  =& $valid;
        $this->_result =& $result;
        $this->has_error();
    }
    
    /**
    * 指定されたチェックキー名にエラーがあった場合に真を、エラーが無かった場合に偽を返します。
    * 引数にチェック処理の項目名を渡すことでどのチェック処理でエラーがあったかを調べることも出来ます。
    * <pre>
    *  $valid->add_check_table(array(
    *      'hoge',array('NOT_NULL',array('LENGTH',1,10))
    *  ));
    *          :
    *          :
    *  $result =& $valid->validate();
    *  if( $reuslt->is_error('hoge') ){
    *      // hogeにエラーが発生
    *      if( $result->is_error('hoge','NOT_NULL') ){
    *          // 空文字チェックでエラーになった場合
    *      }elsif( $result->is_error('hoge','LENGTH') ){
    *          // 文字制限チェックでエラーになった場合
    *      }
    *  }
    * </pre>
    * @access public
    * @param string チェックキー名
    * @param string チェック処理項目名(省略可)
    * @return boolean 真偽値
    */
    function is_error ($key,$type=null) {
        if( !isset($this->_result[$key]) ) return false;
        
        if( is_null($type) ) {
            return $this->_result[$key]['error'];
        }
        
        if( !preg_match('/:/',$type ) )
            $type = $this->_valid->default_plugin_prefix().'::'.$type;
        
        if( !isset($this->_result[$key]['errors'][$type]) )
            trigger_error("unknown check name '$type'",E_USER_ERROR);
        
        return $this->_result[$key]['errors'][$type];
    }
    
    /**
    * チェックする値が空文字（もしくは未定義値）だったら真を返す。
    * @access public
    * @return boolean 真偽値
    */
    function is_blank ($key) {
        if( !isset($this->_result[$key]) ) return false;
        return $this->_result[$key]['blank'];
    }
    
    /**
    * 全ての値のチェック結果を返します。
    * 一つでもエラーがあった場合は真を返し、全てのチェック処理が通った場合は偽を返します。
    * また、メンバー変数has_errorにも結果が格納されているので下記のように書いても問題ありません。
    * <pre>
    *  $result =& $valid->validate();
    *  if( $result->has_error() ) { ... }
    *  if( $result->has_error   ) { ... } // 同じ意味
    * </pre>
    * @access public
    */
    function has_error () {
        if( !is_null($this->has_error) ) return $this->has_error;
        
        $this->has_error = false;
        foreach( $this->_result as $val ) {
            if( $val['error'] === true ) {
                $this->has_error = true;
                break;
            }
        }
        
        return $this->has_error;
    }
    
    /**
    * チェックする値を返します。
    * <pre>
    *  $valid->add_check_table(array( 'hoge',array('NUM') ));
    *  $valid->add_data(array( 'hoge' => 123 ));
    *  $result =& $valid->validate();
    *  print $result->value('hoge'); // 123
    * </pre>
    * add_check_tableするときにリスト形式指定していた場合はvalueの戻り値はリストになります。
    * <pre>
    *  $valid->add_check_table(array(
    *      array('date',array('year','month','day') ),array('DATE')
    *  ));
    *  $valid->add_data(array(
    *      'year'  => 2005,
    *      'month' => 12,
    *      'day'   => 22,
    *  ));
    *  $result =& $valid->validte();
    *  list($year,$month,$day) = $result->value('date'); // リストで取得できる
    * </pre>
    * @access public
    * @param  string チェックキー名
    * @return string/array チェックする値
    */
    function value ($key) {
        if( !isset($this->_result[$key]) ) return null;
        return is_array($this->_result[$key]['data'])
            ? array_values($this->_result[$key]['data'])
            : $this->_result[$key]['data'];
    }
    
    /**
    * 名前つきでチェックする値を返します。
    * 戻り値はハッシュです。
    * <pre>
    *  $valid->add_check_table(array(
    *      array('date',array('year','month','day') ),array('DATE')
    *  ));
    *  $valid->add_data(array(
    *      'year'  => 2005,
    *      'month' => 12,
    *      'day'   => 22,
    *  ));
    *  $result =& $valid->validte();
    *  $date = $result->nvalue('date');
    *  // $date = array(
    *  //     'year'  => 2005,
    *  //     'month' => 12,
    *  //     'day'   => 22,
    *  // );
    * </pre>
    * @access public
    * @param string チェックキー名
    * @return array チェックする値
    */
    function nvalue ($key) {
        if( !isset($this->_result[$key]) ) return null;
        return is_array($this->_result[$key]['data'])
            ? $this->_result[$key]['data']
            : array( $key => $this->_result[$key]['data'] );
    }
    
}
