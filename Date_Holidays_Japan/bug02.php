<?php
/**
 * @see http://labs.uechoco.com/blog/2008/05/phppeardate_holidays_driversetyear%e3%81%8c%e5%a4%89.html
 */
ini_set("include_path", dirname(__FILE__)."/src/" . PATH_SEPARATOR . ini_get("include_path"));


require_once "Date/Holidays.php";
define('LANG_FILE', '/usr/local/lib/php5/pear/data/Date_Holidays_Japan/lang');
$obj =& Date_Holidays::factory('Japan');
if (Date_Holidays::isError($obj)) {
    die('Factory was unable to produce driver-object');
}
$obj->addTranslationFile(LANG_FILE . '/Japan/ja_JP.xml', 'ja_JP');
if (Date_Holidays::isError($obj)) {
    die($obj->getMessage());
}

$obj->setYear(intval(2008));
$arr1 = $obj->_internalNames;
$dates1 = $obj->_dates;
$holidays1 = $obj->_holidays;
$titles1 = $obj->_titles;

$obj->setYear(intval(2008));
$arr2 = $obj->_internalNames;
$dates2 = $obj->_dates;
$holidays2 = $obj->_holidays;
$titles2 = $obj->_titles;
assert($arr1 === $arr2);
assert($dates1 === $dates2);
var_dump($dates1);
var_dump($dates2);
assert($holidays1 === $holidays2);
assert($titles1 === $titles2);
