<?php
/**
 * MissileLauncher.php
 *
 * @package Device
 */

/**
 * Device_USB_MissileLauncher
 *
 * @package Device
 */
class Device_USB_MissileLauncher
{
    var $bus;

    var $timeout = 1000;

    function Device_USB_MissileLauncher()
    {
        usb_init();
        $this->bus = usb_find_device(hexdec(1941), hexdec(8021));
    }

    function up($interval = 0.5)
    {
        $this->command('up', $interval);
    }

    function down($interval = 0.5)
    {
        $this->command('down', $interval);
    }

    function left($interval = 0.5)
    {
        $this->command('left', $interval);
    }

    function right($interval = 0.5)
    {
        $this->command('right', $interval);
    }

    function fire($interval = 0.5)
    {
        $this->command('fire', $interval);
    }

    function command($action, $interval = 1)
    {
        $action_list = array(
            'up' => 0x01,
            'down' => 0x02,
            'left' => 0x04,
            'right' => 0x08,
            'fire' => 0x10
        );

        $handle = usb_open($this->bus);

        $msg = array_fill(0, 8, 0x0);
        $msg[0] = $action_list[$action];
        $msg = implode('', array_map('chr', $msg));

        usb_detach_kernel_driver_np($handle, 0);
        usb_detach_kernel_driver_np($handle, 1);
        usb_set_configuration($handle, 1);
        usb_claim_interface($handle, 0);
        usb_control_msg($handle, 0x21, 0x9, 0x2, 0x00, $msg, 8, $timeout);

        sleep($interval);

        $msg = array_fill(0, 8, 0x0);
        $msg = implode('', array_map('chr', $msg));
        usb_control_msg($handle, 0x21, 0x9, 0x2, 0x00, $msg, 8, $timeout);
        usb_release_interface($handle, 0);
        usb_close($handle);
    }
}

/**
 * example
$ml = new Device_USB_MissileLauncher();
$ml->left(3);
//$ml->fire();
 */
?>
