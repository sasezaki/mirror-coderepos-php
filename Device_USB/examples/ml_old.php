<?php
/**
 * MissileLauncher
 *
 */

/**
 * Device_USB
 *
 * @package Device
 */
class Device_USB
{
    /**
     * Device_USB
     *
     * @access public
     */
    function Device_USB()
    {
        //if !extensionloaded; fail;
        if(!extension_loaded('usb')) {
            exit('usb.so not loaded');
        }

        usb_init();
    }

    function getBusses()
    {
        return usb_get_busses();
    }

    function findBusses()
    {
        return usb_find_busses();
    }

    function findDevices()
    {
        return usb_find_devices();
    }

    /**
     * findDevice
     *
     */
    function findDevicePHP($vendor_id, $product_id)
    {
        $this->findBusses();
        $this->findDevices();
        $bus = $this->getBusses();

        foreach ($bus as $row) {
            foreach ($row['devices'] as $dev) {
                if ($dev['descriptor']['idProduct'] || $dev['descriptor']['idVendor']) {
                    var_dump($dev['descriptor']);
                }
            }
        }
    }

    function findDevice($vendor_id, $product_id)
    {
        return usb_find_device($vendor_id, $product_id);
    }
}

class Device_USB_MissileLauncher
{
    var $bus;

    var $timeout = 1000;

    function Device_USB_MissileLauncher()
    {
        usb_init();
        $this->bus = usb_find_device(hexdec(1941), hexdec(8021));
    }

    function up()
    {
        $this->command('up');
    }

    function down()
    {
        $this->command('down');
    }

    function left()
    {
        $this->command('left');
    }

    function right()
    {
        $this->command('right');
    }

    function fire()
    {
        $this->command('fire');
    }

    function command($action, $interval = 1000)
    {
        $action_list = array(
            'up' => 0x01,
            'down' => 0x02,
            'left' => 0x04,
            'right' => 0x08,
            'fire' => 0x10
        );

        $handle = usb_open($this->bus);

        $msg = array_fill(0, 8, 0x0);
        $msg[0] = $action_list[$action];
        $msg = implode('', array_map('chr', $msg));

        usb_detach_kernel_driver_np($handle, 0);
        usb_detach_kernel_driver_np($handle, 1);
        usb_set_configuration($handle, 1);
        usb_claim_interface($handle, 0);
        usb_control_msg($handle, 0x21, 0x9, 0x2, 0x00, $msg, 8, $timeout);
        sleep(1);

        $msg = array_fill(0, 8, 0x0);
        $msg = implode('', array_map('chr', $msg));
        usb_control_msg($handle, 0x21, 0x9, 0x2, 0x00, $msg, 8, $timeout);
        usb_release_interface($handle, 0);
        usb_close($handle);
    }
}

$ml = new Device_USB_MissileLauncher();
$ml->left();
//$ml->fire();

/*
$USB = new Device_USB();

$bus = $USB->findBusses();
var_dump($bus);

$bus = $USB->findDevices();
var_dump($bus);

$bus = $USB->getBusses();
var_dump($bus);
 */

//$bus = $USB->findDevicePHP(-1, 44444444444441);

//$bus = $USB->findDevice(5, 6);
//var_dump($bus);

/*
$bus = $USB->findDevice(hexdec(1941), hexdec(8021));

$timeout = 1000;

$msg = array_fill(0, 8, 0x0);
$msg[0] = 0x01;
$msg[0] = 0x02;
$msg[0] = 0x04;
$msg[0] = 0x08;
$msg[0] = 0x10;
$msg = array_map('chr', $msg);

$inita = implode('', $msg);

$handle = usb_open($bus);

usb_detach_kernel_driver_np($handle, 0);
usb_detach_kernel_driver_np($handle, 1);
usb_set_configuration($handle, 1);
usb_claim_interface($handle, 0);
usb_control_msg($handle, 0x21, 0x9, 0x2, 0x00, $inita, 8, $timeout);
usb_release_interface($handle, 0);
 */

//$bus = $USB->findDevice(2131, 256);
//var_dump($bus);

//$bus = $USB->findDevice(1112, 7);
//var_dump($bus);

/*
$perl = new Perl();
$perl->eval('use Device::USB::MissileLaunchers;');

$mls = $perl->eval('Device::USB::MissileLaunchers->new(devices => 2);');
$mls->left;# left trun by the device 1
$mls->up;# rises by the device 2
$mls->fire;# device 0 to fire
 */
?>
