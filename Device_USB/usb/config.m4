dnl
dnl $ Id: $
dnl

PHP_ARG_ENABLE(usb, whether to enable usb functions,
[  --enable-usb         Enable usb support])

if test "$PHP_USB" != "no"; then

PHP_ARG_WITH(libusb, whether libusb is available,[  --with-libusb[=DIR]  With libusb support])


  PHP_ADD_INCLUDE($PHP_LIBUSB_DIR/include)

  export OLD_CPPFLAGS="$CPPFLAGS"
  export CPPFLAGS="$CPPFLAGS $INCLUDES -DHAVE_LIBUSB"
  AC_CHECK_HEADER([usb.h], [], AC_MSG_ERROR('usb.h' header not found))
  PHP_SUBST(USB_SHARED_LIBADD)

  PHP_ADD_LIBRARY_WITH_PATH(usb, $PHP_LIBUSB_DIR/lib, USB_SHARED_LIBADD)
  export CPPFLAGS="$OLD_CPPFLAGS"

  export OLD_CPPFLAGS="$CPPFLAGS"
  export CPPFLAGS="$CPPFLAGS $INCLUDES -DHAVE_USB"
  AC_CHECK_TYPE(struct usb_bus *, [], [AC_MSG_ERROR(required payload type for resource usb_bus not found)], [#include "$srcdir/php_usb.h"])
  AC_CHECK_TYPE(struct usb_device *, [], [AC_MSG_ERROR(required payload type for resource usb_device not found)], [#include "$srcdir/php_usb.h"])
  AC_CHECK_TYPE(struct usb_dev_handle *, [], [AC_MSG_ERROR(required payload type for resource usb_dev_handle not found)], [#include "$srcdir/php_usb.h"])
  export CPPFLAGS="$OLD_CPPFLAGS"


  PHP_SUBST(USB_SHARED_LIBADD)
  AC_DEFINE(HAVE_USB, 1, [ ])
  PHP_NEW_EXTENSION(usb, usb.c , $ext_shared)

fi

