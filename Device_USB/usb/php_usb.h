/*
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.0 of the PHP license,       |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_0.txt.                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Authors: TSURUOKA Naoya <tsuruoka at php.net>                        |
   +----------------------------------------------------------------------+
*/

/* $ Id: $ */ 

#ifndef PHP_USB_H
#define PHP_USB_H

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <php.h>

#ifdef HAVE_USB

#include <php_ini.h>
#include <SAPI.h>
#include <ext/standard/info.h>
#include <Zend/zend_extensions.h>
#ifdef  __cplusplus
} // extern "C" 
#endif
#include <usb.h>
#ifdef  __cplusplus
extern "C" {
#endif

extern zend_module_entry usb_module_entry;
#define phpext_usb_ptr &usb_module_entry

#ifdef PHP_WIN32
#define PHP_USB_API __declspec(dllexport)
#else
#define PHP_USB_API
#endif

PHP_MINIT_FUNCTION(usb);
PHP_MSHUTDOWN_FUNCTION(usb);
PHP_RINIT_FUNCTION(usb);
PHP_RSHUTDOWN_FUNCTION(usb);
PHP_MINFO_FUNCTION(usb);

#ifdef ZTS
#include "TSRM.h"
#endif

#define FREE_RESOURCE(resource) zend_list_delete(Z_LVAL_P(resource))

#define PROP_GET_LONG(name)    Z_LVAL_P(zend_read_property(_this_ce, _this_zval, #name, strlen(#name), 1 TSRMLS_CC))
#define PROP_SET_LONG(name, l) zend_update_property_long(_this_ce, _this_zval, #name, strlen(#name), l TSRMLS_CC)

#define PROP_GET_DOUBLE(name)    Z_DVAL_P(zend_read_property(_this_ce, _this_zval, #name, strlen(#name), 1 TSRMLS_CC))
#define PROP_SET_DOUBLE(name, d) zend_update_property_double(_this_ce, _this_zval, #name, strlen(#name), d TSRMLS_CC)

#define PROP_GET_STRING(name)    Z_STRVAL_P(zend_read_property(_this_ce, _this_zval, #name, strlen(#name), 1 TSRMLS_CC))
#define PROP_GET_STRLEN(name)    Z_STRLEN_P(zend_read_property(_this_ce, _this_zval, #name, strlen(#name), 1 TSRMLS_CC))
#define PROP_SET_STRING(name, s) zend_update_property_string(_this_ce, _this_zval, #name, strlen(#name), s TSRMLS_CC)
#define PROP_SET_STRINGL(name, s, l) zend_update_property_string(_this_ce, _this_zval, #name, strlen(#name), s, l TSRMLS_CC)


PHP_FUNCTION(usb_init);
PHP_FUNCTION(usb_find_busses);
PHP_FUNCTION(usb_find_devices);
PHP_FUNCTION(usb_get_busses);
PHP_FUNCTION(usb_open);
PHP_FUNCTION(usb_close);
PHP_FUNCTION(usb_set_configuration);
PHP_FUNCTION(usb_claim_interface);
PHP_FUNCTION(usb_release_interface);
PHP_FUNCTION(usb_control_msg);
PHP_FUNCTION(usb_detach_kernel_driver_np);
PHP_FUNCTION(usb_find_device);
#ifdef  __cplusplus
} // extern "C" 
#endif

#endif /* PHP_HAVE_USB */

#endif /* PHP_USB_H */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
