/*
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.0 of the PHP license,       |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_0.txt.                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Authors: TSURUOKA Naoya <tsuruoka at php.net>                        |
   +----------------------------------------------------------------------+
*/

/* $ Id: $ */ 

#include "php_usb.h"

#if HAVE_USB

/* {{{ Resource destructors */
int le_usb_bus;
void usb_bus_dtor(zend_rsrc_list_entry *rsrc TSRMLS_DC)
{
	struct usb_bus * resource = (struct usb_bus *)(rsrc->ptr);

	do {
	} while (0);
}

int le_usb_device;
void usb_device_dtor(zend_rsrc_list_entry *rsrc TSRMLS_DC)
{
	struct usb_device * resource = (struct usb_device *)(rsrc->ptr);

	do {
	} while (0);
}

int le_usb_dev_handle;
void usb_dev_handle_dtor(zend_rsrc_list_entry *rsrc TSRMLS_DC)
{
	struct usb_dev_handle * resource = (struct usb_dev_handle *)(rsrc->ptr);

	do {
	} while (0);
}

/* }}} *

/* {{{ usb_functions[] */
function_entry usb_functions[] = {
	PHP_FE(usb_init            , NULL)
	PHP_FE(usb_find_busses     , NULL)
	PHP_FE(usb_find_devices    , NULL)
	PHP_FE(usb_get_busses      , NULL)
	PHP_FE(usb_open            , NULL)
	PHP_FE(usb_close           , NULL)
	PHP_FE(usb_set_configuration, NULL)
	PHP_FE(usb_claim_interface , NULL)
	PHP_FE(usb_release_interface, NULL)
	PHP_FE(usb_control_msg     , NULL)
	PHP_FE(usb_detach_kernel_driver_np, NULL)
	PHP_FE(usb_find_device     , NULL)
	{ NULL, NULL, NULL }
};
/* }}} */


/* {{{ usb_module_entry
 */
zend_module_entry usb_module_entry = {
	STANDARD_MODULE_HEADER,
	"usb",
	usb_functions,
	PHP_MINIT(usb),     /* Replace with NULL if there is nothing to do at php startup   */ 
	PHP_MSHUTDOWN(usb), /* Replace with NULL if there is nothing to do at php shutdown  */
	PHP_RINIT(usb),     /* Replace with NULL if there is nothing to do at request start */
	PHP_RSHUTDOWN(usb), /* Replace with NULL if there is nothing to do at request end   */
	PHP_MINFO(usb),
	"0.0.1", 
	STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_USB
ZEND_GET_MODULE(usb)
#endif


/* {{{ PHP_MINIT_FUNCTION */
PHP_MINIT_FUNCTION(usb)
{
	le_usb_bus = zend_register_list_destructors_ex(usb_bus_dtor, 
						   NULL, "usb_bus", module_number);
	le_usb_device = zend_register_list_destructors_ex(usb_device_dtor, 
						   NULL, "usb_device", module_number);
	le_usb_dev_handle = zend_register_list_destructors_ex(usb_dev_handle_dtor, 
						   NULL, "usb_dev_handle", module_number);

	/* add your stuff here */

	return SUCCESS;
}
/* }}} */


/* {{{ PHP_MSHUTDOWN_FUNCTION */
PHP_MSHUTDOWN_FUNCTION(usb)
{

	/* add your stuff here */

	return SUCCESS;
}
/* }}} */


/* {{{ PHP_RINIT_FUNCTION */
PHP_RINIT_FUNCTION(usb)
{
	/* add your stuff here */

	return SUCCESS;
}
/* }}} */


/* {{{ PHP_RSHUTDOWN_FUNCTION */
PHP_RSHUTDOWN_FUNCTION(usb)
{
	/* add your stuff here */

	return SUCCESS;
}
/* }}} */


/* {{{ PHP_MINFO_FUNCTION */
PHP_MINFO_FUNCTION(usb)
{
	php_info_print_box_start(0);
	php_printf("<p>usb control extension</p>\n");
	php_printf("<p>Version 0.0.1alpha (2007-04-14)</p>\n");
	php_printf("<p><b>Authors:</b></p>\n");
	php_printf("<p>TSURUOKA Naoya &lt;tsuruoka at php.net&gt; (lead)</p>\n");
	php_info_print_box_end();
	/* add your stuff here */

}
/* }}} */


/* {{{ proto void usb_init(void)
   */
PHP_FUNCTION(usb_init)
{
	if (ZEND_NUM_ARGS()>0)  {
		WRONG_PARAM_COUNT;
	}

	do {
		usb_init();
	} while (0);
}
/* }}} usb_init */


/* {{{ proto int usb_find_busses(void)
   */
PHP_FUNCTION(usb_find_busses)
{
	if (ZEND_NUM_ARGS()>0)  {
		WRONG_PARAM_COUNT;
	}

	do {
		RETURN_LONG(usb_find_busses());
	} while (0);
}
/* }}} usb_find_busses */


/* {{{ proto int usb_find_devices(void)
   */
PHP_FUNCTION(usb_find_devices)
{
	if (ZEND_NUM_ARGS()>0)  {
		WRONG_PARAM_COUNT;
	}

	do {
		RETURN_LONG(usb_find_devices());
	} while (0);
}
/* }}} usb_find_devices */


/* {{{ proto resource usb_get_busses(void)
   */
PHP_FUNCTION(usb_get_busses)
{
	void * return_res;
	long return_res_id = -1;
	if (ZEND_NUM_ARGS()>0)  {
		WRONG_PARAM_COUNT;
	}

	do {
		/**
		 * get_usb_busses
		 * +build_bus
		 * +-list_devices
		 * +--build_device
		 * +---build_descriptor
		 * +----convert_bcd
		 * +---list_configurations
		 * +----build_configuration
		 * +-----list_interfaces
		 * +------build_inteface
		 * +-------list_endpoints
		 * +--------build_endpoint
		 */
		
		//{{{ convert_bcd
		/**
		 * convert_bcd
		 *
		 */
		char* convert_bcd(long value) {
		  int major = (value >> 8) & 0xff;
		  int minor = (value >> 4) & 0xf;
		  int subminor = value & 0xf;

		  //should not be able to exceed 6.
		  char buffer[10] = "";

		  sprintf(buffer, "%d.%d%d", major, minor, subminor);

		  return buffer;
		}
		//}}}
		
		//{{{ build_descriptor
		/**
		 * build_descriptor
		 *
		 */
		zval* build_descriptor(struct usb_device *dev) {
		  zval *array;
		  MAKE_STD_ZVAL(array);
		  array_init(array);

		  add_assoc_long(array, "bDescriptorType", dev->descriptor.bDescriptorType);
		  add_assoc_long(array, "bDeviceClass", dev->descriptor.bDeviceClass);
		  add_assoc_long(array, "bDeviceSubClass", dev->descriptor.bDeviceSubClass);
		  add_assoc_long(array, "bDeviceProtocol", dev->descriptor.bDeviceProtocol);
		  add_assoc_long(array, "bMaxPacketSize0", dev->descriptor.bMaxPacketSize0);
		  add_assoc_long(array, "idVendor", dev->descriptor.idVendor);
		  add_assoc_long(array, "idProduct", dev->descriptor.idProduct);
		  add_assoc_long(array, "iManufacturer", dev->descriptor.iManufacturer);
		  add_assoc_long(array, "iProduct", dev->descriptor.iProduct);
		  add_assoc_long(array, "iSerialNumber", dev->descriptor.iSerialNumber);
		  add_assoc_long(array, "bNumConfigurations", dev->descriptor.bNumConfigurations);

		  add_assoc_string(array, "bcdDevice", convert_bcd(dev->descriptor.bcdDevice), 1);
		  add_assoc_string(array, "bcdUSB", convert_bcd(dev->descriptor.bcdUSB), 1);

		  return array;
		}
		//}}}

		//{{{ build_endpoint
		zval* build_endpoint(struct usb_endpoint_descriptor* endpt) {
		  zval *array;
		  MAKE_STD_ZVAL(array);
		  array_init(array);
		  
		  add_assoc_long(array, "bDescriptorType", endpt->bDescriptorType);
		  add_assoc_long(array, "bEndpointAddress", endpt->bEndpointAddress);
		  add_assoc_long(array, "wMaxPacketSize", endpt->wMaxPacketSize);
		  add_assoc_long(array, "bInterval", endpt->bInterval);
		  add_assoc_long(array, "bRefresh", endpt->bRefresh);
		  add_assoc_long(array, "bSynchAddress", endpt->bSynchAddress);

		  //return sv_bless( newRV_noinc( (SV*)hash ),
		  //  gv_stashpv( "Device::USB::DevEndpoint", 1 )
		  //);

		  return array;
		}
		//}}}

		//{{{ list_endpoints
		zval* list_endpoints(struct usb_endpoint_descriptor *endpt, unsigned int count) {
		  zval *array;
		  MAKE_STD_ZVAL(array);
		  array_init(array);
		  unsigned int i = 0;

		  for (i = 0; i < count; ++i) {
			add_next_index_zval(array, build_endpoint(endpt + i));
		  }

		  return array;
		}
		//}}}

		//{{{ build_interface
		/**
		 * build_interface
		 *
		 */
		zval* build_interface(struct usb_interface_descriptor *inter) {
		  zval *array;
		  MAKE_STD_ZVAL(array);
		  array_init(array);

		  add_assoc_long(array, "bDescriptorType", inter->bDescriptorType);
		  add_assoc_long(array, "bInterfaceNumber", inter->bInterfaceNumber);
		  add_assoc_long(array, "bAlternateSetting", inter->bAlternateSetting);
		  add_assoc_long(array, "bNumEndPoints", inter->bNumEndpoints);
		  add_assoc_long(array, "bInterfaceClass", inter->bInterfaceClass);
		  add_assoc_long(array, "bInterfaceSubClass", inter->bInterfaceSubClass);
		  add_assoc_long(array, "bInterfaceProtocol", inter->bInterfaceProtocol);
		  add_assoc_long(array, "iInterface", inter->iInterface);
		  add_assoc_zval(array, "endpoints",
			list_endpoints(inter->endpoint, inter->bNumEndpoints)
		  );

		  /* TODO: handle the 'extra' data */
		  
		  //return sv_bless( newRV_noinc( (SV*)hash ),
		  //  gv_stashpv( "Device::USB::DevInterface", 1 )
		  //);

		  return array;
		}
		//}}}
		
		//{{{ list_interfaces
		/**
		 * list_interfaces
		 */
		zval* list_interfaces(struct usb_interface *ints, unsigned count) {
		  zval *array;
		  MAKE_STD_ZVAL(array);
		  array_init(array);
		  unsigned int i = 0;

		  for (i = 0; i < count; ++i) {
			zval *inters;
			MAKE_STD_ZVAL(inters);
			array_init(inters);
			unsigned j = 0;

			for (j = 0; j < ints[i].num_altsetting; ++j) {
			  add_next_index_zval(inters, build_interface(ints[i].altsetting + j));
			}

			add_next_index_zval(array, inters);
		  }

		  return array;
		}
		//}}}

		//{{{ build_configuration
		/**
		 * build_configuration
		 */
		zval* build_configuration(struct usb_config_descriptor *cfg) {
		  zval *array;
		  MAKE_STD_ZVAL(array);
		  array_init(array);

		  add_assoc_long(array, "bDescriptorType", cfg->bDescriptorType);
		  add_assoc_long(array, "wTotalLength", cfg->wTotalLength);
		  add_assoc_long(array, "bNumInterfaces", cfg->bNumInterfaces);
		  add_assoc_long(array, "bConfigurationValue", cfg->bConfigurationValue);
		  add_assoc_long(array, "iConfiguration", cfg->iConfiguration);
		  add_assoc_long(array, "bmAttributes", cfg->bmAttributes);
		  add_assoc_long(array, "MaxPower", cfg->MaxPower * 2);
		  add_assoc_zval(array, "interfaces", list_interfaces(cfg->interface, cfg->bNumInterfaces));

		  return array;
		}
		//}}}

		//{{{ list_configurations
		zval* list_configurations(struct usb_config_descriptor *cfg, unsigned int count) {
		  zval *array;
		  MAKE_STD_ZVAL(array);
		  array_init(array);
		  unsigned int i = 0;

		  for (i = 0; i < count; ++i) {
			add_next_index_zval(array, build_configuration(cfg + i));
		  }

		  return array;
		
		}
		//}}}

		//{{{ build_device
		zval* build_device(struct usb_device* dev) {
		  zval *array;
		  MAKE_STD_ZVAL(array);
		  array_init(array);
 
		  add_assoc_string(array, "filename", dev->filename, 1);
		  add_assoc_zval(array, "descriptor", build_descriptor(dev));
		  add_assoc_long(array, "device", (unsigned long)dev);
		  add_assoc_zval(array, "config",
			list_configurations(dev->config, dev->descriptor.bNumConfigurations)
		  );
		  
		  //return sv_bless( newRV_noinc( (SV*)hash ),
		  //  gv_stashpv( "Device::USB::Device", 1 )
		  //);

		  return array;
		}

		zval* list_devices(struct usb_device* dev) {
		  zval *array;
		  MAKE_STD_ZVAL(array);
		  array_init(array);
		  
		  for(; 0 != dev; dev= dev->next) {
			add_next_index_zval(array, build_device(dev));
		  }

		  return array;
		}
		//}}}

		//{{{ build_bus
		zval* build_bus(struct usb_bus* bus) {
		  zval *array_bus;
		  MAKE_STD_ZVAL(array_bus);
		  array_init(array_bus);

		  add_assoc_string(array_bus, "dirname", bus->dirname, 1);
		  add_assoc_long(array_bus, "location", bus->location);
		  add_assoc_zval(array_bus, "devices", list_devices(bus->devices));

		  return array_bus;
		}
		//}}}

		//variables
		array_init(return_value);
		struct usb_bus* bus = 0;
		int return_value_index = 0;

		for(bus = usb_busses; 0 != bus; bus = bus->next) {
			add_index_zval(return_value, return_value_index, build_bus(bus));
			return_value_index++;
		}
	} while (0);
}
/* }}} usb_get_busses */


/* {{{ proto resource usb_dev_handle usb_open(resource usb_device dev)
   */
PHP_FUNCTION(usb_open)
{
	struct usb_dev_handle * return_res;
	long return_res_id = -1;
	zval * dev_res = NULL;
	int dev_resid = -1;
	struct usb_device * dev;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "r", &dev_res) == FAILURE) { 
		return;
	}
	ZEND_FETCH_RESOURCE(dev, struct usb_device *, &dev_res, dev_resid, "usb_device", le_usb_device);


	do {
		return_res = usb_open(dev);
		if (!return_res) {
		 RETURN_FALSE;
		}
	} while (0);
	return_res_id = ZEND_REGISTER_RESOURCE(return_value, return_res, le_usb_dev_handle);
}
/* }}} usb_open */


/* {{{ proto int usb_close(resource usb_dev_handle dev)
   */
PHP_FUNCTION(usb_close)
{
	zval * dev_res = NULL;
	int dev_resid = -1;
	struct usb_dev_handle * dev;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "r", &dev_res) == FAILURE) { 
		return;
	}
	ZEND_FETCH_RESOURCE(dev, struct usb_dev_handle *, &dev_res, dev_resid, "usb_dev_handle", le_usb_dev_handle);


	do {
		RETURN_LONG(usb_close(dev));
	} while (0);
}
/* }}} usb_close */


/* {{{ proto int usb_set_configuration(resource usb_dev_handle handle, int configuration)
   */
PHP_FUNCTION(usb_set_configuration)
{
	zval * handle_res = NULL;
	int handle_resid = -1;
	struct usb_dev_handle * handle;
	long configuration = 0;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "rl", &handle_res, &configuration) == FAILURE) { 
		return;
	}
	ZEND_FETCH_RESOURCE(handle, struct usb_dev_handle *, &handle_res, handle_resid, "usb_dev_handle", le_usb_dev_handle);


	do {
		RETURN_LONG(usb_set_configuration(handle, configuration));
	} while (0);
}
/* }}} usb_set_configuration */


/* {{{ proto int usb_claim_interface(resource usb_dev_handle handle, int interface)
   */
PHP_FUNCTION(usb_claim_interface)
{
	zval * handle_res = NULL;
	int handle_resid = -1;
	struct usb_dev_handle * handle;
	long interface = 0;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "rl", &handle_res, &interface) == FAILURE) { 
		return;
	}
	ZEND_FETCH_RESOURCE(handle, struct usb_dev_handle *, &handle_res, handle_resid, "usb_dev_handle", le_usb_dev_handle);


	do {
		RETURN_LONG(usb_claim_interface(handle, interface));
	} while (0);
}
/* }}} usb_claim_interface */


/* {{{ proto int usb_release_interface(resource usb_dev_handle handle, int interface)
   */
PHP_FUNCTION(usb_release_interface)
{
	zval * handle_res = NULL;
	int handle_resid = -1;
	struct usb_dev_handle * handle;
	long interface = 0;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "rl", &handle_res, &interface) == FAILURE) { 
		return;
	}
	ZEND_FETCH_RESOURCE(handle, struct usb_dev_handle *, &handle_res, handle_resid, "usb_dev_handle", le_usb_dev_handle);


	do {
		RETURN_LONG(usb_release_interface(handle, interface));
	} while (0);
}
/* }}} usb_release_interface */


/* {{{ proto int usb_control_msg(
	resource usb_dev_handle dev,
	int requesttype,
	int request,
	int value,
	int index,
	string bytes,
	int size,
	int timeout
  )
   */
PHP_FUNCTION(usb_control_msg)
{
	zval * dev_res = NULL;
	int dev_resid = -1;
	struct usb_dev_handle * dev;
	long requesttype = 0;
	long request = 0;
	long value = 0;
	long index = 0;
	const char * bytes = NULL;
	int bytes_len = 0;
	long size = 0;
	long timeout = 0;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "rllllsll", &dev_res, &requesttype, &request, &value, &index, &bytes, &bytes_len, &size, &timeout) == FAILURE) { 
		return;
	}
	ZEND_FETCH_RESOURCE(dev, struct usb_dev_handle *, &dev_res, dev_resid, "usb_dev_handle", le_usb_dev_handle);


	do {
		RETURN_LONG(usb_control_msg(
		  dev,
		  requesttype,
		  request,
		  value,
		  index,
		  bytes,
		  size,
		  timeout
		));
	} while (0);
}
/* }}} usb_control_msg */


/* {{{ proto int usb_detach_kernel_driver_np(resource usb_dev_handle handle, int interface)
   */
PHP_FUNCTION(usb_detach_kernel_driver_np)
{
	zval * handle_res = NULL;
	int handle_resid = -1;
	struct usb_dev_handle * handle;
	long interface = 0;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "rl", &handle_res, &interface) == FAILURE) { 
		return;
	}
	ZEND_FETCH_RESOURCE(handle, struct usb_dev_handle *, &handle_res, handle_resid, "usb_dev_handle", le_usb_dev_handle);


	do {
		RETURN_LONG(usb_detach_kernel_driver_np(handle, interface));
	} while (0);
}
/* }}} usb_detach_kernel_driver_np */


/* {{{ proto resource usb_device usb_find_device(int vendor, int product)
   */
PHP_FUNCTION(usb_find_device)
{
	struct usb_device * return_res;
	long return_res_id = -1;
	long vendor = 0;
	long product = 0;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &vendor, &product) == FAILURE) { 
		return;
	}
	do {
		struct usb_bus *bus = 0;
		int flag = 0;

		usb_find_busses();
		usb_find_devices();
		
		for (bus = usb_busses; 0 != bus; bus = bus->next) {
		  struct usb_device *dev = 0;
		  for(dev = bus->devices; 0 != dev; dev = dev->next) {
			if((dev->descriptor.idVendor == vendor) &&
			  (dev->descriptor.idProduct == product)) {
			  flag = 1;
			  return_res = dev;
			  break;
			}
		  }
		}

		if (flag == 0) {
			RETURN_FALSE;
		}
	} while (0);
	return_res_id = ZEND_REGISTER_RESOURCE(return_value, return_res, le_usb_device);
}
/* }}} usb_find_device */

#endif /* HAVE_USB */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
