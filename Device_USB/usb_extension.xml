<?xml version="1.0" ?>
<extension name="usb" version="1.0.0">

<summary>usb control extension</summary>
<description>libusb binding</description>

<maintainers>
  <maintainer>
    <user>tsuruoka</user>
    <name>TSURUOKA Naoya</name>
    <email>tsuruoka at php.net</email>
    <role>lead</role>
  </maintainer>
</maintainers>

<release>
  <version>0.0.1</version>
  <date>2007-04-14</date>
  <state>alpha</state>
  <notes>
          first release
  </notes>
</release>

<changelog />

<license>PHP</license>

<deps language="c" platform="all">
  <with name="libusb" defaults="/lib:/usr:/usr/local">
   <header name="usb.h" />
   <lib name="usb" platform="unix" />
   <lib name="libusb0" platform="win32" />
  </with>
</deps>

<resources>

  <resource name="usb_bus" payload="struct usb_bus" alloc="no">
    <description>usb_bus resource</description>
  </resource>

  <resource name="usb_device" payload="struct usb_device" alloc="no">
    <description>usb_device resource</description>
  </resource>

  <resource name="usb_dev_handle" payload="struct usb_dev_handle" alloc="no">
    <description>usb_dev_handle resource</description>
    <!--
    <destruct>
    <![CDATA[
      usb_close(resource);
    ]]>
    </destruct>
    -->
  </resource>

</resources>

<globals />

<!-- Core -->

<function name="usb_init">
  <proto>void usb_init(void)</proto>
  <code>
  <![CDATA[
    usb_init();
  ]]>
  </code>
</function>

<function name="usb_find_busses">
  <proto>int usb_find_busses(void)</proto>
  <code>
  <![CDATA[
    RETURN_LONG(usb_find_busses());
  ]]>
  </code>
</function>

<function name="usb_find_devices">
  <proto>int usb_find_devices(void)</proto>
  <code>
  <![CDATA[
    RETURN_LONG(usb_find_devices());
  ]]>
  </code>
</function>

<function name="usb_get_busses">
 <proto>resource usb_get_busses(void)</proto>
  <code>
  <![CDATA[
    /**
     * get_usb_busses
     * +build_bus
     * +-list_devices
     * +--build_device
     * +---build_descriptor
     * +----convert_bcd
     * +---list_configurations
     * +----build_configuration
     * +-----list_interfaces
     * +------build_inteface
     * +-------list_endpoints
     * +--------build_endpoint
     */
    
    //{{{ convert_bcd
    /**
     * convert_bcd
     *
     */
    char* convert_bcd(long value) {
      int major = (value >> 8) & 0xff;
      int minor = (value >> 4) & 0xf;
      int subminor = value & 0xf;

      //should not be able to exceed 6.
      char buffer[10] = "";

      sprintf(buffer, "%d.%d%d", major, minor, subminor);

      return buffer;
    }
    //}}}
    
    //{{{ build_descriptor
    /**
     * build_descriptor
     *
     */
    zval* build_descriptor(struct usb_device *dev) {
      zval *array;
      MAKE_STD_ZVAL(array);
      array_init(array);

      add_assoc_long(array, "bDescriptorType", dev->descriptor.bDescriptorType);
      add_assoc_long(array, "bDeviceClass", dev->descriptor.bDeviceClass);
      add_assoc_long(array, "bDeviceSubClass", dev->descriptor.bDeviceSubClass);
      add_assoc_long(array, "bDeviceProtocol", dev->descriptor.bDeviceProtocol);
      add_assoc_long(array, "bMaxPacketSize0", dev->descriptor.bMaxPacketSize0);
      add_assoc_long(array, "idVendor", dev->descriptor.idVendor);
      add_assoc_long(array, "idProduct", dev->descriptor.idProduct);
      add_assoc_long(array, "iManufacturer", dev->descriptor.iManufacturer);
      add_assoc_long(array, "iProduct", dev->descriptor.iProduct);
      add_assoc_long(array, "iSerialNumber", dev->descriptor.iSerialNumber);
      add_assoc_long(array, "bNumConfigurations", dev->descriptor.bNumConfigurations);

      add_assoc_string(array, "bcdDevice", convert_bcd(dev->descriptor.bcdDevice), 1);
      add_assoc_string(array, "bcdUSB", convert_bcd(dev->descriptor.bcdUSB), 1);

      return array;
    }
    //}}}

    //{{{ build_endpoint
    zval* build_endpoint(struct usb_endpoint_descriptor* endpt) {
      zval *array;
      MAKE_STD_ZVAL(array);
      array_init(array);
      
      add_assoc_long(array, "bDescriptorType", endpt->bDescriptorType);
      add_assoc_long(array, "bEndpointAddress", endpt->bEndpointAddress);
      add_assoc_long(array, "wMaxPacketSize", endpt->wMaxPacketSize);
      add_assoc_long(array, "bInterval", endpt->bInterval);
      add_assoc_long(array, "bRefresh", endpt->bRefresh);
      add_assoc_long(array, "bSynchAddress", endpt->bSynchAddress);

      //return sv_bless( newRV_noinc( (SV*)hash ),
      //  gv_stashpv( "Device::USB::DevEndpoint", 1 )
      //);

      return array;
    }
    //}}}

    //{{{ list_endpoints
    zval* list_endpoints(struct usb_endpoint_descriptor *endpt, unsigned int count) {
      zval *array;
      MAKE_STD_ZVAL(array);
      array_init(array);
      unsigned int i = 0;

      for (i = 0; i < count; ++i) {
        add_next_index_zval(array, build_endpoint(endpt + i));
      }

      return array;
    }
    //}}}

    //{{{ build_interface
    /**
     * build_interface
     *
     */
    zval* build_interface(struct usb_interface_descriptor *inter) {
      zval *array;
      MAKE_STD_ZVAL(array);
      array_init(array);

      add_assoc_long(array, "bDescriptorType", inter->bDescriptorType);
      add_assoc_long(array, "bInterfaceNumber", inter->bInterfaceNumber);
      add_assoc_long(array, "bAlternateSetting", inter->bAlternateSetting);
      add_assoc_long(array, "bNumEndPoints", inter->bNumEndpoints);
      add_assoc_long(array, "bInterfaceClass", inter->bInterfaceClass);
      add_assoc_long(array, "bInterfaceSubClass", inter->bInterfaceSubClass);
      add_assoc_long(array, "bInterfaceProtocol", inter->bInterfaceProtocol);
      add_assoc_long(array, "iInterface", inter->iInterface);
      add_assoc_zval(array, "endpoints",
        list_endpoints(inter->endpoint, inter->bNumEndpoints)
      );

      /* TODO: handle the 'extra' data */
      
      //return sv_bless( newRV_noinc( (SV*)hash ),
      //  gv_stashpv( "Device::USB::DevInterface", 1 )
      //);

      return array;
    }
    //}}}
    
    //{{{ list_interfaces
    /**
     * list_interfaces
     */
    zval* list_interfaces(struct usb_interface *ints, unsigned count) {
      zval *array;
      MAKE_STD_ZVAL(array);
      array_init(array);
      unsigned int i = 0;

      for (i = 0; i < count; ++i) {
        zval *inters;
        MAKE_STD_ZVAL(inters);
        array_init(inters);
        unsigned j = 0;

        for (j = 0; j < ints[i].num_altsetting; ++j) {
          add_next_index_zval(inters, build_interface(ints[i].altsetting + j));
        }

        add_next_index_zval(array, inters);
      }

      return array;
    }
    //}}}

    //{{{ build_configuration
    /**
     * build_configuration
     */
    zval* build_configuration(struct usb_config_descriptor *cfg) {
      zval *array;
      MAKE_STD_ZVAL(array);
      array_init(array);

      add_assoc_long(array, "bDescriptorType", cfg->bDescriptorType);
      add_assoc_long(array, "wTotalLength", cfg->wTotalLength);
      add_assoc_long(array, "bNumInterfaces", cfg->bNumInterfaces);
      add_assoc_long(array, "bConfigurationValue", cfg->bConfigurationValue);
      add_assoc_long(array, "iConfiguration", cfg->iConfiguration);
      add_assoc_long(array, "bmAttributes", cfg->bmAttributes);
      add_assoc_long(array, "MaxPower", cfg->MaxPower * 2);
      add_assoc_zval(array, "interfaces", list_interfaces(cfg->interface, cfg->bNumInterfaces));

      return array;
    }
    //}}}

    //{{{ list_configurations
    zval* list_configurations(struct usb_config_descriptor *cfg, unsigned int count) {
      zval *array;
      MAKE_STD_ZVAL(array);
      array_init(array);
      unsigned int i = 0;

      for (i = 0; i < count; ++i) {
        add_next_index_zval(array, build_configuration(cfg + i));
      }

      return array;
    
    }
    //}}}

    //{{{ build_device
    zval* build_device(struct usb_device* dev) {
      zval *array;
      MAKE_STD_ZVAL(array);
      array_init(array);
 
      add_assoc_string(array, "filename", dev->filename, 1);
      add_assoc_zval(array, "descriptor", build_descriptor(dev));
      add_assoc_long(array, "device", (unsigned long)dev);
      add_assoc_zval(array, "config",
        list_configurations(dev->config, dev->descriptor.bNumConfigurations)
      );
      
      //return sv_bless( newRV_noinc( (SV*)hash ),
      //  gv_stashpv( "Device::USB::Device", 1 )
      //);

      return array;
    }

    zval* list_devices(struct usb_device* dev) {
      zval *array;
      MAKE_STD_ZVAL(array);
      array_init(array);
      
      for(; 0 != dev; dev= dev->next) {
        add_next_index_zval(array, build_device(dev));
      }

      return array;
    }
    //}}}

    //{{{ build_bus
    zval* build_bus(struct usb_bus* bus) {
      zval *array_bus;
      MAKE_STD_ZVAL(array_bus);
      array_init(array_bus);

      add_assoc_string(array_bus, "dirname", bus->dirname, 1);
      add_assoc_long(array_bus, "location", bus->location);
      add_assoc_zval(array_bus, "devices", list_devices(bus->devices));

      return array_bus;
    }
    //}}}

    //variables
    array_init(return_value);
    struct usb_bus* bus = 0;
    int return_value_index = 0;

    for(bus = usb_busses; 0 != bus; bus = bus->next) {
        add_index_zval(return_value, return_value_index, build_bus(bus));
        return_value_index++;
    }
  ]]>
  </code>
</function>

<!-- Device Operators -->

<function name="usb_open">
  <!-- usb_dev_handle *usb_open(struct *usb_device dev); -->
  <proto>resource usb_dev_handle usb_open(resource usb_device dev)</proto>
  <code>
  <![CDATA[
    return_res = usb_open(dev);
    if (!return_res) {
     RETURN_FALSE;
    }
   ]]>
  </code>
</function>

<function name="usb_close">
  <!-- int usb_close(usb_dev_handle *dev); -->
  <proto>int usb_close(resource usb_dev_handle dev)</proto>
  <code>
  <![CDATA[
    RETURN_LONG(usb_close(dev));
  ]]>
  </code>
</function>

<function name="usb_set_configuration">
  <!-- int usb_set_configuration(usb_dev_handle * dev, int configuration); -->
  <proto>int usb_set_configuration(resource usb_dev_handle handle, int configuration)</proto>
  <code>
  <![CDATA[
    RETURN_LONG(usb_set_configuration(handle, configuration));
  ]]>
  </code>
</function>

<function name="usb_claim_interface">
  <!-- int usb_claim_interface(usb_dev_handle * dev, int interface); -->
  <proto>int usb_claim_interface(resource usb_dev_handle handle, int interface)</proto>
  <code>
  <![CDATA[
    RETURN_LONG(usb_claim_interface(handle, interface));
  ]]>
  </code>
</function>

<function name="usb_release_interface">
  <!-- int usb_release_interface(usb_dev_handle * dev, int interface); -->
  <proto>int usb_release_interface(resource usb_dev_handle handle, int interface)</proto>
  <code>
  <![CDATA[
    RETURN_LONG(usb_release_interface(handle, interface));
  ]]>
  </code>
</function>

<!-- Control Transfars -->

<function name="usb_control_msg">
  <!-- int usb_control_msg(usb_dev_handle *dev, int requesttype, int request, int value, int index, char *bytes, int size, int timeout); -->
  <proto>
  int usb_control_msg(
    resource usb_dev_handle dev,
    int requesttype,
    int request,
    int value,
    int index,
    string bytes,
    int size,
    int timeout
  )
  </proto>
  <code>
  <![CDATA[
    RETURN_LONG(usb_control_msg(
      dev,
      requesttype,
      request,
      value,
      index,
      bytes,
      size,
      timeout
    ));
  ]]>
  </code>
</function>

<!-- Non Portable -->

<function name="usb_detach_kernel_driver_np">
  <!-- int usb_detach_kernel_driver_np(usb_dev_handle * handle, int interface) -->
  <proto>int usb_detach_kernel_driver_np(resource usb_dev_handle handle, int interface)</proto>
  <code>
  <![CDATA[
    RETURN_LONG(usb_detach_kernel_driver_np(handle, interface));
  ]]>
  </code>
</function>

<function name="usb_find_device">
  <!-- SV* lib_find_usb_device(int vendor, int product) -->
  <proto>resource usb_device usb_find_device(int vendor, int product)</proto>
  <code>
  <![CDATA[
    struct usb_bus *bus = 0;
    int flag = 0;

    usb_find_busses();
    usb_find_devices();
    
    for (bus = usb_busses; 0 != bus; bus = bus->next) {
      struct usb_device *dev = 0;
      for(dev = bus->devices; 0 != dev; dev = dev->next) {
        if((dev->descriptor.idVendor == vendor) &&
          (dev->descriptor.idProduct == product)) {
          flag = 1;
          return_res = dev;
          break;
        }
      }
    }

    if (flag == 0) {
        RETURN_FALSE;
    }
  ]]>
  </code>
</function>

</extension>
