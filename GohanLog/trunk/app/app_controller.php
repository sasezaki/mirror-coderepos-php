<?php
class AppController extends Controller
{
    var $you = array();
    var $components = array('Session', 'Mail');
    var $uses = array('User');

    function __construct()
    {
        parent::__construct();
        vendor('vendor_init');  // vendor ディレクトリをインクルードパスに設定
    }
    function beforeFilter(){
        if ($this->params['controller'] != 'thumbs'){
            $openid_identity = $this->Session->read('openid.identity');
            $nickname = $this->User->get_nickname($openid_identity);

            $this->set('nickname', $nickname);
        }
        parent::beforeFilter();        
    }
}
?>