<?php
class MailComponent extends Object
{
    var $is_mail = false;
    var $from = '';
    var $to = '';
    var $subject = '';
    var $body = '';
    var $attaches = array();
    var $from_encoding = 'auto';
    var $to_encoding = 'UTF-8';

    function startup(&$controller)
    {
        $stdin = fopen('php://stdin', 'rb');
        $raw_mail = '';
        do {
            $data = fread($stdin, 8192);
            if (strlen($data) == 0) {
                break;
            }
            $raw_mail .= $data;
        } while(true);
        fclose($stdin);

        if (!empty($raw_mail)) {
            $this->is_mail = true;

            $option = array(
                'include_bodies' => true,
                'decode_bodies' => true,
                'decode_headers' => true,
                'input' => $raw_mail,
            );
            vendor('Mail/mimeDecode');
            $mail =& Mail_mimeDecode::decode($option);

            $this->to = $this->_getAddress($mail->headers['to']);
            $this->from = $this->_getAddress($mail->headers['from']);
            if (!$this->to || !$this->from) {
                $this->is_mail = false;
            }

            $this->subject = $this->_convertText($mail->headers['subject']);
            $this->body = $this->_parseBodyData($mail);
        }
    }

    function getToAddress()
    {
        return $this->to;
    }

    function getFromAddress()
    {
        return $this->from;
    }

    function getSubject()
    {
        return $this->subject;
    }

    function getBody()
    {
        return $this->body;
    }

    function getImages()
    {
        return $this->attaches;
    }

    function isMail()
    {
        return $this->is_mail;
    }

    function _getAddress($str)
    {
        $str = str_replace('"', '', $str);
        $matches = array();
        $regexp = '/[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+/';
        if (preg_match_all($regexp, $str, $matches)) {
            return array_pop($matches[0]);
        } else {
            return false;
        }
    }

    function _parseBodyData(&$mail)
    {
        $body = '';
        $types = array('jpeg', 'gif', 'png');

        if (isset($mail->parts) && is_array($mail->parts)) {
            foreach ($mail->parts as $part) {
                if ($_body = $this->_parseBodyData($part)) {
                    $body = $_body;
                }
            }
        } elseif (strtolower($mail->ctype_primary) === 'text' && strtolower($mail->ctype_secondary) === 'plain') {
            $body = $mail->body;
            $charset = $mail->ctype_parameters['charset'];

            $body = $this->_convertText($body, $charset);
        } elseif (strtolower($mail->ctype_primary) === 'image'
            && in_array(strtolower($mail->ctype_secondary), $types)) {
            $image_data = $mail->body;

            switch (strtolower($mail->ctype_secondary)) {
            case 'jpeg' :
            case 'jpg' :
                $image_ext = '.jpg';
                break;
            case 'png' :
                $image_ext = '.png';
                break;
            case 'gif' :
                $image_ext = '.gif';
                break;
            }

            // 画像かどうかチェック
            if (!@imagecreatefromstring($image_data)) {
                // base64_decodeしてリトライ
                $image_data = base64_decode($image_data);
                if (!@imagecreatefromstring($image_data)) {
                    return null;
                }
            }

            $this->_addImage(time() . $image_ext, $image_data);
            return null;
        }

        return $body;
    }

    function _addImage($filename, $bin)
    {
        $this->attaches[] = array('filename' => $filename, 'bin' => $bin);
    }

    function _convertText($str, $from_encoding = '', $to_encoding = '')
    {
        if (!$from_encoding) {
            $from_encoding = $this->from_encoding;
        }

        if (!$to_encoding) {
            $to_encoding = $this->to_encoding;
        }

        $str = mb_convert_encoding($str, $to_encoding, $from_encoding);
        $str = str_replace("\0", '', $str);
        $str = rtrim($str);

        return $str;
    }
}
?>
