<?php

class GohansController extends AppController
{
    var $name = 'Gohans';
	var $components = array('Pager','Openid');
	var $helpers = array('Html','Form');
    var $uses = array('Tag','Gohan','Master_tag');
    
    function index($page = 1)
    {

    }
    function gohan()
    {
    }

    function search()
    {
//        print "<PRE>";
//        print_r($this);
//        print "</PRE>";
        $search_mode = $this->passedArgs[0];
        $search_key1 = $this->passedArgs[1];
        switch($search_mode){
            case 'normal':
                $currentPage = $search_key1;
                if(!$currentPage){
                    $currentPage = 1;
                }
                {
                    $conditions = '';
                    $fields = array('COUNT(DISTINCT DATE_FORMAT(date_time_original, "%Y-%m-%d")) date_time_originalYMD_count');
                    $order = 'date_time_original DESC';
                    $result = $this->Gohan->find($conditions, $fields, $order);
                    $date_time_originalYMD_count = $result[0]['date_time_originalYMD_count'];
                }
                {
                    // setup the pager 
                    $params = array(
                        'fileName'              => '/gohans/search/normal/%d',
                        'mode'                  => 'sliding',
                        'perPage'               => 8,
                        'totalItems'            => $date_time_originalYMD_count, 
                        'currentPage'           => $currentPage, 
                        'delta'                 => 7,
                        'separator'             => '|',
                        'prevImg'               => '&#171;　Previous',
                        'nextImg'               => 'Next　&#187;',
                        'firstPagePre'          => '&#171;&#171;',
                        'firstPagePost'         => '',
                        'lastPagePre'           => '&#187;&#187;',
                        'lastPagePost'          => '',
                        'spacesBeforeSeparator' => 1,
                        'spacesAfterSeparator'  => 1,
                        'altFirst'              => 'Go to page 1',
                        'altPrev'               => 'Go to Previous Page',
                        'altNext'               => 'Go to Next Page',
                        'altLast'               => 'Go to Last Page',
                        'altPage'               => 'Go to page',
                    ); 
                    $this->Pager->init($params);        
                }
                {
                    $conditions = '';
                    $fields = array('DISTINCT DATE_FORMAT(date_time_original, "%Y-%m-%d") date_time_originalYMD');
                    $order = 'date_time_original DESC';
                    $day_list = $this->Gohan->findAll($conditions, $fields, $order, $this->Pager->params['perPage'], $this->Pager->params['currentPage']);
                }
                
                {
                    $gohans = array();
                    foreach($day_list as $value){
                        $date_time_originalYMD = $value[0]['date_time_originalYMD'];
                        $conditions = 'DATE_FORMAT(date_time_original, "%Y-%m-%d") = '."'$date_time_originalYMD'";
                        $fields = '';
                        $order = 'date_time_original ASC';
                        
                        $gohans[$date_time_originalYMD] = $this->Gohan->findAll($conditions, $fields, $order);
                    }
                }
                foreach ($gohans as $key => $value){
                    foreach($value as $key2 => $value2){
                        $id = $value2['Gohan']['id'];
                        $conditions = 'target_id = '."'$id'";
                        $fields = '';
                        $order = '';
                        $tags = $this->Tag->findAll($conditions, $fields, $order);
                        foreach($tags as $k3 => $v3){
                            $conditions = 'id = '."'".$v3['Tag']['master_tag_id']."'";
                            $fields = '';
                            $order = '';
                            $master_tags = $this->Master_tag->findAll($conditions, $fields, $order);
                            $tags[$k3]['Tag']['name'] = $master_tags[0]['Master_tag']['name'];
                        }
                        $gohans[$key][$key2]['Gohan']['tags'] = $tags;
                        $gohans[$key][$key2]['Gohan']['is_disp'] = true;
                    }
                }

                break;
            case 'day':
                $r_datetime = $search_key1;
                
                {
                    $conditions = 'DATE(date_time_original) = '."'".$r_datetime."'";
                    $fields = '';
                    $order = 'date_time_original ASC';
        //          $day_list = $this->Gohan->findAll($conditions, $fields, $order, $this->Pager->params['perPage'], $this->Pager->params['currentPage']);
                }
                $day = $this->Gohan->findAll($conditions, $fields, $order, null, null);
                
                foreach($day as $key2 => $value2){
                    $id = $value2['Gohan']['id'];
                    $conditions = 'target_id = '."'$id'";
                    $fields = '';
                    $order = '';
                    $tags = $this->Tag->findAll($conditions, $fields, $order);
                    foreach($tags as $k3 => $v3){
                        $conditions = 'id = '."'".$v3['Tag']['master_tag_id']."'";
                        $fields = '';
                        $order = '';
                        $master_tags = $this->Master_tag->findAll($conditions, $fields, $order);
                        $tags[$k3]['Tag']['name'] = $master_tags[0]['Master_tag']['name'];
                    }
                    $day[$key2]['Gohan']['tags'] = $tags;
                    $day[$key2]['Gohan']['is_disp'] = true;
                }
                $gohans[$r_datetime] = $day;
                $this->set('gohan_day', $r_datetime);
                                
                break;
            case 'tag':
                $q_urlencode = $search_key1;
                $q = urldecode($q_urlencode);
                {
                    $conditions = 'where name='."'".$q."'";
                    $fields = '';
                    $order = '';
                    $master_tags = $this->Master_tag->findAll($conditions, $fields, $order);
                    $master_tag_id = $master_tags[0]['Master_tag']['id'];
                }
                {
                    $conditions = 'where master_tag_id='."'".$master_tag_id."'";
                    $fields = '';
                    $order = '';
                    $tag_list = $this->Tag->findAll($conditions, $fields, $order, null, null);
                }
                $target_gohan_id_str = "";
                foreach($tag_list as $k => $v){
                    if($k==0){
                        $target_gohan_id_str = $target_gohan_id_str."id=".$v['Tag']['target_id'];
                    }else{
                        $target_gohan_id_str = $target_gohan_id_str." OR id=".$v['Tag']['target_id'];
                    }
                }
                $search_result_gohan_id_list = array();
                foreach($tag_list as $k => $v){
                    $search_result_gohan_id_list[$v['Tag']['target_id']] = 1;
                }
                
                {
                    $conditions = 'where '.$target_gohan_id_str;
                    $fields = array('DISTINCT DATE_FORMAT(date_time_original, "%Y-%m-%d") date_time_originalYMD');
                    $order = 'date_time_original DESC';
                    $day_list = $this->Gohan->findAll($conditions, $fields, $order);
                }
                {
                    $gohans = array();
                    foreach($day_list as $value){
                        $date_time_originalYMD = $value[0]['date_time_originalYMD'];
                        $conditions = 'DATE_FORMAT(date_time_original, "%Y-%m-%d") = '."'$date_time_originalYMD'";
                        $fields = '';
                        $order = 'date_time_original ASC';
                        
                        $gohans[$date_time_originalYMD] = $this->Gohan->findAll($conditions, $fields, $order);
                    }
                }
                foreach ($gohans as $key => $value){
                    foreach($value as $key2 => $value2){
                        $id = $value2['Gohan']['id'];
                        $conditions = 'target_id = '."'$id'";
                        $fields = '';
                        $order = '';
                        $tags = $this->Tag->findAll($conditions, $fields, $order);
                        foreach($tags as $k3 => $v3){
                            $conditions = 'id = '."'".$v3['Tag']['master_tag_id']."'";
                            $fields = '';
                            $order = '';
                            $master_tags = $this->Master_tag->findAll($conditions, $fields, $order);
                            $tags[$k3]['Tag']['name'] = $master_tags[0]['Master_tag']['name'];
                        }
                        $gohans[$key][$key2]['Gohan']['tags'] = $tags;
                        if(array_key_exists($id,$search_result_gohan_id_list)){
                            $gohans[$key][$key2]['Gohan']['is_disp'] = true;
                        }else{
                            $gohans[$key][$key2]['Gohan']['is_disp'] = false;
                        }
                    }
                }
                
                $this->set('q_urlencode', $q_urlencode);
                break;
        }
        
        $this->set('search_mode', $search_mode);
        $this->set('gohans', $gohans);
        $baseurl = 'http://' . env('HTTP_HOST');
        $this->set('baseurl', $baseurl);
    }
    
    
    private function setMessage($message) {
        $this->set('message', $message);
    }
    function mail_img_add()
    {
        if  (!$this->Mail->isMail()) {
    		exit;
        }
        
        $from = $this->Mail->getFromAddress();
        $title = $this->Mail->getBody();
        $images = $this->Mail->getImages();

        if (empty($images)) {
            exit;
        }
        $to_address = $this->Mail->getToAddress();
        $valid_mail_address = TO_MAIL_ADDRESS_TOKEN.'@'.MAIL_SERVER_DOMAIN;

        if($to_address !== $valid_mail_address){
        	exit;
        }
	    foreach ($images as $image) {
	        $path = $this->Gohan->save_img($image['bin']);
	        $this->Gohan->add_gohan($path);
	    }
    }
    
    
    
}

?>