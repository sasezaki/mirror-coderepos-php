<?php

class TagsController extends AppController
{
    var $name = 'Tags';
    
	var $helpers = array('Html','Form');
	var $uses = array('Tag','User','Gohan','Master_tag');
    function add()
    {
        $return_path = $this->params['form']['return_path'];
        if(!empty($this->data)) {
            $openid_identity = $this->Session->read('openid.identity');
            $user_id = $this->User->get_user_id($openid_identity);

            if($user_id){
                $this->log('openid_identity='.$openid_identity);
                $this->log('user_id='.$user_id);
                foreach($this->data as $key => $value){
                    $name = $value['name'];
                    $target_id = $value['target_id'];
                    $target_table = $value['target_table'];
                    
                    $this->add_tag($name,$user_id,$target_table,$target_id);
                }
                $this->Session->setFlash('更新');
                $this->redirect($return_path);
            }else{
                $this->Session->setFlash('書き込みするにはログインしてください。');
                $this->redirect($return_path);
            }
        }
    }
    function add_parent()
    {
//        print "<PRE>";
//        print_r($this);
//        print "</PRE>";
        
        print "parent_tag_name=".$this->params['form']['parent_tag_name'];
        
        
        $master_tag_id = $this->passedArgs[0];
        $target_table = 'tags';
        $conditions = 'id = '."'".$master_tag_id."'";
        $fields = '';
        $order = '';
        $master_tag = $this->Master_tag->findAll($conditions, $fields, $order);
        $this->set('master_tag',$master_tag);
    }
    
    private function add_tag($name,$user_id,$target_table,$target_id) {
        if(!$name)return false;
        
        $name_arr = explode(" ", $name);
        foreach($name_arr as $key => $value){
            $this->Master_tag->create();
            
            $this->Master_tag->cacheQueries = false;
            $conditions = 'name = '."'".$value."'";
            $fields = '';
            $order = '';
            $master_tag = $this->Master_tag->findAll($conditions, $fields, $order);
            $this->log('master_tag_count='.count($master_tag));
            $this->log('name='.$value);
            
            if($master_tag){
                $master_tag_id = $master_tag[0]['Master_tag']['id'];
                $data = array(
                        'Tag' => array(
                            'name' => $value,
                            'user_id' => $user_id,
                            'target_table' => $target_table,
                            'target_id' => $target_id,
                            'master_tag_id' => $master_tag_id
                        )
                );
                $this->Tag->create();
                $res = $this->Tag->save($data);
            }else{
                $data = array(
                        'Master_tag' => array(
                            'name' => $value,
                        )
                );
                $this->Master_tag->create();
                $res = $this->Master_tag->save($data);
                
                $master_tag_id = $this->Master_tag->getLastInsertID();

                $data = array(
                        'Tag' => array(
                            'name' => $value,
                            'user_id' => $user_id,
                            'target_table' => $target_table,
                            'target_id' => $target_id,
                            'master_tag_id' => $master_tag_id
                        )
                );
                $this->Tag->create();
                $res = $this->Tag->save($data);
            }
        }
        
        return true;
    }
    
    function index()
    {
        {
            $conditions = 'group by master_tag_id';
            $fields = 'id,master_tag_id,count(master_tag_id) master_tag_count';
            $order = 'order by master_tag_count desc';
            $tag_list = $this->Tag->findAll($conditions, $fields, $order, null, null);
        }
        foreach($tag_list as $k => $v){
            $conditions = 'id = '."'".$v['Tag']['master_tag_id']."'";
            $fields = '';
            $order = '';
            $master_tags = $this->Master_tag->findAll($conditions, $fields, $order);
            $tag_list[$k]['Tag']['name'] = $master_tags[0]['Master_tag']['name'];
        }
        
        $this->set('tag_list',$tag_list);
    }
            
    private function setMessage($message) {
        $this->set('message', $message);
    }
    
}

?>