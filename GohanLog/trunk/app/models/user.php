<?php

class User extends AppModel
{
    var $name = 'User';

    function add_user($openid_identity,$nickname){
        $conditions = 'openid_identity = '."'$openid_identity'";
        $fields = '';
        $order = '';
        $res = $this->find($conditions, $fields, $order);
        $this->log('res1='.print_r($res,true));
        $this->log('res2='.$res['User']['id']);
        $id = $res['User']['id'];
        
		$data = array(
                'User' => array(
                'id' => $id,
		        'openid_identity' => $openid_identity,
                'nickname' => $nickname,
                )
        );
//        $res = $this->create();
        $res = $this->save($data);
        
        return true;
    }
    function get_nickname($openid_identity){
        $conditions = 'openid_identity = '."'$openid_identity'";
        $fields = '';
        $order = '';
        $res = $this->find($conditions, $fields, $order);
        $nickname = $res['User']['nickname'];
        
        return $nickname;
    }
    function get_user_id($openid_identity){
        $conditions = 'openid_identity = '."'$openid_identity'";
        $fields = '';
        $order = '';
        $res = $this->find($conditions, $fields, $order);
        $this->log('get_user_id='.print_r($res,true));
        
        $id = $res['User']['id'];
                
        return $id;
    }
}

?>