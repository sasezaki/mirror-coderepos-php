<?php

error_reporting( E_ALL );

set_time_limit(0);

$files = glob('*.t.php');

$max_length = 0;
foreach ( $files as $file ) {
    if ( strlen($file) > $max_length ) {
        $max_length = strlen($file);
    }
}
$max_length += 5;

print count($files)." files test start\n\n";
ob_flush();

$err_global = 0;
foreach ( $files as $file ) {
    print $file;
    print str_repeat('.',$max_length-strlen($file));
    $output = `php $file`;
    $data = preg_split('/^((?:not )?ok \d+)/sm',$output,-1,PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY);
    $i = 0;
    $err = 0;
    foreach ( $data as $val ) {
        if ( preg_match('/^not ok \d+$/',$val) ) {  
            print "not ok". $data[$i+1];
            $err = 1;
            $err_global++;
        }
        $i++;
    }
    if (!$err) {
        print "ok\n";
    }
    ob_flush();
}

if ( $err_global ) {
    print "\n$err_global test error\n";
}
else {
    print "\ntest all ok";
}

