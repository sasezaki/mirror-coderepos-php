<?php
/**
 * HatenaSyntax
 *
 * PHP version 5.3
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author    anatoo <study.anatoo@gmail.com> 
 * @license   http://www.opensource.org/licenses/mit-license.php The MIT License
 * @version   0.0.6
 * @link      http://d.hatena.ne.jp/anatoo/
 */
namespace Guevara::Text::Parser::Hatena;
class Footnote
{
  protected $footnotes = array();
  protected $id;
  public function __construct($id = '')
  {
    $this->id = empty($id) ? '': '_' . $id;
  }
  public function parse($line)
  {
    while(true) {
      if(($result = $this->getFootnote($line)) === false) break;
      
      $this->footnotes[] = $result[1];
      $num = count($this->footnotes);
      $id = $this->id;
      $line = $result[0] 
            . '(<a href="#f' . $num . $id . '" name ="#b' . $num  . $id 
            .'" title="' . $result[1] . '">*' . $num . '</a>)' . $result[2] . PHP_EOL;
    }
    
    return $line;
  }
  public function getFootnote($line)
  {
    if(($_ = $this->seekFootnote($line)) === false) return false;
    
    $left = substr($line, 0, $_[0]);
    $contents = substr($line, $_[0] + 2, $_[1] - $_[0] - 2);
    $right = substr($line, $_[1] + 2);
    
    return array($left, $contents, $right);
  }
  public function seekFootnote($line)
  {
    $opening = strpos($line, '((');
    if($opening === false) return false;
    
    $closing = strpos($line, '))', $opening);
    if($closing === false) return false;
    
    return array($opening, $closing);
  }
  public function getResult()
  {
    $footnotes = $this->footnotes;
    $this->footnotes = array();
    $result = '<div class="footnote">' . PHP_EOL;
    $id = $this->id;
    
    foreach($footnotes as $num => $note)
      $result .= '<p><a href="#b' . ++$num  . $id . '" name="#f' . $num  . $id . '">*' . $num 
              . '</a>: ' . $note . '</p>' .  PHP_EOL;
    
    $result .= '</div>' . PHP_EOL;
    
    return $result;
  }
}