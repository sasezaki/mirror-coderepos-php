<?php
/**
 * HatenaSyntax
 *
 * PHP version 5.3
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author    anatoo <study.anatoo@gmail.com> 
 * @license   http://www.opensource.org/licenses/mit-license.php The MIT License
 * @version   0.0.6
 * @link      http://d.hatena.ne.jp/anatoo/
 */
namespace Guevara::Text::Parser::Hatena;
require_once 'Guevara/Text/Parser/Hatena/FirstCharSyntaxInterface.php';
use Guevara::Text::Parser::Hatena::FirstCharSyntaxInterface as FirstCharSyntaxInterface;

class Head implements FirstCharSyntaxInterface
{
  protected $result = '';
  protected $baseLevel;
  public function __construct($baseLevel = 3)
  {
    $baseLevel = (int)$baseLevel;
    if($baseLevel <= 0) throw new Exception("invalid argument: {$baseLevel}");
    $this->baseLevel = $baseLevel;
  }
  public function getIdentifier()
  {
    return '*';
  }
  public function parse($line)
  {
    $base = $this->baseLevel;
    $level = $this->countLevel($line);
    
    $this->result .= '<h' . ($base + $level - 1) . '>' 
                  . substr($line, $level) 
                  . '</h' . ($base + $level - 1) . '>' 
                  . PHP_EOL;
  }
  /**
   * $lineの先頭の*の数を数える
   */
  public function countLevel($line)
  {
    $level = 0;
    $len = strlen($line);
    for($i = 0; $i < $len || $i < 3; $i++) {
      if($line[$i] === '*') $level++;
      else break;
    }
    
    return $level;
  }
  public function getResult()
  {
    $result = $this->result;
    $this->result = '';
    return $result;
  }
}