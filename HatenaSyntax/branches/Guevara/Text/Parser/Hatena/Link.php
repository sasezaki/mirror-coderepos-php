<?php
/**
 * HatenaSyntax
 *
 * PHP version 5.3
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author    anatoo <study.anatoo@gmail.com> 
 * @license   http://www.opensource.org/licenses/mit-license.php The MIT License
 * @version   0.0.6
 * @link      http://d.hatena.ne.jp/anatoo/
 */
namespace Guevara::Text::Parser::Hatena;
require_once 'Guevara/Text/Parser/Hatena/InlineSyntaxInterface.php';

use Guevara::Text::Parser::Hatena::InlineSyntaxInterface as InlineSyntaxInterface;

class Link implements InlineSyntaxInterface
{
  public function parse($line)
  {
    return
      preg_replace_callback('/\[(https?:\/\/[^:]+)(:title=([^\]]*))?\]/', 
      create_function('$m', '
        return "<a href=\"{$m[1]}\">" 
              . (isset($m[3]) ? $m[3] : $m[1] )
              . "</a>";
      '),
      $line);
  }
}