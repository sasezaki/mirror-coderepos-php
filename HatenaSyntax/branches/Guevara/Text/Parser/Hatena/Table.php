<?php
/**
 * HatenaSyntax
 *
 * PHP version 5.3
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author    anatoo <study.anatoo@gmail.com> 
 * @license   http://www.opensource.org/licenses/mit-license.php The MIT License
 * @version   0.0.6
 * @link      http://d.hatena.ne.jp/anatoo/
 */
namespace Guevara::Text::Parser::Hatena;
require_once 'Guevara/Text/Parser/Hatena/FirstCharSyntaxInterface.php';

use Guevara::Text::Parser::Hatena::FirstCharSyntaxInterface as FirstCharSyntaxInterface;

class Table implements FirstCharSyntaxInterface
{
  protected $table = array();
  public function getIdentifier()
  {
    return '|';
  }
  public function parse($line)
  {
    $_ = explode('|', $line);
    array_pop($_);
    array_shift($_);
    $this->table[] = $_;
  }
  public function getResult()
  {
    $table = $this->table;
    $this->table = array();
    
    $result = '<table>' . PHP_EOL;
    foreach($table as $col) {
      $result .= '<tr>';
      foreach($col as $cell) {
        if(substr($cell, 0, 1) === '*') {
          $result .= '<th>' . substr($cell, 1) . '</th>';
        }
        else {
          $result .= '<td>' . $cell . '</td>';
        }
      }
      $result .= '</tr>' . PHP_EOL;
    }
    $result .= '</table>' . PHP_EOL;
    
    return $result;
  }
}