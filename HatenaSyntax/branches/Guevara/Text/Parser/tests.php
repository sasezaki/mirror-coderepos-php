<?php
namespace Guevara::Text::Parser;
set_include_path('/workspace/incubator/library/');
require_once 'Guevara/Text/Parser/Hatena.php';
use Guevara::Text::Parser::Hatena as HatenaSyntax;

$text = <<<'EOD'
* みだし
ほんぶん

>>
引用
<<

* head 1
text_text text_text
text_text text_text

** head 2
text_text text_text
text_text text_text

*** head 3
text_text text_text
text_text text_text

>|php|
<?php
$days = date('w');
echo $days;
||<

EOD;

$hatena = new HatenaSyntax();
echo $hatena->parse($text);
