<?php

/**
 * PECL::Imagick Thumbnailer 
 *
 * PHP version 5
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * @author Tomohiro MITSUMUNE <tmitsumune@gmail.com>
 * @license http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version 0.1.1
 * @link http://d.hatena.ne.jp/Kiske
 */
class ImagickThumbnailer
{
    /**
     * convert command path 
     */
    const CONVERT_COMMAND_PATH = '/usr/local/bin/convert';

    /**
     * identify command path 
     */
    const IDENTIFY_COMMAND_PATH = '/usr/local/bin/identify';

    /**
     * image 
     * 
     * @var object
     */
    private $image;
    /**
     * currentDimensions 
     * 
     * @var array
     */
    private $currentDimensions;
    /**
     * newDimensions 
     * 
     * @var array
     */
    private $newDimensions;
    /**
     * fileName 
     * 
     * @var string
     */
    private $fileName;
    /**
     * imageMeta 
     * 
     * @var array
     */
    private $imageMeta;
    /**
     * maxWidth 
     * 
     * @var int
     */
    private $maxWidth;
    /**
     * maxHeight 
     * 
     * @var int
     */
    private $maxHeight;

    /**
     * version 
     * 
     * @var mixed
     */
    private $version;

    /**
     * __construct 
     * 
     * @param string $fileName 
     * @return void
     */
    public function __construct($fileName)
    {
    	//initialize variables
        $this->currentDimensions    = array();
        $this->newDimensions        = array();
        $this->imageMeta			= array();
        $this->fileName             = $fileName;
        $this->percent              = 100;
        $this->maxWidth             = 0;
        $this->maxHeight            = 0;
        $this->version              = '';

        // check installed ImageMagick & Imagick Extension
        exec(self::CONVERT_COMMAND_PATH, $stdout);
        if (strpos($stdout[0], 'ImageMagick') === false)
        {
            throw new ImagickException(sprintf("ImageMagick convert command not found"));
        }
        exec(self::IDENTIFY_COMMAND_PATH, $stdout);
        if (strpos($stdout[0], 'ImageMagick') === false)
        {
            throw new ImagickException(sprintf("ImageMagick identify command not found"));
        }
        if (class_exists('Imagick') === false) {
            throw new ImagickException(sprintf("Imagick Extension not installed"));
        }

        // read the image
        $this->image = new Imagick($this->fileName);
        $this->currentDimensions = array('width' => $this->image->getImageWidth(), 'height' => $this->image->getImageHeight());
        // read exif data if exists
        $this->gatherImageMeta();
        $this->version = $this->image->getVersion();
    }

    /**
     * __destruct 
     * 
     */
    public function __destruct()
    {
        if(is_resource($this->image)) @$this->image->destroy();
    }

    /**
     * resize 
     * 
     * @param int $width 
     * @param int $height 
     */
    public function resize($width, $height)
    {
        if(is_numeric($width) and is_numeric($height)) {
            $this->maxWidth = $width;
            $this->maxHeight = $height;
            $this->scaleImage($this->currentDimensions['width'], $this->currentDimensions['height']);
            $this->image->thumbnailImage($width, $height, true);
        } else {
            throw new ImagickException(sprintf("resize method parameter is wrong"));
        }
        $this->currentDimensions['width'] = $this->newDimensions['width'];
        $this->currentDimensions['height'] = $this->newDimensions['height'];
    }

    /**
     * resizePercent 
     * 
     * @param int $percent 
     * @return void
     */
    public function resizePercent($percent = 0)
    {
        $this->percent = $percent;
        $this->scaleImagePercent($this->currentDimensions['width'], $this->currentDimensions['height']);

        $this->image->thumbnailImage($this->newDimensions['width'], $this->newDimensions['height']);

        $this->currentDimensions['width'] = $this->newDimensions['width'];
        $this->currentDimensions['height'] = $this->newDimensions['height'];
    }

    /**
     * scaleImagePercent 
     * 
     * @param int $x 
     * @param int $y 
     * @return void
     */
    private function scaleImagePercent($x, $y)
    {
        if($this->percent > 0) {
            $this->newDimensions = $this->calcPercent($x, $y);
        }
    }

    /**
     * calcPercent 
     * 
     * @param int $x 
     * @param int $y 
     * @return void
     */
    private function calcPercent($x, $y)
    {
        $nx = ($x * $this->percent) / 100;
        $ny = ($y * $this->percent) / 100;
        return array('width' => intval($nx), 'height' => intval($ny));
    }

    /**
     * scaleImage 
     * 画像アスペクト比を維持させるように調整
     * 
     * @param int $x 
     * @param int $y 
     * @param int $cx 
     * @param int $cy 
     * @return void
     */
    private function scaleImage($x, $y)
    {
        $newSize = array('nx' => $x, 'ny' => $y);

        if($this->maxWidth > 0) {
            $newSize = $this->calcWidth($x, $y);

            if($this->maxHeight > 0 && $newSize['ny'] > $this->maxHeight) {
                $newSize = $this->calcHeight($newSize['nx'], $newSize['ny']);
            }
        }

        if($this->maxHeight > 0) {
            $newSize = $this->calcHeight($x, $y);

            if($this->maxWidth > 0 && $newSize['nx'] > $this->maxWidth) {
                $newSize = $this->calcWidth($newSize['nx'], $newSize['ny']);
            }
        }
        $this->newDimensions['width'] = $newSize['nx'];
        $this->newDimensions['height'] = $newSize['ny'];
    }

    /**
     * calcWidth 
     * new Image width
     * 
     * @param int $width 
     * @param int $height 
     * @return array
     */
    private function calcWidth($width,$height) {
        $newWp = (100 * $this->maxWidth) / $width;
        $newHeight = ($height * $newWp) / 100;
        return array('nx'=>intval($this->maxWidth),'ny'=>intval($newHeight));
    }

    /**
     * calcHeight 
     * new Image height
     * 
     * @param int $width 
     * @param int $height 
     * @return array
     */
    private function calcHeight($width,$height) {
        $newHp = (100 * $this->maxHeight) / $height;
        $newWidth = ($width * $newHp) / 100;
        return array('nx'=>intval($newWidth),'ny'=>intval($this->maxHeight));
    }

    /**
     * cropFromCenter 
     * 
     * @param mixed $width 
     * @param mixed $height 
     * @return void
     */
    public function cropFromCenter($width, $height)
    {
        if($width > $this->currentDimensions['width']) $width = $this->currentDimensions['width'];
        if($height > $this->currentDimensions['height']) $height = $this->currentDimensions['height'];

        $this->image->cropthumbnailImage($width, $height);
    }

    /**
     * crop 
     * 
     * @param mixed $width 
     * @param mixed $height 
     * @param mixed $x 
     * @param mixed $y 
     * @return void
     */
    public function crop($width, $height, $x, $y)
    {
        // cropeed area is not greater than the size of image
        if($width > $this->currentDimensions['width']) $width = $this->currentDimensions['width'];
        if($height > $this->currentDimensions['height']) $height = $this->currentDimensions['height'];
        // not starting outside the image
        if(($x + $width) > $this->currentDimensions['width']) $x = $this->currentDimensions['width'] - $width;
        if(($y + $height) > $this->currentDimensions['height']) $y = $this->currentDimensions['height'] - $height;
        if($x < 0) $x = 0;
        if($y < 0) $y = 0;
        
        // cropping
        $this->image->cropImage($width, $height, $x, $y);

        $this->currentDimensions['width'] = $width;
        $this->currentDimensions['height'] = $height;
    }

    /**
     * rotate 
     * 
     * @param int $degree 
     * @param string $backgroundColor 
     */
    public function rotate($degree, $backgroundColor = null)
    {
        $color = new ImagickPixel();
        if(!is_null($backgroundColor)) {
            $color->setColor($backgroundColor);
        }
        $this->image->rotateImage($color, $degree);

        $this->currentDimensions['width'] = $this->image->getImageWidth();
        $this->currentDimensions['height'] = $this->image->getImageHeight();
    }

    /**
     * polaroize
     * required ImageMagick 6.3.8 or later
     * 
     * @return void
     */
    public function polaroize($degree = -5, $shadowColor = 'black')
    {
        $draw = new ImagickDraw();
        $draw->setGravity(Imagick::GRAVITY_SOUTH);

        // setting shadow color
        $this->image->setImageBackgroundColor($shadowColor);
        $this->image->polaroidImage($draw, $degree);

        // backgrouond image
        $bgimage = new Imagick();
        $bgimage->newImage($this->image->getImageWidth(), $this->image->getImageHeight(), new ImagickPixel('white'));
        $bgimage->setImageFormat($this->image->getImageFormat());
        $bgimage->addImage($this->image);
        // merged image layer
        $newimage = $bgimage->flattenImages();
        $this->image = $newimage;
    }

    /**
     * roundCorners 
     * convert png format
     * 
     * @param int $x 
     * @param int $y 
     * @return void
     */
    public function roundCorners($x = 30, $y = 30)
    {
        // rounded corners
        $this->image->roundCorners($x, $y);
        $this->image->setImageFormat('PNG');

        $shadow = $this->image->clone();
        $shadow->setImageBackgroundColor('black');
        $shadow->shadowImage(80, 3, 5, 5);

        $shadow->compositeImage($this->image, imagick::COMPOSITE_OVER, 0, 0);
        $this->image = $shadow;
    }

    /**
     * edge 
     * 
     * @param int $radius 
     * @return void
     */
    public function edge($radius = 0)
    {
        $this->image->edgeImage($radius);
    }

    /**
     * sepia 
     * 
     * @param int $threshold 
     * @return void
     */
    public function sepia($threshold = 80)
    {
        $this->image->sepiaToneImage($threshold);
    }

    /**
     * createReflection 
     * 
     * @return void
     */
    public function createReflection($opacity = 0.4) 
    {
        // cloned reflection image
        $reflection = $this->image->clone();
        $reflection->flipImage();

        // gradation image
        $gradient = new Imagick();
        $gradient->newPseudoImage($reflection->getImageWidth() + 10 , $reflection->getImageHeight() + 10 , "gradient: transparent-black" );

        $reflection->compositeImage($gradient, Imagick::COMPOSITE_OVER, 0, 0);
        // setting opacity. required ImageMagick 6.2.9 or later
        $reflection->setImageOpacity($opacity);

        $canvas = new Imagick();
        $width = $this->image->getImageWidth() + 10;
        $height = ($this->image->getImageHeight() * 2) + 10;
        $canvas->newImage($width, $height, new ImagickPixel("black"));
        $canvas->setImageFormat($this->image->getImageFormat());
        $canvas->compositeImage($this->image, Imagick::COMPOSITE_OVER, 5, 5);
        $canvas->compositeImage($reflection, Imagick::COMPOSITE_OVER, 5, $this->image->getImageHeight() + 5 );

        $this->image = $canvas;
    }

    /**
     * show 
     * 
     * @param mixed $name 
     * @param int $quality 
     * @return void
     */
    public function show($name = null, $quality = 100)
    {
        if($name != '') {
	        $this->image->writeImage($name);
        } else {
	        switch($this->image->getImageFormat()) {
	            case 'GIF':
	                header('Content-type: image/gif');
	                echo $this->image;
	                break;
	            case 'JPG':
	            case 'JPEG':
	                header('Content-type: image/jpeg');
                    $this->image->setCompression(Imagick::COMPRESSION_JPEG);
                    $this->image->setCompressionQuality($quality);
	                echo $this->image;
	                break;
	            case 'PNG':
	                header('Content-type: image/png');
	                echo $this->image;
	                break;
                default:
                    throw new ImagickException(sprintf('not support file format'));
	        }
        }
    }

    /**
     * save 
     * 
     * @param mixed $name 
     * @param int $quality 
     * @return void
     */
	public function save($name, $quality = 100)
    {
	    $this->show($name, $quality);
	}

    /**
     * getCurrentWidth 
     * 
     * @return void
     */
    public function getCurrentWidth()
    {
        return $this->currentDimensions['width'];
    }

    /**
     * getCurrentHeight 
     * 
     * @return void
     */
    public function getCurrentHeight()
    {
        return $this->currentDimensions['height'];
    }

    /**
     * gatherImageMeta 
     * reterieved exif data
     * 
     * @return void
     */
    private function gatherImageMeta() 
    {
    	// only attempt to retrieve info if exif exists
    	if(function_exists("exif_read_data") && $this->image->getImageFormat() == 'JPEG') {
			$imageData = exif_read_data($this->fileName);
			if(isset($imageData['Make'])) 
				$this->imageMeta['make'] = ucwords(strtolower($imageData['Make']));
			if(isset($imageData['Model'])) 
				$this->imageMeta['model'] = $imageData['Model'];
			if(isset($imageData['COMPUTED']['ApertureFNumber'])) {
				$this->imageMeta['aperture'] = $imageData['COMPUTED']['ApertureFNumber'];
				$this->imageMeta['aperture'] = str_replace('/','',$this->imageMeta['aperture']);
			}
			if(isset($imageData['ExposureTime'])) {
				$exposure = explode('/',$imageData['ExposureTime']);
				$exposure = round($exposure[1]/$exposure[0],-1);
				$this->imageMeta['exposure'] = '1/' . $exposure . ' second';
			}
			if(isset($imageData['Flash'])) {
				if($imageData['Flash'] > 0) {
					$this->imageMeta['flash'] = 'Yes';
				}
				else {
					$this->imageMeta['flash'] = 'No';
				}
			}
			if(isset($imageData['FocalLength'])) {
				$focus = explode('/',$imageData['FocalLength']);
				$this->imageMeta['focalLength'] = round($focus[0]/$focus[1],2) . ' mm';
			}
			if(isset($imageData['DateTime'])) {
				$date = $imageData['DateTime'];
				$date = explode(' ',$date);
				$date = str_replace(':','-',$date[0]) . ' ' . $date[1];
				$this->imageMeta['dateTaken'] = date('m/d/Y g:i A',strtotime($date));
			}
    	}
    }
}
