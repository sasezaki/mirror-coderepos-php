<?php
//exthtml.php
//@see http://fuba.jottit.com/exthtml
//how to use : あとで
//
//miyagawaさんのwatchingを監視する場合
//php exthtml.php http://twitter.com/miyagawa -x //td//span -d 2 -f *isWatching

//array(6) {
//  [0] => string(22) "watching kurenai ep7-9"
//  [1] => string(25) "watching ゴルゴ13 ep13"
//  [2] => string(36) "watching ゲームセンターCX #65"
//


///////////////Your Filter
//適当にていぎする　オートフィルタしたい場合は取りたい値をそのまま返す
function isWatching($var)
{
    if(substr(trim($var), 0, 8) == 'watching') {
        return $var;
    } else return "";
}
////////////////////////////


require_once 'Diggin/Scraper.php';
require_once 'Diggin/Debug.php';
require_once 'Zend/Http/Client.php';
require_once 'Zend/Console/Getopt.php';

$console = new Zend_Console_Getopt(
    array(
    'xpath|x=s' => 'xpath',
    'referer|e=s' => 'referer',
    //'cookie|k=s' => 'cookie',
    'cookieJar|c=s' => 'cookie',
    'agent|a=s' => 'useragent',
    'nextlink|n=s' => 'nextlink',
    'depth|d=i' => 'depth "if not set nextlink, using wedata"',
//    "s|as-source" => '$as_xml',
    'basic|b=s' => 'basic auth "user/pass"',
//    'cache|h=s' => '"',
    'stoptime|t=s' => 'sleep() :default 1',
    'filter|f=s' => 'filter for Diggin_Scraper',
      )
);
if(!$console->xpath) throw new InvalidArgumentException('"now" must xpath: -x //html/body'.$console->getUsageMessage());
if(count($console->getRemainingArgs()) === 0) throw new InvalidArgumentException('URL is not set'.$console->getUsageMessage());

//now suport one url
$url = array_shift($console->getRemainingArgs());

$client = new Zend_Http_Client();

if ($console->agent) {
    $client->setConfig(array('useragent'=> $console->agent));
}
if ($console->basic) {
    list($basicusername, $basicpassword) = explode('/', $console->basic);
    if(!$basicpassword) throw new InvalidArgumentException('argument is not user/pass');
    $client->setAuth($basicusername, $basicpassword, Zend_Http_Client::AUTH_BASIC);
}
if ($console->cookieJar) {
    require_once 'Diggin/Http/CookieJar/Loader/Firefox3.php';
    //Diggin_Http_CookieJar_Loader_Firefox3::load();
$client->setCookieJar();
//$cookie = (is_file($console->cookie)) ? file_get_contents($console->cookie) : $console->cookie;
//$client->setCookie($cookie);
}
if ($console->stoptime) {
    $stoptime = $console->stoptime;
} else $stoptime = 1;

if (!isset($console->depth)) {
    $depth = 1;
} else $depth = $console->depth;

for ($i = 0; $i < $depth; $i++) {

    $client->setUri($url);
    $response = $client->request();

    if($console->depth && !isset($console->nextlink)){
        //searching wedata
       $nextLink = getNextLinkFromWedata($url) ;
    } else if ($console->depth && isset($console->nextlink)) {
       $nextLink = $console->nextlink;
    }
    
    $scraper = new Diggin_Scraper();
    $scraper->setUrl($url);
    if($console->filter) {
        $scraper->process($console->xpath, 'xpath => ["TEXT", "'.$console->filter. '"]');
    } else $scraper->process($console->xpath, 'xpath => "TEXT"');
    if ($nextLink) {
        $scraper->process($nextLink, 'nextLink => "@href"');
    }
    $scraper->scrape($response);
    
    //YAML::dumpっぽく吐きたいですね。
    Diggin_Debug::dump($scraper->xpath);

    if(!isset($console->depth)) exit;
    if(count($scraper->nextLink) === 0) {
        //Diggin_Debug::dump($nextLink);
        //Diggin_Debug::dump($scraper->nextLink);
        Diggin_Debug::dump('next page not found');
        exit;
    } else {
        if(is_array($scraper->nextLink)) {
            $url = array_shift($scraper->nextLink);
        } else $url = $scraper->nextLink;
        sleep($stoptime);
    }
}



function getNextLinkFromWedata($url)
{
    require_once 'Zend/Cache.php';
    require_once 'Diggin/Service/Wedata.php';
    $frontendOptions = array(
    'lifetime' => 86400, // キャッシュの有効期限を 24 時間とします
    'automatic_serialization' => true,
    );
    $backendOptions = array(
//        'cache_dir' => $cahe_dir // キャッシュファイルを書き込むディレクトリ
        'cache_dir' => dirname(__FILE__)
    );
    
    $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
    if(!$items = $cache->load('wedata_items')) {
        $items = Diggin_Service_Wedata::getItems('AutoPagerize');
        $cache->save($items, 'wedata_items');
    }
    $nextLink = getNextlink($items, $url);
    if($nextLink === false) {
        Diggin_Debug::dump('not found from wedata with url:'.$url);
        exit;
    }
    
    return $nextLink;
}
/**
 * Get next url from wedata('AutoPagerize')
 *
 * @param array $items
 * @param string $url base url
 * @return mixed
 */
function getNextlink($items, $url) {
    foreach ($items as $item) {
    $pattern = '#'.$item['data']['url'].'#i';
        //hAtom 対策
        if ('^https?://.' != $item['data']['url'] && (preg_match($pattern, $url) == 1)) {
            $nextLink = $item['data']['nextLink'];
            return $nextLink;
        }
    }
    
    return false;
}
