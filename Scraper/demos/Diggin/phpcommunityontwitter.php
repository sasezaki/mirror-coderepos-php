<?php
//phpにいるすごいひとリストのOPML（＊ただし、zend.com編）
require_once 'Diggin/Scraper.php';
require_once 'Zend/Cache.php';
$cache = Zend_Cache::factory('Core', 'File',
                            array('lifetime' => 86400,
    							  'automatic_serialization' => true),
                            array('cache_dir' => './temp/'));
//scraping zend.com
if(!$phper = $cache->load('phptwitter')) {
    $scraper = new Diggin_Scraper();//[starts-with(.,"http://")]
    $scraper->process('//a/@href[starts-with(.,"http://twitter.com/")]', 'phper => "@href"')
            ->scrape('http://devzone.zend.com/article/3467-PHP-Community-on-Twitter');
    $phper = $scraper->results['phper'];
    $cache->save($phper, 'phptwitter');
    
    echo "getting from zend.com";
}

//scraping twitter
foreach($phper as $url) {
    $scraper = new Diggin_Scraper();
    $scraper->process('head/link[@type="application/rss+xml"]', 'rss => "@href"')
            ->process('//ul[@class="about vcard entry-author"]//span[@class="fn"]', 'name => "TEXT"')
            ->process('//ul[@class="about vcard entry-author"]//a', 'usersite => "@href"')
            ->scrape($url);
    sleep(1);
    $scraper->results['htmlurl'] = $url; 
    $userlists[] = $scraper->results;
//    break;
}
//print_r($userlists);

//making opml
require_once 'rhaco/Rhaco.php';
Rhaco::import("tag.feed.Opml");
Rhaco::import("lang.DateUtil");
$opml = new Opml();
foreach ($userlists as $user) {
    $opmloutline = new OpmlOutline();
    $opmloutline->setTitle($user['name']);
    $opmloutline->setDescription($user['usersite']);
    $opmloutline->setXmlUrl($user['rss'][0]);
    $opmloutline->setType("rss");
    $opmloutline->setHtmlUrl($user['htmlurl']);
    $opml->setOutline($opmloutline);
}
print_r($opml->output());

//今のところ、取得時に空でもarray()で返しちゃってるので、↓は置換でちょっとなおした（ダサイ）

/**
<?xml version="1.0" encoding="utf-8"?>
<opml version="1.0">
<head />
<body>
<outline type="rss" htmlUrl="http://twitter.com/calevans" xmlUrl="http://twitter.com/statuses/user_timeline/4795561.rss" title="CalEvans" description="http://blog.calevans.com/epk" />
<outline type="rss" htmlUrl="http://twitter.com/phpa" xmlUrl="http://twitter.com/statuses/user_timeline/9632542.rss" title="phpa" />
<outline type="rss" htmlUrl="http://twitter.com/zendcon" xmlUrl="http://twitter.com/statuses/user_timeline/8854172.rss" title="zendcon" />
<outline type="rss" htmlUrl="http://twitter.com/zend" xmlUrl="http://twitter.com/statuses/user_timeline/14182874.rss" title="Zend" description="http://framework.zend.com" />
<outline type="rss" htmlUrl="http://twitter.com/andigutmans" xmlUrl="http://twitter.com/statuses/user_timeline/14309129.rss" title="andigutmans" />
<outline type="rss" htmlUrl="http://twitter.com/weierophinney" xmlUrl="http://twitter.com/statuses/user_timeline/9453382.rss" title="weierophinney" description="http://weierophinney.net/matthew/" />
<outline type="rss" htmlUrl="http://twitter.com/akrabat" xmlUrl="http://twitter.com/statuses/user_timeline/9244712.rss" title="RobAllen" description="http://akrabat.com" />
<outline type="rss" htmlUrl="http://twitter.com/coveloper" xmlUrl="http://twitter.com/statuses/user_timeline/11967932.rss" title="coveloper" description="http://www.jonbauer.com" />
<outline type="rss" htmlUrl="http://twitter.com/nickbelhomme" xmlUrl="http://twitter.com/statuses/user_timeline/13560822.rss" title="NickBelhomme" description="http://blog.nickbelhomme.com" />
<outline type="rss" htmlUrl="http://twitter.com/s_bergmann" xmlUrl="http://twitter.com/statuses/user_timeline/13311252.rss" title="SebastianBergmann" description="http://sebastian-bergmann.de/" />
<outline type="rss" htmlUrl="http://twitter.com/tonybibbs" xmlUrl="http://twitter.com/statuses/user_timeline/9549602.rss" title="tonybibbs" description="http://www.tonybibbs.com" />
<outline type="rss" htmlUrl="http://twitter.com/ashnazg" xmlUrl="http://twitter.com/statuses/user_timeline/14399725.rss" title="ChuckBurgess" description="http://thenazg.blogspot.com/" />
<outline type="rss" htmlUrl="http://twitter.com/CaseySoftware" xmlUrl="http://twitter.com/statuses/user_timeline/8170372.rss" title="D. KeithCasey, Jr." description="http://CaseySoftware.com/blog" />
<outline type="rss" htmlUrl="http://twitter.com/tychay" xmlUrl="http://twitter.com/statuses/user_timeline/5668132.rss" title="terrychay" description="http://terrychay.com/blog/" />
<outline type="rss" htmlUrl="http://twitter.com/phpcodemonkey" xmlUrl="http://twitter.com/statuses/user_timeline/14173119.rss" title="JeremyCoates" description="http://blog.solutionperspectivemedia.co.uk/" />
<outline type="rss" htmlUrl="http://twitter.com/seancoates" xmlUrl="http://twitter.com/statuses/user_timeline/14249124.rss" title="SeanCoates" description="http://blog.phpdoc.info/" />
<outline type="rss" htmlUrl="http://twitter.com/coogle" xmlUrl="http://twitter.com/statuses/user_timeline/9759612.rss" title="coogle" description="http://www.coggeshall.org/" />
<outline type="rss" htmlUrl="http://twitter.com/enygma" xmlUrl="http://twitter.com/statuses/user_timeline/8854032.rss" title="enygma" description="http://blog.phpdeveloper.org" />
<outline type="rss" htmlUrl="http://twitter.com/phpdeveloper" xmlUrl="http://twitter.com/statuses/user_timeline/12778882.rss" title="phpdeveloper" />
<outline type="rss" htmlUrl="http://twitter.com/stephdau" xmlUrl="http://twitter.com/statuses/user_timeline/13348572.rss" title="StephaneDaury" description="http://tekartist.org/" />
<outline type="rss" htmlUrl="http://twitter.com/bdeshong" xmlUrl="http://twitter.com/statuses/user_timeline/8226322.rss" title="bdeshong" description="http://www.deshong.net/" />
<outline type="rss" htmlUrl="http://twitter.com/felixdv" xmlUrl="http://twitter.com/statuses/user_timeline/891371.rss" title="Felix DeVliegher" />
<outline type="rss" htmlUrl="http://twitter.com/funkatron" xmlUrl="http://twitter.com/statuses/user_timeline/65583.rss" title="funkatron" description="http://funkatron.com" />
<outline type="rss" htmlUrl="http://twitter.com/spooons" xmlUrl="http://twitter.com/statuses/user_timeline/3163981.rss" title="christian" description="http://www.nexdot.ner/blog" />
<outline type="rss" htmlUrl="http://twitter.com/wezfurlong" xmlUrl="http://twitter.com/statuses/user_timeline/9589662.rss" title="WezFurlong" description="http://netevil.org/" />
<outline type="rss" htmlUrl="http://twitter.com/kenguest" xmlUrl="http://twitter.com/statuses/user_timeline/14407919.rss" title="kenguest" description="http://blogs.linux.ie/kenguest/" />
<outline type="rss" htmlUrl="http://twitter.com/saramg" xmlUrl="http://twitter.com/statuses/user_timeline/8295352.rss" title="SaraMG" />
<outline type="rss" htmlUrl="http://twitter.com/nickhalstead" xmlUrl="http://twitter.com/statuses/user_timeline/3364401.rss" title="NickHalstead" description="http://www.nickhalstead.com" />
<outline type="rss" htmlUrl="http://twitter.com/chartjes" xmlUrl="http://twitter.com/statuses/user_timeline/7418052.rss" title="ChrisHartjes" description="http://www.littlehart.net/atthekeyboard" />
<outline type="rss" htmlUrl="http://twitter.com/stuherbert" xmlUrl="http://twitter.com/statuses/user_timeline/9450192.rss" title="StuartHerbert" description="http://blog.stuartherbert.com/" />
<outline type="rss" htmlUrl="http://twitter.com/njames/" xmlUrl="http://twitter.com/statuses/user_timeline/60063.rss" title="NigelJames" />
<outline type="rss" htmlUrl="http://twitter.com/ijansch" xmlUrl="http://twitter.com/statuses/user_timeline/11853742.rss" title="IvoJansch" description="http://www.jansch.nl" />
<outline type="rss" htmlUrl="http://twitter.com/ibuildings" xmlUrl="http://twitter.com/statuses/user_timeline/14523605.rss" title="ibuildings" description="http://www.ibuildings.com" />
<outline type="rss" htmlUrl="http://twitter.com/mgkimsal" xmlUrl="http://twitter.com/statuses/user_timeline/6292902.rss" title="michaelkimsal" description="http://www.webdevradio.com" />
<outline type="rss" htmlUrl="http://twitter.com/skoop" xmlUrl="http://twitter.com/statuses/user_timeline/1524641.rss" title="Stefan" description="http://www.leftontheweb.com/" />
<outline type="rss" htmlUrl="http://twitter.com/janl" xmlUrl="http://twitter.com/statuses/user_timeline/819606.rss" title="JanLehnardt" description="http://jan.prima.de/" />
<outline type="rss" htmlUrl="http://twitter.com/vluther" xmlUrl="http://twitter.com/statuses/user_timeline/639023.rss" title="vluther" description="http://www.phpcult.com/" />
<outline type="rss" htmlUrl="http://twitter.com/ElizabethN" xmlUrl="http://twitter.com/statuses/user_timeline/9697482.rss" title="ElizabethNaramore" description="http://www.naramore.net/blog" />
<outline type="rss" htmlUrl="http://twitter.com/maggie1000" xmlUrl="http://twitter.com/statuses/user_timeline/14033052.rss" title="maggie1000" />
<outline type="rss" htmlUrl="http://twitter.com/phpclasses" xmlUrl="http://twitter.com/statuses/user_timeline/12577772.rss" title="PHP Classessite" description="http://www.phpclasses.org/" />
<outline type="rss" htmlUrl="http://twitter.com/ramsey" xmlUrl="http://twitter.com/statuses/user_timeline/7794552.rss" title="BenRamsey" description="http://benramsey.com/" />
<outline type="rss" htmlUrl="http://twitter.com/derickrethans" xmlUrl="http://twitter.com/statuses/user_timeline/14310583.rss" title="DerickRethans" description="http://derickrethans.nl" />
<outline type="rss" htmlUrl="http://twitter.com/andriesss" title="AndriesSeutens" description="http://andries.systray.be" />
<outline type="rss" htmlUrl="http://twitter.com/jeefy" xmlUrl="http://twitter.com/statuses/user_timeline/14293328.rss" title="jeefy" description="http://morethanotaku.net" />
<outline type="rss" htmlUrl="http://twitter.com/dshafik" xmlUrl="http://twitter.com/statuses/user_timeline/7920672.rss" title="DaveyShafik" description="http://pixelated-dreams.com" />
<outline type="rss" htmlUrl="http://twitter.com/shiflett" xmlUrl="http://twitter.com/statuses/user_timeline/4664501.rss" title="ChrisShiflett" description="http://shiflett.org/" />
<outline type="rss" htmlUrl="http://twitter.com/auroraeosrose" xmlUrl="http://twitter.com/statuses/user_timeline/8854222.rss" title="auroraeosrose" description="http://elizabethmariesmith.com" />
<outline type="rss" htmlUrl="http://twitter.com/mtabini" xmlUrl="http://twitter.com/statuses/user_timeline/14492118.rss" title="MarcoTabini" description="http://mtabini.blogspot.com" />
<outline type="rss" htmlUrl="http://twitter.com/lxt" xmlUrl="http://twitter.com/statuses/user_timeline/3058771.rss" title="LauraThomson" description="http://www.laurathomson.com" />
<outline type="rss" htmlUrl="http://twitter.com/helgith" xmlUrl="http://twitter.com/statuses/user_timeline/11924252.rss" title="Helgi ÞormarÞ." description="http://www.helgi.ws" />
<outline type="rss" htmlUrl="http://twitter.com/elazar" title="elazar" description="http://ishouldbecoding.com" />
<outline type="rss" htmlUrl="http://twitter.com/lig" xmlUrl="http://twitter.com/statuses/user_timeline/14136842.rss" title="lig" description="http://blog.khankennels.com" />
<outline type="rss" htmlUrl="http://twitter.com/DragonBe" xmlUrl="http://twitter.com/statuses/user_timeline/9594462.rss" title="DragonBe" description="http://dragonbe.blogspot.com" />
<outline type="rss" htmlUrl="http://twitter.com/chwenz" xmlUrl="http://twitter.com/statuses/user_timeline/10288832.rss" title="chwenz" description="http://www.hauser-wenz.de/blog" />
<outline type="rss" htmlUrl="http://twitter.com/EliW" xmlUrl="http://twitter.com/statuses/user_timeline/9777152.rss" title="EliW" description="http://eliw.com/" />
<outline type="rss" htmlUrl="http://twitter.com/sidhighwind" xmlUrl="http://twitter.com/statuses/user_timeline/14175987.rss" title="JonW" />
<outline type="rss" htmlUrl="http://twitter.com/caseyw" xmlUrl="http://twitter.com/statuses/user_timeline/13398392.rss" title="CaseyWilson" />
</body>
</opml>
*/