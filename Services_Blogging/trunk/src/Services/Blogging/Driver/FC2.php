<?php
//* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

require_once 'Services/Blogging/Driver/MetaWeblog.php';

/**
 * FC2 API implementation
 *
 * PHP versions 5
 *
 * @category   Services
 * @package    Services_Blogging
 * @author     Taiji Inoue <inudog@gmail.com>
 * @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
 * @version    SVN: $Id: FC2.php 7810 2008-03-11 12:43:24Z inudog $
 */
class Services_Blogging_Driver_FC2 extends Services_Blogging_Driver_MetaWeblog
{
    const XML_RPC_SERVER = 'http://blog.fc2.com';
    const XML_RPC_PATH   = '/xmlrpc.php';

    public function __construct($user, $pass, $server = null, $path = null)
    {
        if ($server === null) {
            $server = self::XML_RPC_SERVER;
            $path   = self::XML_RPC_PATH;
        }
        parent::__construct($user, $pass, $server, $path);
    }
}

