<?php
//* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

require_once 'Services/Blogging/Driver.php';
require_once 'Services/Blogging/MultipleBlogsInterface.php';
require_once 'Services/Blogging/Driver/MetaWeblog.php';

/**
 * MovableType API implementation
 *
 * PHP versions 5
 *
 * @category   Services
 * @package    Services_Blogging
 * @author     Taiji Inoue <inudog@gmail.com>
 * @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
 * @version    SVN: $Id: MovableType.php 8201 2008-03-20 10:53:42Z inudog $
 */
class Services_Blogging_Driver_MovableType
extends Services_Blogging_Driver_MetaWeblog
implements Services_Blogging_MultipleBlogsInterface
{
    /**
     *   Id of the blog to be used.
     *   Some blogs support multiple blogs with one account.
     *   @var int
     */
    protected $nBlogId = null;


    protected $arSupportedPostProperties =
        array(
              Services_Blogging_Post::TITLE,
              Services_Blogging_Post::CONTENT,
              Services_Blogging_Post::PUBDATE,
              Services_Blogging_Post::DATE,
              Services_Blogging_Post::CATEGORIES,
              Services_Blogging_Post::URL,
              'publish',
              'userid',
              //'description',
              //'postid',
              //'dateCreated',
              //'link'
              'mt_allow_comments',
              'mt_allow_pings',
              'mt_convert_breaks',
              'mt_text_more',
              'mt_excerpt',
              'mt_keywords',
              'mt_tb_ping_urls',
              'mt_tags',
              'mt_basename',
              );

    /**
     *   Sets the blog id to use (some blogging APIs support multiple
     *   blogs with one account)
     *
     *   @param int  $nBlogId    Id of the blog to use
     */
    public function setBlogId($nBlogId)
    {
        $this->nBlogId = $nBlogId;
        $this->userdata['rpc_blogid'] = new XML_RPC_Value($nBlogId, 'string');
    } //public function setBlogId($nBlogId)

    /**
     *   Returns the id of the currently used blog.
     *
     *   @return int     Blog id
     */
    public function getBlogId()
    {
        return $this->nBlogId;
    } //public function getBlogId()

    public function getBlogs()
    {
        return $this->_getDriver('Blogger')->getBlogs();
    }

    public function mtGetRecentPostTitles()
    {
        // @todo
    }

    public function mtSupportedMethods()
    {
        $request = new XML_RPC_Message('mt.supportedMethods');
        return Services_Blogging_XmlRpc::sendRequest($request, $this->rpc_client);
    }

    public function mtPublishPost($postid)
    {
        $request = new XML_RPC_Message('mt.publishPost',
                                       array(
                                             new XML_RPC_Value($postid, 'string'),
                                             $this->userdata['rpc_user'],
                                             $this->userdata['rpc_pass'],
                                             ));
        return Services_Blogging_XmlRpc::sendRequest($request, $this->rpc_client);
    }

    public function mtGetCategoryList()
    {
        $request = new XML_RPC_Message('mt.getCategoryList',
                                       array(
                                             $this->userdata['rpc_blogid'],
                                             $this->userdata['rpc_user'],
                                             $this->userdata['rpc_pass'],
                                             )
                                       );
        return Services_Blogging_XmlRpc::sendRequest($request, $this->rpc_client);
    }

    public function mtGetPostCategories($postid)
    {
        $request = new XML_RPC_Message('mt.getPostCategories',
                                       array(
                                             new XML_RPC_Value($postid, 'string'),
                                             $this->userdata['rpc_user'],
                                             $this->userdata['rpc_pass'],
                                             )
                                       );
        return Services_Blogging_XmlRpc::sendRequest($request, $this->rpc_client);
    }

    public function mtSetPostCategories($postid, $categories)
    {
        $categories_value = array();
        foreach ($categories as $id) {
            $categories_value[] = new XML_RPC_Value(
                                                    array(
                                                          'categoryId' => new XML_RPC_Value($id, 'string'),
                                                          //'isPrimary'  => new XML_RPC_Value(false, 'boolean'),
                                                          ),
                                                    'struct'
                                                    );
        }

        $request = new XML_RPC_Message('mt.setPostCategories',
                                       array(
                                             new XML_RPC_Value($postid, 'string'),
                                             $this->userdata['rpc_user'],
                                             $this->userdata['rpc_pass'],
                                             new XML_RPC_Value($categories_value, 'array'),
                                             )
                                       );
        return Services_Blogging_XmlRpc::sendRequest($request, $this->rpc_client);
    }

    public function mtSupportedTextFilters()
    {
        $request = new XML_RPC_Message('mt.supportedTextFilters');
        return Services_Blogging_XmlRpc::sendRequest($request, $this->rpc_client);
    }

    public function mtGetTrackbackPings($postid)
    {
        $request = new XML_RPC_Message('mt.getTrackbackPings',
                                       array(
                                             new XML_RPC_Value($postid, 'string'),
                                             )
                                       );
        return Services_Blogging_XmlRpc::sendRequest($request, $this->rpc_client);
    }

    public function mtGetTagList()
    {
        $request = new XML_RPC_Message('mt.getTagList',
                                       array(
                                             $this->userdata['rpc_blogid'],
                                             $this->userdata['rpc_user'],
                                             $this->userdata['rpc_pass'],
                                             )
                                       );
        return Services_Blogging_XmlRpc::sendRequest($request, $this->rpc_client);
    }

    public function savePost(Services_Blogging_Post $post)
    {
        parent::savePost($post);
        if ($post->id && $post->publish) {
            $this->mtPublishPost($post->id);
        }
        //$this->mtSetPostCategories($post->id, array(1));
    }

    protected function convertPostToStruct($post)
    {
        $time = $post->{Services_Blogging_Post::DATE};
        if ($time == ''  || $time == 0) {
            $time = time();
        }

        $values =
            array(
                  'dateCreated' => new XML_RPC_Value(date('Ymd\\TH:i:s', $time), 'dateTime.iso8601'),
                  'description' => new XML_RPC_Value($post->{Services_Blogging_Post::CONTENT}, 'string'),
                  'title'       => new XML_RPC_Value($post->{Services_Blogging_Post::TITLE}, 'string')
                  );

        $mt_keys = array('mt_allow_comments' => 'int',
                         'mt_allow_pings'    => 'int',
                         'mt_convert_breaks' => 'string',
                         'mt_text_more'      => 'string',
                         'mt_excerpt'        => 'string',
                         'mt_keywords'       => 'string',
                         'mt_tb_ping_urls'   => 'array',
                         'mt_tags'           => 'string',
                         );
        foreach ($mt_keys as $name => $type) {
            if ($post->$name !== null) {
                $values[$name] = new XML_RPC_Value($post->$name, $type);
            }
        }
        return new XML_RPC_Value($values, 'struct');
    } //protected function convertPostToStruct($post)

    protected function convertStructToPost($arStruct)
    {
        $post = parent::convertStructToPost($arStruct);
        foreach ($this->arSupportedPostProperties as $key) {
            if (ereg('^mt_', $key)) {
                $post->$key = $arStruct[$key];
            }
        }
        return $post;
    } //protected function convertStructToPost($arStruct)

    protected function _getDriver($driver)
    {
        $driver = Services_Blogging::factory(
                                             $driver,
                                             $this->userdata['user'],
                                             $this->userdata['pass'],
                                             $this->userdata['server'],
                                             $this->userdata['path']
                                             );
        return $driver;
    }
}
