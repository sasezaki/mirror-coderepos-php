<?php
/**
 * Services_HTML2PDF example
 *
 * @author halt feits <halt.feits at gmail.com>
 * @package Services_HTML2PDF
 */

require_once 'Services/HTML2PDF.php';

//PDF
function example_pdf()
{
    $pdf = Services_HTML2PDF::convert("http://project-p.jp/halt/", 'PDF');
    if (PEAR::isError($pdf)) {
        print($pdf->getMessage() . "\n");
        exit();
    }
    file_put_contents(dirname(__FILE__) . '/hoge.pdf' , $pdf);
}

//PNG
function example_png()
{
    $pdf = Services_HTML2PDF::convert("http://project-p.jp/halt/", 'PNG');
    if (PEAR::isError($pdf)) {
        print($pdf->getMessage() . "\n");
        exit();
    }
    file_put_contents(dirname(__FILE__) . '/hoge.pdf' , $pdf);
}

//JSON example
function example_json()
{
    $pdf = Services_HTML2PDF::convert("http://project-p.jp/halt/", 'JSON');

    if (PEAR::isError($pdf)) {
        print($pdf->getMessage() . "\n");
        exit();
    }

    foreach ($pdf as $key => $binary) {
        $filename = dirname(__FILE__) . '/hoge.' . strtolower($key);
        file_put_contents($filename, $binary);
    }
}

//example_pdf();
example_png();
//example_json();
?>
