<?php

/**
 * Web API wrapper for Hatena Favorites API
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Hatena_Favorites
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Hatena_Favorites
 * @see       References to other sections (if any)...
 */

require_once 'Services/Hatena/Favorites/API.php';

/**
 * Web API wrapper for Hatena Favorites API
 *
 * @category  Services
 * @package   Services_Hatena_Favorites
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: 0.0.1
 * @link      http://pear.php.net/package/Services_Hatena_Favorites
 * @see       References to other sections (if any)...
 */
class Services_Hatena_Favorites {

    /**
     * a service instance
     * @var    object Service_Hatena_Favorites_API
     * @access private
     */
	private $api;

    /**
     * fetched result in the json format
     * @var    string
     * @access private
     */
	private $result;

    /**
     * constructor
     *
     * @param  Services_Hatena_Favorites_API $api  Services_Hatena_Favorites_API object
     * @access public
     */
	public function __construct(Services_Hatena_Favorites_API $api) {
		$this->api = $api;
	}

    /**
     * execute your service
     *
     * @param  string $username username
     * @return void
     * @access public
     */
	public function execute($username) {
		$this->result = $this->api->execute($username);
	}

    /**
     * return fetched result in a json format
     *
     * @return string fetched result
     * @access public
     */
	public function get() {
		return $this->result;
	}

    /**
     * return fetched result in an array format
     *
     * @return string fetched result
     * @access public
     * @experimental
     */
	public function getAsArray() {
		$stdobj = json_decode($this->get());
		if ($stdobj === null || !isset($stdobj->favorites)) {
			return null;
		}
		$ret = array();
		foreach ($stdobj->favorites as $obj) {
			$ret[] = $obj->name;
		}
		sort($ret);
		return $ret;
	}
}
