<?php

/**
 * Abstract service class for Hatena Favorites API
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Hatena_Favorites
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Hatena_Favorites
 * @see       References to other sections (if any)...
 */

require_once 'Services/Hatena/Favorites/API.php';
require_once 'HTTP/Request.php';

/**
 * Abstract service class for Hatena Favorites API
 *
 * @category  Services
 * @package   Services_Hatena_Favorites
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: 0.0.1
 * @link      http://pear.php.net/package/Services_Hatena_Favorites
 * @see       References to other sections (if any)...
 */
abstract class Services_Hatena_Favorites_AbstractAPI implements Services_Hatena_Favorites_API {

    /**
     * execute service
     *
     * @param  string $username username
     * @return string fetched result in the json format
     * @access public
     * @throws Exception Exception description (if any) ...
     */
	public function execute($username) {
		$request = new HTTP_Request($this->getApiUrl($username));
		if (PEAR::isError($request->sendRequest())) {
			throw new Exception();
		}
		return $request->getResponseBody();
	}

}