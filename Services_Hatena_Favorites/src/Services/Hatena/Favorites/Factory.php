<?php

/**
 * Service factory class for Hatena Favorites API
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Hatena_Favorites
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Hatena_Favorites
 * @see       References to other sections (if any)...
 */

require_once 'Services/Hatena/Favorites.php';

/**
 * Service factory class for Hatena Favorites API
 *
 * @category  Services
 * @package   Services_Hatena_Favorites
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: 0.0.1
 * @link      http://pear.php.net/package/Services_Hatena_Favorites
 * @see       References to other sections (if any)...
 */
class Services_Hatena_Favorites_Factory {

    /**
     * 'Diary' mode
     */
	const API_DIARY = 'Diary';

    /**
     * ANTENNA
     */
	const API_ANTENNA = 'Antenna';

    /**
     * RSS
     */
	const API_RSS = 'Rss';

    /**
     * GROUP
     */
	const API_GROUP = 'Group';

    /**
     * STAR
     */
	const API_STAR = 'Star';


    /**
     * constructor
     *
     * @return void
     * @access private
     */
    private function __construct() {
    }

    /**
     * create and return an service instance
     *
     * @param  string    $api  the API mode
     * @access public
     * @throws Exception throws Exception if any errors occur
     * @static
     */
    public static function getInstance($api) {
        switch ($api) {
	        case self::API_DIARY:
	        case self::API_ANTENNA:
	        case self::API_RSS:
	        case self::API_GROUP:
	        case self::API_STAR:
	            require_once 'Services/Hatena/Favorites/' . $api . '.php';
	            $classname = 'Services_Hatena_Favorites_' . $api;
	            if (class_exists($classname)) {
	                return new Services_Hatena_Favorites(new $classname());
	            } else {
	                throw new Exception('missing the class "' . $classname . '"');
	            }
	            break;
	        default :
	            throw new Exception('Invalid API "' . $api . '"');
        }
	}
}
