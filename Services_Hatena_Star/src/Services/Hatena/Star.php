<?php

/**
 * Webservices for Hatena::Star
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Hatena_Star
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.2
 * @link      http://d.hatena.ne.jp/shimooka/
 */

require_once 'PEAR.php';
require_once 'HTTP/Request.php';

/**
 * Webservices for Hatena_Star
 *
 * @category  Services
 * @package   Services_Hatena_Star
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.2
 * @link      http://d.hatena.ne.jp/shimooka/
 * @see       References to other sections (if any)...
 */
class Services_Hatena_Star {

    /**
     * raw data
     * @var    array
     * @access private
     */
    private $raw_data;

    /**
     * the User-Agent you use
     * @var    string
     * @access private
     */
    private $user_agent;

    /**
     * the given URL for a feed
     * @var    unknown
     * @access private
     */
    private $url;

    /**
     * the current request mode
     * @var    unknown
     * @access private
     */
    private $request_mode;

    /**
     * the 'entry' request mode.
     *
     * It will return the summarized data
     */
	const REQUEST_ENTRY = 'entry';

    /**
     * the 'entries' request mode.
     *
     * It will return the all of user data who 'stared'
     */
	const REQUEST_ENTRIES = 'entries';

    /**
     * the version number of this package
     */
	const VERSION = '0.0.2';

    /**
     * a part of User-Agent string (without Services_Hatena_Star::VERSION)
     */
	const BASE_USER_AGENT = 'Services_Hatena_Star/';

    /**
     * a part of the request URL string
     */
	const BASE_URL = 'http://s.hatena.ne.jp/';

    /**
     * Constructor
     *
     * @param  string $url the 'stared' URL
     * @param  string $request_mode the current request mode
     * @return void
     * @access public
     * @throws Exception
     */
    public function __construct($url, $request_mode = 'entry') {
        if (is_null($url) || $url === '') {
            throw new Exception('Invalid feed URL : [' . $url . ']');
        }
        $this->url = $url;
        $this->setRequestMode($request_mode);
        $this->setUserAgent();
        $this->raw_data = null;
    }

    /**
     * set the current request mode
     *
     * @param string the current request mode
     * @access public
     */
    public function setRequestMode($request_mode) {
        if ($this->validateMode($request_mode) === true) {
			$this->request_mode = $request_mode;
			$this->clear();
		}
    }

    /**
     * get the current request mode
     *
     * @return string the current request mode
     * @access public
     */
    public function getRequestMode() {
		return $this->request_mode;
    }

    /**
     * return the version number of this package
     *
     * @return string the version number
     * @access public
     */
    public function getVersion() {
        return Services_Hatena_Star::VERSION;
    }

    /**
     * return the User-Agent you set
     *
     * @return string the User-Agent
     * @access public
     */
    public function getUserAgent() {
        return $this->user_agent;
    }

    /**
     * set the User-Agent you want
     *
     * @param  string $user_agent the User-Agent
     * @return void
     * @access public
     */
    public function setUserAgent($user_agent = null) {
        if (is_null($user_agent) || $user_agent === '') {
            $this->user_agent = Services_Hatena_Star::BASE_USER_AGENT . Services_Hatena_Star::VERSION;
        } else {
            $this->user_agent = $user_agent;
        }
    }

    /**
     * return the current 'stared' URL
     *
     * @return string the current 'stared' URL
     * @access public
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * set the current 'stared' URL
     *
     * @param string the current 'stared' URL
     * @access public
     */
    public function setUrl($url) {
        if (is_null($url) || $url === '') {
			$this->url = $url;
			$this->clear();
		}
    }

    /**
     * return the current request URL
     *
     * @return string Return the current request URL
     * @access public
     */
    public function getRequestUrl() {
        return $this->buildRequestUrl();
    }

    /**
     * return a current rank
     *
     * @return mixed  a current rank
     * @access public
     */
    public function execute() {
        if (is_null($this->raw_data)) {
            $this->fetch();
        }
        $class_name = 'Services_Hatena_Star_' . ucfirst($this->getRequestMode()) . 'Data';
        $class_file_name = 'Services/Hatena/Star/' . ucfirst($this->getRequestMode()) . 'Data.php';
        require_once $class_file_name;
        return new $class_name($this->raw_data);
    }

    /**
     * execute a request and fetch data
     *
     * @return boolean true
     * @access private
     * @throws Exception throws Exception if any errors occur
     */
    private function fetch() {
        $req = new HTTP_Request($this->buildRequestUrl());
        $req->addHeader('User-Agent', $this->getUserAgent());
        $ret = $req->sendRequest();
        if (PEAR::isError($ret)) {
            throw new Exception('Hatena_Star: failed to send request');
        }
        switch ($req->getResponseCode()) {
        case 200:
            break;
        default:
            throw new Exception('Hatena_Star: return HTTP ' . $req->getResponseCode());
        }

        $this->raw_data = $req->getResponseBody();
        return true;
    }

    /**
     * validate the current request mode
     *
     * @param  string    $request_mode the current request mode
     * @return boolean   true if success. false will never return
     * @access private
     * @throws Exception throw if the mode is invalid
     */
	private function validateMode($request_mode) {
		switch ($request_mode) {
		case Services_Hatena_Star::REQUEST_ENTRY:
		case Services_Hatena_Star::REQUEST_ENTRIES:
			return true;
		default:
            throw new Exception('Hatena_Star: Invalid mode ' . $request_mode);
		}
	}

    /**
     * build a request url
     *
     * @return string  Return a built url
     * @access private
     */
	private function buildRequestUrl() {
		return Services_Hatena_Star::BASE_URL .
		       $this->getRequestMode() . '.json?uri=' .
		       urlencode($this->getUrl());
	}

    /**
     * clear the internal cache of the result
     *
     * @return void
     * @access public
     */
    public function clear() {
        $this->raw_data = null;
    }

}
