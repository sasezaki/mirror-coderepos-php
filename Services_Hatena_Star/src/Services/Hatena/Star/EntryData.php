<?php

/**
 * Webservices for Hatena::Star
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Hatena_Star
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.2
 * @link      http://d.hatena.ne.jp/shimooka/
 */

require_once 'Services/Hatena/Star/Entry.php';

/**
 * 'Bean class' for entry
 *
 * @category  Services
 * @package   Services_Hatena_Star
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.2
 * @link      http://d.hatena.ne.jp/shimooka/
 */
final class Services_Hatena_Star_EntryData implements Services_Hatena_Star_Entry {

    /**
     * the raw JSON data that returned from hatena::star
     * @var    string
     * @access private
     */
	private $raw_data;

    /**
     * the array of user names who 'stared'
     * @var    array
     * @access private
     */
	private $stars_as_array;

    /**
     * Constructor
     *
     * @param  the raw JSON data that returned from hatena::star
     * @access public
     */
	public function __construct($raw_data) {
		$this->raw_data = json_decode($raw_data);
		$this->stars_as_array = null;
	}

    /**
     * return either you can comment or not
     *
     * @return boolean true if you can comment, false if not
     * @access public
     */
	public function canComment() {
		return ($this->raw_data->can_comment === 1);
	}

    /**
     * return the 'stars' data
     *
     * @return array stars data
     * @access public
     */
	public function getStars() {
		return isset($this->raw_data->entries[0]->stars) ? $this->raw_data->entries[0]->stars : null;
	}

    /**
     * return the 'stared' URI
     *
     * @return string the 'stared' URI
     * @access public
     */
	public function getUri() {
		return isset($this->raw_data->entries[0]->uri) ? $this->raw_data->entries[0]->uri : null;
	}

    /**
     * return the raw JSON data
     *
     * @return object the raw JSON data
     * @access public
     */
	public function getRaw() {
		return isset($this->raw_data) ? $this->raw_data : null;
	}

    /**
     * return the array of string user names who 'stared'
     *
     * @return array  the array of string user names who 'stared'
     * @access public
     */
	public function getStarsAsArray() {
		if (!is_null($this->stars_as_array)) {
			return $this->stars_as_array;
		}
		$this->stars_as_array = array();
		if (is_null($this->getStars())) {
			return $this->stars_as_array;
		}
		foreach ($this->getStars() as $obj) {
			if ($obj->count === '') {
				if (!isset($this->stars_as_array[$obj->name])) {
					$this->stars_as_array[$obj->name] = 0;
				}
				$this->stars_as_array[$obj->name]++;
			} else {
				$this->stars_as_array[$obj->name] = ($obj->count === '' ? 1 : $obj->count);
			}
		}
		arsort($this->stars_as_array);
		return $this->stars_as_array;
	}
}