<?php
//* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Webservices for Heartrails Expres
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Heartrails_Expres
 * @author    Taiji Inoue <inudog@gmail.com>
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.2
 * @see       http://express.heartrails.com/
 */

require_once 'PEAR.php';
require_once 'HTTP/Request.php';
require_once 'XML/Unserializer.php';

class Services_Heartrails_Express
{
    var $url = 'http://express.heartrails.com/api/xml';
    var $http;

    function Services_Heartrails_Express()
    {
        $this->http = new HTTP_Request();
        $options = array('complexType'       => 'array');
        $this->unserializer = new XML_Unserializer($options);
    }

    function getAreas()
    {
        return $this->_callMethod('getAreas', 'area');
    }


    function getPrefectures()
    {
        return $this->_callMethod('getPrefectures', 'prefecture');
    }

    /**
     * @param array $parameters
     *                 - area
     *                 - prefecture
     *
     * @return array
     */
    function getLines($parameters)
    {
        return $this->_callMethod('getLines', 'line', $parameters);
    }

    /**
     * @param array $parameters
     *                 - line
     *                 - name
     *                 - prefecture
     *
     * @return array
     */
    function getStations($parameters)
    {
        return $this->_callMethod('getStations', 'station', $parameters);
    }

    /**
     * @param array $parameters
     *                 - x
     *                 - y
     *
     * @return array
     */
    function getNerStations($parameters)
    {
        return $this->getStations($parameters);
    }

    /**
     * @param string $method
     * @param string $key
     * @param array  $parameters
     *
     * @return array
     */
    function _callMethod($method, $key, $parameters = array())
    {
        $url = sprintf("%s?method=%s", $this->url, $method);
        if ($parameters) {
            $url .= "&" . $this->_buildParameters($parameters);
        }

        $this->http->seturl($url);
        $res = $this->http->sendRequest();

        if ($this->http->getResponseCode() != 200) {
            return PEAR::raiseError('bad response.');
        }

        $xml = $this->http->getResponseBody();
        if ($this->unserializer->unserialize($xml)) {
            $array = $this->unserializer->getUnserializedData();
            if (array_key_exists('error', $array)) {
                return PEAR::raiseError($array['error']);
            } elseif (array_key_exists($key, $array)) {
                return $array[$key];
            }
            return $array;
        } else {
            return PEAR::raiseError('xml parse error.');
        }
    }

    /**
     * @param array
     * @return string
     */
    function _buildParameters($parameters)
    {
        $array = array();
        foreach ($parameters as $k => $v) {
            $array[] = sprintf("%s=%s", $k, urlencode($v));
        }
        return join("&", $array);
    }

}

