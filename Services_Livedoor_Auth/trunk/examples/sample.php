<?php
require_once 'Services/Livedoor/Auth.php';
$livedoor = new Service_Livedoor_Auth('***', '***');
$login = $livedoor->getSignInUrl();

session_start();

if (isset($_GET['logout'])) {
    unset($_SESSION['login']);
    echo "ログアウトしました。<a href='livedoor.php'>戻る</a>";
} else if (isset($_SESSION['login'])) {
    echo "ログイン中";
    echo "<br /><a href='livedoor.php?logout=1'>ログアウトする</a><pre>";
    print_r($_SESSION);
    echo "</pre>";
} else if (isset($_GET['sig']) && $livedoor->validate($_GET['sig'], $_GET) === true) {
    $_SESSION['id'] = $livedoor->getName();
    $_SESSION['login'] = true;
    echo "認証成功！";
    echo "<br /><a href='livedoor.php'>戻る</a>";
} else {
    echo "<a href='$login'>ここから認証</a>";
}
