<?php

/**
 * Muboh Labs
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Muboh
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.1.0
 * @see       http://labs.unoh.net/2008/04/api.html
 */

require_once 'HTTP/Request.php';

/**
 * Muboh Labs puchipuchi
 *
 * @category  Services
 * @package   Services_Muboh
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.1.0
 * @link      http://pear.php.net/package/Services_Muboh
 * @see       http://labs.unoh.net/2008/04/api.html
 */
class Services_Muboh {

    /**
     * API URL
     */
    const API_URL = 'http://m.unoh.net/puchipuchi?';

    /**
     * API response
     *
     * @var    SimpleXMLElement
     * @access private
     */
    private $result;

    /**
     * constructor
     *
     * @return void
     * @access public
     */
    public function __construct() {
        $this->result = null;
    }

    /**
     * execute
     *
     * @return void
     * @access public
     */
    public function execute($staff, $action, $comment) {
        $request = new HTTP_Request($this->getRequestUrl($staff, $action, $comment));
        if (PEAR::isError($request->sendRequest())) {
            throw new RuntimeException($request->getMessage());
        }
        $this->result = new SimpleXMLElement($request->getResponseBody());
    }

    /**
     * return the request url
     *
     * @param string $staff staff name
     * @param string $action action name
     * @param string $comment comment
     * @return string  the request url
     * @access private
     */
    private function getRequestUrl($staff, $action, $comment) {
        $qs = array('staff' => $staff,
                    'action' => $action,
                    'comment' => $comment);
        return Services_Muboh::API_URL . join('&', $qs);
    }

    /**
     * return the status code
     *
     * @return string status code
     * @access public
     * @throws RuntimeException when API is not called yet
     */
    public function getStatus() {
        if (is_null($this->result)) {
            throw new RuntimeException('API is not called yet');
        }
        return $this->result['stat'];
    }

    /**
     * return the response comment
     *
     * @return string the response comment
     * @access public
     * @throws RuntimeException when API is not called yet
     */
    public function getComment() {
        if (is_null($this->getResponse())) {
            throw new RuntimeException('API not called yet');
        }
        return $this->getResponse()->comment;
    }

    /**
     * return the voice url
     *
     * @return string the voice url
     * @access public
     * @throws RuntimeException when API is not called yet
     */
    public function getVoice() {
        if (is_null($this->getResponse())) {
            throw new RuntimeException('API not called yet');
        }
        return $this->getResponse()->voice;
    }

    /**
     * return the response in a SimpleXMLElement object
     *
     * @return SimpleXMLElement the response in a SimpleXMLElement object
     * @access public
     * @throws RuntimeException when API is not called yet
     */
    public function getResponse() {
        if (is_null($this->result)) {
            throw new RuntimeException('API not called yet');
        }
        return $this->result;
    }

}
