<?php
ini_set("include_path", dirname(__FILE__)."/src/" . PATH_SEPARATOR . ini_get("include_path"));
error_reporting(E_ALL);

function invoke($obj)
{
    try {
        var_dump($obj->getRequestUrl());
        var_dump($obj->invoke());
    } catch (Exception $e) {
        var_dump($e->getMessage());
    }
}


// Test
require_once "Services/Pathtraq/Factory.php";

/**
 * ニュースランキング取得API
 */
$obj = Services_Pathtraq_Factory::getInstance(Services_Pathtraq_Factory::API_NEWS_RANKING);
invoke($obj);

/**
 * カテゴリランキング取得API
 */
$obj = Services_Pathtraq_Factory::getInstance(Services_Pathtraq_Factory::API_CATEGORY_RANKING);
$obj->setCategory(Services_Pathtraq_Category::ANIME);
$obj->setScope(Services_Pathtraq_Scope::POPULAR);
invoke($obj);

/**
 * キーワード・URL検索API
 */
$obj = Services_Pathtraq_Factory::getInstance(Services_Pathtraq_Factory::API_KEYWORD_SEARCH);
$obj->addKeyword('ソフトバンク');
$obj->addCategory(Services_Pathtraq_Category::MOBILE);
invoke($obj);

$obj = Services_Pathtraq_Factory::getInstance(Services_Pathtraq_Factory::API_KEYWORD_SEARCH);
$obj->addPrevsite('http://d.hatena.ne.jp/shimooka/');
invoke($obj);

/**
 * URL正規化API
 */
$obj = Services_Pathtraq_Factory::getInstance(Services_Pathtraq_Factory::API_NORMALIZE_URL);
$obj->setUrl('http://www.amazon.co.jp/exec/obidos/ASIN/4166605917/cybozulabs-22/');
invoke($obj);

$obj->setUrl('http://d.hatena.ne.jp/shimooka');
invoke($obj);

/**
 * ページカウンタAPI
 */
$obj = Services_Pathtraq_Factory::getInstance(Services_Pathtraq_Factory::API_PAGE_COUNTER);
$obj->setUrl('http://d.hatena.ne.jp/shimooka/');
invoke($obj);

/**
 * ページチャートAPI
 */
$obj = Services_Pathtraq_Factory::getInstance(Services_Pathtraq_Factory::API_PAGE_CHART);
$obj->setUrl('http://d.hatena.ne.jp/shimooka/');
$obj->setScale(Services_Pathtraq_Scale::MONTH);
invoke($obj);

