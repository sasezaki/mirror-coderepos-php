<?php

/**
 * Web api wrapper for pathtraq 'news'
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Pathtraq
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Pathtraq
 * @see       http://pathtraq.com/developer
 */

require_once 'Services/Pathtraq.php';
require_once 'Services/Pathtraq/Genre.php';
require_once 'Services/Pathtraq/Scope.php';

/**
 * Web api wrapper class for pathtraq 'news'
 *
 * @category  Services
 * @package   Services_Pathtraq
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Pathtraq
 * @see       http://pathtraq.com/developer
 */
class Services_Pathtraq_NewsRanking extends Services_Pathtraq
{
    /**
     * returning 'RSS' format (default)
     */
    const FORMAT_RSS = 'rss';

    /**
     * returning 'JSON' format
     */
    const FORMAT_JSON = 'json';


    /**
     * the current genre
     * @var    string
     * @access private
     */
    private $genre = null;

    /**
     * the current scope
     * @var    string
     * @access private
     */
    private $scope = null;

    /**
     * the current callback function name
     * @var    string
     * @access private
     */
    private $callback = null;

    /**
     * constructor
     *
     * @param  string $apikey the apikey string
     * @return void
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * return the API url. you must override in a subclass.
     *
     * @access protected
     */
    protected function getApiUrl()
    {
        return 'http://api.pathtraq.com/news_ja';
    }

    /**
     * build the Query-String string
     *
     * @return string   built the Query-String string
     * @access protected
     */
    protected function buildParameters()
    {
        $params = '&genre=' . $this->getGenre()
                . '&m=' . $this->getScope()
                . '&callback=' . $this->getCallback();
        return $params;
    }

    /**
     * set the current returning format
     *
     * @param  string    $format the current returning format
     * @return void
     * @access public
     * @throws Exception throws Exception if the format string is invalid
     */
    public function setFormat($format)
    {
        switch ($format) {
        case self::FORMAT_RSS:
        case self::FORMAT_JSON:
            $this->format = $format;
            break;
        default:
            throw new UnexpectedValueException('Invalid format "' . $format . '"');
        }
    }


    /**
     * return the current callback function name
     *
     * @return string the current callback function name
     * @access public
     */
    public function getCallback() {
        return $this->callback;
    }

    /**
     * set the callback function name
     *
     * @return string the callback function name
     * @access public
     */
    public function setCallback($callback) {
        $this->callback = $callback;
    }

    /**
     * return the current genre
     *
     * @return string the current genre
     * @access public
     */
    public function getGenre() {
        return $this->genre;
    }

    /**
     * set the genre
     *
     * @return string the genre
     * @access public
     */
    public function setGenre($genre) {
        if (!Services_Pathtraq_Genre::has($genre)) {
            throw new UnexpectedValueException('Invalid genre "' . $genre . '"');
        }
        $this->genre = $genre;
    }

    /**
     * clear the current genre
     *
     * @return string the genre
     * @access public
     */
    public function clearGenre() {
        $this->genre = $genre;
    }

    /**
     * return the current scope
     *
     * @return string the current scope
     * @access public
     */
    public function getScope() {
        return $this->scope;
    }

    /**
     * set the scope
     *
     * @return string the scope
     * @access public
     */
    public function setScope($scope) {
        if (!Services_Pathtraq_Scope::has($scope)) {
            throw new UnexpectedValueException('Invalid scope "' . $scope . '"');
        }
        $this->scope = $scope;
    }
}
