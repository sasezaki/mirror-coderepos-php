<?php

/**
 * Web api wrapper class for pathtraq 'normalize url2'
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Pathtraq
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Pathtraq
 * @see       http://pathtraq.com/developer
 */

require_once 'Services/Pathtraq.php';

/**
 * Web api wrapper class for pathtraq 'normalize url2'
 *
 * @category  Services
 * @package   Services_Pathtraq
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Pathtraq
 * @see       http://pathtraq.com/developer
 */
class Services_Pathtraq_NormalizeUrl extends Services_Pathtraq
{
    /**
     * returning 'RSS' format (default)
     */
    const FORMAT_XML = 'xml';

    /**
     * returning 'JSON' format
     */
    const FORMAT_JSON = 'json';


    /**
     * the current url
     * @var    string
     * @access private
     */
    private $url = null;

    /**
     * return the API url. you must override in a subclass.
     *
     * @access protected
     */
    protected function getApiUrl()
    {
        return 'http://api.pathtraq.com/normalize_url2';
    }

    /**
     * build the Query-String string
     *
     * @return string   built the Query-String string
     * @access protected
     */
    protected function buildParameters()
    {
        $params = '&url=' . urlencode($this->getUrl());
        return $params;
    }

    /**
     * set the current returning format
     *
     * @param  string    $format the current returning format
     * @return void
     * @access public
     * @throws Exception throws Exception if the format string is invalid
     */
    public function setFormat($format)
    {
        switch ($format) {
        case self::FORMAT_XML:
        case self::FORMAT_JSON:
            $this->format = $format;
            break;
        default:
            throw new UnexpectedValueException('Invalid format "' . $format . '"');
        }
    }


    /**
     * return the current url
     *
     * @return string the current url
     * @access public
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * set the url
     *
     * @return string the url
     * @access public
     */
    public function setUrl($url) {
        $this->url = $url;
    }
}
