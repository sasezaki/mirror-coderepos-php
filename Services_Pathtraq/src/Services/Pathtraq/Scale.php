<?php
final class Services_Pathtraq_Scale
{
    const DAY = '24h';
    const WEEK = '1w';
    const MONTH = '1m';
    const MONTHS = '3m';

    public static function has($key)
    {
        static $constants;
        $reflection = new ReflectionClass(__CLASS__);
        $constants = $reflection->getConstants();

        foreach ($constants as $value) {
            if ($value === $key) {
                return true;
            }
        }
        return false;
    }
}
