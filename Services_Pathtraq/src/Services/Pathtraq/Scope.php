<?php
final class Services_Pathtraq_Scope
{
    const HOT = 'hot';
    const POPULAR = 'popular';
    const UPCOMING = 'upcoming';

    public static function has($key)
    {
        return defined('Services_Pathtraq_Scope::' . strtoupper($key));
    }
}
