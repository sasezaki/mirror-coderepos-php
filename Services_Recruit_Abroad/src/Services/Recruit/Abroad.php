<?php

/**
 * Web api wrapper for ab-road
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Recruit_Abroad
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Recruit_Abroad
 */

require_once 'PEAR.php';
require_once 'HTTP/Request.php';

/**
 * Abstract service class
 *
 * @category  Services
 * @package   Services_Recruit_Abroad
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Recruit_Abroad
 * @see       http://webservice.recruit.co.jp/ab-road/
 */
abstract class Services_Recruit_Abroad {

    /**
     * the version number of this package
     */
	const VERSION = '0.0.1';

    /**
     * a part of User-Agent string (without self::VERSION)
     */
	const BASE_USER_AGENT = 'Services_Recruit_Abroad';

    /**
     * returning 'XML' format (default)
     */
	const FORMAT_XML = 'xml';

    /**
     * returning 'JSON' format
     */
	const FORMAT_JSON = 'json';

    /**
     * returning 'JSONP' format
     */
	const FORMAT_JSONP = 'jsonp';

    /**
     * the apikey string
     * @var    string
     * @access private
     */
	private $apikey;

    /**
     * the current returning format
     * @var    string
     * @access private
     */
	private $format;

    /**
     * the current callback function name
     * @var    string
     * @access private
     */
	private $callback;

    /**
     * the current User-Agent used by HTTP_Request
     * @var    string
     * @access private
     */
    private $user_agent;

    /**
     * constructor
     *
     * @param  string $apikey the apikey string
     * @return void
     * @access public
     */
	public function __construct($apikey) {
		$this->apikey = $apikey;
		$this->format = self::FORMAT_XML;
		$this->setUserAgent();
	}

    /**
     * return an apikey
     *
     * @return  string an apikey
     * @access public
     */
	public function getApiKey() {
		return $this->apikey;
	}
//	public function setApiKey() {
//		return $this->apikey;
//	}


    /**
     * return the current returning format
     *
     * @return string the current returning format, 'xml' or 'json' or 'jsonp'
     * @access public
     */
	public function getFormat() {
		return $this->format;
	}

    /**
     * set the current returning format
     *
     * @param  string    $format the current returning format, 'xml' or 'json' or 'jsonp'. Also you can use the class constants 'FORMAT_XML', 'FORMAT_JSON', 'FORMAT_JSONP'
     * @return void
     * @access public
     * @throws Exception throws Exception if the format string is invalid
     */
	public function setFormat($format) {
		switch ($format) {
		case self::FORMAT_XML:
		case self::FORMAT_JSON:
		case self::FORMAT_JSONP:
			$this->format = $format;
			break;
		default:
            throw new Exception('Invalid format "' . $format . '"');
		}
	}

    /**
     * return the current callback function name
     *
     * @return string the current callback function name
     * @access public
     */
	public function getCallback() {
		return $this->callback;
	}

    /**
     * set the callback function name
     *
     * @return string the callback function name
     * @access public
     */
	public function setCallback() {
		return $this->callback;
	}

    /**
     * invoke the request
     *
     * @return string the response body if success.
     * @access public
     */
	public function invoke() {
		return  $this->sendRequest();
	}

    /**
     * return the current User-Agent used by HTTP_Request
     *
     * @return string the current User-Agent
     * @access public
     */
    public function getUserAgent() {
        return $this->user_agent;
    }

    /**
     * set the User-Agent used by HTTP_Request
     *
     * @param  string $user_agent the User-Agent used by HTTP_Request
     * @return void
     * @access public
     */
    public function setUserAgent($user_agent = null) {
        if (is_null($user_agent) || $user_agent === '') {
            $this->user_agent = self::BASE_USER_AGENT . '/' . self::VERSION;
        } else {
            $this->user_agent = $user_agent;
        }
    }

    /**
     * send the request
     *
     * @return mixed     the 'raw' response body
     * @access private
     * @throws Exception throws Exception if any errors occur
     */
    private function sendRequest() {
        $req = new HTTP_Request($this->buildRequestUrl());
        $req->addHeader('User-Agent', $this->getUserAgent());

        $ret = $req->sendRequest();
        if (PEAR::isError($ret)) {
            throw new Exception('Services_Recruit_Abroad: failed to send request');
        }
        switch ($req->getResponseCode()) {
        case 200:
            break;
        default:
            throw new Exception('Services_Recruit_Abroad: return HTTP ' . $req->getResponseCode());
        }
        return $req->getResponseBody();
	}

    /**
     * build the request url
     *
     * @return string the request url
     * @access private
     */
	private function buildRequestURL() {
		$url = $this->getApiUrl()
		     . '?key=' . $this->getApiKey()
		     . $this->buildParameters()
		     . '&format=' . $this->getFormat();
		return $url;
	}

    /**
     * return the API url. you must override in a subclass.
     *
     * @access protected
     */
	abstract protected function getApiUrl();

    /**
     * build the Query-String string. you must override in a subclass.
     *
     * @access protected
     */
	abstract protected function buildParameters();
}
