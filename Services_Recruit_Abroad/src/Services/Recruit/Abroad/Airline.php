<?php

/**
 * Web api wrapper for ab-road 'Airline'
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Recruit_Abroad
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Recruit_Abroad
 * @see       http://webservice.recruit.co.jp/ab-road/reference.html
 */

require_once 'Services/Recruit/Abroad.php';

/**
 * Web api wrapper class for ab-road 'Airline'
 *
 * @category  Services
 * @package   Services_Recruit_Abroad
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Recruit_Abroad
 * @see       http://webservice.recruit.co.jp/ab-road/reference.html
 */
class Services_Recruit_Abroad_Airline extends Services_Recruit_Abroad {

    /**
     * a airline name
     * @var array
     * @access private
     */
    private $airline;

    /**
     * your search keyword
     * @var array
     * @access private
     */
    private $keyword;

    /**
     * the number of the first row
     * @var int
     * @access private
     */
    private $start;

    /**
     * the row number of fetching data
     * @var int
     * @access private
     */
    private $count;

    /**
     * constructor
     *
     * @param  string $apikey the apikey string
     * @return void
     * @access public
     */
    public function __construct($apikey) {
        parent::__construct($apikey);
        $this->airline = array();
        $this->keyword = array();
        $this->start = null;
        $this->count = null;
    }

    /**
     * return the API url
     *
     * @return string    the API url
     * @access protected
     */
	protected function getApiUrl() {
		return 'http://webservice.recruit.co.jp/ab-road/airline/v1/';
	}

    /**
     * build the Query-String string
     *
     * @return string   built the Query-String string
     * @access protected
     */
    protected function buildParameters() {
        return '' .
               $this->buildAirlineParameters() .
               $this->buildKeywordParameters() .
               $this->buildStartParameters() .
               $this->buildCountParameters() .
               '';
    }


    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildAirlineParameters() {
        $params = '';
        foreach ($this->getAirline() as $key => $dummy) {
            $params .= '&airline=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildKeywordParameters() {
        $params = '';
        foreach ($this->getKeyword() as $key => $dummy) {
            $params .= '&keyword=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildStartParameters() {
        $params = '';
        if (!is_null($this->getStart())) {
            $params .= '&start=' . $this->getStart();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildCountParameters() {
        $params = '';
        if (!is_null($this->getCount())) {
            $params .= '&count=' . $this->getCount();
        }
        return $params;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putAirline(array $code) {
        $this->airline = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addAirline($code) {
        $this->airline[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeAirline($code) {
        if (isset($this->airline[$code])) {
            unset($this->airline[$code]);
        }
    }

    /**
     * return a airline name
     *
     * @return type a airline name
     * @access public
     */
    public function getAirline() {
        return $this->airline;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putKeyword(array $code) {
        $this->keyword = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addKeyword($code) {
        $this->keyword[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeKeyword($code) {
        if (isset($this->keyword[$code])) {
            unset($this->keyword[$code]);
        }
    }

    /**
     * return your search keyword
     *
     * @return type your search keyword
     * @access public
     */
    public function getKeyword() {
        return $this->keyword;
    }

    /**
     * set the number of the first row
     *
     * @param  int start the number of the first row
     * @return void
     * @access public
     */
    public function setStart($start) {
        $start = $this->validateDigit($start);
        $this->start = $start;
    }

    /**
     * return the number of the first row
     *
     * @return type the number of the first row
     * @access public
     */
    public function getStart() {
        return $this->start;
    }

    /**
     * set the row number of fetching data
     *
     * @param  int count the row number of fetching data
     * @return void
     * @access public
     */
    public function setCount($count) {
        $count = $this->validateDigit($count);
        $this->count = $count;
    }

    /**
     * return the row number of fetching data
     *
     * @return type the row number of fetching data
     * @access public
     */
    public function getCount() {
        return $this->count;
    }

    /**
     * validate if the given value is digit, and return the int value
     *
     * @param  string    $param a parameter
     * @return int     a int value of the given parameter
     * @access private
     * @throws Exception throws when the given parameter is not digit
     */
    private function validateDigit($param) {
        if (!preg_match('/^\d+$/', $param)) {
            throw new Exception('Invalid format "' . $param . '"');
        }
        return (int)$param;
    }
}
