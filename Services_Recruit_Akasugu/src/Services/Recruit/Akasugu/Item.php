<?php

/**
 * Web api wrapper for akasugu 'Item'
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Recruit_Akasugu
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Recruit_Akasugu
 * @see       http://webservice.recruit.co.jp/akasugu/reference.html
 */

require_once 'Services/Recruit/Akasugu.php';

/**
 * Web api wrapper class for akasugu 'Item'
 *
 * @category  Services
 * @package   Services_Recruit_Akasugu
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Recruit_Akasugu
 * @see       http://webservice.recruit.co.jp/akasugu/reference.html
 */
class Services_Recruit_Akasugu_Item extends Services_Recruit_Akasugu {

    /**
     * the insertion code
     * @var    string
     * @access private
     */
	private $code;

    /**
     * the company name
     * @var    string
     * @access private
     */
	private $company;

    /**
     * the brand name
     * @var    string
     * @access private
     */
	private $brand;

    /**
     * the item name
     * @var    string
     * @access private
     */
	private $name;

    /**
     * the large category codes
     * @var    array
     * @access private
     */
	private $large_category_cd;

    /**
     * the middle category codes
     * @var    array
     * @access private
     */
	private $middle_category_cd;

    /**
     * the small category codes
     * @var    array
     * @access private
     */
	private $small_category_cd;

    /**
     * the age code
     * @var    string
     * @access private
     */
	private $age_cd;

    /**
     * the keyword of the items
     * @var    string
     * @access private
     */
	private $keyword;

    /**
     * the minimum price
     * @var    int
     * @access private
     */
	private $price_min;

    /**
     * the maximum price
     * @var    int
     * @access private
     */
	private $price_max;

    /**
     * the sort order code
     * @var    int
     * @access private
     */
	private $order;

    /**
     * the first row number
     * @var    int
     * @access private
     */
	private $start;

    /**
     * the row number of fetching data
     * @var    int
     * @access private
     */
	private $count;

    /**
     * the recommended order
     */
	const ORDER_RECOMMEND = 0;

    /**
     * the order of the price
     */
	const ORDER_LOWPRICE = 1;

    /**
     * the reversed order of the price
     */
	const ORDER_HIGHPRICE = 2;

    /**
     * the lexical order
     */
	const ORDER_NAME = 3;

    /**
     * constructor
     *
     * @param  string $apikey the apikey string
     * @return void
     * @access public
     */
    public function __construct($apikey) {
        parent::__construct($apikey);
		$this->code = null;
		$this->company = null;
		$this->brand = null;
		$this->name = null;
        $this->large_category_cd = array();
        $this->middle_category_cd = array();
        $this->small_category_cd = array();
        $this->keyword = null;
		$this->age_cd = null;
		$this->price_min = null;
		$this->price_max = null;
		$this->order = null;
		$this->start = null;
		$this->count = null;
    }

    /**
     * return the API url
     *
     * @return string    the API url
     * @access protected
     */
	protected function getApiUrl() {
		return 'http://webservice.recruit.co.jp/akasugu/item/v1/';
	}

    /**
     * build the Query-String string
     *
     * @return string   built the Query-String string
     * @access protected
     */
	protected function buildParameters() {
		return $this->buildCodeParameters() .
		       $this->buildCompanyParameters() .
		       $this->buildBrandParameters() .
		       $this->buildNameParameters() .
		       $this->buildLargeCategoryParameters() .
		       $this->buildMiddleCategoryParameters() .
		       $this->buildSmallCategoryParameters() .
		       $this->buildKeywordParameters() .
		       $this->buildAgeCdParameters() .
		       $this->buildPriceMinParameters() .
		       $this->buildPriceMaxParameters() .
		       $this->buildOrderParameters() .
		       $this->buildStartParameters() .
		       $this->buildCountParameters();
	}

    /**
     * build the 'large_category_cd' part of Query-String string
     *
     * @return string    the 'large_category_cd' part of Query-String string
     * @access protected
     */
	protected function buildLargeCategoryParameters() {
		$params = '';
		foreach ($this->getLargeCategory() as $key => $dummy) {
			$params .= '&large_category_cd=' . $key;
		}
		return $params;
	}

    /**
     * build the 'middle_category_cd' part of Query-String string
     *
     * @return string    the 'middle_category_cd' part of Query-String string
     * @access protected
     */
	protected function buildMiddleCategoryParameters() {
		$params = '';
		foreach ($this->getMiddleCategory() as $key => $dummy) {
			$params .= '&middle_category_cd=' . $key;
		}
		return $params;
	}

    /**
     * build the 'small_category_cd' part of Query-String string
     *
     * @return string    the 'small_category_cd' part of Query-String string
     * @access protected
     */
	protected function buildSmallCategoryParameters() {
		$params = '';
		foreach ($this->getSmallCategory() as $key => $dummy) {
			$params .= '&small_category_cd=' . $key;
		}
		return $params;
	}

    /**
     * build the 'code' part of Query-String string
     *
     * @return string    the 'code' part of Query-String string
     * @access protected
     */
	protected function buildCodeParameters() {
		$params = '';
		if (!is_null($this->getCode())) {
			$params .= '&code=' . $this->getCode();
		}
		return $params;
	}

    /**
     * build the 'company' part of Query-String string
     *
     * @return string    the 'company' part of Query-String string
     * @access protected
     */
	protected function buildCompanyParameters() {
		$params = '';
		if (!is_null($this->getCompany())) {
			$params .= '&company=' . $this->getCompany();
		}
		return $params;
	}

    /**
     * build the 'brand' part of Query-String string
     *
     * @return string    the 'brand' part of Query-String string
     * @access protected
     */
	protected function buildBrandParameters() {
		$params = '';
		if (!is_null($this->getBrand())) {
			$params .= '&brand=' . $this->getBrand();
		}
		return $params;
	}

    /**
     * build the 'name' part of Query-String string
     *
     * @return string    the 'name' part of Query-String string
     * @access protected
     */
	protected function buildNameParameters() {
		$params = '';
		if (!is_null($this->getName())) {
			$params .= '&name=' . $this->getName();
		}
		return $params;
	}

    /**
     * build the 'keyword' part of Query-String string
     *
     * @return string    the 'keyword' part of Query-String string
     * @access protected
     */
	protected function buildKeywordParameters() {
		$params = '';
		if (!is_null($this->getKeyword())) {
			$params .= '&keyword=' . $this->getKeyword();
		}
		return $params;
	}

    /**
     * build the 'age_cd' part of Query-String string
     *
     * @return string    the 'age_cd' part of Query-String string
     * @access protected
     */
	protected function buildAgeCdParameters() {
		$params = '';
		if (!is_null($this->getAgeCd())) {
			$params .= '&age_cd=' . $this->getAgeCd();
		}
		return $params;
	}

    /**
     * build the 'price_min' part of Query-String string
     *
     * @return string    the 'price_min' part of Query-String string
     * @access protected
     */
	protected function buildPriceMinParameters() {
		$params = '';
		if (!is_null($this->getPriceMin())) {
			$params .= '&price_min=' . $this->getPriceMin();
		}
		return $params;
	}

    /**
     * build the 'price_max' part of Query-String string
     *
     * @return string    the 'price_max' part of Query-String string
     * @access protected
     */
	protected function buildPriceMaxParameters() {
		$params = '';
		if (!is_null($this->getPriceMax())) {
			$params .= '&price_max=' . $this->getPriceMax();
		}
		return $params;
	}

    /**
     * build the 'order' part of Query-String string
     *
     * @return string    the 'order' part of Query-String string
     * @access protected
     */
	protected function buildOrderParameters() {
		$params = '';
		if (!is_null($this->getOrder())) {
			$params .= '&order=' . $this->getOrder();
		}
		return $params;
	}

    /**
     * build the 'start' part of Query-String string
     *
     * @return string    the 'start' part of Query-String string
     * @access protected
     */
	protected function buildStartParameters() {
		$params = '';
		if (!is_null($this->getStart())) {
			$params .= '&start=' . $this->getStart();
		}
		return $params;
	}

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
	protected function buildCountParameters() {
		$params = '';
		if (!is_null($this->getCount())) {
			$params .= '&count=' . $this->getCount();
		}
		return $params;
	}

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
	public function putLargeCategory(array $code) {
		$this->large_category_cd = $code;
	}

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
	public function addLargeCategory($code) {
		$this->large_category_cd[$code] = true;
	}

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
	public function removeLargeCategory($code) {
		if (isset($this->large_category_cd[$code])) {
			unset($this->large_category_cd[$code]);
		}
	}

    /**
     * return the array of the large category codes
     *
     * @return array  the array of the large category codes
     * @access public
     */
	public function getLargeCategory() {
		return $this->large_category_cd;
	}

    /**
     * replace the middle category codes
     *
     * @param  array $code an array of the middle category codes
     * @return void
     * @access public
     */
	public function putMiddleCategory(array $code) {
		$this->middle_category_cd = $code;
	}

    /**
     * add the middle category code
     *
     * @param  string $code the middle category code
     * @return void
     * @access public
     */
	public function addMiddleCategory($code) {
		$this->middle_category_cd[$code] = true;
	}

    /**
     * remove the middle category code
     *
     * @param  string $code the middle category code
     * @return void
     * @access public
     */
	public function removeMiddleCategory($code) {
		if (isset($this->middle_category_cd[$code])) {
			unset($this->middle_category_cd[$code]);
		}
	}

    /**
     * return the array of the middle category codes
     *
     * @return array  the array of the middle category codes
     * @access public
     */
	public function getMiddleCategory() {
		return $this->middle_category_cd;
	}

    /**
     * replace the small category codes
     *
     * @param  array $code an array of the small category codes
     * @return void
     * @access public
     */
	public function putSmallCategory(array $code) {
		$this->small_category_cd = $code;
	}

    /**
     * add the small category code
     *
     * @param  string $code the small category code
     * @return void
     * @access public
     */
	public function addSmallCategory($code) {
		$this->small_category_cd[$code] = true;
	}

    /**
     * remove the small category code
     *
     * @param  string $code the small category code
     * @return void
     * @access public
     */
	public function removeSmallCategory($code) {
		if (isset($this->small_category_cd[$code])) {
			unset($this->small_category_cd[$code]);
		}
	}

    /**
     * return the array of the small category codes
     *
     * @return array  the array of the small category codes
     * @access public
     */
	public function getSmallCategory() {
		return $this->small_category_cd;
	}

    /**
     * set the insertion code
     *
     * @param  string $code the insertion code
     * @return void
     * @access public
     */
	public function setCode($code) {
		$this->code = $code;
	}

    /**
     * return the insertion code
     *
     * @return string the insertion code
     * @access public
     */
	public function getCode() {
		return $this->code;
	}

    /**
     * set the company name
     *
     * @param  string $company the company name
     * @return void
     * @access public
     */
	public function setCompany($company) {
		$this->company = $company;
	}

    /**
     * return the company name
     *
     * @return string the company name
     * @access public
     */
	public function getCompany() {
		return $this->company;
	}

    /**
     * set the brand name
     *
     * @param  string $brand the brand name
     * @return void
     * @access public
     */
	public function setBrand($brand) {
		$this->brand = $brand;
	}

    /**
     * return the brand name
     *
     * @return string the brand name
     * @access public
     */
	public function getBrand() {
		return $this->brand;
	}

    /**
     * set the item name
     *
     * @param  string $name the item name
     * @return void
     * @access public
     */
	public function setName($name) {
		$this->name = $name;
	}

    /**
     * return the item name
     *
     * @return string the item name
     * @access public
     */
	public function getName() {
		return $this->name;
	}

    /**
     * set the age code
     *
     * @param  string $age_cd the age code
     * @return void
     * @access public
     */
	public function setAgeCd($age_cd) {
		$this->age_cd = $age_cd;
	}

    /**
     * return the age code
     *
     * @return string the age code
     * @access public
     */
	public function getAgeCd() {
		return $this->age_cd;
	}

    /**
     * set the keyword of the items
     *
     * @param  string $keyword the keyword of the items
     * @return void
     * @access public
     */
	public function setKeyword($keyword) {
		$this->keyword = $keyword;
	}

    /**
     * return the keyword of the items
     *
     * @return string the keyword of the items
     * @access public
     */
	public function getKeyword() {
		return $this->keyword;
	}

    /**
     * set the minimum price
     *
     * @param  int $price_min the minimum price
     * @return void
     * @access public
     */
	public function setPriceMin($price_min) {
		$price_min = $this->validateDigit($price_min);
		$this->price_min = $price_min;
	}

    /**
     * return the minimum price
     *
     * @return int return the minimum price
     * @access public
     */
	public function getPriceMin() {
		return $this->price_min;
	}

    /**
     * set the maximum price
     *
     * @param  int $price_max the maximum price
     * @return void
     * @access public
     */
	public function setPriceMax($price_max) {
		$price_max = $this->validateDigit($price_max);
		$this->price_max = $price_max;
	}

    /**
     * return the maximum price
     *
     * @return int return the maximum price
     * @access public
     */
	public function getPriceMax() {
		return $this->price_max;
	}

    /**
     * set the sort order code
     *
     * @param  int    $order any one of ORDER_RECOMMEND, ORDER_LOWPRICE, ORDER_HIGHPRICE, ORDER_NAME
     * @return void
     * @access public
     * @throws Exception throws if given value is invalid
     */
	public function setOrder($order) {
		switch ($order) {
		case self::ORDER_RECOMMEND:
		case self::ORDER_LOWPRICE:
		case self::ORDER_HIGHPRICE:
		case self::ORDER_NAME:
			break;
		default:
			throw new Exception('Invalid order "' . $order . '"');
		}
		$this->order = $order;
	}

    /**
     * return the the sort order code
     *
     * @return int the sort order code
     * @access public
     */
	public function getOrder() {
		return $this->order;
	}

    /**
     * set the first row number
     *
     * @param  int $start the first row number
     * @return void
     * @access public
     */
	public function setStart($start) {
		$start = $this->validateDigit($start);
		$this->start = $start;
	}

    /**
     * return the first row number
     *
     * @return int return the first row number
     * @access public
     */
	public function getStart() {
		return $this->start;
	}

    /**
     * set the row number of fetching data
     *
     * @param  int $count the row number of fetching data
     * @return void
     * @access public
     */
	public function setCount($count) {
		$count = $this->validateDigit($count);
		$this->count = $count;
	}

    /**
     * return the row number of fetching data
     *
     * @return int return the row number of fetching dataf any) ...
     * @access public
     */
	public function getCount() {
		return $this->count;
	}

    /**
     * validate if the given value is digit, and return the int value
     *
     * @param  string    $param a parameter
     * @return int     a int value of the given parameter
     * @access private
     * @throws Exception throws when the given parameter is not digit
     */
	private function validateDigit($param) {
		if (!preg_match('/^\d+$/', $param)) {
			throw new Exception('Invalid format "' . $param . '"');
		}
		return (int)$param;
	}
}
