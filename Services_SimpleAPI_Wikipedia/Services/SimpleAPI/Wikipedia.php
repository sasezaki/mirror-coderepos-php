<?php
/**
 * Services_SimpleAPI_Wikipedia
 */

/**
 * Services_SimpleAPI_Wikipedia
 *
 * @author halt feits <halt.feits at gmail.com>
 *
 * $wikipedia = new Services_SimpleAPI_Wikipedia();
 * $results = $wikipedia->api('YouTube');
 * foreach ($results as $result) {
 *     var_dump($result);
 * }
 *
 */
class Services_SimpleAPI_Wikipedia
{
    /**
     * api
     *
     * @access public
     * @param string $keyword
     */
    function api($keyword, $search = 1, $output = 'php', $callback = false)
    {
        $keyword = urlencode(mb_convert_encoding($keyword, 'utf-8', 'auto'));
        $url = "http://wikipedia.simpleapi.net/api?keyword={$keyword}&output={$output}";

        if (ini_get('allow_url_fopen')) {
            $result = file_get_contents($url);
        } else {
            //T.B.D
        }

        switch ($output) {
        case 'php':
            return unserialize($result);
        default:
        }
    }
}
?>
