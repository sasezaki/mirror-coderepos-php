--TEST--
verify test 1
--SKIPIF--
--POST--
--POST_RAW--
--GET--
--COOKIE--
--STDIN--
--INI--
--ARGS--
--ENV--
--FILE--
<?php
require_once dirname(dirname(__FILE__)) . '/Services/SimpleAPI/Wikipedia.php';

$wikipedia = new Services_SimpleAPI_Wikipedia();
$results = $wikipedia->api('YouTube');
if (count($results)) {
    print('ok');
} else {
    print('fail');
}
?>
--EXPECT--
ok
