<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Twitter Client Class
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Twitter
 * @author    Takeshi KAWAMOTO <takeshi@transrain.net>
 * @copyright 2007 Takeshi KAWAMOTO <takeshi@transrain.net>
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 * @version   CVS: $Id:$
 * @link      http://www.transrain.net/product/services_twitter/
 */
require_once 'Services/Twitter/Connector.php';

/**
 * Twitter Client Class
 *
 * @category  Services
 * @package   Services_Twitter
 * @author    Takeshi KAWAMOTO <takeshi@transrain.net>
 * @copyright 2007 Takeshi KAWAMOTO <takeshi@transrain.net>
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 * @version   Release: @package_version@
 * @link      http://www.transrain.net/product/services_twitter/
 */
class Services_Twitter
{

    /**
     * URL to get public_timeline data.
     *
     * @access private
     * @var string
     */
    var $sm_public = 'http://twitter.com/statuses/public_timeline.json';

    /**
     * URL to get following_timeline data.
     *
     * @access private
     * @var string
     */
    var $sm_friends = 'http://twitter.com/statuses/friends_timeline.json';

    /**
     * URL to get following_timeline data.
     * (intended users following timeline)
     *
     * @access private
     * @var string
     */
    var $sm_friends_target = 'http://twitter.com/statuses/friends_timeline/%s.json';
    
    /**
     * URL to get user_timeline data.
     *
     * @access private
     * @var string
     */
    var $sm_user = 'http://twitter.com/statuses/user_timeline.json';

    /**
     * URL to get user_timeline data.
     * (intended users timeline)
     *
     * @access private
     * @var string
     */
    var $sm_user_target = 'http://twitter.com/statuses/user_timeline/%s.json';
    
    /**
     * URL to get intended one status.
     *
     * @access private
     * @var string
     */
    var $sm_show = 'http://twitter.com/statuses/show/%d.json';
    
    /**
     * URL to status update.
     *
     * @access private
     * @var string
     */
    var $sm_update = 'http://twitter.com/statuses/update.json';
    
    /**
     * URL to get replies statuses.
     *
     * @access private
     * @var string
     */
    var $sm_replies = 'http://twitter.com/statuses/replies.json';
    
    /**
     * URL to delete intended one status.
     *
     * @access private
     * @var string
     */
    var $sm_destroy = 'http://twitter.com/statuses/destroy/%s.json';
    
    /**
     * URL to get following user list.
     *
     * @access private
     * @var string
     */
    var $um_friends = 'http://twitter.com/statuses/friends.json';
    
    /**
     * URL to get following user list.
     * (intended user's following list)
     *
     * @access private
     * @var string
     */
    var $um_friends_target = 'http://twitter.com/statuses/friends/%s.json';
    
    /**
     * URL to get followers user list.
     *
     * @access private
     * @var string
     */
    var $um_followers = 'http://twitter.com/statuses/followers.json';
    
    /**
     * URL to get login user list with least status.
     *
     * @access private
     * @var string
     */
    var $um_featured = 'http://twitter.com/statuses/featured.json';
    
    /**
     * URL to get intended following user's information.
     *
     * @access private
     * @var string
     */
    var $um_show = 'http://twitter.com/users/show/%s.json';
    
    /**
     * URL to get direct messages.
     *
     * @access private
     * @var string
     */
    var $dm_show = 'http://twitter.com/direct_messages.json';
    
    /**
     * URL to get transmitted direct messages.
     *
     * @access private
     * @var string
     */
    var $dm_sent = 'http://twitter.com/direct_messages/sent.json';
    
    /**
     * URL to send direct message.
     *
     * @access private
     * @var string
     */
    var $dm_new = 'http://twitter.com/direct_messages/new.json';
    
    /**
     * URL to delete intended direct message.
     *
     * @access private
     * @var string
     */
    var $dm_destroy = 'http://twitter.com/direct_messages/destroy/%d.json';
    
    /**
     * URL to add following intended user.
     *
     * @access private
     * @var string
     */
    var $fm_create = 'http://twitter.com/friendships/create/%s.json';
    
    /**
     * URL to remove following intended user.
     *
     * @access private
     * @var string
     */
    var $fm_destroy = 'http://twitter.com/friendships/destroy/%s.json';
    
    /**
     * URL to start credentials session.
     *
     * @access private
     * @var string
     */
    var $am_verify = 'http://twitter.com/account/verify_credentials';
    
    /**
     * URL to stop credentials session.
     *
     * @access private
     * @var string
     */
    var $am_endsess = 'http://twitter.com/end_session';
    
    /**
     * URL to get favorites statuses.
     *
     * @access private
     * @var string
     */
    var $fv_favorites = 'http://twitter.com/favorites.json';
    
    /**
     * URL to get intended user's favorites statuses.
     *
     * @access private
     * @var string
     */
    var $fv_favorites_target = 'http://twitter.com/favorites/%s.json';
    
    /**
     * URL to add intended status to favorites.
     *
     * @access private
     * @var string
     */
    var $fv_create = 'http://twitter.com/favourings/create/%d.json';
    //var $fv_create = 'http://twitter.com/favorites/create/%d.json';
    
    /**
     * URL to remove intended status from favorites.
     *
     * @access private
     * @var string
     */
    var $fv_destroy = 'http://twitter.com/favourings/destroy/%d.json';
    //var $fv_destroy = 'http://twitter.com/favorites/destroy/%d.json';

    /**
     * stored username of twitter.
     *
     * @access private
     * @var string
     */
    var $_user;

    /**
     * stored password of twitter.
     *
     * @access private
     * @var string
     */
    var $_pass;

    /**
     * stored application name.
     *
     * @access private
     * @var string
     */
    var $_app_name;

    /**
     * stored application version.
     *
     * @access private
     * @var string
     */
    var $_app_ver;

    /**
     * stored application status xml url.
     *
     * @access private
     * @var string
     */
    var $_app_url;

    /**
     * stored gotten etag.
     *
     * @access private
     * @var string
     */
    var $_etag;

    /**
     * stored gotten modified time.
     *
     * @access private
     * @var string
     */
    var $_last_modified;

    /**
     * stored json convert flag.
     *
     * @access private
     * @var bool
     */
    var $_json_convert;

    /**
     * stored modified check flag.
     *
     * @access private
     * @var bool
     */
    var $_modified_check;

    /**
     * stored "modified check"'s data directory path.
     *
     * @access private
     * @var string
     */
    var $_cache_dir;
    
    /**
     * Proxy server
     * @var string
     */
    var $_proxy_host;
    
    /**
     * Proxy port
     * @var integer
     */
    var $_proxy_port;
    
    /**
     * Proxy username
     * @var string
     */
    var $_proxy_user;
    
    /**
     * Proxy password
     * @var string
     */
    var $_proxy_pass;
    
    /**
     * class constructor
     *
     * @access public
     * @param string $user     username of twitter
     * @param string $password password of twitter
     *
     */
    function Services_Twitter($user, $password)
    {
        $this->_user = $user;
        $this->_pass = $password;
    }
    
    /**
     * set application's name
     *
     * @access public
     * @param string $name application's name
     * @return string setup value
     */
    function setAppName($name)
    {
        $this->_app_name = $name;
        return $this->_app_name;
    }
    
    /**
     * set application's version
     *
     * @access public
     * @param string $ver application's version
     * @return string setup value
     */
    function setAppVersion($ver)
    {
        $this->_app_ver = $ver;
        return $this->_app_ver;
    }
    
    /**
     * set url of application's information xml
     *
     * @access public
     * @param string $url url of application's information xml
     * @return string setup value
     */
    function setAppURL($url)
    {
        $this->_app_url = $url;
        return $this->_app_url;
    }

    /**
     * set append etag(If-None-Match) to next request
     *
     * @access public
     * @param string $etag send etag to next request
     * @return string setup value
     */
    function setEtag($etag)
    {
        $this->_etag = $etag;
        return $this->_etag;
    }
    
    /**
     * get etag(If-None-Match) from previous request
     * @access public
     * @return string etag from previous request
     */
    function getEtag()
    {
        return $this->_etag;
    }
    
    /**
     * set append last_modified(last_modified) to next request
     *
     * @access public
     * @param string $last_modified send last_modified to next request
     * @return string setup value
     */
    function setLastModified($last_modified)
    {
        $this->_last_modified = $last_modified;
        return $this->_last_modified;
    }
    
    /**
     * get last_modified(last_modified)from previous request
     *
     * @access public
     * @return string last_modified from previous request
     */
    function getLastModified()
    {
        return $this->_last_modified;
    }
    
    /**
     * set modified check file cache direcotry path
     *
     * @access public
     * @param string $dir cache direcotry path
     * @return string last_modified from previous request
     */
    function setCacheDir($dir)
    {
        $this->_cache_dir = $dir;
        return $this->_cache_dir;
    }
    
    /**
     * json convert function enable
     *
     * @access public
     * @return string last_modified from previous request
     */
    function enableJsonConvert()
    {
        $this->_json_convert = true;
        return $this->_json_convert;
    }
    
    /**
     * json convert function disable
     *
     * @access public
     * @return string last_modified from previous request
     */
    function disableJsonConvert()
    {
        $this->_json_convert = false;
        return $this->_json_convert;
    }
    
    /**
     * Modified check enable
     *
     * @access public
     * @return string last_modified from previous request
     */
    function enableModifiedCheck()
    {
        $this->_modified_check = true;
        return $this->_modified_check;
    }
    
    /**
     * Modified check disable
     *
     * @access public
     * @return string last_modified from previous request
     */
    function disableModifiedCheck()
    {
        $this->_modified_check = false;
        return $this->_modified_check;
    }
    
    /**
     * Sets a proxy to be used
     *
     * @param string     Proxy host
     * @param int        Proxy port
     * @param string     Proxy username
     * @param string     Proxy password
     * @access public
     */
    function setProxy($host, $port = 8080, $user = null, $pass = null)
    {
        $this->_proxy_host = $host;
        $this->_proxy_port = $port;
        $this->_proxy_user = $user;
        $this->_proxy_pass = $pass;
    }
    
    /**
     * send update request
     *
     * @access private
     * @param string $url     call update API's URL
     * @param mixed  $values  send values (if array, key is POST name and value is POST value) 
     * @param array  $headers send headers (key is header name and value is header value)
     * @return mixed processed success, return response body. and processed faild, return false.
     */
    function _setData($url, $values, $headers = null)
    {
        $req =& new Services_Twitter_Connector();
        
        if( isset($this->_proxy_host) ) {
            $req->setProxy( $this->_proxy_host
                          , $this->_proxy_port
                          , $this->_proxy_user
                          , $this->_proxy_pass );
        }
        
        if (!$this->checkAuth()) {
            $req->enableBasicAuth($this->_user, $this->_pass);
        }
        
        $req->addHeader('User-Agent', 'PHP/' . phpversion());
        
        if ($this->_app_name !== null) {
            $req->addHeader('X-Twitter-Client', $this->_app_name);
        }
        
        if ($this->_app_ver !== null) {
            $req->addHeader('X-Twitter-Client-Version', $this->_app_ver);
        }
        
        if ($this->_app_url !== null) {
            $req->addHeader('X-Twitter-Client-URL', $this->_app_url);
        }
        
        // header setting
        if ($headers !== null && is_array($headers)) {
            foreach ($headers as $key => $value) {
                $req->addHeader($key, $value);
            }
        }
        
        if (is_array($values)) {
            foreach ($values as $key => $value) {
                $req->addPostData($key, $value);
            }
        } else {
            $req->addBodyData($values);
        }
        
        $response = $req->sendRequest($url, 'POST');
        
        $resp = explode("\r\n", $response);
        $bst  = false;
        $chk  = false;
        $body = array();
        foreach ($resp as $line) {
            if (!$bst && !$chk) {
                // HTTP status validation
                if (preg_match('/^HTTP\\/[0-9]\.[0-9] ([0-9]{3}) .+$/', $line, $matches)) {
                    $status = intval($matches[1]);
                    if ($status !== 200) {
                        return false;
                    } else {
                        $chk = true;
                    }
                }
            }
            if ($bst) {
                $body[] = $line;
            }
            if ($line == '') {
                $bst = true;
            }
        }
        
        $body = implode("\r\n", $body);
        
        if ($this->_json_convert) {
            if (@include_once 'Services/Twitter/Jsphon.php') {
                include_once 'Services/Twitter/Wrapper/Jsphon.php';
                $body = Services_Twitter_Wrapper_Jsphon::decode($body);
            }
        }
        
        return $body;
    }
    
    /**
     * send get data request
     *
     * @access private
     * @param string $method  HTTP request method (POST or GET)
     * @param string $url     call get data API's URL
     * @param string $etagkey etag cache key setting
     * @return mixed processed success, return response body. and processed faild, return false.
     */
    function _getData($method, $url, $etagkey) 
    {
        $id = sha1($etagkey);
        
        if (defined('SERVICES_TWITTER_FORCE_POST') && SERVICES_TWITTER_FORCE_POST) {
            $method = 'POST';
        }
        
        if ($this->_modified_check && $this->_cache_dir !== null) {
            $modf = realpath($this->_cache_dir) . DIRECTORY_SEPARATOR . $id . '.modified';
            $tagf = realpath($this->_cache_dir) . DIRECTORY_SEPARATOR . $id . '.etag';
            
            if (file_exists($modf)) {
                $this->setLastModified(file_get_contents($modf));
            }
            if (file_exists($tagf)) {
                $this->setEtag(file_get_contents($tagf));
            }
        }
        
        $req =& new Services_Twitter_Connector();
        
        if( isset($this->_proxy_host) ) {
            $req->setProxy( $this->_proxy_host
                          , $this->_proxy_port
                          , $this->_proxy_user
                          , $this->_proxy_pass );
        }
        
        if (!$this->checkAuth()) {
            $req->enableBasicAuth($this->_user, $this->_pass);
        }
        
        $req->addHeader('User-Agent', 'PHP/' . phpversion());
        
        if ($this->_app_name !== null) {
            $req->addHeader('X-Twitter-Client', $this->_app_name);
        }
        
        if ($this->_app_ver !== null) {
            $req->addHeader('X-Twitter-Client-Version', $this->_app_ver);
        }
        
        if ($this->_app_url !== null) {
            $req->addHeader('X-Twitter-Client-URL', $this->_app_url);
        }
        
        if ($this->getEtag() !== null) {
            $req->addHeader('If-None-Match', $this->getEtag());
        }
        
        if ($this->getLastModified() !== null) {
            $req->addHeader('If-Modified-Since', $this->getLastModified());
        }
        
        $response = $req->sendRequest($url, $method);
        $response = explode("\r\n", $response);
        
        $this->setEtag(null);
        $this->setLastModified(null);
        
        $bst  = false;
        $body = array();
        foreach ($response as $line) {
            if (!$bst) {
                // HTTP status validation
                if (preg_match('/^HTTP\\/[0-9]\.[0-9] ([0-9]{3}) .+$/', $line, $matches)) {
                    $status = intval($matches[1]);
                    if ($status !== 200) {
                        return false;
                    }
                }
                
                // get etag header value
                if (preg_match('/^ETag: (.+)$/', $line, $matches)) {
                    $this->setEtag($matches[1]);
                }
                
                // get last modified header value
                if (preg_match('/^Last-Modified: (.+)$/', $line, $matches)) {
                    $this->setLastModified($matches[1]);
                }
            }
            if ($bst) {
                $body[] = $line;
            }
            if ($line == '') {
                $bst = true;
            }
        }

        if ($this->_modified_check && $this->_cache_dir !== null) {
            $modf = realpath($this->_cache_dir) . DIRECTORY_SEPARATOR . $id . '.modified';
            $tagf = realpath($this->_cache_dir) . DIRECTORY_SEPARATOR . $id . '.etag';
            
            if ($this->getLastModified() !== null) {
                $fp = fopen($modf, 'w');
                flock($fp, LOCK_EX);
                fwrite($fp, $this->getLastModified());
                fclose($fp);
            }
            if ($this->getEtag() !== null) {
                $fp = fopen($tagf, 'w');
                flock($fp, LOCK_EX);
                fwrite($fp, $this->getEtag());
                fclose($fp);
            }
        }
        
        $body = implode("\r\n", $body);
        
        if ($this->_json_convert) {
            if (@include_once 'Services/Twitter/Jsphon.php') {
                include_once 'Services/Twitter/Wrapper/Jsphon.php';
                $body = Services_Twitter_Wrapper_Jsphon::decode($body);
            }
        }
        
        return $body;
    }

    /**
     * send auth check request
     *
     * @access private
     * @param string $method  HTTP request method (POST or GET)
     * @param string $url     call get data API's URL
     * @return mixed processed success, return true. and processed faild, return false.
     */
    function _authConnect($method, $url) 
    {
        if (defined('SERVICES_TWITTER_FORCE_POST') && SERVICES_TWITTER_FORCE_POST) {
            $method = 'POST';
        }

        $req =& new Services_Twitter_Connector();
        if( isset($this->_proxy_host) ) {
            $req->setProxy( $this->_proxy_host
                          , $this->_proxy_port
                          , $this->_proxy_user
                          , $this->_proxy_pass );
        }
        
        $req->addHeader('User-Agent', 'PHP/' . phpversion());
        $response = $req->sendRequest($url, $method);
        $response = explode("\r\n", $response);
        
        foreach ($response as $line) {
        var_dump($line);
            // HTTP status validation
            if (preg_match('/^HTTP\\/[0-9]\.[0-9] ([0-9]{3}) .+$/', $line, $matches)) {
                $status = intval($matches[1]);
                if ($status !== 200) {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * build url option method
     *
     * @access private
     * @since Release 0.5.0
     * @param string $url base url string
     * @param array $options request options array
     * @param array $allows build target settings
     * @return string builded url
     */
    function _buildUrl($url, $options, $allows) {
        $appended = false;
        
        foreach ($allows as $name => $type) {
            if (isset($options[$name])) {
                if (!$appended) {
                    $url .= '?';
                    $appended = true;
                } else {
                    $url .= '&';
                }
                
                if ($type === 'int') {
                    $url .= $name . '=' . intval($options[$name]);
                } else {
                    $url .= $name . '=' . urlencode($options[$name]);
                }
            }
        }
        
        return $url;
    }
    
    /**
     * get statuses of public_timeline in greatest 20 cases 
     *
     * @access public
     * @since Release 0.5.0
     * @param int $since_id (Optional) get new data to designated $since_id
     * @return mixed false or response data.
     */
    function getPublicStatuses($since_id = null)
    {
        $url = $this->sm_public;
        if (!is_null($since_id)) {
            $url .= '?since_id=' . urlencode($id);
        }
        return $this->_getData('GET', $url, 'getPublicTimeline');
    }
    
    /**
     * get statuses of public_timeline in greatest 20 cases
     *
     * @access public
     * @param int $since_id (Optional) get new data to designated $since_id
     * @return mixed false or response data.
     * @see getPublicStatuses
     * @deprecated Method deprecated in Release 0.5.0
     */
    function getPublicTimeline($since_id = null)
    {
        return $this->getPublicStatuses($since_id);
    }
    
    /**
     * get statuses of following_timeline in greatest 20 cases and past 24 hours data
     *
     * @access public
     * @since Release 0.5.0
     * @param array $options (Optional) request options(id, since, page)
     *      <br />id = set user ID or screen name
     *      <br />since = get new data to designated this value(GMT format)
     *      <br />page = get past data (20x(page-1)+1~20xpage)
     * @return mixed false or response data.
     */
    function getFollowingStatuses($options = null)
    {
        $url = $this->sm_friends;
        if (!is_null($options)) {
            // parameter 'ID' setted.
            if (isset($options['id'])) {
                $url = sprintf($this->sm_friends_target, urlencode($options['id']));
            }
                        
            $arrows = array(
                'since' => 'str',
                'page' => 'int',
            );
            $url = $this->_buildUrl($url, $options, $arrows);
        }
        return $this->_getData('GET', $url, 'getFriendsTimeline');
    }
        
    /**
     * get statuses of following_timeline in greatest 20 cases and past 24 hours data
     *
     * @access public
     * @param array $options (Optional) request options(id, since, page)
     *      <br />id = set user ID or screen name
     *      <br />since = get new data to designated this value(GMT format)
     *      <br />page = get past data (20x(page-1)+1~20xpage)
     * @see getFollowingStatuses
     * @deprecated Method deprecated in Release 0.5.0
     */
    function getFriendsTimeline($options = null)
    {
        return $this->getFollowingStatuses($options);
    }

    /**
     * user_timeline access
     *
     * @access public
     *
     * @param array $options (Optional) request options(id, count, since)
     *
     * @return mixed false or response data.
     */
    function getUserTimeline($options = null)
    {
        $url = $this->sm_user;
        if (!is_null($options)) {
            if (isset($options['id'])) {
                $url = sprintf($this->sm_user_target, urlencode($options['id']));
            }
            
            $arrows = array(
                'count' => 'int',
                'since' => 'str',
            );
            $url = $this->_buildUrl($url, $options, $arrows);
        }
        return $this->_getData('GET', $url, 'getUserTimeline');
    }
        
    /**
     * show(status method) access
     *
     * @access public
     *
     * @param array $id (Optional) post id
     *
     * @return mixed false or response data.
     */
    function getStatusShow($id)
    {
        return $this->_getData('GET', sprintf($this->sm_show, $id), 'getStatusShow');
    }
    
    /**
     * update access
     *
     * @access public
     *
     * @param string $value update status sentence.
     * @param string $source an application name
     *
     * @return mixed false or response data.
     */
    function setUpdate($value, $source = null)
    {
        $data = array('status' => urlencode($value));
        if (!is_null($source)) {
            $data['source'] = urlencode($source);
        }
        return $this->_setData($this->sm_update, $data,
            array('Content-Type' => 'application/x-www-form-urlencoded'));
    }
    
    /**
     * Replies access
     *
     * @access public
     *
     * @return mixed false or response data.
     */
    function getReplies($page = false)
    {
        $url = $this->sm_replies;
        
        if ($page) {
            $url .= '?page=' . intval($page);
        }

        return $this->_getData('GET', $url, 'getReplies');
    }
    
    /**
     * friends access
     *
     * @access public
     *
     * @param array $id (Optional) user id
     *
     * @return mixed false or response data.
     */
    function getFriends($id = null)
    {
        $url = $this->um_friends;
        if (!is_null($id)) {
            $url = sprintf($this->um_friends_target, $id);
        }
        return $this->_getData('GET', $url, 'getFriends');
    }
    
    /**
     * followers access
     *
     * @access public
     *
     * @return mixed false or response data.
     */
    function getFollowers()
    {
        return $this->_getData('GET', $this->um_followers, 'getFollowers');
    }
    
    /**
     * featured access
     *
     * @access public
     *
     * @return mixed false or response data.
     */
    function getFeatured()
    {
        return $this->_getData('POST', $this->um_featured, 'getFeatured');
    }
    
    /**
     * show(user method) access
     *
     * @access public
     *
     * @param string $id user id
     *
     * @return mixed false or response data.
     */
    function getUserShow($id)
    {
        return $this->_getData('GET', sprintf($this->um_show, $id), 'getUserShow');
    }
    
    /**
     * direct message access
     *
     * @access public
     *
     * @param array $options (Optional) request options(since, page)
     *
     * @return mixed false or response data.
     */
    function getDirectMessage($options = null)
    {
        $url = $this->dm_show;
        if (!is_null($options)) {
            
            $arrows = array(
                'since' => 'str',
                'page' => 'int',
            );
            $url = $this->_buildUrl($url, $options, $arrows);
        }
        return $this->_getData('GET', $url, 'getDirectMessage');
    }
    
    /**
     * direct message new access
     *
     * @access public
     *
     * @param string $id   user id.
     * @param string $text send directmessage text.
     *
     * @return mixed false or response data.
     */
    function setDirectMessage($id, $text)
    {
        return $this->_setData($this->dm_new,
            array(
                'user'    => urlencode($id),
                'text'    => urlencode($text)
            ),
            array(
                'Content-Type'    => 'application/x-www-form-urlencoded'
            ));
    }
    
    /**
     * add new friend
     *
     * @access public
     *
     * @param string $id target user id
     *
     * @return mixed false or response data.
     */
    function addFriend($id)
    {
        return $this->_setData(sprintf($this->fm_create, $id), null);
    }
    
    /**
     * remove friend
     *
     * @access public
     *
     * @param string $id target user id
     *
     * @return mixed false or response data.
     */
    function removeFriend($id)
    {
        return $this->_setData(sprintf($this->fm_destroy, $id), null);
    }
    
    /**
     * verify credentials
     *
     * @access public
     *
     * @return boolean If "Authrized" return true
     */
    function checkAuth()
    {
        $result = $this->_authConnect('POST', $this->am_verify);
        if ($result !== false) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * end session
     *
     * @access public
     *
     * @return boolean If "success" return true
     */
    function endAuth()
    {
        $result = $this->_authConnect('POST', $this->am_endsess);
        if ($result !== false) {
            return true;
        } else {
            return false;
        }
    }
}
