<?php
/**
 * @author twk (http://nonn-et-twk.net/twk/)
 */

// Comment out if you do not have __autoload to load Smarty class
// require_once 'Smarty.class.php';

/**
 * Smarty Prefilter to have multiple delimiters
 * 
 * Usage:
 * require_once 'Twk/Smarty/Prefilter/AlternateDelimiters.php';
 * $smarty = new Smarty();
 * $smarty->left_delimiter = '&lt;?//{';
 * $smarty->right_delimiter = '}?&gt;';
 * $prefilter = new Twk_Smarty_Prefilter_AlternateDelimiters('{{', '}}');
 * $smarty-&gt;register_prefilter(array($prefilter, 'filter'));
 * // &lt;a href="{{$value}}"&gt;&lt;?//{$label}?&gt;&lt;/a&gt;
 */
class Twk_Smarty_Prefilter_AlternateDelimiters
{
	/** @var String */
	private $_alternateLeftDelimiter;
	/** @var String */
	private $_alternateRightDelimiter;
	
	/**
	 * @param String $alternateLeftDelimiter OPTIONAL '&lt;%' if omitted
	 * @param String $alternateRightDelimiter OPTIONAL '%&gt;' if omitted
	 */
	public function __construct($alternateLeftDelimiter = '<%', $alternateRightDelimiter = '%>')
	{
		$this->_alternateLeftDelimiter = $alternateLeftDelimiter;
		$this->_alternateRightDelimiter = $alternateRightDelimiter;
	}
	
	/**
	 * @param String $tpl_source
	 * @param Smarty $smarty
	 * @return String
	 */
	public function filter($tpl_source, Smarty &$smarty)
	{
	    return preg_replace(
    		'/' . preg_quote($this->_alternateLeftDelimiter, '/')
	    		. '(.*?)'
	    		. preg_quote($this->_alternateRightDelimiter, '/') . '/', 
	    	$smarty->left_delimiter . '\1' . $smarty->right_delimiter,
	    	$tpl_source);
	}
}
