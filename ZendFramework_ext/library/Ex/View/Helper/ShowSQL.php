<?php
/**
 * Show SQL Debug Plugin
 *
 * (IE bug) In the case of IE, an arrow image is not displayed.
 *
 * @package    Zend_View
 * @subpackage Helper
 */
class Ex_View_Helper_ShowSQL
{
    private $_db         = null;
    private $_profiler   = null;
    private $_queryCount = 0;
    private $_totalTime  = 0;
    
    public function showSQL()
    {
        // Connect DB
        $this->_db = Zend_Db_Table::getDefaultAdapter();

        $this->_profiler = $this->_db->getProfiler();
        $this->_profiles = $this->_profiler->getQueryProfiles();
        $this->_queryCount = $this->_profiler->getTotalNumQueries();
        $this->_totalTime = $this->_profiler->getTotalElapsedSecs();
        
        return self::createCss() . self::createJs() . $this->createHtml();
        
    }
    
    private static function createCss()
    {
        $css = '
        <style type="text/css" media="screen">
            #debug_sql table {
                width: 100%;
                color: #000;
                background-color: #fff;
                border-collapse: collapse;
                margin-top: -1px;
            }
            #debug_sql table th {
                color: #fff;
                font-weight: normal;
                background-color: #666;
            }
            #debug_sql .time {
                text-align: left;
            }
            #debug_logger th {
                cursor: pointer;
            }
            #debug_sql table th, #debug_sql table td {
                font-family: monospace;
                font-size: 14px;
                border: 1px solid #333;
                padding: 5px;
            }
            #debug2 th {
                text-align: left;
            }
        </style>
        
        <style type="text/css" media="screen">
            /* 
                tables (theme blue)
            */
            table.tablesorter thead tr .header {
            	background-image: url(\'data:image/gif;base64,R0lGODlhFQAJAIAAACMtMP///yH5BAEAAAEALAAAAAAVAAkAAAIXjI+AywnaYnhUMoqt3gZXPmVg94yJVQAAOw==\');
            	background-repeat: no-repeat;
            	background-position: center right;
            	cursor: pointer;
            }
            table.tablesorter thead tr .headerSortUp {
            	background-image: url(\'data:image/gif;base64,R0lGODlhFQAEAIAAACMtMP///yH5BAEAAAEALAAAAAAVAAQAAAINjB+gC+jP2ptn0WskLQA7\');
            }
            table.tablesorter thead tr .headerSortDown {
            	background-image: url(\'data:image/gif;base64,R0lGODlhFQAEAIAAACMtMP///yH5BAEAAAEALAAAAAAVAAQAAAINjI8Bya2wnINUMopZAQA7\');
            }
        </style>
        ';
        
        return $css;
    }
    
    private static function createJs()
    {
        $js = '
        <script type="text/javascript" src="http://www.google.com/jsapi"></script>
        <script type="text/javascript">google.load("jquery", "1.2");</script>
        <script type="text/javascript" src="http://tablesorter.com/jquery.tablesorter.min.js"></script>
        <script type="text/javascript">
            $(function(){
                $("#debug_logger").tablesorter();
            });
        </script>
        ';
        
        return $js;
    }
    
    private function createHtml()
    {
        $html = "
            <div id=\"debug_sql\">
            <table id=\"debug_logger\" class=\"tablesorter\">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Query</th>
                    <th>Params</th>
                    <th>Took (seconds)</th>
                </tr>
                </thead>";
                
        /**
         * get Profile
         */
        $no = 1;
        foreach ($this->_profiles as $query) {
            $html .= "<tr>
                          <td>{$no}</td>
                          <td>{$query->getQuery()}</td>
                          <td>";
                          if (count($query->getQueryParams) > 0) {
                              foreach ($query->getQueryParams as $param) {
                                $html .= "{$param}<br />";
                              }
                          } else {
                              $html .= "&nbsp";
                          }
                $html .= "</td>
                          <td class=\"time\">{$query->getElapsedSecs()}</td>
                      </tr>";
            $no += 1;
            
            $longestTime = 0;
            $longestQuery = null;
            if ($query->getElapsedSecs() > $longestTime) {
                $longestTime  = $query->getElapsedSecs();
                $longestQuery = $query->getQuery();
            }
        }
        
        $html .= '</table>';
        
        /**
         * get Info
         */
        $html .= "
            <table id=\"debug2\">
            <tr>
                <th colspan=\"4\">Executed {$this->_queryCount} queries in {$this->_totalTime} seconds.</th>
            </tr>
            <tr>
                <th colspan=\"2\">Average query length</th>
                <td colspan=\"2\">" . $this->_totalTime / $this->_queryCount . "</td>
            </tr>
            <tr>
                <th colspan=\"2\">Queries per second</th>
                <td colspan=\"2\">" . $this->_queryCount / $this->_totalTime . "</td>
            </tr>
            <tr>
                <th colspan=\"2\">Longest query length</th>
                <td colspan=\"2\">" . $longestTime . "</td>
            </tr>
            <tr>
                <th colspan=\"2\">Longest query</th>
                <td colspan=\"2\">" . $longestQuery . "</td>
            </tr>
            </table>
        ";
        
        $html .= '</div>';
        
        return $html;
    }
}
