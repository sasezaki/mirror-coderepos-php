<?php
/**
 * Pathtraq
 *
 * PHP version 5.2
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 *
 * @category Gene
 * @package Gene_Webservices
 * @version $id$
 * @copyright 2008 Heavens hell
 * @author Heavens hell <heavenshell.jp@gmail.com>
 * @license New BSD License
 */

/**
 * @see Zend_Http_Client
 */
require_once 'Zend/Http/Client.php';

/**
 * @see Gene_Webservices_Pathtraq_Exception
 */
require_once 'Pathtraq/Exception.php';

/**
 * Pathtraq
 *
 * @category Gene
 * @package Gene_Webservices
 * @version $id$
 * @copyright 2008 Heavens hell
 * @author Heavens hell <heavenshell.jp@gmail.com>
 * @license New BSD License
 */
class Gene_Webservices_Pathtraq
{
    /**
     * Library version
     */
    const VERSION = '0.01';

    /**
     * Uri of Pathtrae
     */
    const BASE_URL = 'http://api.pathtraq.com/';

    /**
     * Http client
     *
     * @var mixed
     * @access private
     */
    private $_httpClient = null;

    /**
     * Constructor
     *
     * @param mixed $client Zend_Http_Client
     * @access public
     * @return void
     */
    public function __construct($client = null)
    {
        $this->setHttpClient($client);
    }

    /**
     * Get the Zend_Http_Client object used for communication
     *
     * @access public
     * @return Zend_Http_Client
     */
    public function getHttpClient()
    {
        return $this->_httpClient;
    }

    /**
     * Set the Zend_Http_Client object used for communication
     *
     * @param mixed $client
     * @access public
     * @return Gene_Webservices_Pathtraq
     */
    public function setHttpClient($client = null)
    {
        if ($client === null) {
            $client = new Zend_Http_Client();
        }
        if (!$client instanceof Zend_Http_Client) {
            throw new Gene_Webservices_Pathtraq_Exception('argument is not an instance of Zend_Http_Client.');
        }
        // Get user agent
        $useragent = (PHP_SAPI === 'cli') ? '' : getEnv('HTTP_USER_AGENT');
        if ($useragent === '') {
            require_once 'Zend/Version.php';
            $useragent = 'Zend_Framework/' . Zend_Version::VERSION;
        }
        $client->setConfig(
            array(
                'keepalive'       => true,
                'strictredirects' => true,
                'useragent'       => $useragent
            )
        );
        $this->_httpClient = $client;
        return $this;
    }

    /**
     * News ranking api
     *
     * @param array $values Query parameters
     * @access public
     * @return Zend_Http_Response
     */
    public function newsRanking(array $values)
    {
        $uri = self::BASE_URL . 'news_ja';

        $params = array();
        if (isset($values['api'])) {
            $params['api'] = $values['api'];
        }

        if (isset($values['genre'])) {
            $params['genre'] = $values['genre'];
        }

        if (isset($values['m'])) {
            $params['m'] = $values['m'];
        }

        $query    = $this->_buildQuery($params);
        $response = $this->_get($uri . $query);

        return $response;
    }

    /**
     * Category ranking api
     *
     * @param array $values Query parameters
     * @access public
     * @return Zend_Http_Response
     */
    public function categoryRanking(array $values)
    {
        $uri = self::BASE_URL . 'popular';

        $params = array();
        if (isset($values['api'])) {
            $params['api'] = $values['api'];
        }

        if (isset($values['category'])) {
            $params['category'] = $values['category'];
        }

        if (isset($values['m'])) {
            $params['m'] = $values['m'];
        }
        $query = $this->_buildQuery($params);
        $response = $this->_get($uri . $query);

        return $response;
    }

    /**
     * Keyword search api
     *
     * @param array $values Query parameters
     * @access public
     * @return Zend_Http_Response
     */
    public function keywordSearch(array $values)
    {
        $uri = self::BASE_URL . 'pages';

        $params = array();
        if (isset($values['api'])) {
            $params['api'] = $values['api'];
        }
        if (isset($values['m'])) {
            $params['m'] = $values['m'];
        }
        if (isset($values['url'])) {
            $params['url'] = $values['url'];
        }
        $query = $this->_buildQuery($params);
        $response = $this->_get($uri . $query);

        return $response;
    }


    /**
     * Normalize url2 api
     *
     * @param array $values Query parameters
     * @access public
     * @return Zend_Http_Response
     */
    public function normalizeUrl2(array $values)
    {
        $uri = self::BASE_URL . 'normalize_url2';

        $params = array();
        if (isset($values['api'])) {
            $params['api'] = $values['api'];
        }
        if (isset($values['url'])) {
            $params['url'] = $values['url'];
        }

        $query    = $this->_buildQuery($params);
        $response = $this->_get($uri . $query);

        return $response;
    }

    /**
     * Page counter api
     *
     * @param array $values Query parameters
     * @access public
     * @return Zend_Http_Response
     */
    public function pageCounter(array $values)
    {
        $uri = self::BASE_URL . 'page_counter';

        $params = array();
        if (isset($values['api'])) {
            $params['api'] = $values['api'];
        }
        if (isset($values['m'])) {
            $params['m'] = $values['m'];
        }
        if (isset($values['url'])) {
            $params['url'] = $values['url'];
        }

        $query    = $this->_buildQuery($params);
        $response = $this->_get($uri . $query);

        return $response;
    }

    /**
     * Page chart api
     *
     * @param array $values Query parameters
     * @access public
     * @return Zend_Http_Response
     */
    public function pageChart(array $values)
    {
        $uri = self::BASE_URL . 'page_chart';

        $params = array();
        if (isset($values['api'])) {
            $params['api'] = $values['api'];
        }
        if (isset($values['url'])) {
            $params['url'] = $values['url'];
        }
        if (isset($values['scale'])) {
            $params['scale'] = $values['scale'];
        }
        $query    = $this->_buildQuery($params);
        $response = $this->_get($uri . $query);

        return $response;
    }

    /**
     * Get data
     *
     * @param string $uri Pathtraq api uri
     * @access private
     * @return Zend_Http_Response
     */
    private function _get($uri)
    {
        // Validate uri
        require_once 'Zend/Uri.php';
        if (Zend_Uri::check($uri) === false) {
            throw new Gene_Webservices_Pathtraq_Exception('Invaild uri.');
        }
        $client   = $this->getHttpClient();
        $response = $client->setUri($uri)
                        ->setMethod(Zend_Http_Client::GET)
                        ->request('GET');

        return $response;
    }

    /**
     * Build query string
     *
     * @param array $values Parameters
     * @access private
     * @return string Query parameters
     */
    private function _buildQuery(array $values)
    {
        $query = '';
        foreach ($values as $key => $val) {
            $query .= $key . '=' . $val . '&';
        }
        $query = rtrim($query, '&');
        if ($query !== '') {
            $query = '?' . $query;
        }

        return $query;
    }
}
