<?php
// require_once 'Zend/Filter/Input.php';
/**
 * @author twk (http://nonn-et-twk.net/twk)
 */

class Twk_Filter_Ja_Input extends Zend_Filter_Input
{
    /**
     * @var array Default namespaces, to search after user-defined namespaces.
     */
    protected $_namespaces = array('Twk_Filter', 'Zend_Filter', 'Twk_Validate_Ja', 'Zend_Validate');
	
    /**
     * @var array Default values to use when processing filters and validators.
     */
    protected $_defaults = array(
        self::ALLOW_EMPTY         => false,
        self::BREAK_CHAIN         => false,
        self::ESCAPE_FILTER       => 'HtmlEntities',
        self::MISSING_MESSAGE     => "項目「%field%」はルール「%rule%」により必須ですが項目がありません",
        self::NOT_EMPTY_MESSAGE   => "「%field%」には空でない値が必要です",
        self::PRESENCE            => parent::PRESENCE_OPTIONAL
    );
}