<?php

/**
 * @see http://mecab.sourceforge.net/bindings.html
 * 
 * @author twk (http://nonn-et-twk.net/twk/)
 * @version 20080628
 */
class Twk_MeCab_Node
{
	private $_surface;
	/* @var _surface String */

	const CHARACTER_TYPE_KANJI = 2;
	const CHARACTER_TYPE_SIGN = 3;
	const CHARACTER_TYPE_NUMBER = 4;
	const CHARACTER_TYPE_HIRAGANA = 6;
	const CHARACTER_TYPE_KATAKANA = 7;
	private $_characterType;
	
	private $_features;
	/* @var _features array of string */
	
	private $_hinshiId;
	/* @var _hinshiId int */

	private $_startPosition;
	/* @var _startPosition int */
	
	private $_endPosition;
	/* @var _endPosition int */
	
	public function __construct(array $columns)
	{
		$this->_surface = array_shift($columns);
		$this->_characterType = array_shift($columns);
		$this->_hinshiId = array_shift($columns);
		$this->_startPosition = array_shift($columns);
		$this->_endPosition = array_shift($columns);
		
		$this->_features = $columns;
	}
	
	/**
	 * @param String $name surface, features, characterType, startPosition
	 * @return unknown
	 */
	public function __get($name)
	{
		if ($name == 'surface') return $this->_surface;
		else if ($name == 'features') return $this->_features;
		else if ($name == 'characterType') return $this->_characterType;
		else if ($name == 'startPosition') return $this->_startPosition;
		else if ($name == 'endPosition') return $this->_endPosition;
		else return null;
	}
}