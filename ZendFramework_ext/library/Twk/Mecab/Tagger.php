<?php

/**
 * @see http://mecab.sourceforge.net/bindings.html
 * 
 * @author twk (http://nonn-et-twk.net/twk/)
 * @version 20080628
 */
class Twk_MeCab_Tagger
{
	// @see http://mecab.sourceforge.net/format.html
	// %m for surface, %t for characterType, %h for hinshiId, %ps for startPosition, $pe for endPosition, %F\t[0,1,2] for features
	private $_strArg = '--node-format=%m\\t%t\\t%h\\t%ps\\t%pe\\t%F\\t[0,1,2]\\n --eos-format=';

	private $_config;
	/* @var $_config Zend_Config|null */
	
	/**
	 * @param Zend_Config $options path and encoding
	 */
	public function __construct(Zend_Config $config = null)
	{
		$this->_config = $config;
	}
	
	/**
	 * @see http://mecab.sourceforge.net/format.html
	 *
	 * @param String $str
	 * @param callback $callback falseを返したら
	 * @return array of String
	 */
	public function parse($str, $callback = null)
	{
		if (empty($str)) return array();
		
		$mecab = new Twk_Mecab($this->_config);
		
		$strArg = $this->_strArg;
		
		$data = array();
		foreach (explode("\n", $mecab->exec($strArg, $str)) as $line)
		{
			$line = trim($line);
			if (empty($line)) continue; // skip last empty line
			
			// just add line
			$datum = $line;
			
			if (is_callable($callback))
			{
				$datum = call_user_func($callback, $datum);
				if ($datum === false) continue;
			}
			
			$data[] = $datum;
		}
		
		return $data;
	}
	
	/**
	 * @param string $str
	 * @param callback $callback
	 * @return array of Twk_Mecab_Tagger
	 */
	public function parseToNodes($str, $callback = null)
	{
		if (empty($str)) return array();
		
		$mecab = new Twk_Mecab($this->_config);
		
		$strArg = $this->_strArg;
		
		$data = array();
		foreach (explode("\n", $mecab->exec($strArg, $str)) as $line)
		{
			$line = trim($line);
			if (empty($line)) continue; // skip last empty line
			
			// parseToNodes specific
			$columns = explode("\t", $line);
			$datum = new Twk_Mecab_Node($columns);
			
			if (is_callable($callback))
			{
				$datum = call_user_func($callback, $datum);
				if ($datum === false) continue;
			}
			
			$data[] = $datum;
		}
		
		return $data;
	}
}
