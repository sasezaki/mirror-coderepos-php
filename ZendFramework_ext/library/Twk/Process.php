<?php

/**
 * 指定したパスのコマンドを実行する
 * 
 * @see http://jp2.php.net/manual/ja/function.proc-open.php#76711
 */
class Twk_Process
{
	/**
	 * 終了コードは戻ってこないので注意
	 * 
	 * @param String $command
	 * @return String stdout of $command
	 * @throws Exception Cannot open the process or having stderr
	 * @see proc_open
	 */
    public static function open($command, $input = null, $cwd = null, $env = null, $other_options = null)
    {
        $descriptorspec = array(
            1 => array('pipe', 'w'),
            2 => array('pipe', 'w')
        );
        if (!empty($input))
        	$descriptorspec[0] = array('pipe', 'r');

        $pipes = null;
        // echo $command . ' < ' . mb_convert_encoding($input, mb_internal_encoding(), mb_detect_encoding($input));
        $resource = proc_open($command, $descriptorspec, $pipes, $cwd, $env, $other_options);
        if (!is_resource($resource))
        	throw new Exception('Cannot open the process');
        
		foreach ($pipes as $pipe)
        	stream_set_blocking($pipe, false);
        
        if (!empty($input))
			$stdin = $pipes[0];
		$stdout = @$pipes[1];
		$stderr = @$pipes[2];

		// 先にすべての入力を注入
		if (!empty($input))
		{
			stream_set_timeout($stdin, 3);
			fwrite($stdin, $input);
			fclose($stdin);
		}
		
		// その後すべての出力を取得
		$retval = '';
		$error = '';
        $read = array($stdout, $stderr);
        $write = null;
        $except = null;
        for ($i = 0; (!feof($stdout) || !feof($stderr)) && $i < 100; ++$i)
		{
			if (!stream_select($read, $write, $except, 10)) break;
		    
	        foreach ($read as $sock)
	        {
	            if ($sock === $stdout)
	                $retval .= fread($sock, 4096);
	            else if ($sock === $stderr)
	                $error .= fread($sock, 4096);
	        }
		}
		fclose($stdout);
		fclose($stderr);
		
		// $exit_code is not used
		$exit_code = proc_close($resource);
        	
        if (!empty($error))
            throw new Exception($error);
        else
            return $retval;
    }
}
