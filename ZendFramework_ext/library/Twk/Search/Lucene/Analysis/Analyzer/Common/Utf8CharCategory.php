<?php
/**
 * 文字種はあまり正確ではありません
 * @version 20080204
 * @author twk (http://nonn-et-twk.net/twk/)
 */

// Requires analyzer.php
// @see http://framework.zend.com/issues/browse/ZF-991
require_once 'Zend/Search/Lucene/Analysis/Analyzer.php';

/** Zend_Search_Lucene_Analysis_Analyzer_Common */
//require_once 'Zend/Search/Lucene/Analysis/Analyzer/Common.php';


/**
 * @category   Twk
 * @package    Twk_Search_Lucene
 * @subpackage Analysis
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */

class Twk_Search_Lucene_Analysis_Analyzer_Common_Utf8CharCategory extends Zend_Search_Lucene_Analysis_Analyzer_Common
{
    /**
     * Current char position in an UTF-8 stream
     *
     * @var integer
     */
    private $_position;

    /**
     * Current binary position in an UTF-8 stream
     *
     * @var integer
     */
    private $_bytePosition;

    private $_lastPosition;
    private $_lastBytePosition;
    
    /**
     * Stream length
     *
     * @var integer
     */
    private $_streamLength;

	/**
	 * Char category
	 */
    private $_currentCategory;

    /**
     *
     */
    const CATEGORY_ALNUM = 1;
    const CATEGORY_PUNCT = 2;
    const CATEGORY_CNTRL = 3;
    const CATEGORY_SPACE = 4;
	const CATEGORY_SBCS_OTHER = 5;
	const CATEGORY_HIRAGANA = 11;
	const CATEGORY_KATAKANA = 12;
	const CATEGORY_JAPANESE_KANJI = 13;
	const CATEGORY_FULLWIDTH_ALNUM = 13;
	const CATEGORY_JAPANESE_PUNCT = 14;
	const CATEGORY_MBCS_OTHER = 15;
	
    /**
     * Reset token stream
     */
    public function reset()
    {
        $this->_position     = 0;
        $this->_bytePosition = 0;
        
        $this->_lastPosition = 0;
    	$this->_lastBytePosition = 0;

        // convert input into UTF-8
        if (strcasecmp($this->_encoding, 'utf8' ) != 0  &&
            strcasecmp($this->_encoding, 'utf-8') != 0 ) {
                $this->_input = iconv($this->_encoding, 'UTF-8', $this->_input);
                $this->_encoding = 'UTF-8';
        }

        // Get UTF-8 string length.
        // It also checks if it's a correct utf-8 string
        $this->_streamLength = iconv_strlen($this->_input, 'UTF-8');
    }

    /**
     * @return CATEGORY_xxx
     */
    private static function _getCharCategory($char)
    {
    	if (strlen($char) == 1)
        {
        	if (ctype_alnum($char))
				$category = self::CATEGORY_ALNUM;
			else if (ctype_punct($char))
				$category = self::CATEGORY_PUNCT;
        	else if (ctype_cntrl($char))
        		$category = self::CATEGORY_CNTRL;
        	else if (ctype_space($char))
        		$category = self::CATEGORY_SPACE;
           	else
        		$category = self::CATEGORY_SBCS_OTHER;
        }
        else
        {
        	// @see http://ablog.seesaa.net/article/20969848.html
        	if (mb_ereg_match('^[ぁ-ん]+$', $char))
        		$category = self::CATEGORY_HIRAGANA;
        	else if (mb_ereg_match('^[ァ-ヴー]+$', $char))
				$category = self::CATEGORY_KATAKANA;
			else if (mb_ereg_match('^[一-龠々〆ヵヶ]+$', $char))
				$category = self::CATEGORY_JAPANESE_KANJI;
			else if (mb_ereg_match('^[、。！!？?()（）「」『』]+$', $char)) // 記号はまだまだあるが・・・
				$category = self::CATEGORY_JAPANESE_PUNCT;
			else if (mb_ereg_match('^[ａ-ｚＡ-Ｚ０-９]+$', $char))
				$category = self::CATEGORY_FULLWIDTH_ALNUM;
			else
				$category = self::CATEGORY_MBCS_OTHER;
        }
        
        return $category;
	}
    
    /**
     * Check, how that character is 
     *
     * @param string $char
     * @return boolean
     */
    private static function _isWhiteSpaceCategory($char)
    {
    	$category = self::_getCharCategory($char);
        //echo $category, "\n";
        
        return $category == self::CATEGORY_PUNCT || $category == self::CATEGORY_SPACE || $category == self::CATEGORY_JAPANESE_PUNCT;
	}
    
    /**
     * Check, how that character is 
     *
     * @param string $char
     * @return boolean
     */
    private function _isSameCategory($char)
    {
    	$category = self::_getCharCategory($char);
        //echo $category, "\n";

    	$isSameCategory 
    		= $this->_currentCategory === null || $this->_currentCategory == $category;
    	
        $this->_currentCategory = $category;
        
        return $isSameCategory;
    }

    /**
     * Get next UTF-8 char
     *
     * @param string $char
     * @return boolean
     */
    private function _nextChar()
    {
    	$this->_lastBytePosition = $this->_bytePosition;
    	$this->_lastPosition = $this->_position;
    	
        $char = $this->_input[$this->_bytePosition++];

        if (( ord($char) & 0xC0 ) == 0xC0) {
            $addBytes = 1;
            if (ord($char) & 0x20 ) {
                $addBytes++;
                if (ord($char) & 0x10 ) {
                    $addBytes++;
                }
            }
            $char .= substr($this->_input, $this->_bytePosition, $addBytes);
            $this->_bytePosition += $addBytes;
        }

        $this->_position++;

        return $char;
    }

    private function _undoLastChar()
    {
    	$this->_bytePosition = $this->_lastBytePosition;
    	$this->_position = $this->_lastPosition;
    }
    
    /**
     * Tokenization stream API
     * Get next token
     * Returns null at the end of stream
     *
     * @return Zend_Search_Lucene_Analysis_Token|null
     */
    public function nextToken()
    {
        if ($this->_input === null) {
            return null;
        }

        while ($this->_position < $this->_streamLength) {
            // skip white space
            while ($this->_position < $this->_streamLength &&
                   self::_isWhiteSpaceCategory($char = $this->_nextChar())) {
                $char = '';
            }
            if ($this->_position < $this->_streamLength)
            {
	            $this->_undoLastChar();
            }
            
            $termStartPosition = $this->_position - 1;
            $termText = '';//$char;

            // read token
        	$this->_currentCategory = null;
            while ($this->_position < $this->_streamLength &&
                   $this->_isSameCategory($char = $this->_nextChar())) {
                $termText .= $char;
            }

            // Empty token, end of stream.
            if ($termText == '') {
                return null;
            }

			$termEndPosition = $this->_position - 1;
			
            // if not End, undo the last char
            if ($this->_position < $this->_streamLength)
            {
	            $this->_undoLastChar();
            }
            
            $token = new Zend_Search_Lucene_Analysis_Token(
                                      $termText,
                                      $termStartPosition,
                                      $termEndPosition);
            $token = $this->normalize($token);
            if ($token !== null) {
            	//echo $termText, strlen($termText) , "\n";
            	return $token;
            }
            // Continue if token is skipped
        }

        return null;
    }
}

