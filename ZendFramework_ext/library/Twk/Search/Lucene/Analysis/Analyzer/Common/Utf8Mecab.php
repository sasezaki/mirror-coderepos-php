<?php
/**
 * Multi Byte Character Mecab Analyzer
 * 
 * @version 20080628
 * @author twk (http://nonn-et-twk.net/twk/)
 */

// Requires analyzer.php
// @see http://framework.zend.com/issues/browse/ZF-991
require_once 'Zend/Search/Lucene/Analysis/Analyzer.php';

/** Zend_Search_Lucene_Analysis_Analyzer_Common */
//require_once 'Zend/Search/Lucene/Analysis/Analyzer/Common.php';

//require_once 'Twk/Mecab/Tagger.php';

/**
 * @category   Twk
 * @package    Twk_Search_Lucene
 * @subpackage Analysis
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */

class Twk_Search_Lucene_Analysis_Analyzer_Common_Utf8Mecab extends Zend_Search_Lucene_Analysis_Analyzer_Common
{
	private $_mecab;
	/* @var $_mecab Twk_Mecab_Tagger */
	
	private $_mecabNodes;
	/* @var $_mecabNodes array */
	
	private $_i;

	/**
	 * @param Zend_Config $options path and encoding
	 * note that encoding is for mecab dictionary, text is always treated as UTF-8
	 */
	public function __construct(Zend_Config $config = null)
	{
		$this->_mecab = new Twk_Mecab_Tagger($config);
	}
	
    /**
     * Reset token stream
     * 実際ここで全て変換してしまっているので遅いと思われる
     */
    public function reset()
    {
        // convert input into UTF-8
        if (strcasecmp($this->_encoding, 'utf8' ) != 0  &&
            strcasecmp($this->_encoding, 'utf-8') != 0 ) {
                $this->_input = iconv($this->_encoding, 'UTF-8', $this->_input);
                $this->_encoding = 'UTF-8';
        }
		
		$this->_mecabNodes = $this->_mecab->parseToNodes($this->_input);
		$this->_i = 0;
    }

    public function nextToken()
    {
    	if ($this->_i >= count($this->_mecabNodes))
    		return null;
		else
		{
	    	$node = $this->_mecabNodes[$this->_i++];
			/* @var $node Twk_MeCab_Node */
	    	// XXX startPosition and endPosition is not of UTF-8 but mecab-encoding (EUC-JP or Shift_JIS)
	    	// I don't care for now!
	        $token = new Zend_Search_Lucene_Analysis_Token(
	                                      $node->surface,
	                                      $node->startPosition,
	                                      $node->endPosition);
	        $token = $this->normalize($token);
			return $token;
		}
    }
}
    
// ここから先は直接実行の時のみ動きます。チェックは甘め
if (!count(debug_backtrace()))
{
    $fp = fopen(__FILE__, 'r');
    fseek($fp, __COMPILER_HALT_OFFSET__ + 2); // 2 for ? and >
    echo stream_get_contents($fp);
    
	$text = '日本でWindows 95が発売されたのはいつでしょう?';
	$analyzer = new Twk_Search_Lucene_Analysis_Analyzer_Common_Utf8Mecab();
	$a = $analyzer->tokenize($text, 'UTF-8');
	print_r($a);
}

__halt_compiler();?>
<html>
<head>
<title>Twk_Search_Lucene_Analysis_Analyzer_Common_Utf8Mecab</title>
</head>
<body>
<h1>Twk_Search_Lucene_Analysis_Analyzer_Common_Utf8Mecab</h1>
<pre>
	Zend_Search_Lucene用日本語アナライザー
	作者 twk

	<a href="http://coderepos.org/share/browser/lang/php/ZendFramework_ext/library/Twk/Search/Lucene/Analysis/Analyzer/Common/">配布場所</a>
	
	コード例:
	<code>
		$text = '日本でWindows 95が発売されたのはいつでしょう?';
		$analyzer = new Twk_Search_Lucene_Analysis_Analyzer_Common_Utf8Mecab();
		$a = $analyzer-&gt;tokenize($text, 'UTF-8');
		print_r($a);	
	</code>
</pre>
</body>
</html>