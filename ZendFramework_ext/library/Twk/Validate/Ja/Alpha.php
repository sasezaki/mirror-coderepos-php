<?php
/**
 * @see Zend_Validate_Alpha
 */
require_once 'Zend/Validate/Alpha.php';


/**
 * Zend_Validate_Alpha with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_Alpha extends Zend_Validate_Alpha
{
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_ALPHA    => "「%value%」にアルファベット以外の文字が含まれています",
        self::STRING_EMPTY => "「%value%」は空の文字列です"
    );
}
