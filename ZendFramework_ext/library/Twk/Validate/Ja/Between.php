<?php
/**
 * @see Zend_Validate_Between
 */
require_once 'Zend/Validate/Between.php';


/**
 * Zend_Validate_Between with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_Between extends Zend_Validate_Between
{
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_BETWEEN        => "「%value%」は「%min%」以上「%max%」以下ではありません",
        self::NOT_BETWEEN_STRICT => "「%value%」は「%min%」以下か「%max%」以上です"
    );
}
