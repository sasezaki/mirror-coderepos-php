<?php
/**
 * @see Zend_Validate_Digits
 */
require_once 'Zend/Validate/Digits.php';


/**
 * Zend_Validate_Digits with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_Digits extends Zend_Validate_Digits
{
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_DIGITS   => "「%value%」に数字以外の文字が含まれています",
        self::STRING_EMPTY => "「%value%」は空の文字列です"
    );
}
