<?php
/**
 * @see Zend_Validate_EmailAddress
 */
require_once 'Zend/Validate/EmailAddress.php';

/**
 * @see Twk_Validate_Ja_Hostname
 */
require_once 'Twk/Validate/Ja/Hostname.php';

/**
 * @see Zend_Validate_Hostname
 */
require_once 'Zend/Validate/Hostname.php';

/**
 * Zend_Validate_EmailAddress with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_EmailAddress extends Zend_Validate_EmailAddress
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::INVALID            => "「%value%」はメールアドレスの基本的な形式 local-part@hostname ではありません",
        self::INVALID_HOSTNAME   => "メールアドレス「%value%」内の「%hostname%」は有効なホスト名ではありません",
        self::INVALID_MX_RECORD  => "メールアドレス「%value%」内の「%hostname%」は有効な MX レコードではないようです",
        self::DOT_ATOM           => "メールアドレス「%value%」内の「%localPart%」はドットアトム形式に合いません",
        self::QUOTED_STRING      => "メールアドレス「%value%」内の「%localPart%」は引用文字列形式に合いません",
        self::INVALID_LOCAL_PART => "メールアドレス「%value%」内の「%localPart%」は有効なローカルパートではありません"
    );
    
    /**
     * @param Zend_Validate_Hostname $hostnameValidator OPTIONAL Twk_Validate_Ja_Hostname instance is used if omitted
     * @param int                    $allow             OPTIONAL
     * @return void
     */
    public function setHostnameValidator(Zend_Validate_Hostname $hostnameValidator = null, $allow = Zend_Validate_Hostname::ALLOW_DNS)
    {
        if ($hostnameValidator === null) {
            $hostnameValidator = new Twk_Validate_Ja_Hostname($allow);
        }
    	parent::setHostnameValidator($hostnameValidator, $allow);
    }
}
