<?php
/**
 * @see Zend_Validate_GreaterThan
 */
require_once 'Zend/Validate/GreaterThan.php';


/**
 * Zend_Validate_GreaterThan with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_GreaterThan extends Zend_Validate_GreaterThan
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_GREATER => "「%value%」は「%min%」より大きくありません"
    );
}
