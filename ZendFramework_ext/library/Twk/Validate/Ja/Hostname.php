<?php
/**
 * @see Zend_Validate_Hostname
 */
require_once 'Zend/Validate/Hostname.php';

/**
 * @see Zend_Validate_Ip
 */
require_once 'Zend/Validate/Ip.php';

/**
 * @see Twk_Validate_Ja_Ip
 */
require_once 'Twk/Validate/Ja/Ip.php';

/**
 * Zend_Validate_Hostname with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_Hostname extends Zend_Validate_Hostname
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::IP_ADDRESS_NOT_ALLOWED  => "「%value%」は IP アドレスのようですが IP アドレスは許されていません",
        self::UNKNOWN_TLD             => "「%value%」は DNS ホスト名のようですが TLD が一覧に見つかりません",
        self::INVALID_DASH            => "「%value%」は DNS ホスト名のようですが不適当な位置にダッシュ (-) があります",
        self::INVALID_HOSTNAME_SCHEMA => "「%value%」は DNS ホスト名のようですが TLD 「%tld%」のホスト名スキーマに合いません",
        self::UNDECIPHERABLE_TLD      => "「%value%」は DNS ホスト名のようですが TLD 部を展開できません",
        self::INVALID_HOSTNAME        => "「%value%」は DNS ホスト名の構造に合いません",
        self::INVALID_LOCAL_NAME      => "「%value%」は有効なローカルネットワーク名ではないようです",
        self::LOCAL_NAME_NOT_ALLOWED  => "「%value%」はローカルネットワーク名のようですがローカルネットワーク名は許されていません"
    );
    
    /**
     * @param Zend_Validate_Ip $ipValidator OPTIONAL  Twk_Validate_Ja_Ip instance is used if omitted
     * @return void;
     */
    public function setIpValidator(Zend_Validate_Ip $ipValidator = null)
    {
        if ($ipValidator === null) {
            $ipValidator = new Twk_Validate_Ja_Ip();
        }
        parent::setIpValidator($ipValidator);
    }
}
