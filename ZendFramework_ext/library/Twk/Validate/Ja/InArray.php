<?php
/**
 * @see Zend_Validate_InArray
 */
require_once 'Zend/Validate/InArray.php';


/**
 * Zend_Validate_InArray with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_InArray extends Zend_Validate_InArray
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_IN_ARRAY => "「%value%」は候補の中にありません"
    );
}
