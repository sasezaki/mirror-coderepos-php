<?php
/**
 * @see Zend_Validate_Regex
 */
require_once 'Zend/Validate/Regex.php';


/**
 * Zend_Validate_Regex with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_Regex extends Zend_Validate_Regex
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_MATCH => "「%value%」はパターン「%pattern%」に合いません"
    );
}
