<?php
/**
 * @see Zend_Validate_StringLength
 */
require_once 'Zend/Validate/StringLength.php';


/**
 * Zend_Validate_StringLength with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_StringLength extends Zend_Validate_StringLength
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::TOO_SHORT => "「%value%」は %min% 文字より短いです",
        self::TOO_LONG  => "「%value%」は %max% 文字より長いです"
    );
}
