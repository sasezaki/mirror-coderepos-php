Libraries extends ZendFramework

Twk_Validate_Ja
	Created for ZF 1.0.x.  Probably not suited for ZF1.5+
	Zend_Validate classes with Japanese error messages
	http://nonn-et-twk.net/twk/node/132
	
	// usage 1:
	require_once 'Twk/Validate/Ja/EmailAddress.php';
	$validator = new Twk_Validate_Ja_EmailAddress();
	$email = 'test@test.asia';
	if (!$validator->isValid($email)) {
		foreach ($validator->getMessages() as $message) {
			echo "$message\n";
		}
	}

	// usage 2:
	$validators = array(
		'month' => array(
		'Digits',
		'Int',
		array('Between', 1, 12)
		)
	);
	$inputFilter = new Zend_Filter_Input($filters, $validators, $data, $options);
	$inputFilter->addNamespace('Twk_Validate_Ja');
	if (!$inputFilter->isValid())
	{
	}
	
	// usage 3: (with strange name class Twk_Filter_Ja_Input)
	$inputFilter = new Twk_Filter_Ja_Input($filters, $validators, $data, $options);
	if (!$inputFilter->isValid())
	{
	}

Twk_Filter
	Twk_Filter_Kana
		mb_convert_kana
	Twk_Filter_AntiEmoji
		emoji buster

Twk_Search
	Japanese uni-gram and Mecab analyzers
	@see http://nonn-et-twk.net/twk/zend-search-lucene-analyzer

Twk_Process
	Implementation of http://jp2.php.net/manual/ja/function.proc-open.php#76711
    $stdout = Twk_Process::open($command);

Twk_Mecab
	Mecab Process executor.  required for Twk_Search_Lucene Mecab analyzer
	If you just want to use mecab only on Linux, you may prefer php_mecab (http://d.hatena.ne.jp/rsky/20071228/1198841049)
	$mecabTagger = new Twk_MeCab_Tagger();
	$mecabNodes = $mecabTagger->parseToNodes($text);
    