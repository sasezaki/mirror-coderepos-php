<?php
$baseDir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
$includePath = ini_get('include_path');
$libPath = $baseDir . '/library/';
ini_set('include_path', $includePath . ':' . $libPath);

require_once 'Gene/Webservices/Pathtraq.php';

