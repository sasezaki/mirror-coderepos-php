--TEST--
Pathtraq NewsRanking api test for json
--FILE--
<?php
require_once 'Base.php';
$pathtraq = new Gene_Webservices_Pathtraq();
$params = array(
    'api'   => 'json',
    'genre' => 'national',
    'm'     => 'popular'
);
$response = $pathtraq->newsRanking($params);
var_dump($response instanceof Zend_Http_Response);
?>
--EXPECT--
bool(true)
