--TEST--
Pathtraq NormalizeUrl api test
--FILE--
<?php
require_once 'Base.php';
$pathtraq = new Gene_Webservices_Pathtraq();
$params = array(
    'api'   => 'xml',
    'url'   => 'http://www.amazon.co.jp/exec/obidos/ASIN/4839922829/cybozulabs-22'
);
$response = $pathtraq->normalizeUrl2($params);
var_dump($response instanceof Zend_Http_Response);
var_dump($response->getBody());
?>
--EXPECT--
bool(true)
string(117) "<?xml version="1.0" encoding="utf-8"?>
<result>
  <url>http://www.amazon.co.jp/gp/product/4839922829</url>
</result>
"
