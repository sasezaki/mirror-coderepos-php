--TEST--
Pathtraq PageChart api test
--FILE--
<?php
require_once 'Base.php';
$pathtraq = new Gene_Webservices_Pathtraq();
$params = array(
    'api'   => 'xml',
    'url'   => urlencode('http://narabete.com/'),
    'scale' => '24h'
);
$response = $pathtraq->pageChart($params);
var_dump($response instanceof Zend_Http_Response);
var_dump($response->getBody());
?>
--EXPECT--
bool(true)
string(650) "<?xml version="1.0" encoding="utf-8"?>
<result>
  <url>http://narabete.com/</url>
  <plots>
    <plot>0</plot>
    <plot>1</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>0</plot>
    <plot>1</plot>
    <plot>0</plot>
    <plot>0</plot>
  </plots>
  <start>Fri, 25 Jul 2008 17:00:00 GMT</start>
  <step>1</step>
</result>"
