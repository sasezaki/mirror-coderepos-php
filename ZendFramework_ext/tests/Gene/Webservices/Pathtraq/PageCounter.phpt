--TEST--
Pathtraq PageCounter api test
--FILE--
<?php
require_once 'Base.php';
$pathtraq = new Gene_Webservices_Pathtraq();
$params = array(
    'api' => 'xml',
    'm'   => 'upcoming',
    'url' => urlencode('http://cybozu.co.jp/')
);
$response = $pathtraq->pageCounter($params);
var_dump($response instanceof Zend_Http_Response);
var_dump($response->getBody());
?>
--EXPECT--
bool(true)
string(134) "<?xml version="1.0" encoding="utf-8"?>
<result>
  <count>3</count>
  <mode>upcoming</mode>
  <url>http://cybozu.co.jp/</url>
</result>"
