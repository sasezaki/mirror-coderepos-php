dnl $Id$
dnl config.m4 for extension bp

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

dnl PHP_ARG_WITH(bp, for bp support,
dnl Make sure that the comment is aligned:
dnl [  --with-bp             Include bp support])

dnl Otherwise use enable:

dnl PHP_ARG_ENABLE(bp, whether to enable bp support,
dnl Make sure that the comment is aligned:
dnl [  --enable-bp           Enable bp support])

if test "$PHP_BP" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-bp -> check with-path
	dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/bp.h"  # you most likely want to change this
  dnl if test -r $PHP_BP/; then # path given as parameter
  dnl   BP_DIR=$PHP_BP
  dnl else # search default path list
  dnl   AC_MSG_CHECKING(for bp files in default path)
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       BP_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$BP_DIR"; then
  dnl   AC_MSG_RESULT(not found)
  dnl   AC_MSG_ERROR(Please reinstall the bp distribution)
  dnl fi

  dnl # --with-bp -> add include path
  dnl PHP_ADD_INCLUDE($BP_DIR/include)

  dnl # --with-bp -> chech for lib and symbol presence
  dnl LIBNAME=bp # you may want to change this
  dnl LIBSYMBOL=bp # you most likely want to change this 
  dnl old_LIBS=$LIBS
  dnl LIBS="$LIBS -L$BP_DIR/lib -lm -ldl"
  dnl AC_CHECK_LIB($LIBNAME, $LIBSYMBOL, [AC_DEFINE(HAVE_BPLIB,1,[ ])],
	dnl			[AC_MSG_ERROR(wrong bp lib version or lib not found)])
  dnl LIBS=$old_LIBS
  dnl
  dnl PHP_SUBST(BP_SHARED_LIBADD)
  dnl PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $BP_DIR/lib, BP_SHARED_LIBADD)

  PHP_EXTENSION(bp, $ext_shared)
fi
