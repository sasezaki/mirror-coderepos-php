#ifndef PHP_BP_HTML_OPTIMIZE_H
#define PHP_BP_HTML_OPTIMIZE_H

unsigned char *bp_get_element( HashTable *element, int elements, char *name, int no_value, int *in_esc);
//unsigned char *bp_stradd( char *to, char *from);


/*
 * 属性定義構造体
 *
 */
typedef struct _tag_element_type {
  char *name;/* 属性名 */
  int bp_level;/* 属性を使用できるレベル */
  int no_value;/* 属性名だけか */
  int f_use;/* 属性がが必須項目対象か */
} TAG_ELEMENT_TYPE;

/*
TE_NUM_??? 属性の数
TE_USE_??? 最低限必要な必須項目の数
 */

/* A タグの属性定義 */
#define TE_NUM_a 12
#define TE_USE_a 1
TAG_ELEMENT_TYPE te_a[TE_NUM_a] = {
  { "href", 40, 0, 1},
  { "name", 30, 0, 1},
  { "cti", 40, 0, 0},
  { "accesskey", 10, 0, 0},
  { "ijam", 30, 0, 1},
  { "ista", 30, 0, 1},
  { "ilet", 30, 0, 1},
  { "iswf", 30, 0, 1},
  { "irst", 30, 0, 1},
  { "telbook", 0, 0, 0},
  { "kana", 0, 0, 0},
  { "email", 0, 0, 0},
  { "utn", 40, 1, 0},
};

/* FORM タグの属性定義 */
#define TE_NUM_form 3
#define TE_USE_form 1
TAG_ELEMENT_TYPE te_form[TE_NUM_form] = {
  { "action", 40, 0, 1},
  { "method", 40, 0, 0},
  { "utn", 40, 1, 0},
};

/* INPUT タグの属性定義 */
#define TE_NUM_input 8
#define TE_USE_input 1
TAG_ELEMENT_TYPE te_input[TE_NUM_input] = {
  { "type", 40, 0, 1},
  { "name", 40, 0, 1},
  { "value", 40, 0, 1},
  { "size", 30, 0, 0},
  { "maxlength", 30, 0, 0},
  { "istyle", 10, 0, 0},
  { "accesskey", 10, 0, 0},
  { "checked", 40, 1, 0},
};

/* TEXTAREA タグの属性定義 */
#define TE_NUM_textarea 5
#define TE_USE_textarea 1
TAG_ELEMENT_TYPE te_textarea[TE_NUM_textarea] = {
  { "name", 40, 0, 1},
  { "rows", 30, 0, 0},
  { "cols", 30, 0, 0},
  { "istyle", 10, 0, 0},
  { "accesskey", 10, 0, 0},
};

/* SELECT タグの属性定義 */
#define TE_NUM_select 3
#define TE_USE_select 1
TAG_ELEMENT_TYPE te_select[TE_NUM_select] = {
  { "name", 40, 0, 1},
  { "size", 30, 0, 0},
  { "multiple", 40, 1, 0},
};

/* OPTION タグの属性定義 */
#define TE_NUM_option 2
#define TE_USE_option 0
TAG_ELEMENT_TYPE te_option[TE_NUM_option] = {
  { "value", 40, 0, 0},
  { "selected", 40, 1, 0},
};

/* OBJECT タグの属性定義 */
#define TE_NUM_object 6
#define TE_USE_object 3
TAG_ELEMENT_TYPE te_object[TE_NUM_object] = {
  { "data", 30, 0, 1},
  { "id", 30, 0, 1},
  { "type", 30, 0, 0},
  { "declare", 30, 1, 1},
  { "width", 30, 1, 0},
  { "height", 30, 1, 0},
};

/* PARAM タグの属性定義 */
#define TE_NUM_param 2
#define TE_USE_param 2
TAG_ELEMENT_TYPE te_param[TE_NUM_param] = {
  { "name", 30, 0, 1},
  { "value", 30, 0, 1},
};

/* IMG タグの属性定義 */
#define TE_NUM_img 7
#define TE_USE_img 1
TAG_ELEMENT_TYPE te_img[TE_NUM_img] = {
  { "src", 10, 0, 1},
  { "border", 10, 0, 0},
  { "align", 10, 0, 0},
  { "width", 10, 0, 0},
  { "height", 10, 0, 0},
  { "hspace", 10, 0, 0},
  { "vspace", 10, 0, 0},
};

/* BODY タグの属性定義 */
#define TE_NUM_body 5
#define TE_USE_body 0
TAG_ELEMENT_TYPE te_body[TE_NUM_body] = {
  { "bgcolor", 10, 0, 0},
  { "text", 10, 0, 0},
  { "link", 10, 0, 0},
  { "alink", 10, 0, 0},
  { "vlink", 10, 0, 0},
};

/* FONT タグの属性定義 */
#define TE_NUM_font 2
#define TE_USE_font 0
TAG_ELEMENT_TYPE te_font[TE_NUM_font] = {
  { "color", 10, 0, 0},
  { "size", 10, 0, 0},
};

/* DIV タグの属性定義 */
#define TE_NUM_div 1
#define TE_USE_div 0
TAG_ELEMENT_TYPE te_div[TE_NUM_div] = {
  { "align", 10, 0, 0},
};

/* P タグの属性定義 */
#define TE_NUM_p 1
#define TE_USE_p 0
TAG_ELEMENT_TYPE te_p[TE_NUM_p] = {
  { "align", 10, 0, 0},
};

/* HR タグの属性定義 */
#define TE_NUM_hr 4
#define TE_USE_hr 0
TAG_ELEMENT_TYPE te_hr[TE_NUM_hr] = {
  { "align", 10, 0, 0},
  { "width", 10, 0, 0},
  { "size", 10, 0, 0},
  { "color", 10, 0, 0},
};

/* MARQUEE タグの属性定義 */
#define TE_NUM_marquee 4
#define TE_USE_marquee 0
TAG_ELEMENT_TYPE te_marquee[TE_NUM_marquee] = {
  { "direction", 20, 0, 0},
  { "behavior", 20, 0, 0},
  { "loop", 20, 0, 0},
  { "bgcolor", 20, 0, 0},
};

/* DIR タグの属性定義 */
#define TE_NUM_dir 1
#define TE_USE_dir 0
TAG_ELEMENT_TYPE te_dir[TE_NUM_dir] = {
  { "type", 30, 0, 0},
};

/* MENU タグの属性定義 */
#define TE_NUM_menu 1
#define TE_USE_menu 0
TAG_ELEMENT_TYPE te_menu[TE_NUM_menu] = {
  { "type", 30, 0, 0},
};

/* UL タグの属性定義 */
#define TE_NUM_ul 1
#define TE_USE_ul 0
TAG_ELEMENT_TYPE te_ul[TE_NUM_ul] = {
  { "type", 30, 0, 0},
};


/* LI タグの属性定義 */
#define TE_NUM_li 1
#define TE_USE_li 0
TAG_ELEMENT_TYPE te_li[TE_NUM_li] = {
  { "type", 30, 0, 0},
};


/*
 * 属性の再構成して、toにコピーする
 */
#define bp_make_tag_element(tag_name,to,element,elements,bp_level){\
  unsigned char *to2 = to;\
  int i;\
  int f_use = 0;\
  int in_esc;\
  unsigned char *element_value;\
  unsigned char quote = '\0';\
  for( i = 0; i < TE_NUM_##tag_name; i++){\
    if( te_##tag_name[i].bp_level >= bp_level){\
      element_value = bp_get_element( element, elements, te_##tag_name[i].name, te_##tag_name[i].no_value, &in_esc);\
      if( element_value == NULL) continue;\
      if( strlen( element_value) < 1) continue;\
\
      f_use += te_##tag_name[i].f_use;/* 必須フラグを上げる */\
      *to++ = ' ';/* スペース */\
      if( te_##tag_name[i].no_value){\
        /* 属性名だけ */\
        bp_stradd( to, te_##tag_name[i].name);\
      }else{\
        /* name=value */\
        bp_stradd( to, te_##tag_name[i].name);\
        *to++ = '=';\
	if( in_esc >= 4){\
          /* クオートが必要 */\
          if( ( in_esc & 3) == 3){\
            quote = '\0';\
          }else if( ( in_esc & 1) == 1){\
            quote = '\'';\
          }else{\
            quote = '"';\
          }\
        }else{\
          quote = '\0';\
        }\
	if( quote != '\0') *to++ = quote;\
        bp_stradd( to, element_value);\
	if( quote != '\0') *to++ = quote;\
      }\
    }\
  }\
\
  /* 属性の必須条件を満たしていなければ処理を無効にする */\
  if( f_use < TE_USE_##tag_name) to = to2;\
\
}



/*
 * 全属性の再構成して、toにコピーする
 */
#define bp_make_tag_element_all(to,element,elements){\
  zval **data, **list;\
  int i;\
  int f_novalue = 0;\
  int in_esc;\
  unsigned char *name, *value;\
  unsigned char quote = '\0';\
\
  for( i = 0; i < elements; i++) {\
    zend_hash_index_find( element, i, (void **) &list);\
    zend_hash_find( Z_ARRVAL_PP( list), "f_novalue", 10, (void **) &data);\
    f_novalue = Z_LVAL_PP( data);\
    zend_hash_find( Z_ARRVAL_PP( list), "in_esc", 7, (void **) &data);\
    in_esc = Z_LVAL_PP( data);\
    zend_hash_find( Z_ARRVAL_PP( list), "name", 5, (void **) &data);\
    name = Z_STRVAL_PP( data);\
\
    *to++ = ' ';/* スペース */\
\
    if( f_novalue) {\
      /*  属性名のみ */\
      bp_stradd( to, name);\
    } else {\
      /* 属性値も */\
      zend_hash_find( Z_ARRVAL_PP( list), "value", 6, (void **) &data);\
      value = Z_STRVAL_PP( data);\
      bp_stradd( to, name);\
      *to++ = '=';\
      if( in_esc >= 4){\
        /* クオートが必要 */\
        if( ( in_esc & 3) == 3){\
          quote = '\0';\
        }else if( ( in_esc & 1) == 1){\
          quote = '\'';\
        }else{\
          quote = '"';\
        }\
      }else{\
        quote = '\0';\
      }\
      if( quote != '\0') *to++ = quote;\
      bp_stradd( to, value);\
      if( quote != '\0') *to++ = quote;\
    }\
  }\
\
}


/*
 * コピーされる側のポインタを進めつつ文字列コピー
 */
#define bp_stradd(to,from){\
  unsigned char *now = from;\
  while(1){\
    if( ! *now) break;\
      *to++ = *now++;\
  }\
}

/*
 *文字列のコピー、bp_levelによってマクロが変わる
 *
 * char *bp コピー先
 * char *from コピー元
 * long bp_level BPレベル
 *
 */
#define bp_text_copy(to,from,f_pre,bp_level){\
	if( bp_level < 40){\
		bp_text_copy_30( to, from, f_pre);\
	} else {\
		bp_text_copy_40( to, from, f_pre);\
	}\
}

/* 全角カナと英数字と全角スペースと記号を半角にし,
   ひらがなを半角カナにする */
#define bp_text_copy_40(to,from,f_pre){\
  int f_space = 0;/* 直前にスペースがあったか */\
  int f_htmlesc = 0;/* HTMLエスケープ  */\
  char buff_htmlesc[10];\
  unsigned char byte = '\0';\
  int i;\
  int i_tmp_1;\
  char c_tmp_1, c_tmp_2;\
  while(1){\
    if( ! *from) break;\
\
    if( byte){\
      /* 2バイト目 */\
      if( byte == 130 && *from >= 64){\
        /*  */\
        if( *from >= 79 && *from <= 121){\
          /* 全角大英字 全角数字*/\
          *to++ = (*from++) - 31;\
        }else if( *from >= 129 && *from <= 155){\
          /* 全角小英字 */\
          *to++ = (*from++) - 32;\
        }else if( *from >= 159 && *from <= 242){\
          /* ひらがな */\
          if( *from < 222){\
            i = *from - 95 - 64;\
          }else{\
            i = *from - 94 - 64;\
          }\
          if( bp_zen2han_kana[i][0]){\
            *to++ = bp_zen2han_kana[i][0];\
            if( bp_zen2han_kana[i][1]){\
              *to++ = bp_zen2han_kana[i][1];\
              if( bp_zen2han_kana[i][2]){\
                *to++ = bp_zen2han_kana[i][2];\
              }\
            }\
          }\
          from++;\
	}else{\
          *to++ = byte;\
          *to++ = *from++;\
	}\
      }else if( byte == 131 && ( *from >= 64 && *from <= 148)){\
        /* 全角カナ */\
        i = *from - 64;\
        if( bp_zen2han_kana[i][0]){\
          *to++ = bp_zen2han_kana[i][0];\
          if( bp_zen2han_kana[i][1]){\
            *to++ = bp_zen2han_kana[i][1];\
            if( bp_zen2han_kana[i][2]){\
              *to++ = bp_zen2han_kana[i][2];\
            }\
          }\
        }\
        from++;\
      }else if( byte == 129 && ( *from >= 64 && *from <= 151)){\
        /* 全角記号 */\
/*\
        if( *from == 64){\
          *to++ = ' ';\
          from++;\
        }else if( *from == 91){\
          *to++ = '-';\
          from++;\
        }else{\
          *to++ = byte;\
          *to++ = *from++;\
        }\
*/\
        i = *from - 64;\
        if( bp_zen2han_kigo[i][0]){\
          *to++ = bp_zen2han_kigo[i][0];\
          if( bp_zen2han_kigo[i][1]){\
            *to++ = bp_zen2han_kigo[i][1];\
            if( bp_zen2han_kigo[i][2]){\
              *to++ = bp_zen2han_kigo[i][2];\
            }\
          }\
        }\
        from++;\
      }else{\
        *to++ = byte;\
        *to++ = *from++;\
      }\
\
      byte = '\0';\
      c_tmp_1 = *( to - 1);\
      if( c_tmp_1 == ' '){\
        if( f_space){\
          /* 連続スペース */\
          to--;\
	}else{\
	  f_space = 1;\
        }\
      }else{\
	f_space = 0;\
      }\
    } else {\
      /* 1バイト目 */\
      if( *from < 128 || ( *from >= 161 && *from <= 223)){\
	f_space = 0;\
\
	if( f_htmlesc){\
	  /* 絵文字判定中 */\
	  if( f_htmlesc == 7 && *from == ';'){\
	    /* 終了 */\
            buff_htmlesc[7] = '\0';\
            i_tmp_1 = atoi( buff_htmlesc + 2);\
            *to++ = i_tmp_1 / 256;\
            *to++ = i_tmp_1 % 256;\
            f_htmlesc = 0;\
            from++;\
            continue;\
	  }else if( f_htmlesc == 1){\
            if( *from == '#'){\
              buff_htmlesc[f_htmlesc] = *from++;\
	      f_htmlesc++;\
    	      continue;\
            }else{\
              *to++ = '&';\
	      f_htmlesc = 0;\
            }\
	  }else if( f_htmlesc > 6 || ! isalnum( *from)){\
	    /* 見当違いだった */\
	    for( i = 0; i < f_htmlesc; i++) *to++ = buff_htmlesc[i];\
	    f_htmlesc = 0;\
	  }else{\
	    /* まだなんともいえない */\
            if( ( f_htmlesc == 2 && *from != '6') || ( f_htmlesc == 3 && *from != '3')){\
              /* このコードは絵文字ではない */\
              *to++ = '&';\
              *to++ = '#';\
              if( f_htmlesc == 3) *to++ = '6';\
              f_htmlesc = 0;\
            }else{\
	      buff_htmlesc[f_htmlesc] = *from++;\
	      f_htmlesc++;\
	      continue;\
            }\
	  }\
	}\
        if( *from == '&'){\
          /* 絵文字? */\
          buff_htmlesc[f_htmlesc] = *from++;\
          f_htmlesc = 1;\
          continue;\
        }\
\
        /* スペース等の処理 */\
\
        if( *from == '\r'){\
          /* CR */\
          from++;\
          continue;\
        }\
\
        if( *from == ' ' || *from == '\t'){\
	  /* スペース/タブ */\
          if( *( to - 1) == ' ') to--;\
          while(1){\
            c_tmp_1 = *( from + 1);\
            if( c_tmp_1 == '\n'){\
              from++;\
              break;\
            }\
            if( c_tmp_1 != ' ' && c_tmp_1 != '\t'){\
              *to++ = ' ';\
              from++;\
              f_space = 1;\
              break;\
            }\
            from++;\
            if( ! *from) break;\
          }\
	  continue;\
	}\
        if( *from == '\n' && f_pre == 0){\
	  /* 改行 */\
          if( *( to - 1) == ' ') to--;\
          while(1){\
            if( ! *from) break;\
            c_tmp_1 = *( from + 1);\
            if( c_tmp_1 != ' ' && c_tmp_1 != '\t' && c_tmp_1 != '\n'){\
              from++;\
              break;\
            }\
            from++;\
          }\
          continue;\
        }\
\
\
\
        *to++ = *from++;\
      } else {\
        byte = *from++;\
      }\
    }\
\
  }\
}

/* 全角カナと英数字と全角スペースを半角にする */
#define bp_text_copy_30(to,from,f_pre){\
  int f_space = 0;/* 直前にスペースがあったか */\
  int f_htmlesc = 0;/* HTMLエスケープ  */\
  char buff_htmlesc[10];\
  unsigned char byte = '\0';\
  int i;\
  int i_tmp_1;\
  char c_tmp_1, c_tmp_2;\
  while(1){\
    if( ! *from) break;\
\
    if( byte){\
      /* 2バイト目 */\
      if( byte == 130 && *from >= 64){\
        /*  */\
        if( *from >= 79 && *from <= 121){\
          /* 全角大英字 全角数字*/\
          *to++ = (*from++) - 31;\
        }else if( *from >= 129 && *from <= 155){\
          /* 全角小英字 */\
          *to++ = (*from++) - 32;\
/*\
        }else if( *from >= 159 && *from <= 242){\
          // ひらがな \
          if( *from < 222){\
            i = *from - 95 - 64;\
          }else{\
            i = *from - 94 - 64;\
          }\
          if( bp_zen2han_kana[i][0]){\
            *to++ = bp_zen2han_kana[i][0];\
            if( bp_zen2han_kana[i][1]){\
              *to++ = bp_zen2han_kana[i][1];\
              if( bp_zen2han_kana[i][2]){\
                *to++ = bp_zen2han_kana[i][2];\
              }\
            }\
          }\
          from++;\
*/\
	}else{\
          *to++ = byte;\
          *to++ = *from++;\
	}\
      }else if( byte == 131 && ( *from >= 64 && *from <= 148)){\
        /* 全角カナ */\
        i = *from - 64;\
        if( bp_zen2han_kana[i][0]){\
          *to++ = bp_zen2han_kana[i][0];\
          if( bp_zen2han_kana[i][1]){\
            *to++ = bp_zen2han_kana[i][1];\
            if( bp_zen2han_kana[i][2]){\
              *to++ = bp_zen2han_kana[i][2];\
            }\
          }\
        }\
        from++;\
      }else if( byte == 129 && ( *from >= 64 && *from <= 151)){\
        /* 全角記号 */\
\
        if( *from == 64){\
          *to++ = ' ';\
          from++;\
        }else if( *from == 91){\
          *to++ = '-';\
          from++;\
        }else{\
          *to++ = byte;\
          *to++ = *from++;\
        }\
/*\
        i = *from - 64;\
        if( bp_zen2han_kigo[i][0]){\
          *to++ = bp_zen2han_kigo[i][0];\
          if( bp_zen2han_kigo[i][1]){\
            *to++ = bp_zen2han_kigo[i][1];\
            if( bp_zen2han_kigo[i][2]){\
              *to++ = bp_zen2han_kigo[i][2];\
            }\
          }\
        }\
        from++;\
*/\
      }else{\
        *to++ = byte;\
        *to++ = *from++;\
      }\
\
      byte = '\0';\
      c_tmp_1 = *( to - 1);\
      if( c_tmp_1 == ' '){\
        if( f_space){\
          /* 連続スペース */\
          to--;\
	}else{\
	  f_space = 1;\
        }\
      }else{\
	f_space = 0;\
      }\
    } else {\
      /* 1バイト目 */\
      if( *from < 128 || ( *from >= 161 && *from <= 223)){\
	f_space = 0;\
\
	if( f_htmlesc){\
	  /* 絵文字判定中 */\
	  if( f_htmlesc == 7 && *from == ';'){\
	    /* 終了 */\
            buff_htmlesc[7] = '\0';\
            i_tmp_1 = atoi( buff_htmlesc + 2);\
            *to++ = i_tmp_1 / 256;\
            *to++ = i_tmp_1 % 256;\
            f_htmlesc = 0;\
            from++;\
            continue;\
	  }else if( f_htmlesc == 1){\
            if( *from == '#'){\
              buff_htmlesc[f_htmlesc] = *from++;\
	      f_htmlesc++;\
    	      continue;\
            }else{\
              *to++ = '&';\
	      f_htmlesc = 0;\
            }\
	  }else if( f_htmlesc > 6 || ! isalnum( *from)){\
	    /* 見当違いだった */\
	    for( i = 0; i < f_htmlesc; i++) *to++ = buff_htmlesc[i];\
	    f_htmlesc = 0;\
	  }else{\
	    /* まだなんともいえない */\
            if( ( f_htmlesc == 2 && *from != '6') || ( f_htmlesc == 3 && *from != '3')){\
              /* このコードは絵文字ではない */\
              *to++ = '&';\
              *to++ = '#';\
              if( f_htmlesc == 3) *to++ = '6';\
              f_htmlesc = 0;\
            }else{\
              buff_htmlesc[f_htmlesc] = *from++;\
	      f_htmlesc++;\
	      continue;\
            }\
	  }\
	}\
        if( *from == '&'){\
          /* 絵文字? */\
          buff_htmlesc[f_htmlesc] = *from++;\
          f_htmlesc = 1;\
          continue;\
        }\
\
        /* スペース等の処理 */\
\
        if( *from == '\r'){\
          /* CR */\
          from++;\
          continue;\
        }\
\
        if( *from == ' ' || *from == '\t'){\
	  /* スペース/タブ */\
          if( *( to - 1) == ' ') to--;\
          while(1){\
            c_tmp_1 = *( from + 1);\
            if( c_tmp_1 == '\n'){\
              from++;\
              break;\
            }\
            if( c_tmp_1 != ' ' && c_tmp_1 != '\t'){\
              *to++ = ' ';\
              from++;\
              f_space = 1;\
              break;\
            }\
            from++;\
            if( ! *from) break;\
          }\
	  continue;\
	}\
        if( *from == '\n' && f_pre == 0){\
	  /* 改行 */\
          if( *( to - 1) == ' ') to--;\
          while(1){\
            if( ! *from) break;\
            c_tmp_1 = *( from + 1);\
            if( c_tmp_1 != ' ' && c_tmp_1 != '\t' && c_tmp_1 != '\n'){\
              from++;\
              break;\
            }\
            from++;\
          }\
          continue;\
        }\
\
\
\
        *to++ = *from++;\
      } else {\
        byte = *from++;\
      }\
    }\
\
  }\
}




/*
 * 全角カナ -> 半角カナ テーブル
 */

unsigned char bp_zen2han_kana[100][3] = { 
  { 167, 0, 0},
  { 177, 0, 0},
  { 168, 0, 0},
  { 178, 0, 0},
  { 169, 0, 0},
  { 179, 0, 0},
  { 170, 0, 0},
  { 180, 0, 0},
  { 171, 0, 0},
  { 181, 0, 0},
  { 182, 0, 0},
  { 182, 222, 0},
  { 183, 0, 0},
  { 183, 222, 0},
  { 184, 0, 0},
  { 184, 222, 0},
  { 185, 0, 0},
  { 185, 222, 0},
  { 186, 0, 0},
  { 186, 222, 0},
  { 187, 0, 0},
  { 187, 222, 0},
  { 188, 0, 0},
  { 188, 222, 0},
  { 189, 0, 0},
  { 189, 222, 0},
  { 190, 0, 0},
  { 190, 222, 0},
  { 191, 0, 0},
  { 191, 222, 0},
  { 192, 0, 0},
  { 192, 222, 0},
  { 193, 0, 0},
  { 193, 222, 0},
  { 175, 0, 0},
  { 194, 0, 0},
  { 194, 222, 0},
  { 195, 0, 0},
  { 195, 222, 0},
  { 196, 0, 0},
  { 196, 222, 0},
  { 197, 0, 0},
  { 198, 0, 0},
  { 199, 0, 0},
  { 200, 0, 0},
  { 201, 0, 0},
  { 202, 0, 0},
  { 202, 222, 0},
  { 202, 223, 0},
  { 203, 0, 0},
  { 203, 222, 0},
  { 203, 223, 0},
  { 204, 0, 0},
  { 204, 222, 0},
  { 204, 223, 0},
  { 205, 0, 0},
  { 205, 222, 0},
  { 205, 223, 0},
  { 206, 0, 0},
  { 206, 222, 0},
  { 206, 223, 0},
  { 207, 0, 0},
  { 208, 0, 0},
  { 0, 0, 0},
  { 209, 0, 0},
  { 210, 0, 0},
  { 211, 0, 0},
  { 172, 0, 0},
  { 212, 0, 0},
  { 173, 0, 0},
  { 213, 0, 0},
  { 174, 0, 0},
  { 214, 0, 0},
  { 215, 0, 0},
  { 216, 0, 0},
  { 217, 0, 0},
  { 218, 0, 0},
  { 219, 0, 0},
  { 220, 0, 0},
  { 220, 0, 0},
  { 178, 0, 0},
  { 180, 0, 0},
  { 166, 0, 0},
  { 221, 0, 0},
  { 179, 222, 0}
};

/*
 * 全角記号 -> 半角記号 テーブル
 */

unsigned char bp_zen2han_kigo[100][3] = { 
  { 32, 0, 0},
  { 164, 0, 0},
  { 161, 0, 0},
  { 44, 0, 0},
  { 46, 0, 0},
  { 165, 0, 0},
  { 58, 0, 0},
  { 59, 0, 0},
  { 63, 0, 0},
  { 33, 0, 0},
  { 222, 0, 0},
  { 223, 0, 0},
  { 39, 0, 0},
  { 34, 0, 0},
  { 129, 78, 0},
  { 94, 0, 0},
  { 129, 80, 0},
  { 95, 0, 0},
{ 129, 82, 0},
{ 129, 83, 0},
{ 129, 84, 0},
{ 129, 85, 0},
{ 129, 86, 0},
{ 129, 87, 0},
{ 129, 88, 0},
{ 129, 89, 0},
{ 129, 90, 0},
{ 176, 0, 0},
{ 45, 0, 0},
{ 45, 0, 0},
{ 47, 0, 0},
{ 92, 0, 0},
{ 126, 0, 0},
{ 129, 97, 0},
{ 124, 0, 0},
{ 129, 99, 0},
{ 129, 100, 0},
{ 96, 0, 0},
{ 39, 0, 0},
{ 34, 0, 0},
{ 34, 0, 0},
{ 40, 0, 0},
{ 41, 0, 0},
{ 91, 0, 0},
{ 93, 0, 0},
{ 91, 0, 0},
{ 93, 0, 0},
{ 123, 0, 0},
{ 125, 0, 0},
{ 129, 113, 0},
{ 129, 114, 0},
{ 129, 115, 0},
{ 129, 116, 0},
{ 162, 0, 0},
{ 163, 0, 0},
{ 162, 0, 0},
{ 163, 0, 0},
{ 91, 0, 0},
{ 93, 0, 0},
{ 43, 223, 0},
{ 45, 0, 0},
{ 129, 125, 0},
{ 129, 126, 0},
{ 0, 0, 0},
{ 129, 128, 0},
{ 61, 0, 0},
{ 129, 130, 0},
{ 129, 131, 0},
{ 129, 132, 0},
{ 129, 133, 0},
{ 129, 134, 0},
{ 129, 135, 0},
{ 129, 136, 0},
{ 129, 137, 0},
{ 129, 138, 0},
{ 223, 0, 0},
{ 39, 0, 0},
{ 34, 0, 0},
{ 129, 142, 0},
{ 92, 0, 0},
{ 36, 0, 0},
{ 129, 145, 0},
{ 129, 146, 0},
{ 37, 0, 0},
{ 35, 0, 0},
{ 38, 0, 0},
{ 42, 0, 0},
{ 64, 0, 0},
  };

#endif
