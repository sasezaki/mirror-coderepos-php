/*
   +----------------------------------------------------------------------+
   | PHP version 4.0                                                      |
   +----------------------------------------------------------------------+
   | Copyright (c) 1997, 1998, 1999, 2000, 2001 The PHP Group             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 2.02 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available at through the world-wide-web at                           |
   | http://www.php.net/license/2_02.txt.                                 |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Authors:                                                             |
   |                                                                      |
   +----------------------------------------------------------------------+
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "php_bp.h"


/*
HTMLをパースする
 */
PHP_FUNCTION(bp_html_parse) {
	char *html = NULL;
	zval **parm_html;

	zval *tag_array = NULL;/* タグ情報配列 */

	int  f_intag = 0;/* パーサがタグをの中にいるか */
	int  f_tagname = 0;/* タグ名を取得済か */
	int  f_tagclose = 0;/* 閉じタグか 1/ コメントタグか 2 */
	int  f_inelementname = 0;/* 属性名の探索中 */
	int  f_inelementvalue = 0;/* 属性値の探索中 */
	int  f_elementfind = 0;/* 属性名か属性値の解析処理に入ったら 1 */
	int  f_equfind = 0;/* =を探す */
	int  in_esc = 0;/* 特殊文字列の種類フラグ  */
	char *start_pos, *now_pos;/* 解析対象の最初と現在のポインタ */
	char *start_tag_pos;/* タグ解析対象の最初のポインタ */

	int  tag_quote_out = 1;/* 属性値の"や'の外にいる時に1 */

	char *text = NULL;/* 解析対象文字列 */
	char *tag_name;/* タグ名のバッファ */
	char *element_name;/* 属性名のバッファ */
	char *element_value;/* 属性のバッファ */
	char element_quote = 0;/* 属性値のクオート  */
	zval *element_array_list;/* 属性配列リストのバッファ */
	zval *element_array;/* 属性配列のバッファ */

	int elements = 0;/* 属性の数 */
	int values = 0;/* 属性に対する値の数 */

	int f_injis = 0;/* jisコードを処理中 1=4=esc検出 2=$検出 5=(検出 */

	/*
	 メインの配列作成 
	 */
	if( array_init( return_value) != SUCCESS) {
		zend_error( E_WARNING, "make array error ( bp module / return_value)");
	}

	/*
	  引数を取得
	 */
	if( ZEND_NUM_ARGS() != 1) {
		zend_error( E_WARNING, "Use bp_html_parse( $html)");
		WRONG_PARAM_COUNT;		
	}
	if( zend_get_parameters_ex( 1, &parm_html) == FAILURE) {
				WRONG_PARAM_COUNT;
	}
	convert_to_string_ex( parm_html);
	html = Z_STRVAL_PP( parm_html);

	/*
	  HTML解析開始
	 */
	start_pos = now_pos = html;
	now_pos--;
	while( *(++now_pos)) {

		if( *now_pos == 0x1B && ! ( *(now_pos - 1) == '>' && tag_quote_out && f_intag && f_tagname)) {
			/* ESC検出 */
			f_injis++;
		}else if( f_injis != 0) {
			/* ESC検出中 Or jisコードの中身 */

			switch( f_injis) {
			case 1:
				if( *now_pos == '$') {
					f_injis++;
				}else{
					f_injis = 0;
				}
				break;
			case 2:
				if( *now_pos == 'B') {
					/* jis in */
					f_injis++;
				}else{
					f_injis = 0;
				}
				break;
			case 4:
				if( *now_pos == '(') {
					f_injis++;
				}else{
					f_injis = 3;
				}
				break;
			case 5:
				if( *now_pos == 'B') {
					/* jis out */
					f_injis = 0;
				}else{
					f_injis = 3;
				}
				break;
			}
		}else if( f_intag) {
			/*
			  タグの解析
			 */
			if( ! f_tagname) {
				/* タグ名を取得する */
				if( start_tag_pos == now_pos){
					/* タグの最初の文字なら */
					switch( *now_pos) {
					case '/':
						f_tagclose = 1;
						start_tag_pos++;
						break;
					case '!':
						f_tagclose = 2;
						start_tag_pos++;
						break;
					}
				} else {
					if( isspace( *now_pos) || *now_pos == '>') {
						/* タグ名が終っている */
						char tmp;
						f_tagname = 1;
						
						tmp = *now_pos;
						*now_pos = NULL;
						tag_name = estrdup( start_tag_pos);
						*now_pos = tmp;

						/* 属性探索に関する変数の初期化 */
						f_inelementname    = 1;/* 属性名探索 */
						f_inelementvalue   = 0;
						f_elementfind      = 1;
						f_equfind          = 0;
						tag_quote_out      = 1;
						element_name       = NULL;
						element_value      = NULL;
						element_array_list = NULL;
						element_array      = NULL;
						element_quote      = 0;
						elements           = 0;
						values             = 0;
						in_esc             = 0;

						start_tag_pos = now_pos;
						
						/* 配列の初期化 */
						MAKE_STD_ZVAL( element_array_list);
						if( array_init( element_array_list) != SUCCESS) {
							zend_error( E_WARNING,
										"make array error ( bp module / element_array_list)");
						}
					}
				}

			} else if( f_tagclose == 2) {
				/* コメントの処理 */
				if( ( *(now_pos - 3) == *(start_pos + 2) && 
					*(now_pos - 2) == *(start_pos + 3) && *(now_pos - 1) == '>') || 
					( isalpha( *(start_pos + 2)) && *(now_pos - 1) == '>')  || 
					( ! isalpha( *(start_pos + 2)) && *(start_pos + 2) != '-' &&*(now_pos - 1) == '>')) {
					/* コメント終了 */
					char tmp;
					f_intag = 0;
					
					tmp = *now_pos;
					*now_pos = NULL;
					text = estrdup( start_pos);
					*now_pos = tmp;
				
					/* HTML配列にタグ情報をいれる  */
					bp_make_tag_array ( tag_array, BP_IS_TAG, text, tag_name, f_tagclose,
										elements, values, element_array_list);
					bp_array_insert( return_value, tag_array);
					
					start_pos = now_pos;
					now_pos--;/* もう一度このポイントを評価する */
					
					/* 使わないものを整理 */
					tag_array = NULL;
					element_array_list = NULL;
					efree( tag_name);				
					tag_name = NULL;
					efree( text);
					text = NULL;
				}
			} else if( *(now_pos - 1) == '>' && tag_quote_out) {
				/* タグが終っていた */
				char tmp;
				f_intag = 0;
				
				if( element_name != NULL) {
					/* 属性配列に属性をいれる  */

					bp_make_element_array ( element_array, element_name, element_value, in_esc);
					bp_array_insert( element_array_list, element_array);

					/* 使わないものを整理 */
					element_array = NULL;
					efree( element_name);
					element_name = NULL;
					if( element_value != NULL)
						efree( element_value);
					element_value = NULL;
				}
				
				tmp = *now_pos;
				*now_pos = NULL;
				text = estrdup( start_pos);
				*now_pos = tmp;
				
				/* HTML配列にタグ情報をいれる  */
				bp_make_tag_array ( tag_array, BP_IS_TAG, text, tag_name, f_tagclose,
									elements, values, element_array_list);
				bp_array_insert( return_value, tag_array);
				
				start_pos = now_pos;
				now_pos--;/* もう一度このポイントを評価する */
				
				/* 使わないものを整理 */
				tag_array = NULL;
				element_array_list = NULL;
				efree( tag_name);				
				tag_name = NULL;
				efree( text);
				text = NULL;
			} else {
				/* 属性探索 */
				if( f_elementfind) {
					/* 属性名か属性値の開始ポイントを探す */
					if( ! isspace( *now_pos)) {
						/* 開始 */

						if( f_inelementvalue) {
							/* 属性値の始め */
							if( *now_pos == '"' || *now_pos == '\'') {
								/* クオート  */
								element_quote = *now_pos;
								tag_quote_out = 0;
								start_tag_pos = now_pos + 1;
							} else {
								element_quote = 0;
								start_tag_pos = now_pos;
							}

						} else {
							/* 属性名 */
							start_tag_pos = now_pos;
						}

						f_elementfind = 0;
					}
				} else if( f_equfind) {
					/* = を探す */
					if( *now_pos == '=') {
						/* 発見 */
						f_equfind = 0;
						f_elementfind = 1;/* 属性値を探す */
						start_tag_pos = now_pos + 1;
					} else if( ! isspace( *now_pos)) {
						/* 次の属性名が来てしまった */
							f_inelementname = 1;
							f_inelementvalue = 0;
							f_elementfind = 0;
							f_equfind = 0;
							start_tag_pos = now_pos;

							/* 属性配列に属性をいれる  */
							
							bp_make_element_array ( element_array, element_name, element_value, in_esc);
							bp_array_insert( element_array_list, element_array);
							
							/* 使わないものを整理 */
							element_array = NULL;
							efree( element_name);
							element_name = NULL;
							element_value = NULL;
					}
				} else {
					/* 属性名、属性値の内容を終りまで探索 */
					int f = 0;
					if( element_quote && f_inelementvalue) {
						/* 属性値探索時で"か'で囲まれている場合*/
						if( *now_pos == element_quote) {
							f = 1;
							tag_quote_out = 1;
							element_quote = 0;
						}
					}else if( f_inelementvalue){
						/* 属性値探索時 */
						if( isspace( *now_pos) || *now_pos == '>')
							f = 1;
					} else {
						/* 属性名探索時 */
						if( isspace( *now_pos) || *now_pos == '=' || *now_pos == '>')
							f = 1;
					}
					if( f) {
						/* 名前もしくは値の終り */
						char tmp, *value_tmp;

						tmp = *now_pos;
						*now_pos = NULL;
						value_tmp = estrdup( start_tag_pos);
						*now_pos = tmp;
						if( f_inelementname){
							/* 属性名 */
							element_name = value_tmp;
							elements++;
							if( *now_pos == '='){
								f_elementfind = 1;/* 属性値を探す */
								start_tag_pos = now_pos + 1;
							} else {
								f_equfind = 1;/* = を探す */
							}
							f_inelementname = 0;
							f_inelementvalue = 1;

							element_value = NULL;
						} else {
							element_value = value_tmp;
							f_inelementname = 1;
							f_inelementvalue = 0;
							f_elementfind = 1;/* 属性名を探す */

							values++;

							/* 属性配列に属性をいれる  */
							
							bp_make_element_array ( element_array, element_name, element_value, in_esc);
							bp_array_insert( element_array_list, element_array);

							/* 使わないものを整理 */
							element_array = NULL;
							efree( element_name);
							element_name = NULL;
							efree( element_value);
							element_value = NULL;
						}

					} else if( f_inelementvalue) {
						/* 属性値の中の特殊文字を調べる */
						switch( *now_pos) {
						case '"':
							in_esc |= 1;
							break;
						case '\'':
							in_esc |= 2;
							break;
						case '<':
							in_esc |= 4;
							break;
						case '>':
							in_esc |= 8;
							break;
						case ' ':
							in_esc |= 16;
							break;
						case '\t':
							in_esc |= 32;
							break;
						case '\n':
							in_esc |= 64;
							break;
						case '\r':
							in_esc |= 128;
							break;
						}
					}
				}
			}

		} else {
			/*
			  タグの解析でない
			 */

			if( *now_pos == '<') {
				/* タグの始まりに来た */
				f_intag = 1;
				
				if( now_pos != start_pos) {
					/* 解析開始ポイントではない */
					char tmp;
					
					tmp = *now_pos;
					*now_pos = NULL;
					text = estrdup( start_pos);
					*now_pos = tmp;


					/* HTML配列にTEXT情報をいれる  */
					bp_make_tag_array ( tag_array, BP_IS_TEXT, text, NULL, 0, 0, 0, NULL);
					bp_array_insert( return_value, tag_array);

					start_pos = now_pos;

					/* 使わないものを整理 */
					efree( text);
					text = NULL;
				}

				/* タグ解析に関する変数の初期化*/
				f_tagname = f_tagclose = in_esc = 0;
				start_tag_pos       = now_pos + 1;
				tag_name           = NULL;
				
			}
		}
	}
}



/*
  上記関数で戻されるデータフォーマット
  
  struct z_html_array {
    int f_type;//タグかただのテキストか  1 = タグ / 2 = テキスト
	char *text;//解析された文字列の全体
	char *tag_name;//タグの名前が入る(a br inputとか)
	int f_close;//閉じタグの場合は1が入り、コメントなら2で、それ以外は0
	int elements;//属性の数
	int values;//name=value形式の属性の数
	struct element {//属性
	  char *name;//属性名
	  char *value;//値
	  int f_novalue;//name=value形式の属性でない場合は1
	  int in_esc;//valueの各種記号が入っているかどうか、入っていればそれぞれbit単位でフラグが立つ
	  //" 1
	  //' 2
	  //> 4
	  //< 8
	  //スペース 16
	  //TAB 32
	  //\n  64
	  //\r 128
	  //
	  //
	}
  }
 */



/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: sw=4 ts=4 tw=78 fdm=marker
 * vim<600: sw=4 ts=4 tw=78
 */


