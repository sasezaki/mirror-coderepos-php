/*
   +----------------------------------------------------------------------+
   | PHP version 4.0                                                      |
   +----------------------------------------------------------------------+
   | Copyright (c) 1997, 1998, 1999, 2000, 2001 The PHP Group             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 2.02 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available at through the world-wide-web at                           |
   | http://www.php.net/license/2_02.txt.                                 |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Authors:                                                             |
   |                                                                      |
   +----------------------------------------------------------------------+
 */

#ifndef PHP_BP_H
#define PHP_BP_H

/*
定数
 */
#define BP_IS_TAG  1
#define BP_IS_TEXT 2


extern zend_module_entry bp_module_entry;
#define phpext_bp_ptr &bp_module_entry

#ifdef PHP_WIN32
#define PHP_BP_API __declspec(dllexport)
#else
#define PHP_BP_API
#endif

#ifdef ZTS
#include "TSRM.h"
#endif

PHP_MINIT_FUNCTION(bp);
PHP_MSHUTDOWN_FUNCTION(bp);
PHP_RINIT_FUNCTION(bp);
PHP_RSHUTDOWN_FUNCTION(bp);
PHP_MINFO_FUNCTION(bp);

PHP_FUNCTION(bp_html_parse);
PHP_FUNCTION(bp_html_optimize);

/* 
  	Declare any global variables you may need between the BEGIN
	and END macros here:     

ZEND_BEGIN_MODULE_GLOBALS(bp)
	int   global_value;
	char *global_string;
ZEND_END_MODULE_GLOBALS(bp)
*/

/* In every utility function you add that needs to use variables 
   in php_bp_globals, call TSRM_FETCH(); after declaring other 
   variables used by that function, or better yet, pass in TSRMG_CC
   after the last function argument and declare your utility function
   with TSRMG_DC after the last declared argument.  Always refer to
   the globals in your function as BP_G(variable).  You are 
   encouraged to rename these macros something shorter, see
   examples in any other php module directory.
*/

#ifdef ZTS
#define BP_G(v) TSRMG(bp_globals_id, zend_bp_globals *, v)
#else
#define BP_G(v) (bp_globals.v)
#endif


/*
 * 属性情報を配列にまとめる
 *
 *
 * zval *array 追加先の配列
 * char *name  属性名
 * char *value 属性値
 * int  in_esc 属性値に入っている特殊文字フラグ
 *
 */
#define bp_make_element_array(array,name,value,in_esc){\
    array = NULL;\
    if( name != NULL){\
	MAKE_STD_ZVAL( array);\
	if( array_init( array) != SUCCESS)\
		zend_error( E_WARNING, "make array error ( bp module / bp_make_element_array/array)");\
    bp_lower( name);\
	add_assoc_string( array, "name", name, 1);\
	if( value == NULL) {\
		add_assoc_long( array, "f_novalue", 1);\
	} else {\
		add_assoc_string( array, "value", value, 1);\
		add_assoc_long( array, "f_novalue", 0);\
	}\
	add_assoc_long( array, "in_esc", in_esc);\
    }\
}


/*
 * タグ情報を配列にまとめる
 *
 *
 * zval *array         追加先の配列
 * int  f_type         テキスト/タグのフラグ
 * char *text          解析対象文字列
 * char *tag_name      タグ名
 * int  f_close        閉じタグフラグ
 * int elements        属性数
 * int values          属性に対する値の数
 * zval *element_array 属性情報配列
 *
 */
#define bp_make_tag_array(array,f_type,text,tag_name,f_close,elements,values,element){\
    array = NULL;\
	if( ! ( text == NULL || ( f_type != BP_IS_TAG && f_type != BP_IS_TEXT))){\
   	MAKE_STD_ZVAL( array);\
	if( array_init( array) != SUCCESS)\
		zend_error( E_WARNING, "make array error ( bp module / bp_make_element_array/array)");\
	add_assoc_long( array, "f_type", f_type);\
	add_assoc_string( array, "text", text, 1);\
	if( f_type == BP_IS_TAG) {\
		bp_lower( tag_name);\
		add_assoc_string( array, "tag_name", tag_name, 1);\
		add_assoc_long( array, "f_close", f_close);\
		add_assoc_long( array, "elements", elements);\
		add_assoc_long( array, "values", values);\
		if( elements)\
			add_assoc_zval( array, "element", element);\
	}\
    }\
}


/*
 * 配列に配列を追加する
 *
 * zval *to 追加先の配列
 * zval *from
 *
 */
#define bp_array_insert(to,from) {\
	if( from != NULL)\
	add_next_index_zval( to, from);\
}


/*
 *文字列の大文字英字を小文字英字に
 */
#define bp_lower(str){\
  char *p;\
  p = str - 1;\
 while( *(++p)){\
   *p = tolower( *p);\
 }\
}


#endif	/* PHP_BP_H */





/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 */
