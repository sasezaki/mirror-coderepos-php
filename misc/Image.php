<?php
//***
//@author takuya_1st
//@note   画像を次々と変換する
//	      New構文で、変換後は元に戻れないことをわかりやすく。
//		  変換がメソッドではオブジェクト指向っぽくない。
//$img = Image_Factory::createFromFile("sample.png");
//$img = new Image_Filter_GaussianBlur($img);//ぼかす
//$img = new Image_Filter_GrayScale($img);//グレースケール
//$img = new Image_Filter_Brightness( new Image_Resize( $img, $x, $y ) ); //拡大して、明るく

//Imageクラスの生成
class Image_Factory{
	const SRC_NEW    =  0;
	const SRC_FILE   =  1;
	const SRC_BASE64 =  2;
	const SRC_STREAM =  4;
	const SRC_URL    = 16;
	const SRC_IMG    =  8;
	public static $_id;
	public static function getInstance($type,$resource,$resource_type)
	{
		self::getResource( $resource, $resource_type );
		/*Image*/ $img = null;//Javaっぽい型宣言
		switch(strtolower($type)){
			case "png":
				$img = new Image_PNG();
				break;
			case"layer":
				$img = new Image_Layer();
		}
		$img->setResource( self::$_id );
		return $img;
	}
	public static function getResource( $string, $from ){
		switch($from){
			case(self::SRC_NEW):
				eval( $string );//ex. $string = '$w=160;$h=120;$bg=array(0,0,0);';
				$img = imagecreatetruecolor( $w, $h );
				if( $bg != null ){
					$color = imagecolorallocate($img, $bg[0], $bg[1], $bg[2]);
					imagefill( $img, 0,0, $color );
				}
				self::$_id = $img;
				break;
			case(self::SRC_URL):
			case(self::SRC_FILE):
				self::$_id = self::createFromFile( $string );
				break;
			case(self::SRC_BASE64):
				self::$_id = self::createFromBase64( $string );
				break;
			case(self::SRC_STREAM):
				self::$_id = self::createFromBinary( $string );
				break;
			case(self::SRC_IMG):
				self::$_id = $string;
				break;
		}
		return self::$_id;
	}
	public static function createFromBinary( $data ){
		return imagecreatefromstring( $data );
	}
	public static function createFromBase64( $base64_encoded_string )
	{
		return self::createFromBinary(base64_decode($base64_encoded_string));
	}
	public static function createFromFile( $filename )
	{
		$data = file_get_contents($filename);
		return self::createFromBinary($data);
	}

}
abstract class Image
{
	
	protected $resource;
	public function getRawData(){
		return $this->fetch();
	}
	protected function fetch(){
		ob_start();
		$this->display();
		$data = ob_get_contents();
		ob_end_clean();
		return $data;
	
	}
	public function height(){
		return imagesy( $this->resource );
	}
	public function width(){
		return imagesx( $this->resource );
	}
	public function setResource( $res ){
		$this->resource = $res;
	}
	public function getResource(){
		return $this->resource;
	}
	abstract protected function display();
	abstract public function imageTypeName();
	abstract public function imageType();
}
class Image_PNG extends Image
{
	public function setResource( $res ){
		imagesavealpha( $res, true );
		$this->resource = $res;
	}
	public function imageTypeName(){
		return "png";
	}
	public function imageType(){
		return IMG_PNG;
	}
	public function display( $str = null ){
		if( $str == null ){
			return imagepng( $this->getResource());
		}else{
			return imagepng( $this->getResource(), $str);
		}
	}
}
class Image_Layer extends Image
{
	public function setResource( $res ){
		imagesavealpha( $res, true );
		$this->resource = $res;
	}
	public function imageTypeName(){
		return "gd";
	}
	public function imageType(){
		return IMG_PNG;
	}
	public function display( $str = null ){
		if( $str == null ){
			return imagepng( $this->getResource());
		}else{
			return imagepng( $this->getResource(), $str);
		}
	}

}
abstract class Image_Filter extends Image
{
	protected $img;
	public function __construct( Image $img ){
		$this->img = $img;
		$res = $this->filter();
	}
	public function display($str=null){
		return $this->img->display($str);
	}
	public function imageTypeName(){
		return $this->img->imageTypeName();
	}
	public function imageType(){
		return $this->img->imageType();
	}
	public function height(){
		return $this->img->height();
	}
	public function width(){
		return $this->img->width();
	}
	public function setResource( $res ){
		return $this->img->setResource($res);
	}
	public function getResource(){
		return $this->img->getResource();
	}
	abstract protected function filter();
	
}
abstract class Image_Filter_noArg extends Image_Filter{
	public function filter(){
		return imagefilter( $this->getResource(), $this->filter );
	}
}
abstract class Image_Filter_withArg extends Image_Filter{
	public function filter(){
		switch( sizeof( $this->args ) ){
			case(3):
				return imagefilter( $this->getResource(), 
									$this->filter,
									$this->args[0],
									$this->args[1],
									$this->args[2]
								);
			case(1):
				return imagefilter( $this->getResource(), 
									$this->filter,
									$this->args[0]
								);
				
		}
	}
}
class Image_Filter_GrayScale extends Image_Filter_noArg
{
	//画像をグレースケールに変換します。
	protected $filter = IMG_FILTER_GRAYSCALE;

}
class Image_Filter_EmphasizeEdge extends Image_Filter_noArg
{
	//エッジの強調
	protected $filter = IMG_FILTER_EDGEDETECT;
}
class Image_Filter_Negate extends Image_Filter_noArg
{
	//ネガポジ反転
	protected $filter = IMG_FILTER_NEGATE;
}
class Image_Filter_Emboss extends Image_Filter_noArg
{
	// 画像にエンボス処理を行います。 
	protected $filter = IMG_FILTER_EMBOSS;
}
class Image_Filter_Simplefy extends Image_Filter_noArg
{
	//線の数を減らしてイラスト風に仕上げる
	protected $filter = IMG_FILTER_MEAN_REMOVAL;
}
class Image_Filter_GaussianBlur extends Image_Filter_noArg
{
	//ガウス分布を 使用して画像をぼかします。 
	protected $filter = IMG_FILTER_GAUSSIAN_BLUR;
}
class Image_Filter_Blur extends Image_Filter_noArg
{
	//通常のぼかし
	protected $filter = IMG_FILTER_SELECTIVE_BLUR;
}


class Image_Filter_ColorFilter extends Image_Filter_withArg
{
	//グレースケールに似ているが、指定色で覆う
	protected $filter  = IMG_FILTER_COLORIZE;
	protected $args    = array(255, 0, 0);//R,G,B
	public function __construct( Image $img , $rgb=null ){
		if( $rgb != null ){
			$this->args= $rgb;
		}
		parent::__construct( $img, $this->args );
	}
}
class Image_Filter_Brightness extends Image_Filter_withArg{
	protected $filter  = IMG_FILTER_BRIGHTNESS;
	protected $args    = array(50);//輝度
	public function __construct( Image $img , $brightness=null ){
		if( $rgb != null ){
			$this->args= array( $brightness );
		}
		parent::__construct( $img, $this->args );
	}
}
class Image_Filter_Contrast extends Image_Filter_withArg{
	protected $filter  = IMG_FILTER_CONTRAST;
	protected $args    = array(50);//コントラスト
	public function __construct( Image $img , $contrast=null ){
		if( $rgb != null ){
			$this->args= array( $contrast );
		}
		parent::__construct( $img, $this->args );
	}
}
class Image_ResizeByPercent extends Image{
	public function __construct( Image $img, $percent = 0.5 ){
		$this->img = $img;
		$new_w = $this->width()  * $percent;
		$new_h = $this->height() * $percent;
		$this->img = new Image_Resize( $this->img, $new_w, $new_h );
	}
	public function display($str=null){
		return $this->img->display($str);
	}
	public function imageTypeName(){
		return $this->img->imageTypeName();
	}
	public function imageType(){
		return $this->img->imageType();
	}
	public function height(){
		return $this->img->height();
	}
	public function width(){
		return $this->img->width();
	}
	public function setResource( $res ){
		return $this->img->setResource($res);
	}
	public function getResource(){
		return $this->img->getResource();
	}
}
class Image_Resize extends Image{
	public function __construct( Image $img, $x, $y){
		$this->img = $img;
		$new_w = $x;
		$new_h = $y;
		$new_img = imagecreatetruecolor( $new_w, $new_h );
		imagecopyresampled( $new_img,
							$this->getResource(),
							0,
							0, 
							0, 
							0, 
							$new_w, $new_h,
							$this->width(), 
							$this->height()
		);
		$this->setResource( $new_img );
	}
	public function display($str=null){
		return $this->img->display($str);
	}
	public function imageTypeName(){
		return $this->img->imageTypeName();
	}
	public function imageType(){
		return $this->img->imageType();
	}
	public function height(){
		return $this->img->height();
	}
	public function width(){
		return $this->img->width();
	}
	public function setResource( $res ){
		return $this->img->setResource($res);
	}
	public function getResource(){
		return $this->img->getResource();
	}
}
class Image_Rotate extends Image
{
	public function __construct( Image $img, $degree ){
		$this->img = $img;
		$res = imagerotate( $this->getResource(), $degree, 0, false );
		$this->setResource( $res );
	}
	public function display($str=null){
		return $this->img->display($str);
	}
	public function imageTypeName(){
		return $this->img->imageTypeName();
	}
	public function imageType(){
		return $this->img->imageType();
	}
	public function height(){
		return $this->img->height();
	}
	public function width(){
		return $this->img->width();
	}
	public function setResource( $res ){
		return $this->img->setResource($res);
	}
	public function getResource(){
		return $this->img->getResource();
	}
}
//角丸 動くけど、あまりうまく処理できない
class Image_RoundCorner extends Image  
{
	public function __construct( Image $img, $radius/*半径*/ ){
		$this->img = $img;
		$this->r = $radius;
		$this->roundCorner();
	}
	protected function roundCorner(){
		$res  = $this->getResource();
		$mask = $this->getMaskImg();
		imagecopymerge( $res, $mask, 0,0,0,0,$this->width(), $this->height(), 10 );
	}
	protected function getMaskImg(){
		$bg =  imagecreatetruecolor( $this->width(), $this->height() );
		//
		$bg_color = array( 255, 255, 255 );
		$cr_color = array(   0,   0,   0 );
		//top-left
		$img_tl = $this->cornerImg( $this->r, !false, !false, $cr_color, $bg_color );
		//top-right
		$img_tr = $this->cornerImg( $this->r, !false,  !true, $cr_color, $bg_color );
		//bottom-left
		$img_bl = $this->cornerImg( $this->r,  !true, !false, $cr_color, $bg_color );
		//bottom-right
		$img_br = $this->cornerImg( $this->r,  !true,  !true, $cr_color, $bg_color );
		//
		$color = imagecolorallocate( $img, $cr_color[0], $cr_color[1], $cr_color[2] );
		ImageFill( $bg, 0,0, $color );
		imageantialias( $bg, false );
		$dist_x = $this->width()  - $this->r;
		$dist_y = $this->height() - $this->r;
		imagecopy( $bg, $img_tl,       0,       0, 0, 0, $this->r, $this->r );
		imagecopy( $bg, $img_tr,       0, $dist_y, 0, 0, $this->r, $this->r );
		imagecopy( $bg, $img_bl, $dist_x,       0, 0, 0, $this->r, $this->r );
		imagecopy( $bg, $img_br, $dist_x, $dist_y, 0, 0, $this->r, $this->r );
		
		$ret = imagecolortransparent( $bg, $color );
		var_dump( $ret );
		
		return $bg;
		
	}
	/** 
	* A(0,0)
	* --------------D(r,0)
	* |  黒     *  |
	* |      *     |
	* |    *       |
	* |  *         |
	* | *    塗り  |
	* |*           |
	* --------------C(r,r)
	* B(0,r)
	*
	* 左上の例。こういう図形を描画する
	*
	* 引数
	* @param int r 半径
	* @param int top  TOP/BOTTOM = 0/1 上側の時はFALSE　下側の時はTRUE
	* @param int left LEFT/RIGHT = 0/1 左側の時はFALSE　右側の時はTURE
	* @param array color    円部分の色　  RGB カラー array( r, g, b );
	* @param array bg_color 背景部分の色　RGB カラー array( r, g, b );
	*/
	protected function cornerImg( $r, $top, $left, $color, $bg_color ){
		$img = imagecreatetruecolor( $r, $r);
		//円の中心を探す
		$x = $r * $top;
		$y = $r * $left;
		//円の塗り色
		$c_color = imagecolorclosest( $img, $color[0], $color[1], $color[2] );
		$b_color = imagecolorclosest( $img, $bg_color[0], $bg_color[1], $bg_color[2] );
		//背景色で塗りつぶす
		ImageFill( $img, 0,0, $b_color );
		imageantialias( $img, false );
		//imagecolortransparent( $img, $b_color );
		//円を載せる
		imagefilledellipse( $img, $x, $y, $r+8, $r+8, $c_color );//アンチエイリアス分の補正
		return $img;
		
	}
	public function display($str=null){
		return $this->img->display($str);
	}
	public function imageTypeName(){
		return $this->img->imageTypeName();
	}
	public function imageType(){
		return $this->img->imageType();
	}
	public function height(){
		return $this->img->height();
	}
	public function width(){
		return $this->img->width();
	}
	public function setResource( $res ){
		return $this->img->setResource($res);
	}
	public function getResource(){
		return $this->img->getResource();
	}
}
class Image_Cut extends Image
{
	const LEFT   =  1;
	const TOP    =  2;
	const RIGHT  =  4;
	const BOTTOM =  8;
	const CENTER = 16;
	public function __construct( Image $src, $w, $h, $anchor = 16 ){
		$src_w = $src->width();
		$src_h = $src->height();
		$new = Image_Factory::getInstance(
						"png",
						'$w='.$w.';$h='.$h.';$bg=array(255,255,255);',
						Image_Factory::SRC_NEW
					);
		$x = 0;
		$y = 0;
		////
		if( (self::LEFT & $anchor) == self::LEFT ) {
			$x = 0;
		}else if( (self::RIGHT & $anchor) == self::RIGHT ){
			$x = $src_w - $w;
		}else if( (self::CENTER & $anchor) == self::CENTER ){
			$x = intval(($src_w - $w) / 2) ;
		}
		if( $x < 0 ){
			$x = 0;
			$w = $src_w;
		}
		////
		if( (self::TOP & $anchor) == self::TOP ) {
			$y = 0;
		}else if( (self::BOTTOM & $anchor) == self::BOTTOM ){
			$y = $src_h - $h;
		}else if( (self::CENTER & $anchor) == self::CENTER ){
			$y = intval(($src_h - $h) / 2) ;
		}
		if( $y < 0 ){
			$y = 0;
			$h = $src_h;
		}
		imagecopy( $new->getResource(), $src->getResource(), 0, 0, $x, $y, $w, $h );
		$this->img = $new;
	}
	public function display($str=null){
		return $this->img->display($str);
	}
	public function imageTypeName(){
		return $this->img->imageTypeName();
	}
	public function imageType(){
		return $this->img->imageType();
	}
	public function height(){
		return $this->img->height();
	}
	public function width(){
		return $this->img->width();
	}
	public function setResource( $res ){
		return $this->img->setResource($res);
	}
	public function getResource(){
		return $this->img->getResource();
	}

}

class Image_Merge extends Image
{
	//@param boolean resize サイズが違うときは適当にリサイズする
	public function __construct( Image $src, Image $over, $resize = false ){
		$this->img = $src;
		$src_x  = $this->width();
		$src_y  = $this->height();
		$over_x = $over->width();
		$over_y = $over->height();
		$x =$src_x;
		$y =$src_y;
		if( $resize &&  ( $src_x != $over_x  || $src_y != $over_y )   ){
			if( $src_x > $over_x || $src_y > $over_y ){
				$x = $over_x;
				$y = $over_y;
			}
			$this->img = new Image_Resize( $this->img, $x, $y );
			$orver     = new Image_Resize( $over,      $x, $y );
		}
		$white = imagecolorallocate($over->getResource(), 0, 0, 0);
		imagecolortransparent($over->getResource(), $white);
		$res = $this->getResource();
		imagecopymerge( $res, $over->getResource(), 0,0,0,0,$x,$y, 100 );
		$this->setResource( $res );
		imagecolortransparent($this->getResource(), $white);
	}
	public function display($str=null){
		return $this->img->display($str);
	}
	public function imageTypeName(){
		return $this->img->imageTypeName();
	}
	public function imageType(){
		return $this->img->imageType();
	}
	public function height(){
		return $this->img->height();
	}
	public function width(){
		return $this->img->width();
	}
	public function setResource( $res ){
		return $this->img->setResource($res);
	}
	public function getResource(){
		return $this->img->getResource();
	}

}
class Image_Text_Style{
	public $font;		//path to font file;
	public $color;		//array( r,g,b );
	public $size;		//float font-size
	public $angle;		//傾き　時計回り
	public $x;			//int
	public $y;			//int
	public $str;		//書き込む文字
	public $backgroundColor;//
	public $bg_transparent = 100;//背景透過度
	public function getColor( $res )
	{
		return $this->getImageColor( $res, $this->color );
	}
	public function getImageColor( $res, $color ){
		return imagecolorallocate(
				$res,
				$color[0],
				$color[1],
				$color[2]
			);
	
	}
	public function backGround(){
		if( !is_array($this->backgroundColor) ){
			//背景未指定なら何もしない
			return null;
		}
		$bg       = imagecreatetruecolor( $this->width(), $this->height() );
		$bg_color = $this->getImageColor( $bg, $this->backgroundColor );
		//背景色で塗りつぶす 
		imagefilledrectangle($bg, 0, 0, $this->width()-1, $this->height()-1, $bg_color);
		return $bg;
	}
	public function point(){
		return $this->point = imagettfbbox( $this->size, $this->angle, $this->font, $this->str );
	}
	public function width(){
		$point = $this->point();
		return $point[2]- $point[0];
	}
	public function height(){
		$point = $this->point();
		return $point[1]- $point[7];
	}

}
class Image_Text_Style_Shadow
{
	public $offset;
	public $direction;
	public $blur;
	public $color;
	public $transparent;
	public function offsetX(){
		return ceil( $this->offset*sin($this->direction) );
	}
	public function offsetY(){
		return ceil( $this->offset*cos($this->direction) );
	}
}
class Image_Text extends Image
{
	protected $img;
	public function __construct( Image $img, Image_Text_Style $font ){
		$this->img = $img;
		$this->text($font);
	}
	protected function text( Image_Text_Style $font ){
		//文字列のレイヤーを作る
		$bg = $this->drawTextLayer( $font );
		//文字を書き込んだ画像と、元になる画像を重ね合わせる
		$res = $this->getResource();
		$ret = imagecopymerge( $res, 
							    $bg, 
							 	  0, 
								  0,
								  0, 
								  0, 
				  	 $this->width(), 
				  	$this->height(),
					$font->transparent
			);
		return;
	}
	protected function drawTextLayer( $font ){
		$bg       = imagecreatetruecolor( $this->width(), $this->height() );
		$bg_color = $font->getImageColor( $bg, array(255,255, 255) );
		//背景色で塗りつぶす 
		imagefilledrectangle($bg, 0, 0, $this->width()-1, $this->height()-1, $bg_color);
		//透明色で塗りつぶす
		imagecolortransparent( $bg, $bg_color );
		//透明背景に文字を書き込む
		imagettftext(
					$bg,
					$font->size,
					$font->angle,
					$font->x,
					$font->y,
					$font->getColor($this->getResource()),
					$font->font,
					$font->str
				);
		return $bg;
	}
	public function display($str=null){
		return $this->img->display($str);
	}
	public function imageTypeName(){
		return $this->img->imageTypeName();
	}
	public function imageType(){
		return $this->img->imageType();
	}
	public function height(){
		return $this->img->height();
	}
	public function width(){
		return $this->img->width();
	}
	public function setResource( $res ){
		return $this->img->setResource($res);
	}
	public function getResource(){
		return $this->img->getResource();
	}
}
class Image_Text_Shadow extends Image_Text
{
	public function __construct( Image $img, 
								 Image_Text_Style $font, 
								 Image_Text_Style_Shadow $shadow )
	{
	
		$this->shadow = $shadow;
		parent::__construct( $img, $font );
	
	}
	protected function text( $font ){
		$this->shadow( $font );
	}
	protected function shadow($font){
		$style  = clone($font);
		//orverwrite
		$style->x           = $font->x + $this->shadow->offsetX();
		$style->y           = $font->y + $this->shadow->offsetY();
		$style->color       = $this->shadow->color;
		$style->transparent = $this->shadow->transparent;
		
		$bg = $this->drawTextLayer( $style );
		if( $this->shadow->blur > 0 ){
			for( $i=0;$i<$this->shadow->blur;$i++ ){
				imagefilter( $bg, IMG_FILTER_GAUSSIAN_BLUR );
			}
		}
		//文字を書き込んだ画像と、元になる画像を重ね合わせる
		$res = $this->getResource();
		$ret = imagecopymerge( $res, 
							    $bg, 
							 	  0, 
								  0,
								  0, 
								  0, 
				  	 $this->width(), 
				  	$this->height(),
					$style->transparent
			);

	}
}
class Image_Text_Background extends Image_Text{
	public function __construct( Image $img, Image_Text_Style $font ){
		parent::__construct( $img, $font );
	}
	protected function text( $style )
	{
		//文字背景と元画像を合成する->このクラス
		$this->orverlayBackGround( $style );
		//文字と元画像を合成する->親クラス
		parent::text( $style );
	}
	protected function orverlayBackGround(  $style ){
		$bg  = $style->backGround();
		if( $bg == null ){
			return;
		}
		//文字が回転するなら背景も回転させる
		//-->面倒な上にGDの画像回転は結果が荒いので却下
//		if( $style->angle != 0 ){
//			$pad = $style->getImageColor( $bg, array(0, 0, 0) );
//			$bg = imagerotate( $bg, $style->angle, $pad, true );
//			imagecolortransparent( $bg, $pad );
//			imagepng( $bg, "dump.png" );
//			//回転した場合のずれを補正 
//			//$offset["x"] = ceil(cos( $style->angle )*$style->width());
//			$offset["y"] = ceil(sin( $style->angle )*$style->height());
//		}
		$res = $this->getResource();
		$ret = imagecopymerge( $res, 
						$bg, 
						$style->x,
						$style->y - $style->height(), 
								0, 
								0, 
						$style->width(), 
						$style->height(),
						$style->bg_transparent
					);
	}
}
/////////////以下サンプル/////////////
//	public function invalidUrlImage(){
//			$img = Image_Factory::getInstance(  "png", '$w=160;$h=120;$bg=array(255,255,255);', Image_Factory::SRC_NEW );
//			$img = new Image_Filter_GaussianBlur( new Image_Filter_GaussianBlur( $img ) );
//			$img = new Image_Filter_GaussianBlur( new Image_Filter_GaussianBlur( $img ) );
//			$img = new Image_Filter_GrayScale( $img );
//			$f = new Image_Text_Style();
//			
//			//フォント設定
//			$f->font	= "./font/HGRSGU.TTC";//フォント
//			$f->color	= array( rand(120,220), rand(120,220), rand(120,220) );//フォントカラー
//			$f->size	=  20;//フォントサイズ
//			$f->x		=  10;//文字の開始位置　横方向
//			$f->y		= 60;//文字の開始位置　たて方向
//			$f->str		= mb_convert_encoding("ページが\r\nありません","utf8", "shift_jis" );//埋め込む文字
//			$f->transparent  = 100;//文字の透過度
//			$f->angle	=  0;//文字の向き
//			
//			$sd = new Image_Text_Style_Shadow();
//			$sd->color  = array( 100, 100, 100 );
//			$sd->offset = 5;
//			$sd->directrion = 45;
//			$sd->blur = 10;
//			$sd->transparent = 40;
//					
//			$img = new Image_Text( new Image_Filter_GaussianBlur( new Image_Text_Shadow( $img, $f, $sd ) ) , $f );
//			
//			return $img;
//	}
//	public function filterImage( $filtername, $img ){
//		switch($filtername){
//			case"gray":
//				$img = new Image_Filter_GrayScale( $img );
//				break;
//			case"red":
//				$img = new Image_Filter_ColorFilter( $img, array( 255,0, 0 ) );
//				break;
//			case"green":
//				$img = new Image_Filter_ColorFilter( $img, array( 0, 255, 0 ) );
//				break;
//			case"blue":
//				$img = new Image_Filter_ColorFilter( $img, array( 0, 0, 255 ) );
//				break;
//			case"nega":
//				$img = new Image_Filter_Negate( $img );
//				break;
//			case"emboss":
//				$img = new Image_Filter_Emboss( $img );
//				break;
//			case"blur":
//				$img = new Image_Filter_GaussianBlur( $img );
//				break;
//			case"blur2":
//				$img = new Image_Filter_GaussianBlur( $img );
//				$img = new Image_Filter_GaussianBlur( $img );
//				$img = new Image_Filter_GaussianBlur( $img );
//				$img = new Image_Filter_GaussianBlur( $img );
//				$img = new Image_Filter_GaussianBlur( $img );
//				break;
//			case"illust":
//				$img = new Image_Filter_Simplefy( $img );
//				break;
//			case"edge":
//				$img = new Image_Filter_EmphasizeEdge( $img );
//				break;
//			case"simple":
//				$frame = Image_Factory::getInstance( "png", "frame.png", Image_Factory::SRC_FILE );
//				$img = new Image_Resize( $img, 128, 96 );
//				//$img = new Image_Cut( $img, $frame->width(), $frame->height(), Image_Cut::CENTER );
//				$img = new Image_Merge( $img, $frame, true );
//				break;
//		}

?>