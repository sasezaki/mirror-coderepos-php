<?php

interface Appender {
	public function append(LoggingEvent $event);
}
