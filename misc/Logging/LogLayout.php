<?php

interface LogLayout {
	function format(LoggingEvent $event);
}
