<?php

class LoggingEvent {
	
	private $logger;
	private $level;
	private $message;
	private $starttime;
	
	public function __construct(Logger $logger, LogLevel $level, $message){
		$this->logger = $logger;
		$this->level = $level;
		$this->message = $message;
		$this->startTime = new DateTime;
	}
	
	public function getMessage(){
		return $this->message;
	}
	
	public function getLevel(){
		return $this->level;
	}
	
    public function getStartTime(){
        $current = $this->startTime->format('U');
        $logger = $this->logger->getStartTme()->format('U');
        $d = new DateTime;
        $d->setTime($current - $logger);
        return $d;
	}
}
