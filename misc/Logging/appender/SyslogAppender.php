<?php

interface SyslogFacility {
    // kernel messages
    const LOG_KERN = 0;
    // random user-level messages(1 << 3)
    const LOG_USER = 8;
    // mail system(2<<3)
    const LOG_MAIL = 16; 
    // system daemons(3<<3)
    const LOG_DAEMON = 24;
    // security/authorization(4<<3)
    const LOG_AUTH = 32;
    // internal syslogd use(5<<3)
    const LOG_SYSLOG = 40;
    // line printer subsystem(6<<3)
    const LOG_LPR = 48;
    // network news subsystem(7<<3)
    const LOG_NEWS = 56;
    // UUCP subsystem(8<<3)
    const LOG_UUCP = 64;
    // clock daemon(15<<3)
    const LOG_CRON = 120;
    
    // reserved for local use(16<<3)
    const LOG_LOCAL0 = 128;
    // reserved for local use(17<<3)
    const LOG_LOCAL1 = 136;
    // reserved for local use(18<<3)
    const LOG_LOCAL2 = 144;
    // reserved for local use(19<<3)
    const LOG_LOCAL3 = 152;
    // reserved for local use(20<<3)
    const LOG_LOCAL4 = 160;
    // reserved for local use(21<<3)
    const LOG_LOCAL5 = 168;
    // reserved for local use(22<<3)
    const LOG_LOCAL6 = 176;
    // reserved for local use(23<<3)
    const LOG_LOCAL7 = 184;
    
    // mask to extract facility
    const LOG_FACMASK = 0x03F8;
}

interface SyslogPriority {
    // system is unusable
    const LOG_EMERG = 0;
    // action must be taken immediately
    const LOG_ALERT = 1;
    // critical conditions
    const LOG_CRIT = 2;
    // error conditions
    const LOG_ERR = 3;
    // warning conditions
    const LOG_WARNING = 4;
    // normal but significant condition
    const LOG_NOTICE = 5;
    // informational
    const LOG_INFO = 6;
    // debug-level messages
    const LOG_DEBUG = 7;
    // mask to extract priority
    const LOG_PRIMASK = 0x0007;
}

class SyslogAppender implements Appender {

    private $ident;
    private $opt;
    private $facility;

    private $ip = '127.0.0.1';
    private $port = 514;

    public function __construct($ident, $opt, $facility){
        $this->ident = $ident;
        $this->opt = $opt;
        $this->facility = $facility;
    }

    public function setIp($ip){
        $this->ip = $ip;
    }

    public function setPort($port){
        $this->port = $port;
    }

    public function append(LoggingEvent $event){
        $level = $event->getLevel();
        $priority = SyslogPriority::LOG_PRIMASK;
        switch($level->toInt()){
        case LogLevel::ALL_INT:
            $priority = SyslogPriority::LOG_EMERG;
            break;
        case LogLevel::DEBUG_INT:
            $priority = SyslogPriority::LOG_DEBUG;
            break;
        case LogLevel::INFO_INT:
            $priority = SyslogPriority::LOG_DEBUG;
            break;
        case LogLevel::WARN_INT:
            $priority = SyslogPriority::LOG_WARNING;
            break;
        case LogLevel::ERROR_INT:
            $priority = SyslogPriority::LOG_ERR;
            break;
        case LogLevel::FATAL_INT:
            $priority = SyslogPriority::LOG_CRIT;
            break;
        case LogLevel::OFF_INT:
        default:
            return;
        }
        $this->write($event->getMessage(), $priority);
    }

    public function write($message, $priority){
        $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        socket_set_nonblock($socket);
        if(socket_connect($socket, $this->ip, $this->port)){
            $data = '';
            $data .= '<' . self::makePriority($this->facility, $priority) . '>';
            $data .=  $this->ident . ': ' . $message;
            socket_write($socket, $data, strlen($data));
        }
        socket_close($socket);
    }

    private static function makePriority($facility, $priority){
        return ($facility & SyslogFacility::LOG_FACMASK) | $priority;
    }

}
