<?php

class SimpleLayout implements LogLayout {
    protected $message = '%m%n';
    protected static $replaceMap = array(
        '%c' => '__c',
        '%d' => '__d',
        '%f' => '__f',
        '%l' => '__l',
        '%m' => '__m',
        '%n' => '__n',
        '%p' => '__p',
        '%r' => '__r',
        '%t' => '__t',
        '%F' => '__e',
        '%T' => '__u',
        '%S' => '__s'
    );
    public function format(LoggingEvent $event){
        return preg_replace('/%[a-zA-Z%]/e', 'self::replace($event, \'$0\')', $this->message);
    }
    protected static function replace(LoggingEvent $event, $key){
        if(isset(self::$replaceMap[$key])){
            $method = self::$replaceMap[$key];
            return self::$method($event);
        }
        return $key;
    }
    protected static function __c(LoggingEvent $event){
        return $event->getClass()->getName();
    }
    protected static function __d(LoggingEvent $event){
        return $event->getStartTime()->format('c');
    }
    protected static function __f(LoggingEvent $event){
        return $event->getFile();
    }
    protected static function __l(LoggingEvent $event){
        return $event->getLine();
    }
    protected static function __m(LoggingEvent $event){
        return $event->getMessage();
    }
    protected static function __n(LoggingEvent $event){
        return PHP_EOL;
    }
    protected static function __p(LoggingEvent $event){
        return $event->getPriority();
    }
    protected static function __r(LoggingEvent $event){
        return "\r";
    }
    protected static function __t(LoggingEvent $event){
        return "\t";
    }
    protected static function __e(LoggingEvent $event){
        return $event->getMethod();
    }
    protected static function __u(LoggingEvent $event){
        return $event->getStartTime()->format('U');
    }
    protected static function __s(LoggingEvent $event){
        return $event->getStackTraceString();
    }
}
