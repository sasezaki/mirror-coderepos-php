/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2007 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author: Yoshinari Takaoka (reversethis -> gro tod umumum ta umumum)  |
  +----------------------------------------------------------------------+
*/

#include "php_hyperestraier.h" 

#include <estraier.h>
#include <cabin.h>
#include <stdlib.h>

//
//    Hyper Estrainer PHP Resource Type.
//
int le_est_doc;    /*  ESTDOC  */

/* {{{ hyperestraier_functions[]
 *
 * Every user visible function must have an entry in hyperestraier_functions[].
 */
zend_function_entry hyperestraier_functions[] = {
    PHP_FE(est_doc_new, NULL)
    PHP_FE(est_doc_new_from_draft, NULL)
    {NULL, NULL, NULL}  /* Must be the last line in hyperestraier_functions[] */
};
/* }}} */ 

/* {{{ hyperestraier_module_entry
 */
zend_module_entry hyperestraier_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
    STANDARD_MODULE_HEADER,
#endif
    "hyperestraier",
    hyperestraier_functions,
    PHP_MINIT(hyperestraier),
    PHP_MSHUTDOWN(hyperestraier),
    NULL,     /*  PHP_RINIT  */
    NULL,     /*  PHP_RSHUTDOWN  */
    PHP_MINFO(hyperestraier),
#if ZEND_MODULE_API_NO >= 20010901
    "1.0",
#endif
    STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_HYPERESTRAIER
ZEND_GET_MODULE(hyperestraier)
#endif

/* {{{ PHP_MINIT_FUNCTION
 */
PHP_MINIT_FUNCTION(hyperestraier)
{
    le_est_doc = zend_register_list_destructors_ex(NULL, NULL, "ESTDOC", module_number);
    return SUCCESS;
}
/* }}} */

/* {{{ PHP_MSHUTDOWN_FUNCTION
 */
PHP_MSHUTDOWN_FUNCTION(hyperestraier)
{
    /* uncomment this line if you have INI entries
    UNREGISTER_INI_ENTRIES();
     */
    return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION
 */
PHP_MINFO_FUNCTION(hyperestraier)
{
    php_info_print_table_start();
    php_info_print_table_header(2, "hyperestraier support", "enabled");
    php_info_print_table_end();

    /* Remove comments if you have entries in php.ini
    DISPLAY_INI_ENTRIES();
     */
}
/* }}} */


/*  {{{ Hyper Estrainer Documentation API
 *
 */

/*
 *    ESTDOC *est_doc_new(void)
 */
PHP_FUNCTION(est_doc_new)
{
    ESTDOC *doc = est_doc_new();
    int estdoc_rsrc_id = ZEND_REGISTER_RESOURCE(return_value, doc, le_est_doc);
    RETURN_RESOURCE(estdoc_rsrc_id);
}

/*
 *   ESTDOC *est_doc_new_from_draft(const char *draft);
 */
PHP_FUNCTION(est_doc_new_from_draft)
{
    char *draft;
    int len;
    
    if(ZEND_NUM_ARGS() != 1) WRONG_PARAM_COUNT;
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s", &draft, &len) == FAILURE) {
        RETURN_NULL();
    }
    
    ESTDOC *doc = est_doc_new_from_draft(draft);
    int estdoc_rsrc_id = ZEND_REGISTER_RESOURCE(return_value, doc, le_est_doc);
    RETURN_RESOURCE(estdoc_rsrc_id);
}

/* }}} */
