<?php
	/** 
	* Smarty Plugin 
	* @package Ester_Smarty
	*/
	
//require_once "Var_Dump.php";
	/** 
	* Smarty DateFormat 拡張 
	* Type: modifer <br />
	* Name: date_format<br />
	* Purpose: 2006/12/25 を　2006/12/25 4時間前　とする
	* 英語ではAgeとか呼ばれる
	*/
function smarty_modifier_date_format_posted($date){
	$diff = intval(time() - strtotime($date));
	if( $diff < 0 ){ $future = true;}
	$diff = abs( $diff );
	$days    = intval( $diff / ( 60*60*24 ) );
	$hours   = intval( ( $diff % ( 60*60*24 ) ) / ( 60*60 ) );
	$minutes = intval( ( ( $diff % ( 60*60*24 ) ) % ( 60*60 ) ) / 60 );
	$times   = intval( ( ( $diff % ( 60*60*24 ) ) % ( 60*60 ) ) % 60 );
	$sameDay = (date("Y/m/d") == date("Y/m/d", strtotime($date)));
	$start  = $day;
	//一分未満 x秒前
	//一時間未満　X分前
	//一時間以上　6時間未満 x時間前
	//6時間以上　日付同じ 今日
	//6時間以上　日付一つ前　昨日
	//6時間以上　日付が２つ前　おととい
	
	if( $hours < 1 && $minutes < 1 && $days == 0 ){
		return " $times 秒前";//PHPの実行速度の影響をもろに受ける
	}else if( $hours < 1 && $days == 0 ){
		return " $minutes 分前";
	}else if( $hours >= 1 && $hours < 6 && $days == 0 ){
		return " $hours 時間前";
	}else if( $hours > 6 && $days == 0 && $sameDay ){
		return " 今日";
	}else if( $hours > 6 && $days < 2 &&!$sameDay ){
		return " 昨日";
	}else if( $days == 2 ){
		return " おととい";
	}else if( $days == 3 ){
		return " ３日前";
	}else{
		return $date;
	}
}
