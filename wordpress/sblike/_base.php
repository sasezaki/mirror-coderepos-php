// =================================
// Template Name: base html template
// =================================
<?xml version="1.0" encoding="{site_encoding}"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{site_lang}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={site_encoding}" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" href="{site_css}" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="RSS" href="{site_rss}" />
<link rel="alternate" type="application/atom+xml" href="{site_atom}" />
<title>{site_title}</title>
</head>
<body>
<div class="title">
<!-- BEGIN title -->
<h1 id="top">{blog_name}</h1>
<p class="description">{blog_description}</p>
<!-- END title -->
</div>
<div class="body">
<div class="contents">
<!-- BEGIN entry -->
<div class="entry">
<!-- BEGIN sequel -->
<div class="entry_navi">{prev_entry} | <a href="{site_top}">main</a> | {next_entry}</div>
<!-- END sequel -->
<h2><a href="{entry_permalink}">{entry_title}</a></h2>
<div class="entry_author">{entry_date} {entry_time} | posted by {user_name}</div>
<div class="entry_body">{entry_description}</div>
<div class="entry_state"> {category_name} | {comment_num} | {trackback_num}</div>
</div><!--_end_entry_-->
{trackback_auto_discovery}
<!-- END entry -->
<!-- BEGIN comment_area -->
<div class="comment">
<h3 id="comments">Comments</h3>
<!-- BEGIN comment -->
<dl class="body">
<dt>{comment_name} | {comment_time}</dt>
<dd>{comment_description}</dd>
</dl>
<!-- END comment -->
<form action="{site_comment}" method="post">
<h4>Comment Form</h4>
<dl>
<dt><label for="author" id="labelname">name:</label></dt>
<dd><input type="text" name="author" id="author" value="{cookie_name}" size="20" /></dd>
<dt><label for="email" id="labelemail">email:</label></dt>
<dd><input type="text" name="email" id="email" value="{cookie_email}" size="30" /></dd>
<dt><label for="url" id="labelurl">url:</label></dt>
<dd><input type="text" name="url" id="url" value="{cookie_url}" size="30" /></dd>
<dt><label for="comment" id="labeldescription">comments:</label></dt>
<dd><textarea id="comment" name="comment" rows="8" cols="45"></textarea></dd>
<dt><label for="submit" id="labelsubmit">submit</label></dt>
<dd>
<input type="submit" id="submit" value="submit" />
<input type="reset" value="reset" />
<input type="hidden" name="comment_post_ID" value="{entry_id}" />
</dd>
</dl>
</form>
</div><!--_end_comment_-->
<!-- END comment_area -->
<!-- BEGIN trackback_area -->
<div class="trackback">
<h3 id="trackback">Trackbacks</h3>
<p class="link">Trackback URL : {trackback_url}</p>
<!-- BEGIN trackback -->
<dl class="body">
<dt>{trackback_title} | {trackback_blog_name} | {trackback_time}</dt>
<dd>{trackback_excerpt}</dd>
</dl>
<!-- END trackback -->
</div><!--_end_trackback_-->
<!-- END trackback_area -->
<!-- BEGIN individual -->
<div class="individual">
<h2>{page_title}</h2>
<div class="individual_body">{page_description}</div>
</div><!--_end_individual_-->
<!-- END individual -->
<!-- BEGIN page -->
<div class="page">
{prev_page_link} {page_now}/{page_num} {next_page_link}
</div>
<!-- END page -->
</div><!--_end_contents_-->

<div class="navigation">
<dl class="page">
<dt id="pagename">Pages</dt>
<dd id="pagelist">{page_list}</dd>
</dl>
<dl class="calendar">
<dt id="calendarname">Calendar</dt>
<dd id="calendarlist">{calendar}
</dd>
</dl>
<!-- BEGIN selected_entry -->
<dl class="selectedentry">
<dt id="newentryname">Selected Entries</dt>
<dd id="newentrylist">{selected_entry_list}
</dd>
</dl>
<!-- END selected_entry -->
<!-- BEGIN latest_entry -->
<dl class="recententry">
<dt id="newentryname">Latest Entries</dt>
<dd id="newentrylist">{latest_entry_list}
</dd>
</dl>
<!-- END latest_entry -->
<!-- BEGIN recent_comment -->
<dl class="recentcomment">
<dt id="commentname">Recent Comments</dt>
<dd id="commentlist">{recent_comment_list}</dd>
</dl>
<!-- END recent_comment -->
<!-- BEGIN recent_trackback -->
<dl class="recenttrackback">
<dt id="tbname">Recent Trackback</dt>
<dd id="tblist">{recent_trackback_list}</dd>
</dl>
<!-- END recent_trackback -->
<!-- BEGIN category -->
<dl class="category">
<dt id="categoryname">Categories</dt>
<dd id="categorylist">{category_list}</dd>
</dl>
<!-- END category -->
<!-- BEGIN archives -->
<dl class="archives">
<dt id="archivename">Archives</dt>
<dd id="archivelist">{archives_list}</dd>
</dl>
<!-- END archives -->
<dl class="links">
<dt id="linkname">Links</dt>
<dd id="linklist">{link_list}</dd>
</dl>
<dl class="profiles">
<dt id="profilename">Profile</dt>
<dd id="profilelist">{user_list}</dd>
</dl>
<dl class="extra">
<dt id="extraname">Other</dt>
<dd id="extralist">
<ul>
<li><a href="{site_rss}">RSS1.0</a></li>
<li><a href="{site_atom}">Atom0.3</a></li>
<li>Powered by <a href="http://wordpress.org/">WordPress</a> {script_version}</li>
</ul>
</dd>
</dl>
<form method="get" action="{site_search}">
<dl class="search">
<dt>Search this site</dt>
<dd>
<input id="s" name="s" size="16" class="form" />
<input type="submit" value="Search" class="button" />
</dd>
</dl>
</form>
</div><!--_end_navigation_-->
<hr />
</div><!--_end_body_-->
<div class="footer">
<address>Copyright &copy; 2004 SimpleBoxes. All Rights Reserved.</address>
<p id="link2top"><a href="#top">page top</a></p>
</div><!--_end_footer_-->
</body>
</html>
