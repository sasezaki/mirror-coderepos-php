<?php
class sbTemplateManager
{
	var $blocks; // parsed template data (array)
	var $number; // current target block number (integer)
	var $data;   // data to be replaced (array)
	/**
		sbTemplateManager is a class to parse the sb style template.
		This object should be constructed with a template (string).
		@param[in] $template to be parsed
	*/
	function sbTemplateManager($template = '')
	{
		$this->__construct($template);
	}
	function __construct($template = '')
	{
		$this->blocks = array();
		$this->number = 0;
		$this->data = array('tag' => array(), 'block' => array());
		$this->_parse($template); // initialize
	}
	/**
		output returns the parsed string.
		@return parsed string.
	*/
	function output()
	{
		$output = '';
		foreach ($this->blocks as $block)
		{
			$rep = ($block['name'] === '_main') 
				? 1 
				: $this->data['block'][$block['name']];
			if ($rep == 0) continue;
			foreach (range(0, $rep - 1) as $i)
			{
				if ($i < 0) break;
				foreach ($block['text'] as $line)
				{
					foreach ($block['tags'] as $key)
					{
						if (strpos($line,$key) === false) continue;
						$word = '/' . preg_quote($key) . '/';
						$line = ( isset($this->data['tag'][$key][$i]) )
						      ? preg_replace($word,$this->data['tag'][$key][$i],$line)
						      : preg_replace($word,'',$line);
					} // $key
					$output .= $line . "\n";
				} // $line
			} // $i
		} // $block
		return $output;
	}
	/**
		deleteBlock deletes the indicated block (e.g. 'sequel' in 'entry' block)
		@param[in] $target indicates the target block
	*/
	function deleteBlock($target)
	{
		if (! isset($target) || ! $this->isExisted($target) )
			return;
		$check = 0;
		for ($i = 0;$i < count($this->blocks);$i++)
		{
			if ($this->blocks[$i]['name'] !== $target)
				continue;
			$check = $i;
			break;
		}
		// Are both blocks of neighbour the same?
		if ( $check > 0
		  && $check < count($this->blocks) - 1
		  && $this->blocks[$check - 1]['name'] === $this->blocks[$check + 1]['name'])
		{ // then merge them and remove the target
			$this->blocks[$check - 1]['text'] = array_merge($this->blocks[$check - 1]['text'],$this->blocks[$check + 1]['text']);
			$this->blocks[$check - 1]['tags'] = array_merge($this->blocks[$check - 1]['tags'],$this->blocks[$check + 1]['tags']);
			if (count($this->blocks[$check - 1]['tags']) > 2) // remove duplicated tags
				$this->blocks[$check - 1]['tags'] = array_unique($this->blocks[$check - 1]['tags']);
			array_splice($this->blocks,$check,2);
		}
	}
	/**
		resetData clears all tag and block data.
	*/
	function resetData()
	{
		$this->data['tag'] = array();
		foreach (array_keys($this->data['block']) as $block)
		{
			$this->data['block'][$block] = 0;
		}
	}
	/**
		tag sets tag data.  num should be called before setting tags.
		@param[in] $tag indicates a target tag
		@param[in] $content indicates a string to be replaced
	*/
	function tag($tag,$content)
	{
		$num = $this->number;
		if ( isset($tag) )
			$this->data['tag']['{' . $tag . '}'][$num] = $content;
	}
	/**
		num sets the target level.
		@param[in] $num indicates a target level
	*/
	function num($num)
	{
		$this->number = $num;
	}
	/**
		block sets a repetition for a target block.
		@param[in] $block indicates a target block
		@param[in] $num indicates a number of cycles.  if it's set as 0, the target block will be ignored.
	*/
	function block($block,$num)
	{
		if ( isset($block) && isset($num) )
			$this->data['block'][$block] = $num;
	}
	/**
		isExisted checks existance of indicated block in template.
		@param[in] $block indicates target block
		@return true if the block is existed, otherwise false
	*/
	function isExisted($block)
	{
		$check = isset($block) and isset($self->data['block'][$block]);
		return $check;
	}
	/**
		[private] _parse parses a template.  this function is called at constructor.
		@param[in] $template indicates a template.
	*/
	function _parse($template)
	{
		$num = 0;
		$cur = '_main';
		$stack = array();
		$this->_set($num,$cur);
		array_push($stack,$cur);
		foreach (explode("\n",$template,-1) as $line)
		{
			if (preg_match('/^\/\//',$line)) continue;
			if (preg_match('/<\!-- BEGIN (\w+) -->/',$line,$matches))
			{
				$num++;
				$this->_set($num,$matches[1]);
				$this->data['block'][$matches[1]] = 0;
				array_push($stack,$matches[1]);
				continue;
			}
			else if (preg_match('/<\!-- END /',$line))
			{
				if (count($this->blocks[$num]['tags']) > 2) // remove duplicated tags
					$this->blocks[$num]['tags'] = array_unique($this->blocks[$num]['tags']);
				$num++;
				array_pop($stack);
				$cur = $stack[count($stack) - 1];
				$this->_set($num,$cur);
				continue;
			}
			if (preg_match('/\{\w+?\}/',$line))
			{
				preg_match_all('/(\{\w+?\})/',$line,$matches);
				foreach ($matches[0] as $tag)
					array_push($this->blocks[$num]['tags'],$tag);
			}
			array_push($this->blocks[$num]['text'],$line);
		}
	}
	/**
		[private] _set initializes internal block varialbes.
		@param[in] $num indicates an array index.
		@param[in] $name indicates a name of block.
	*/
	function _set($num,$name)
	{
		$this->blocks[$num] = array(
			'text' => array(),
			'tags' => array(),
			'name' => $name,
		);
	}
}
?>