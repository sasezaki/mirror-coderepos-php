======================================================================
=== sb like - WordPress theme ========================================
======================================================================
                                Written by Takuya Otani / 02 Jul 2007 
                     Copyright (C) 2007 SimpleBoxes/SerendipityNZ Ltd.
                                                  All rights reserved.

"sb like" is a theme for Wordpress.
You can use the same format as Serene Bach / Pukeko for template.

- Wordpress   : http://wordpress.org/
- Serene Bach : http://serenebach.net/
- Pukeko      : http://pukeko.net/

[Contents]
This theme includes the following files:

- style.css  : style sheet for "sb like"
- _base.php  : base template
- index.php  : a boot point for normal output
- single.php : a boot point for sigle post page
- page.php   : a boot point for individual page
- lib/       : required libraries for "sb like"
- images/    : place holder for parts(e.g. images)

Only _base.php and style.css are modified normally for customization.
It is not necessary to edit any other php files.

[Install]
Please upload "sblike" directory into wp-content/themes/.

When you set the writable permission for style.css and _base.php, 
you can modify them from your wordpress management screen.

[Template format]
The template format is the almost same as Serene Bach / Pukeko.  
However there are some differences between sb like and Serene Bach.

Using "sb like", probably you may have some limitations for plugins.  
Actually you can NOT use any php statement in _base.php, _base.php 
works as purely HTML template.

* Disbaled unique block for "sb like"
  - amazon
  - recent_trackback
  - trackback
  - profile_area

* Disbaled unique tags for "sb like"
  - {amazon_item}
  - {amazon_ProductName}
  - {amazon_Catalog}
  - {amazon_Creator}
  - {amazon_ReleaseDate}
  - {amazon_Manufacturer}
  - {amazon_ImageUrlSmall}
  - {amazon_ImageUrlMedium}
  - {amazon_ImageUrlLarge}
  - {amazon_Availability}
  - {amazon_ListPrice}
  - {amazon_OurPrice}
  - {amazon_comment}
  - {amazon_url}
  - {subcategory_list}
  - {recent_trackback_list}
  - {calendar_horizontal}
  - {calendar_vertical}
  - {entry_sequel}
  - {trackback_num}
  - {trackback_count}
  - {user_id}
  - {category_id}
  - {category_disp_name}
  - {related_category}
  - {related_category_disp}
  - {entry_excerpt}
  - {entry_keyword}
  - {next_page_link}
  - {next_page_url}
  - {page_now}
  - {page_num}
  - {prev_page_link}
  - {prev_page_url}
  - {profile_description}
  - {profile_name}
  - {comment_icon}
  - {trackback_blog_name}
  - {trackback_excerpt}
  - {trackback_time}
  - {trackback_title}
  - {selected_archive}
  - {site_mobile}

* Additional unique block for "sb like"
  - individual : displays individual page

* Additional unique tags for "sb like"
  - {link_page} : displays page link navigation on page block
  - {page_title} : displays page title on individual page
  - {page_description} : displays page contents on individual page
  - {page_permalink} : displays individual page linik address
  - {site_search} : displays script address for search
  - {site_comment} : displays script address for posting comment

[Terms & Conditions]
- Takuya Otani / SerendipityNZ Ltd holds the copyright of this theme.

- You can use this theme with free of charge.

- In no event shall SerendipityNZ Limited be liable for any loss, 
  expense or damage arising out of or in connection with the use of or 
  reliance upon the informaiton in this theme.
