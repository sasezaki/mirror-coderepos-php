======================================================================
=== sb like - WordPress theme ========================================
======================================================================
                                Written by Takuya Otani / 02 Jul 2007 
                     Copyright (C) 2007 SimpleBoxes/SerendipityNZ Ltd.
                                                  All rights reserved.

「sb like」は、WordPress(http://wordpress.org/)用のテーマです。
Serene Bach / sb (http://serenebach.net/)に類似した書式のテンプレート
が利用できます。

[ファイル構成]
このテーマは以下のようなファイルで構成されています。

- style.css  : sb like で利用されるスタイルシートです
- _base.php  : ベーステンプレートが記述されます
- index.php  : 通常出力時の起点スクリプトです
- single.php : 個別記事出力時の起点スクリプトです
- page.php   : 個別ページ出力時の起点スクリプトです
- lib/       : sb like で利用されるライブラリが含まれています
- images/    : パーツ用の画像などを置くためのディレクトリです

sb like では、_base.php ならびに style.css を編集することによってテン
プレートを編集します。その他の php は修正する必要はありません。

[インストール]
解凍した sblike ディレクトリをそのまま wp-content/themes/ ディレクトリ
内にアップロードして下さい。

style.css ならびに _base.php に対しては、書き込み可能なパーミッション
を設定しておけば、Wordpress の管理画面より編集できます。

[テンプレート書式]
_base.php で利用されるテンプレート記述は Serene Bach / sb で利用できる
テンプレートの書式にほぼ準拠していますが、ツールの仕様の違いにより、い
くつかの相違があります。

sb like では、その性質上、プラグインの利用が制限されることが想定されま
す。また、_base.php 上で php 文を記述しても、実行されませんので、ご注
意下さい。

Wordpress では、コメントとトラックバックは、標準では同等に扱われます。
そのため、trackback に関連する独自ブロック・独自タグはご利用いただけま
せん。

同様に Wordpress では、オススメに該当するアイテムが標準の機能では用意
されていません。そのため、オススメに関連する独自ブロック・独自タグはご
利用いただけません。

また、リスト系の独自タグをはじめとして、いくつかの独自タグは出力内容が
変更されています。

* sb like で利用できない独自ブロック
  - amazon
  - recent_trackback
  - trackback
  - profile_area

* sb like で利用できない独自タグ
  - {amazon_item}
  - {amazon_ProductName}
  - {amazon_Catalog}
  - {amazon_Creator}
  - {amazon_ReleaseDate}
  - {amazon_Manufacturer}
  - {amazon_ImageUrlSmall}
  - {amazon_ImageUrlMedium}
  - {amazon_ImageUrlLarge}
  - {amazon_Availability}
  - {amazon_ListPrice}
  - {amazon_OurPrice}
  - {amazon_comment}
  - {amazon_url}
  - {subcategory_list}
  - {recent_trackback_list}
  - {calendar_horizontal}
  - {calendar_vertical}
  - {entry_sequel}
  - {trackback_num}
  - {trackback_count}
  - {user_id}
  - {category_id}
  - {category_disp_name}
  - {related_category}
  - {related_category_disp}
  - {entry_excerpt}
  - {entry_keyword}
  - {next_page_link}
  - {next_page_url}
  - {page_now}
  - {page_num}
  - {prev_page_link}
  - {prev_page_url}
  - {profile_description}
  - {profile_name}
  - {comment_icon}
  - {trackback_blog_name}
  - {trackback_excerpt}
  - {trackback_time}
  - {trackback_title}
  - {selected_archive}
  - {site_mobile}

sb like では、以下の独自ブロック・独自タグが追加されています。

* sb like で追加された独自ブロック
  - individual : 個別ページを出力します

* sb like で追加された独自タグ
  - {link_page} : page ブロックでページナビゲーションを出力します。
  - {page_title} : ページタイトルを出力します。
  - {page_description} : ページ内容を出力します。
  - {page_permalink} : ページの個別リンクアドレスを出力します。
  - {site_search} : 検索用のスクリプトアドレスを出力します。
  - {site_comment} : コメント投稿用のスクリプトアドレスを出力します。

sb like では、ツールの仕様の違いにより、コメントフォームの内容が多少変
わっています。

[利用条件など]
- 当テーマの著作権は Takuya Otani/SerendipityNZ Ltd. にあります。
- 当テーマは無償でご利用いただけます。
- 当テーマを使用したことによる如何なる損害についても製作者は責任を負わ
  ないものとします。

