<?php
/*
	Plugin Name: WP WebClap Plugin
	Version: 0.0.2
	Plugin URI: http://transrain.net/2007/07/12/135906
	Author: Yuki Kisaragi
	Author URI: http://trainsrain.net/
	Description: このプラグインはタグを指定した部分にWeb拍手のボタンを設置する事が出来ます。 <a href="options-general.php?page=WebClap">設定はこちらでどうぞ</a>.
	
*/

define('WP_WEBCLAP_BASE', dirname(__FILE__) . DIRECTORY_SEPARATOR);

define('WP_WEBCLAP_CODENAME',  'WebClap');
define('WP_WEBCLAP_PANELNAME', 'Web拍手設定');

define('WP_WEBCLAP_CONF_DEFAULT', 'web拍手ボタン');
define('WP_WEBCLAP_CONF_ENABLED', '1');

define('WP_WEBCLAP_CONF_VIEW_NORMAL', '0');
define('WP_WEBCLAP_CONF_VIEW_TEXT',   '1');
define('WP_WEBCLAP_CONF_VIEW_JS',     '2');
define('WP_WEBCLAP_CONF_VIEW_CUSTOM', '3');

define('WP_WEBCLAP_URL', 'http://webclap.simplecgi.com/clap.php');

define('WP_WEBCLAP_CONF_OUT_NORMAL', '<form action="' . WP_WEBCLAP_URL . '?id=[account]" '
                                    .'method="post" target="_blank"><input type="submit" value="[value]"></form>');

define('WP_WEBCLAP_CONF_OUT_TEXT',   '<a href="' . WP_WEBCLAP_URL . '?id=[account]" target="_blank">[value]</a>');

define('WP_WEBCLAP_CONF_OUT_JS',     '<input type="button" value="[value]" onclick="'
                                    ."window.open('" . WP_WEBCLAP_URL . "?id=[account]',"
                                    ."'webclap','toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=yes');"
                                    .'">');
                                    
define('WP_WEBCLAP_TABLE_NAME',	$wpdb->prefix . 'webclap');



class Wp_WebClap
{

    /**
     * コンストラクタ
     */
    function Wp_WebClap()
    {
        global $wpdb;

        // プラグインデータの初期化
        if (get_option('webclap_initialised') != '1') {
            update_option('webclap_account',    '');
            update_option('webclap_default',    WP_WEBCLAP_CONF_DEFAULT);
            update_option('webclap_enabled',    WP_WEBCLAP_CONF_ENABLED);
            update_option('webclap_viewtype',   WP_WEBCLAP_CONF_VIEW_NORMAL);
            update_option('webclap_viewformat', WP_WEBCLAP_CONF_OUT_NORMAL);
            update_option('webclap_initialised','1');
        }
        
        // データベースの確認
        if($wpdb->get_var('show tables like "' . WP_WEBCLAP_TABLE_NAME . '"') != WP_WEBCLAP_TABLE_NAME) {
            $sql = 'CREATE TABLE ' . WP_WEBCLAP_TABLE_NAME . ' ( ' .
                'post_id BIGINT NOT NULL, ' .
                'enabled INT(1) NOT NULL, ' .
                'button_text VARCHAR(100) NOT NULL, ' .
                'PRIMARY KEY (post_id) ' .
            ');';
            
            require_once(ABSPATH . 'wp-admin/upgrade-functions.php');
            dbDelta($sql);
        }

        // 関数のHOOK
        add_action('admin_menu', array(&$this, 'adminOptions'));
        add_action('edit_form_advanced', array(&$this, 'postOptions'));
        add_action('save_post', array(&$this, 'savePost'));
    }
    
    /**
     * インスタンスを生成するメソッド
     */
    function singleton()
    {
        static $instance;
        if ($instance === null) {
            $instance =& new Wp_WebClap();
        }
        return $instance;
    }
    
    /**
     * 管理画面の設定画面の生成
     */
    function adminOptions()
    {
        if (function_exists('add_options_page')) {
            $callback = array(&$this, 'optionsSubPanel');
            add_options_page(WP_WEBCLAP_CODENAME
                            , WP_WEBCLAP_PANELNAME
                            , 10
                            , WP_WEBCLAP_CODENAME
                            , $callback);
        }
    }

    /**
     * 管理画面の描写
     */
    function optionsSubPanel()
    {
        include_once WP_WEBCLAP_BASE . 'admin_options.php';
    }
    
    /**
     * 投稿画面の描写
     */
    function postOptions() {
        global $post;
        global $wpdb;
        
        $clapdata = null;
        
        if (isset($post->ID)) {
            $id = $post->ID;
            if (!is_null($clapdata = $wpdb->get_row('SELECT * FROM ' . WP_WEBCLAP_TABLE_NAME . ' WHERE post_id = ' . $id))) {
            }
        } else {
            $id = -1;
        }
        
        if ($clapdata === null) {
            $clapdata = new StdClass();
            
            $clapdata->post_id = $id;
            $clapdata->enabled = 1;
            $clapdata->button_text = get_option('webclap_default');
        }
        
        include_once WP_WEBCLAP_BASE . 'post_options.php';
    }
    
    /**
     * データのアップデート
     */
    function savePost($post) {
        global $wpdb;
    
        // パラメータ初期化
        $id = null;
        $on = null;
        $vl = null;
    
        // パラメータ取得
        if (isset($_POST['ID'])) $id = $_POST['ID'];
        if (isset($_POST['clapon'])) $on = $_POST['clapon'];
        if (isset($_POST['clapvalue'])) $vl = $_POST['clapvalue'];
        
        // 値設定
        if ($id === null) $id = intval($post);
        if ($on === null) $on = '0';
        if ($vl === null) $vl = get_option('webclap_default');
        
        // 値エスケープ
        if ($id !== null) $id = $wpdb->escape($id);
        if ($on !== null) $on = $wpdb->escape($on);
        if ($vl !== null) $vl = $wpdb->escape($vl);
        
        // 登録/更新
        if (!is_null($clapdata = $wpdb->get_row('SELECT * FROM ' . WP_WEBCLAP_TABLE_NAME . ' WHERE post_id = ' . $id))) {
            $sql = 'UPDATE ' . WP_WEBCLAP_TABLE_NAME . " SET enabled = {$on}, button_text = '{$vl}' WHERE post_id = {$id}";
        } else {
            //insert
            $sql = 'INSERT INTO ' . WP_WEBCLAP_TABLE_NAME . " (post_id, enabled, button_text) VALUES ({$id}, {$on}, '{$vl}');";
        }
        $wpdb->query($sql);
        if ($id === null) {
            $id = $wpdb->get_var('SELECT LAST_INSERT_ID() as id');
        }
    }
    
    /**
     * テンプレートタグ showWebClas 処理部分
     *
     * パラメータを指定するとその文字で表示される。
     */
    function tagShowWebClap($value = null)
    {
        global $post;
        global $wpdb;
        
        $id = null;
        $enabled = false;
        if (trim($value) == '') $value = null;

        if (isset($post->ID)) {
            $id = $post->ID;
        }
    
        // IDがあればDBを確認する。
        if ($id !== null) {
            if (is_null($clapdata = $wpdb->get_row('SELECT * FROM ' . WP_WEBCLAP_TABLE_NAME . ' WHERE post_id = ' . $id))) {
                if ($clapdata === null) {
                    $clapdata = new StdClass();
                    
                    $clapdata->post_id = $id;
                    $clapdata->enabled = get_option('webclap_enabled');
                    $clapdata->button_text = get_option('webclap_default');
                }
            }
            if ($clapdata->enabled == '1') $enabled = true;
            if ($value === null) $value = $clapdata->button_text;
        } else {
            if ($value !== null && get_option('webclap_enabled') == '1') $enabled = true;
        }

        if ($value === null) $value = get_option('webclap_default');
        
        $ac = get_option('webclap_account');
        
        switch (intval($_POST['viewtype'])) {
            case 0:
                $tm  = WP_WEBCLAP_CONF_OUT_NORMAL;
                break;
            case 1:
                $tm = WP_WEBCLAP_CONF_OUT_TEXT;
                break;
            case 2:
                $tm  = WP_WEBCLAP_CONF_OUT_JS;
                break;
            case 3:
                $tm = get_option('viewformat');
                break;
        }
        
        $tm = str_replace('[account]', $ac, $tm);
        $tm = str_replace('[value]', $value, $tm);
        if ($enabled) print($tm);
    }
}

$wp_webclap =& Wp_WebClap::singleton();

/**
 * テンプレートタグ showWebClap
 *
 * パラメータを指定するとその文字で表示される。
 */
function showWebClap($value = null) {
	$wp_webclap =& Wp_WebClap::singleton();
	$wp_webclap->tagShowWebClap($value);
}

/**
 * テンプレートタグ show_webclap
 *
 * パラメータを指定するとその文字で表示される。
 */
function show_webclap($value = null) {
	$wp_webclap =& Wp_WebClap::singleton();
	$wp_webclap->tagShowWebClap($value);
}